package fr.dark40k.util.xmlimportexport;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Permet de definir le nom du bloc XML contenant la classe.
 * 
 * @author Francois
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface XMLClassDefaultTag {
	String value();
}

package fr.dark40k.util;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.jdom2.Element;

public class FormattersXML {

	//
	// LocalDateTime
	//
	
	public final static DateTimeFormatter LOCALDATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public static void formatAttribute(Element elt, String xmlAttribute, LocalDate time) {
		elt.setAttribute(xmlAttribute, time.format(LOCALDATE_FORMATTER));
	}
	
	public static LocalDate parseLocalDate(Element elt, String xmlAttribute, LocalDate defaultDate) {
		try {
			return parseLocalDate(elt, xmlAttribute);
		} catch (Exception e) {
			return defaultDate;
		}
	}
	
	public static LocalDate parseLocalDate(Element elt, String xmlAttribute) {
		
		String dateString = elt.getAttributeValue(xmlAttribute);
		if (dateString==null) throw new NumberFormatException("Attribute not found:"+xmlAttribute);
		
		try {
			return LocalDate.parse(dateString, LOCALDATE_FORMATTER);
		} catch (IllegalArgumentException e) {
			throw new NumberFormatException("Cannot translate:"+dateString);
		}			
	}
	
	//

	//
	// LocalDateTime
	//
	
	public final static DateTimeFormatter LOCALDATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy-HH:mm");

	public static void formatAttribute(Element elt, String xmlAttribute, LocalDateTime time) {
		elt.setAttribute(xmlAttribute, time.format(LOCALDATETIME_FORMATTER));
	}
	
	public static LocalDateTime parseLocalDateTime(Element elt, String xmlAttribute, LocalDateTime defaultTime) {
		try {
			return parseLocalDateTime(elt, xmlAttribute);
		} catch (Exception e) {
			return defaultTime;
		}
	}
	
	public static LocalDateTime parseLocalDateTime(Element elt, String xmlAttribute) {
		
		String dateString = elt.getAttributeValue(xmlAttribute);
		if (dateString==null) throw new NumberFormatException("Attribute not found:"+xmlAttribute);
		
		try {
			return LocalDateTime.parse(dateString, LOCALDATETIME_FORMATTER);
		} catch (IllegalArgumentException e) {
			throw new NumberFormatException("Cannot translate:"+dateString);
		}			
	}
	
	//
	// Duration
	//
	
	public static void formatAttribute(Element elt, String xmlAttribute, Duration duree) {
		elt.setAttribute(xmlAttribute, duree.toString());
	}
	
	public static Duration parseDuration(Element elt, String xmlAttribute, Duration defaultDuration) {
		try {
			return parseDuration(elt, xmlAttribute);
		} catch (Exception e) {
			return defaultDuration;
		}
	}
	
	public static Duration parseDuration(Element elt, String xmlAttribute) {
		return Duration.parse(elt.getAttributeValue(xmlAttribute));
	}

	
}

package fr.dark40k.util.swing;

import javax.swing.SwingUtilities;

public class GUIUtilities {

	public final static void runEDT(Runnable doRun) {
		if (SwingUtilities.isEventDispatchThread())
			doRun.run();
		else
			SwingUtilities.invokeLater(doRun);
	}
	
}

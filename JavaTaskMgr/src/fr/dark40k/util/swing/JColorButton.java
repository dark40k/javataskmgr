package fr.dark40k.util.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EventListener;
import java.util.EventObject;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import javax.swing.event.EventListenerList;

public class JColorButton extends JButton {

	private Color activeColor;

	private final static Color[] colorList = {Color.black,Color.WHITE, Color.YELLOW, Color.green, Color.CYAN, Color.RED, Color.MAGENTA, Color.GRAY};

	public JColorButton(Color color) {

		activeColor=color;

        final JPopupMenu popup = new JPopupMenu();

        popup.setLayout(new GridLayout(3, 3, 0, 0));

        for (int colIndex=0;colIndex<colorList.length;colIndex++) {
        	popup.add(new SubColorButton(colorList[colIndex]));
        }

        JButton lastButton = new SubColorButton(Color.PINK);
        lastButton.setText("<?>");
        popup.add(lastButton);

		addMouseListener(new MouseAdapter() {
			boolean popupShow;
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					Dimension size = popup.getPreferredSize();
					int x = (getWidth() - size.width) / 2;
					int y = getHeight();
					popupShow=true;
					new Timer(200,new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							if (popupShow) popup.show(JColorButton.this, x, y);
						}
					}).start();
				}
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				popup.setVisible(false);
				popupShow=false;
			}

			@Override
			public void mouseReleased(MouseEvent e) {

				popupShow=false;
				if (!popup.isVisible()) return;

				Point p = SwingUtilities.convertPoint((Component) e.getSource(), e.getPoint(), popup.getParent());
				Component b = popup.getComponentAt(p);
				popup.setVisible(false);

				if (b==lastButton) {
					Color col = JColorChooser.showDialog(JColorButton.this, "Color", lastButton.getBackground());
					if (col!=null) {
						lastButton.setBackground(col);
						setColor(col);
					}
				}
				else if (b instanceof SubColorButton) {
					setColor(((SubColorButton) b).color);
				}

			}
		});

	}

	public Color getColor() {
		return activeColor;
	}

	public void setColor(Color newColor) {

		if (activeColor==newColor) return;

		if ((activeColor!=null)&&(activeColor.equals(newColor))) return;

		this.activeColor = newColor;
		repaint();

		fireColorChangedEvent();

	}

	@Override
	public void paint(Graphics g) {

		super.paint(g);

		if (activeColor != null) {
			g.setColor(activeColor);
			g.fillRect(4, getHeight() - 8, getWidth() - 8, 4);
		}

	}

	//
	// Classe inferieure de boutons
	//
	private class SubColorButton extends JButton {

		private Color color;

		public SubColorButton(Color color) {

			this.color=color;

			setMinimumSize(new Dimension(25,25));
			setPreferredSize(new Dimension(25,25));

			setBorder(new LineBorder(new Color(0, 0, 0)));
			setBackground(color);
			setContentAreaFilled(false);
			setOpaque(true);
		}
	}

	//
	// Change event
	//

	private EventListenerList listenerList = new EventListenerList();

	public int listenersCount() { return listenerList.getListenerCount(); };

	public interface UpdateColorButtonListener extends EventListener {
		public void colorChanged(UpdateColorButtonEvent evt);
	}

	public class UpdateColorButtonEvent extends EventObject {
		public UpdateColorButtonEvent(Object source) {
			super(source);
		}
	}

	public void addColorButtonListener(UpdateColorButtonListener listener) {
		listenerList.add(UpdateColorButtonListener.class, listener);
	}

	public void removeUpdateListener(UpdateColorButtonListener listener) {
		listenerList.remove(UpdateColorButtonListener.class, listener);
	}

	public void fireColorChangedEvent() {
		UpdateColorButtonEvent evt=new UpdateColorButtonEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateColorButtonListener.class) {
				((UpdateColorButtonListener) listeners[i+1]).colorChanged(evt);
			}
		}
	}


}

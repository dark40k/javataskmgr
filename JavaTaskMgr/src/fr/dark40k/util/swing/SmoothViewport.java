package fr.dark40k.util.swing;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JViewport;
import javax.swing.Timer;

public class SmoothViewport extends JViewport {

    private int dxPerFrame = 50; // pixel / frame
    private int dyPerFrame = 50; // pixel / frame
	
    private int dxPerFrameStart = 50; // pixel / frame
    private int dyPerFrameStart = 50; // pixel / frame
    private double accPerFrame = 1.1; // acceleration perf frame
    
    private final Timer timer;
    
    private Point targetPos = new Point(0,0);
    private Point newPos = new Point(0,0);
    
    public SmoothViewport(int dxPerFrame, int dyPerFrame, int msecPerFrameDelay, double accPerFrame) {
    	
    	super();
    	
    	this.dxPerFrameStart=dxPerFrame;
    	this.dyPerFrameStart=dyPerFrame;
		this.accPerFrame=accPerFrame;
		
    	timer = new Timer(msecPerFrameDelay,null);
    	
        timer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	updateViewPosition();
            }
        });
        
	}
    
    @Override
    public void setViewPosition(Point p) {
    	
    	if (isSamePosition(p, newPos)) return;
    	if (isSamePosition(p, targetPos)) return;
    	
    	targetPos = p.getLocation();
    	// System.out.println("setViewPosition = (" + targetPos.x + " , " + targetPos.y + ")" + ", speed= (" + dxPerFrame + " , " + dyPerFrame + ")");
    	
    	updateViewPosition();
    	
    }
   
	/**
	 * @return true si la position recherch�e est atteinte, faux autrement
	 */
	private void updateViewPosition() {
		
    	Point curPos = getViewPosition();

    	newPos.x = newPosition(curPos.x, targetPos.x, dxPerFrame);
    	newPos.y = newPosition(curPos.y, targetPos.y, dyPerFrame);
    	
    	dxPerFrame=(int) (dxPerFrame*accPerFrame);
    	dyPerFrame=(int) (dyPerFrame*accPerFrame);
    	
		// System.out.println("iteration (" + curPos.x + " , " + curPos.y + ") => (" + newPos.x + " , " + newPos.y + ") / (" + targetPos.x + " , " + targetPos.y + ")");
		
    	super.setViewPosition(newPos);
		
    	boolean atFinalPosition = isSamePosition(newPos,targetPos);
    	
    	if (atFinalPosition) {
//    		System.out.println("Position finale demand�e.");
    		dxPerFrame=dxPerFrameStart;
    		dyPerFrame=dyPerFrameStart;
    		if (timer.isRunning()) timer.stop();
    	} else {
    		if (!timer.isRunning()) timer.start();
    	}
    	
	}

    private int newPosition(int current, int target, int maxSpeedPerFrame) {
    	
    	if (maxSpeedPerFrame<=0) return target;
    	
    	int d = target - current;
    	
    	if (d>=0) 
    		d = Math.min(d, maxSpeedPerFrame);
    	else
    		d = Math.max(d, -maxSpeedPerFrame);
    	
    	return current+d;
    }
    
    public static boolean isSamePosition(Point p1, Point p2) {
    	return ((p1.x==p2.x)&&(p1.y==p2.y));
    }
        
}
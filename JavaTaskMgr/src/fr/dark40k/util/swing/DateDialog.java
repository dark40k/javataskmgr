package fr.dark40k.util.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Popup;
import javax.swing.border.EmptyBorder;

public class DateDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	protected Calendar calendar;
	protected boolean exitStatus = false;

	protected JButton prevMonth;
	protected JButton nextMonth;
	protected JButton prevYear;
	protected JButton nextYear;

	protected JTextField textField;

	protected List<ActionListener> popupListeners = new ArrayList<ActionListener>();

	protected Popup popup;

	protected SimpleDateFormat dayName = new SimpleDateFormat("d");
	protected SimpleDateFormat monthName = new SimpleDateFormat("MMMM");

	protected String iconFile = "datepicker.gif";
	protected String[] weekdayNames = { "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim" };

	
	
	// -------------------------------------------------------------------------------------------
	//
	// Fonction d'acces
	//
	// -------------------------------------------------------------------------------------------
	
	public static LocalDate getDateDialog(Component c, LocalDate defaultDate, String title) {
		
		DateDialog dialog = new DateDialog(c,title);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocalDate(defaultDate);
		dialog.setVisible(true);
		
		return (dialog.exitStatus)?dialog.getLocalDate():null;
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Constructeur
	//
	// -------------------------------------------------------------------------------------------
	
	
	public DateDialog(Component c,String title) {
		calendar  = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.setMinimalDaysInFirstWeek(4);
		
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		initGUI(c,title);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Getters / Setters
	//
	// -------------------------------------------------------------------------------------------
	
	public Calendar getCalendar() {
		if (calendar==null) return null;
		return calendar;
	}

	public LocalDate getLocalDate() {
		if (calendar==null) return null;
		return calendar.toInstant().atZone(calendar.getTimeZone().toZoneId()).toLocalDate();
	}

	public void setLocalDate(LocalDate date) {
		if (date==null) return;
		calendar.set(date.getYear(), date.getMonthValue()-1, date.getDayOfMonth());
		createPanel();
		validate();
		repaint();
	}
	
	public boolean getExitStatus() {
		return exitStatus;
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Commandes
	//
	// -------------------------------------------------------------------------------------------
	
	private int expandYear(int year) {
		if (year < 100) { // 2 digit year
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			int current2DigitYear = currentYear % 100;
			int currentCentury = currentYear / 100 * 100;
			// set 2 digit year range +20 / -80 from current year
			int high2DigitYear = (current2DigitYear + 20) % 100;
			if (year <= high2DigitYear) {
				year += currentCentury;
			} else {
				year += (currentCentury - 100);
			}
		}
		return year;
	}

	private void addMonth(int month) {
		calendar.add(Calendar.MONTH, month);
		createPanel();
		validate();
		repaint();
	}

	private void addYear(int year) {
		calendar.add(Calendar.YEAR, year);
		createPanel();
		validate();
		repaint();
	}

	private void editDate(String date) {
		parseDate(date);
		createPanel();
		validate();
		repaint();
	}
	
	private void changeDay(String day) {
		calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(day.trim()));
		createPanel();
		validate();
		repaint();
	}

	private void cmdOk() {
		exitStatus = true;
		this.setVisible(false);
	}
	
	private void cmdReset() {
		calendar=null;
		exitStatus = true;
		this.setVisible(false);
	}
	
	private void cmdCancel() {
		this.setVisible(false);
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Utilitaires
	//
	// -------------------------------------------------------------------------------------------
		
	private String getFormattedDate() {
		return Integer.toString(getDay()) + "/" + Integer.toString(getMonth()) + "/" + Integer.toString(getYear());
	}

	private int getMonth() {
		return calendar.get(Calendar.MONTH) + 1;
	}

	private int getDay() {
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	private int getYear() {
		return calendar.get(Calendar.YEAR);
	}

	private void parseDate(String date) {
		String[] parts = date.split("/");
		if (parts.length == 3) {
			calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(parts[0]));
			calendar.set(Calendar.MONTH, Integer.valueOf(parts[1])-1);
			calendar.set(Calendar.YEAR, expandYear(Integer.valueOf(parts[2])));
		} else if (parts.length == 2) {
			calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(parts[0]));
			calendar.set(Calendar.MONTH, Integer.valueOf(parts[1])-1);
		} else {
			// invalid date
			calendar = Calendar.getInstance();
		}
	}

	// -------------------------------------------------------------------------------------------
	//
	// GUI
	//
	// -------------------------------------------------------------------------------------------
	
	private void createPanel() {
		contentPanel.removeAll();
		contentPanel.setBorder(BorderFactory.createLineBorder(Color.black, 3));
		contentPanel.setFocusable(true);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));

		contentPanel.add(createControls());
		contentPanel.add(createCalendar());

		Dimension d = contentPanel.getPreferredSize();
		contentPanel.setPreferredSize(new Dimension(d.width, d.height + 8));
	}

	private JPanel createControls() {
		JPanel c = new JPanel();
		c.setBorder(BorderFactory.createRaisedBevelBorder());
		c.setFocusable(true);
		c.setLayout(new FlowLayout(FlowLayout.CENTER));

		prevYear = new JButton("<<");
		c.add(prevYear);
		prevYear.setMargin(new Insets(0, 0, 0, 0));
		prevYear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addYear(-1);
			}
		});

		prevMonth = new JButton("<");
		c.add(prevMonth);
		prevMonth.setMargin(new Insets(0, 0, 0, 0));
		prevMonth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addMonth(-1);
			}
		});

		textField = new JTextField(getFormattedDate(), 10);
		c.add(textField);
		textField.setEditable(true);
		textField.setEnabled(true);
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editDate(textField.getText());
			}
		});

		nextMonth = new JButton(">");
		c.add(nextMonth);
		nextMonth.setMargin(new Insets(0, 0, 0, 0));
		nextMonth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addMonth(+1);
			}
		});

		nextYear = new JButton(">>");
		c.add(nextYear);
		nextYear.setMargin(new Insets(0, 0, 0, 0));
		nextYear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addYear(+1);
			}
		});

		return c;
	}

	private JPanel createCalendar() {
		JPanel x = new JPanel();
		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		x.setFocusable(true);
		x.setLayout(gridbag);

		String month = monthName.format(calendar.getTime());
		String year = Integer.toString(getYear());

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 8;
		c.gridheight = 1;
		JLabel title = new JLabel(month + " " + year);
		x.add(title, c);
		Font font = title.getFont();
		// Font titleFont = new Font(font.getName(), font.getStyle(),
		// font.getSize() + 2);
		Font weekFont = new Font(font.getName(), font.getStyle(), font.getSize() - 2);
		title.setFont(font);

		c.gridy = 1;
		c.gridwidth = 1;
		c.gridheight = 1;
		for (c.gridx = 1; c.gridx < 8; c.gridx++) {
			JLabel label = new JLabel(weekdayNames[c.gridx-1]);
			x.add(label, c);
			label.setFont(weekFont);
		}

		Calendar draw = (Calendar) calendar.clone();
		draw.set(Calendar.DATE, 1);
		while (draw.get(Calendar.DAY_OF_WEEK)!=Calendar.MONDAY) draw.add(Calendar.DATE,-1);
		int monthInt = calendar.get(Calendar.MONTH);
		// monthInt = 0;
		// System.out.println("Current month: " + monthInt);

		c.gridwidth = 1;
		c.gridheight = 1;
		int width = getFontMetrics(weekFont).stringWidth(" Wed ");
		int width1 = getFontMetrics(weekFont).stringWidth("Wed");
		int height = getFontMetrics(weekFont).getHeight() + (width - width1);

		
		for (c.gridy = 2; c.gridy < 8; c.gridy++) {
			
			c.gridx =0;
			JLabel weekNbr = new JLabel(String.valueOf(draw.get(Calendar.WEEK_OF_YEAR)));
			x.add(weekNbr, c);
			
			for (c.gridx = 1; c.gridx < 8; c.gridx++) {
				JButton dayButton;
				// System.out.print("Draw month: " + draw.get(Calendar.MONTH));
				if (draw.get(Calendar.MONTH) == monthInt) {
					String dayString = dayName.format(draw.getTime());
					if (draw.get(Calendar.DAY_OF_MONTH) < 10)
						dayString = " " + dayString;
					dayButton = new JButton(dayString);
				} else {
					dayButton = new JButton();
					dayButton.setEnabled(false);
				}
				// System.out.println(", day: " +
				// dayName.format(draw.getTime()));
				x.add(dayButton, c);
				Color color = dayButton.getBackground();
				if ((draw.get(Calendar.DAY_OF_MONTH) == getDay()) && (draw.get(Calendar.MONTH) == monthInt)) {
					dayButton.setBackground(Color.yellow);
					dayButton.setContentAreaFilled(false);
					dayButton.setOpaque(true);
					// dayButton.setFocusPainted(true);
					// dayButton.setSelected(true);
				} else
					dayButton.setBackground(color);
				dayButton.setFont(weekFont);
				dayButton.setFocusable(true);
				dayButton.setPreferredSize(new Dimension(width, height));
				dayButton.setMargin(new Insets(0, 0, 0, 0));
				dayButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						changeDay(e.getActionCommand());
					}

				});
				draw.add(Calendar.DATE, +1);
			}
			// if (draw.get(Calendar.MONTH) != monthInt) break;
		}
		return x;
	}

	private void initGUI(Component c, String title) {

//		setBounds((c!=null)?c.getX():100, (c!=null)?c.getY():100, 225, 250);
		setBounds(0,0, 225, 250);
		setLocationRelativeTo(c);
		
		setTitle((title!=null)?title:"Selectionner date");
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdOk();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdCancel();
					}
				});
				
				JButton resetButton = new JButton("Reset");
				resetButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdReset();
					}
				});
				resetButton.setActionCommand("OK");
				buttonPane.add(resetButton);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

		createPanel();

	}

//	// -------------------------------------------------------------------------------------------
//	//
//	// Test
//	//
//	// -------------------------------------------------------------------------------------------
//	
//	public static void main(String[] args) {
//		try {
//			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		try {
//			DateDialog dialog = new DateDialog(null,null);
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//			System.out.println(dialog.getLocalDate());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

}

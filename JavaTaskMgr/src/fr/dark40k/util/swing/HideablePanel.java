package fr.dark40k.util.swing;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class HideablePanel extends JPanel {

	final JCheckBox checkBox = new JCheckBox();

	public HideablePanel() {

		checkBox.setFocusPainted(false);
		checkBox.setSelected(true);

		ComponentTitledBorder componentBorder = new ComponentTitledBorder(checkBox, this, BorderFactory.createTitledBorder(""));

		checkBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				update();
			}
		});

		setBorder(componentBorder);

	}

	protected void update() {

		boolean enable = checkBox.isSelected();

		Component comp[] = HideablePanel.this.getComponents();

		for (int i = 0; i < comp.length; i++) {
			comp[i].setVisible(enable);
		}

	}

	public void setContentVisible(boolean visible) {
		checkBox.setSelected(visible);
		update();
	}

	public boolean isContentVisible() {
		return checkBox.isSelected();
	}

	public HideablePanel(String title) {
		this();
		setTitle(title);
	}

	public void setTitle(String title) {
		checkBox.setText(title);
	}

	@Override
	public Component add(Component comp) {
		Component c = super.add(comp);
		c.setVisible(checkBox.isSelected());
		return c;
	}

}

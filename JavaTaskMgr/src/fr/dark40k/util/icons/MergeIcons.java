package fr.dark40k.util.icons;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class MergeIcons {

	public static ImageIcon merge(ImageIcon img1, ImageIcon img2) {
		
		Image image1 = img1.getImage();
		Image image2 = img2.getImage();
		
		int w = Math.max(img1.getIconWidth(),img1.getIconWidth());
		int h = Math.max(img1.getIconHeight(),img1.getIconHeight());
		
		Image image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		
		Graphics2D g2 = (Graphics2D) image.getGraphics();
		g2.drawImage(image1, 0, 0, null);
		g2.drawImage(image2, 0, 0, null);
		g2.dispose();

		return new ImageIcon(image);
	}
	
}

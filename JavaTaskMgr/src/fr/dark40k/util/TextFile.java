package fr.dark40k.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;

public class TextFile {

	public static String read(URL url) {
		return read(new File(url.getFile()));
	}

	public static String read(File file) {

		StringBuilder result = new StringBuilder("");

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result.toString();

	}

	public static String read(InputStream in) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(in));

		StringBuilder builder = new StringBuilder();
		String aux = "";

		try {
			while ((aux = reader.readLine()) != null) {
			    builder.append(aux);
			    builder.append("\n");
			}
		} catch (IOException e) {
			LogManager.getLogger().info("Erreur lors de la lecture du InputStream.",e);
		}

		return builder.toString();

	}

}

package fr.dark40k.util.dragdrop;

import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DropFilesHandler {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final DropFileUser user;
	
	private final DropFileListener listener;
	private final DropTarget target;

	public DropFilesHandler(Component dropComponent, DropFileUser user) {
		
		this.user = user;
		
		listener = new DropFileListener();
		
		target = new DropTarget(dropComponent,listener);
		
	}
	
	public void setActive(boolean isActive) {
		target.setActive(isActive);
	}
	
	public boolean isActive() {
		return target.isActive();
	}
	
	//
	// Interface de la classe utilisatrice
	//
	
	public interface DropFileUser {
		public void dropFiles(DropTargetDropEvent e, List<File> list);
	}

	//
	// Classe interne pour la récuperation
	//
	
	private class DropFileListener implements java.awt.dnd.DropTargetListener {

		public void dragEnter(DropTargetDragEvent e) {
		}

		public void dragExit(DropTargetEvent e) {
		}

		public void dragOver(DropTargetDragEvent e) {
		}

		public void dropActionChanged(DropTargetDragEvent e) {
		}

		@SuppressWarnings("unchecked")
		public void drop(DropTargetDropEvent e) {
			
				// Accept the drop first, important!
				e.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);

				// Get the files that are dropped as java.util.List
				List<File> list=null;
				
				try {
					
					list = (List<File>) e.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
					
				} catch (UnsupportedFlavorException e1) {
					LOGGER.warn("Drop UnsupportedFlavorException : "+e1.getMessage());
				} catch (IOException e1) {
					LOGGER.warn("Drop IOException : "+e1.getMessage());
				}

				if (list==null) return;
				
				user.dropFiles(e,list);
				
		}

	}

}

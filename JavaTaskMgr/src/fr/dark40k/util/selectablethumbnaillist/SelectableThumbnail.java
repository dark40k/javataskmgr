package fr.dark40k.util.selectablethumbnaillist;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

public class SelectableThumbnail<SubPanelGenericType extends JPanel> extends JPanel {

	protected final SubPanelGenericType subPanel;
	
	private final LineBorder subPanelBorderSelected;
	private final LineBorder subPanelBorderNotSelected;

	private boolean isSelected = false;
	
	public SelectableThumbnail(SubPanelGenericType subPanel) {
		
		this.subPanel=subPanel;
		
		setLayout(new BorderLayout(0, 0));
		
		subPanelBorderSelected=new LineBorder(Color.BLUE, 3, true);
		subPanelBorderNotSelected=new LineBorder(getBackground(), 3, true);
		
		subPanel.setBorder(subPanelBorderNotSelected);
		add(subPanel, BorderLayout.CENTER);
		
	}
	
	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		
		if (selected==isSelected) return;
		
		isSelected=selected;
		
		updateDisplay();
		
	}

	public void updateDisplay() {
		
		SwingUtilities.invokeLater(() -> {
			
			if (isSelected)
				subPanel.setBorder(subPanelBorderSelected);
			else
				subPanel.setBorder(subPanelBorderNotSelected);
			
			repaint();

		});
		
	};
	
	public SubPanelGenericType getSubPanel() {
		return subPanel;
	}
	
}

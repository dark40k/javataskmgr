package fr.dark40k.util.selectablethumbnaillist;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;

import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;

import fr.dark40k.util.swing.layout.WrapLayout;

public class JScrollablePanel extends JPanel implements Scrollable {

	private WrapLayout wl_panel;

	public JScrollablePanel() {
		wl_panel = new WrapLayout();
		wl_panel.setAlignment(FlowLayout.LEFT);
		setLayout(wl_panel);
	}
	
	@Override
	public Dimension getPreferredScrollableViewportSize() {
		return getPreferredSize();
	}

	@Override
	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		if (orientation == SwingConstants.VERTICAL)
			return (direction > 0) ? calcStepSizeVerticalDown(visibleRect) : calcStepSizeVerticalUp(visibleRect);
		return 0;
	}

	@Override
	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		if (orientation == SwingConstants.VERTICAL)
			return (direction > 0) ? calcStepSizeVerticalDown(visibleRect) : calcStepSizeVerticalUp(visibleRect);
		return 0;
	}
	
	@Override
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	@Override
	public boolean getScrollableTracksViewportWidth() {
		return true;
	}

	private int calcStepSizeVerticalDown(Rectangle visibleRect) {
		
		int lastx = getComponent(0).getX(); // initialise au 1er composant
		
		int lineY=getComponent(0).getY(); // position la plus haute du 1er composant
		
		for (int cindex = 2; cindex < getComponentCount(); cindex++) {
			
			Component c = getComponent(cindex);
		
			// le composant n'est pas le premier de la ligne 
			// => stocke la hauteur de la ligne si elle est plus haute que celle actuellement enregistrée
			if (c.getX()>lastx) {
				lineY = Math.min(lineY,c.getY());
				lastx = c.getX();
				continue;
			}
			
			// le composant est le 1er de la ligne, on vient donc de finir la ligne
			
			// teste si c'est la 1ere ligne en dessous
			// oui => c'est la ligne a afficher
			if ((lineY-wl_panel.getVgap())>visibleRect.getY()) break;
			
			// prepare la boucle suivante
			lastx = c.getX(); // reinitialise la derniere position
			lineY = c.getY(); // initialise la position la plus haute de la ligne
			
		}

		return lineY - wl_panel.getVgap() - (int) visibleRect.getY();
		
	}
	
	private int calcStepSizeVerticalUp(Rectangle visibleRect) {
		
		int lastx = getComponent(0).getX(); // initialise au 1er composant
		
		int lineY1=-1;
		int lineY=getComponent(0).getY(); // position la plus haute du 1er composant
		
		for (int cindex = 2; cindex < getComponentCount(); cindex++) {
			
			Component c = getComponent(cindex);
		
			// le composant n'est pas le premier de la ligne 
			// => stocke la hauteur de la ligne si elle est plus haute que celle actuellement enregistrée
			if (c.getX()>lastx) {
				lineY = Math.min(lineY,c.getY());
				lastx = c.getX();
				continue;
			}
			
			// le composant est le 1er de la ligne, on vient donc de finir la ligne
			
			// teste si c'est la 1ere ligne en dessous
			// oui => c'est la ligne a afficher
			if ((lineY-wl_panel.getVgap())>=visibleRect.getY()) break;
			
			// prepare la boucle suivante
			lastx = c.getX(); // reinitialise la derniere position
			lineY1 = lineY; // stocke la hauteur de ligne precedente
			lineY = c.getY(); // initialise la position la plus haute de la ligne
		}

		if (lineY1<0) return 0;
		
		return (int) visibleRect.getY() - (lineY1 - wl_panel.getVgap());
		
	}
	
}

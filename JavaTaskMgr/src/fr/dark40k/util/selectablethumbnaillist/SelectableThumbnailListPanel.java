package fr.dark40k.util.selectablethumbnaillist;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedHashSet;

import javax.swing.JPanel;

public class SelectableThumbnailListPanel<T extends JPanel> extends JScrollablePanel {

	private SelectableThumbnail<T> cursorSelectableThumbnail;

	public SelectableThumbnailListPanel() {
	}

	public void addSelectablePanel(SelectableThumbnail<T> selectableThumbnail) {

		selectableThumbnail.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) { }

            @Override
            public void mouseReleased(MouseEvent e) {
            	manageDocContentMouseEvent(e);
            }
        });

		add(selectableThumbnail);
	}

	//
	// Selection
	//

	public boolean isSelected(T panel) {

		for (int index = 0; index < getComponentCount(); index++) {
			@SuppressWarnings("unchecked")
			SelectableThumbnail<T> thumbnail = (SelectableThumbnail<T>)getComponent(index);
			if (thumbnail.getSubPanel()==panel)
				return thumbnail.isSelected();
		}

		return false;

	}

	public int getSelectedCount() {

		int count=0;

		for (int index = 0; index < getComponentCount(); index++) {
			@SuppressWarnings("unchecked")
			SelectableThumbnail<T> thumbnail = (SelectableThumbnail<T>)getComponent(index);
			if (thumbnail.isSelected()) count++;
		}

		return count;
	}

	public LinkedHashSet<T> listSelected() {
		LinkedHashSet<T> selected = new LinkedHashSet<>();

		for (int index = 0; index < getComponentCount(); index++) {
			@SuppressWarnings("unchecked")
			SelectableThumbnail<T> thumbnail = (SelectableThumbnail<T>)getComponent(index);
			if (thumbnail.isSelected())
				selected.add(thumbnail.getSubPanel());
		}

		return selected;
	}

	//
	// Internes
	//

	@SuppressWarnings("unchecked")
	private void manageDocContentMouseEvent(MouseEvent e) {

		SelectableThumbnail<T> clickedSelectableThumbnail=(SelectableThumbnail<T>) e.getSource();

		int indexClicked = getComponentIndex(clickedSelectableThumbnail);

		// click classique
		if (!e.isControlDown() && !e.isShiftDown()) {
			for (int index = 0; index < getComponentCount(); index++)
				((SelectableThumbnail<T>) getComponent(index)).setSelected(index == indexClicked);
			cursorSelectableThumbnail = clickedSelectableThumbnail;
			return;
		}

		// control + click
		if (e.isControlDown() && (!e.isShiftDown())) {
			clickedSelectableThumbnail.setSelected(!clickedSelectableThumbnail.isSelected());
			cursorSelectableThumbnail=clickedSelectableThumbnail;
			return;
		}

		//
		int indexCursor = getComponentIndex(cursorSelectableThumbnail);
		int index1 = Math.min(indexClicked, indexCursor);
		int index2 = Math.max(indexClicked, indexCursor);

		// shift + click
		if (!e.isControlDown() && e.isShiftDown()) {
			for (int index=0; index<getComponentCount();index++)
				((SelectableThumbnail<T>)getComponent(index)).setSelected((index>=index1)&&(index<=index2));
			return;
		}

		// shift + control + click
		if (e.isControlDown() && e.isShiftDown()) {
			boolean status = cursorSelectableThumbnail.isSelected();
			for (int index=index1; index <= index2; index++)
				((SelectableThumbnail<T>)getComponent(index)).setSelected(status);
			cursorSelectableThumbnail=clickedSelectableThumbnail;
			return;
		}

	}

	private int getComponentIndex(Component component) {
		for (int i = 0; i < getComponentCount(); i++)
			if (getComponent(i) == component)
				return i;
		return -1;
	}




}

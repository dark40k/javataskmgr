package fr.dark40k.util;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormattersString {

	//
	// Misceleanous
	//
	
	public static String textToHTML(String string) {
		return "<html><code>" + string.replace("\n", "<br>").replace(" ", "&nbsp;") + "</html>";
	}

	//
	// LocalDate
	//
	public final static DateTimeFormatter LOCALDATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public static String format(LocalDate localDate, String defaultString) {
		if (localDate == null) return defaultString;
		return localDate.format(LOCALDATE_FORMATTER);
	}

	public static LocalDate parseLocalDate(String string, LocalDate defaultLocalDate) {
		try {
			return LocalDate.parse(string, LOCALDATE_FORMATTER);
		} catch (Exception e) {
			return defaultLocalDate;
		}
	}
	
	//
	// LocalDateTime
	//
	
	public final static DateTimeFormatter LOCALDATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy-HH:mm");
	
	public static String format(LocalDateTime localDateTime, String defaultString) {
		if (localDateTime == null) return defaultString;
		return localDateTime.format(LOCALDATETIME_FORMATTER);
	}

	public static LocalDateTime parseLocalDateTime(String string, LocalDateTime defaultLocalDateTime) {
		try {
			return LocalDateTime.parse(string, LOCALDATETIME_FORMATTER);
		} catch (Exception e) {
			return defaultLocalDateTime;
		}
	}

	//
	// Duration
	// 
	
	// formule sans les codes : ((?<hh>\d+)\s*h)?(\s*)((?<mm>\d+)\s*m)?(\s*)((?<ss>\d+(.\d*)?)\s*s)?(\s*)
	private final static Pattern regExp = Pattern.compile("((?<hh>\\d+)\\s*h)?(\\s*)((?<mm>\\d+)\\s*m)?(\\s*)((?<ss>\\d+(.\\d*)?)\\s*s)?(\\s*)");
	
	/**
	 * Convertit une dur�e au format "256h 38m 26s" 
	 * 
	 * @param duration
	 * @param defaultString : renvoy� si duration est null
	 * @return
	 */
	public static String format(Duration duration, String defaultString) {
		if (duration == null) return defaultString;
		long seconds = duration.getSeconds();
		long absSeconds = Math.abs(seconds);
		String positive = String.format("%dh %02dm %02ds", absSeconds / 3600, (absSeconds % 3600) / 60, absSeconds % 60);
		return seconds < 0 ? "-" + positive : positive;
	}
	
	/**
	 * Lit une dur�e bas�e sur le format "256h 38m 26s" 
	 * 
	 * Chaque groupe est optionnel
	 * Les espaces entre les groupes sont ignor�s.
	 * Les espaces entre la valeur et le symbole d'unit�s sont ignor�s
	 * 
	 * @param value
	 * @return
	 * 
	 * @exception  NumberFormatException if the {@code String} does not contain a parsable {@code Duration}.
	 *
	 */
	public static Duration parseDuration(String string, Duration defaultDuration) {
		try {
			return parseDuration(string);
		} catch (Exception e) {
			return defaultDuration;
		}		
	}
		
	public static Duration parseDuration(String value) throws NumberFormatException {
		
		Matcher m = regExp.matcher(value);
		
		if (!m.find()) throw new NumberFormatException("Format illisible : "+value);
		
		int hh = Integer.parseInt(m.group("hh")); 
		int mm = Integer.parseInt(m.group("mm")); 
		int ss = Integer.parseInt(m.group("ss")); 
		
		return Duration.ofHours(hh).plusMinutes(mm).plusSeconds(ss);
		
	}

	
}

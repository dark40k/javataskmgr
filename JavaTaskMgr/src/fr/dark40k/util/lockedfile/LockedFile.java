package fr.dark40k.util.lockedfile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Francois
 *
 */
public class LockedFile {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private File file;
	
	private OutputStream outputStream;
	
	private RandomAccessFile paramRAFile;
	private FileChannel paramFileChannel;
	private FileLock paramFileLock;

	/**
	 * <p> Constructruit un fichier verrouill�.
	 * 
	 * @param file : objet File du fichier utilis�
	 * @throws FileNotFoundException : si non trouv�
	 * @throws LockedFileException : si erreur lock
	 */
	public LockedFile(File file) throws FileNotFoundException, LockedFileException {
		
		this.file=file;
		
		// ouvre le fichier avec acces lecture/ecriture
		paramRAFile =  new RandomAccessFile(file, "rw");
		
        // ajoute un deverrouillage automatique � la fin de l'exection de la JVM
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
            	if (paramFileLock==null) return;
            	LOGGER.info("Shutdown release of file : "+file.getAbsolutePath());
            	release();
            }
        });

        // ouvre un channel
		paramFileChannel =paramRAFile.getChannel();

		
		// verrouille le fichier
		try {
			paramFileLock = paramFileChannel.tryLock(0,Long.MAX_VALUE,false);
		} catch (IOException e) {
			throw new LockedFileException(e);
		}
        
	}
	
//	/**
//	 * <p> Genere un fichier verrouill�. Renvoie null si le verrouillage � �chou�.
//	 * 
//	 * <p> Le fichier peut �tre lib�r� � la fin avec release()
//	 * 
//	 * @param file : fichier � verrouiller en lecture/ecriture 
//	 * @return objet LockedFile permettant d'acc�der au fichier
//	 * @throws FileNotFoundException
//	 */
//	public static LockedFile openLockedFile(File file) throws FileNotFoundException {
//		LockedFile lockedFile;
//		try {
//			lockedFile = new LockedFile(file);
//		} catch (LockedFileException e) {
//			return null;
//		}
//		return lockedFile;
//	}
//	
	/**
	 * <p> Renvoie l'objet File ayant servi � la cr�ation
	 * 
	 * @return File
	 */
	public File getFile() {
		return file;
	}
	
	/**
	 * <p>Libere le fichier verrouill�
	 * 
	 * <p>Note: appel� en fin d'execution par un shutdown hook.
	 * 
	 */
	public void release() {
		
		if (outputStream!=null) throw new IllegalStateException("File is locked for writing.");
		
		try {
			paramFileLock.release();
		} catch (Exception e) {
		}
		paramFileLock=null;
		
		try {
			paramFileChannel.close();
		} catch (Exception e) {
		}
		paramFileChannel=null;
		
	}
	
	
	/**
	 * <p> Lit le document en entier et le renvoie dans un InputStream
	 * 
	 * @return InputStream contenant les donn�es du document
	 * @throws IOException
	 */
	public InputStream getDataAsInputStream() throws IOException {
		
		if (outputStream!=null) throw new IllegalStateException("File is locked for writing.");

		if (paramRAFile==null) return null; 

		paramRAFile.seek(0);
		byte[] document = new byte[(int) paramRAFile.length()];
		paramRAFile.readFully(document);

		return new ByteArrayInputStream(document);
		
	}

	/**
	 * <p> Genere un flux OutputStream pour �crire dans le fichier. Le fichier est bloqu� tant que le flux n'est pas ferm�.
	 * 
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public OutputStream getOutputStream() throws IOException {
		
		paramRAFile.seek(0);
		
		outputStream = new OutputStream() {

			@Override
			public void write(int b) throws IOException {
				paramRAFile.write(b);
			}
			
			@Override
			public void close() throws IOException {
				paramRAFile.setLength(paramRAFile.getFilePointer());
				outputStream = null;
				super.close();
			}
			
		};
		
		return outputStream;
				
	}
	
	/**
	 * <p> Erreure generee lors d'un acc�s a un fichier verrouill�
	 * 
	 * @author Francois
	 *
	 */
	public class LockedFileException extends IOException {
		public LockedFileException(Throwable cause) {
			super(cause);
		}
	}

	
	
	
}

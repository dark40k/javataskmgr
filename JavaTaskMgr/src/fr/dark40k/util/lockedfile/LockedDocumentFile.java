package fr.dark40k.util.lockedfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import fr.dark40k.util.lockedfile.LockedFile.LockedFileException;

public class LockedDocumentFile {

	
	private LockedFile lockedFile;

	public LockedDocumentFile(File file) throws FileNotFoundException, LockedFileException {
		
		lockedFile = new LockedFile(file);
		
	}
	
	public void close()  {
		
		if (isClosed()) return;
		
		lockedFile.release();
		lockedFile=null;
	}

	private boolean isClosed() {
		return (lockedFile==null);
	}
	
	public Element readRootElement() throws IOException, JDOMException {
		
		if (isClosed()) throw new IOException("LockedDocumentFile is closed.");
		
		SAXBuilder sxb = new SAXBuilder();
		
		Document document = null;
		
		document = sxb.build(lockedFile.getDataAsInputStream());
		
		return document.getRootElement();
	}
	
	public void writeRootElement(Element rootElt) throws IOException {
		
		XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());

		OutputStream fileOut = lockedFile.getOutputStream();
		try {
			sortie.output(rootElt, fileOut);
			
		} finally {
			fileOut.close();
		}
		
	}

	public File getFile() {
		return lockedFile.getFile();
	}
	
	
}

package fr.dark40k.util.shef40k.shef;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventObject;
import java.util.function.Consumer;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.event.EventListenerList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bushe.swing.action.ActionList;
import org.bushe.swing.action.ActionManager;
import org.bushe.swing.action.ActionUIFactory;

import fr.dark40k.taskmgr.gui.node.notes.PasteOutlookEmailHTMLLinkAction;
import fr.dark40k.util.shef40k.i18n.I18n;
import fr.dark40k.util.shef40k.shef.Shef40kEditPanel.EditMode;
import fr.dark40k.util.shef40k.shef.Shef40kEditPanel.Shef40kEditorEvent;
import fr.dark40k.util.shef40k.ui.ColorActionButton;
import fr.dark40k.util.shef40k.ui.DefaultAction;
import fr.dark40k.util.shef40k.ui.UIUtils;
import fr.dark40k.util.shef40k.ui.text.CompoundUndoManager;
import fr.dark40k.util.shef40k.ui.text.actions.ClearAllStylesAction;
import fr.dark40k.util.shef40k.ui.text.actions.ClearStylesAction;
import fr.dark40k.util.shef40k.ui.text.actions.CopyAction;
import fr.dark40k.util.shef40k.ui.text.actions.CutAction;
import fr.dark40k.util.shef40k.ui.text.actions.FindReplaceAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLBlockAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLChangeColorAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLEditorActionFactory;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLElementPropertiesAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLExecuteLinkAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLFontAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLFontBackColorAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLFontBackColorEditAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLFontColorAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLFontColorEditAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLHorizontalRuleAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLImageAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLInlineAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLLineBreakAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLLinkAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLTableAction;
import fr.dark40k.util.shef40k.ui.text.actions.IndentAction;
import fr.dark40k.util.shef40k.ui.text.actions.PasteAction;
import fr.dark40k.util.shef40k.ui.text.actions.PasteTextAction;
import fr.dark40k.util.shef40k.ui.text.actions.SelectAllAction;
import fr.dark40k.util.shef40k.ui.text.actions.SpecialCharAction;
import fr.dark40k.util.swing.JColorButton;

public class Shef40kEditor extends JPanel implements Shef40kEditPanel.Shef40kEditorListener {

	@SuppressWarnings("unused")
	private final static Logger LOGGER = LogManager.getLogger();

	private final Shef40kEditPanel editor = new Shef40kEditPanel();

	private final Shef40kExecuteLinks executeLinks = new Shef40kExecuteLinks();
    private final HTMLExecuteLinkAction executeLinkAction = new HTMLExecuteLinkAction(executeLinks);

    private final Action objectPropertiesAction = new HTMLElementPropertiesAction();
    private final Action executeLinksAction = new HTMLExecuteLinkAction(executeLinks);

	private final JPanel topPanel = new JPanel(new BorderLayout(0, 0));

	// -------------------------------------------------------------------------------------------
	//
	// Constructeur
	//
	// -------------------------------------------------------------------------------------------

	public Shef40kEditor() {

		//
		// Initialisation et remplissage du panneau
		//
		setLayout(new BorderLayout(0, 0));
		add(topPanel, BorderLayout.NORTH);
		add(editor, BorderLayout.CENTER);

		//
		// insertion des menus
		//

		JMenuBar menuBar = new JMenuBar();

        buildEditMenu();
		menuBar.add(editMenu);

        buildFormatMenu();
		menuBar.add(formatMenu);

        buildInsertMenu();
		menuBar.add(insertMenu);

		topPanel.add(menuBar, BorderLayout.NORTH);

		//
		// insertion de la barre d'outils
		//
        buildToolBar();
		topPanel.add(formatToolBar, BorderLayout.SOUTH);

		//
		// Ajout des popups
		//

		buildWyswigPopupMenu();
		editor.wysEditor.addMouseListener(popupHandler);

        buildSourcePopupMenu();
        editor.srcEditor.addMouseListener(popupHandler);

        //
        // Liens dynamiques
        //
        editor.wysEditor.addMouseListener(dblClickHandler);

	}

	// -------------------------------------------------------------------------------------------
	//
	// Methodes interface
	//
	// -------------------------------------------------------------------------------------------

	public void addHTMLLinkExecution(String prefix, Consumer<String> action) {
		executeLinks.addHTMLLinkExecution(prefix, action);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Manipulation du texte
	//
	// -------------------------------------------------------------------------------------------

	public String getEditorText() {
		return editor.getText();
	}

	@SuppressWarnings("unused")
	public void setEditorText(final String newText, final boolean editable) {

		String newFullText;

		if (newText == null)
			newFullText = "";
		else if (!newText.startsWith("<"))
			newFullText = "<div>" + newText.replaceAll("(\r\n|\n\r|\r|\n)", "</div><div>") + "</div>";
		else
			newFullText = newText;

		if (!editor.getText().equals(newText)) {

			editor.removeUpdateHTMLTextListener(this);

			editor.setText("<h1> &lt;&lt;&lt; Chargement en cours &gt;&gt;&gt; </h1><div>Merci de patienter...</div>");

			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					editor.setText(newFullText);
					editor.addUpdateHTMLTextListener(Shef40kEditor.this);
				}
			});

		}

	}

	// -------------------------------------------------------------------------------------------
	//
	// Change listener
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void updateText(Shef40kEditPanel.Shef40kEditorEvent evt) {
		fireHTMLTextChanged(this);
	}

	@Override
	public void updateState(Shef40kEditorEvent evt) {

//        paragraphCombo.setEnabled(editor.getEditMode()==EditMode.WYSIWYG);

//        fontFamilyCombo.setEnabled(editor.getEditMode()==EditMode.WYSIWYG);
//
//        if(editor.getEditMode() == EditMode.WYSIWYG)
//        {
//            fontFamilyCombo.removeActionListener(fontChangeHandler);
//            String fontName = HTMLUtils.getFontFamily(editor.wysEditor);
//            if(fontName == null)
//                fontFamilyCombo.setSelectedIndex(0);
//            else
//                fontFamilyCombo.setSelectedItem(new Font(fontName, Font.PLAIN, 12));
//            fontFamilyCombo.addActionListener(fontChangeHandler);
//        }

	}

	// -------------------------------------------------------------------------------------------
	//
	// Change event
	//
	// -------------------------------------------------------------------------------------------

	public class UpdateHTMLTextEvent extends EventObject {
		public UpdateHTMLTextEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateHTMLTextListener extends java.util.EventListener {
		public void updateHTMLText(UpdateHTMLTextEvent evt);
	}

	final EventListenerList listenerList = new EventListenerList();

	public void addUpdateHTMLTextListener(UpdateHTMLTextListener l) {
		listenerList.add(UpdateHTMLTextListener.class, l);
	}

	public void removeUpdateHTMLTextListener(UpdateHTMLTextListener l) {
		listenerList.remove(UpdateHTMLTextListener.class, l);
	}

	public void fireHTMLTextChanged(Object source) {
		UpdateHTMLTextEvent updateEvent = new UpdateHTMLTextEvent(source);
		Object[] listeners = listenerList.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2)
			if (listeners[i] == UpdateHTMLTextListener.class)
				((UpdateHTMLTextListener) listeners[i + 1]).updateHTMLText(updateEvent);
	}

	// -------------------------------------------------------------------------------------------
	//
	// UI
	//
	// -------------------------------------------------------------------------------------------

    private static final I18n i18n = I18n.getInstance("fr.dark40k.util.shef40k.shef");

	private ChangeTabAction actionWYSIWYG;
	private ChangeTabAction actionHTML;

    private JMenu editMenu;
    private JMenu formatMenu;
    private JMenu insertMenu;

    private JPopupMenu wysPopupMenu, srcPopupMenu;

    private JToolBar formatToolBar;

//    private JComboBox<Font> fontFamilyCombo;
//    private JComboBox<Action> paragraphCombo;

    private MouseListener dblClickHandler = new DblClickHandler();
    private MouseListener popupHandler = new PopupHandler();
//    private ActionListener fontChangeHandler = new FontChangeHandler();

	// -------------------------------------------------------------------------------------------
	//
	// UI - Popup Menus
	//
	// -------------------------------------------------------------------------------------------

    private void buildWyswigPopupMenu() {

        //
        // create menu
        //

        wysPopupMenu = new JPopupMenu();

        //
        // Undo/redo
        //
        addActionToMenu(wysPopupMenu, CompoundUndoManager.UNDO);
        addActionToMenu(wysPopupMenu, CompoundUndoManager.REDO);

        wysPopupMenu.addSeparator();

        //
        // Copy/Paste
        //
        addActionToMenu(wysPopupMenu, new CutAction());
        addActionToMenu(wysPopupMenu, new CopyAction());
        addActionToMenu(wysPopupMenu, new PasteAction());
        addActionToMenu(wysPopupMenu, new PasteTextAction());
        addActionToMenu(wysPopupMenu, new PasteOutlookEmailHTMLLinkAction());
        //addActionToMenu(editMenu, new PasteFormattedAction());

        wysPopupMenu.addSeparator();

        //
        // Divers
        //
        addActionToMenu(wysPopupMenu, new SelectAllAction());

        wysPopupMenu.addSeparator();

        addActionToMenu(wysPopupMenu, new FindReplaceAction(false, true));

        wysPopupMenu.addSeparator();

        addActionToMenu(wysPopupMenu, executeLinksAction);
        addActionToMenu(wysPopupMenu, objectPropertiesAction);

    }

    private void buildSourcePopupMenu() {

        //
        // create menu
        //

        srcPopupMenu = new JPopupMenu();

        //
        // Undo/redo
        //
        addActionToMenu(srcPopupMenu, CompoundUndoManager.UNDO);
        addActionToMenu(srcPopupMenu, CompoundUndoManager.REDO);

        srcPopupMenu.addSeparator();

        //
        // Copy/Paste
        //
        addActionToMenu(srcPopupMenu, new CutAction());
        addActionToMenu(srcPopupMenu, new CopyAction());
        addActionToMenu(srcPopupMenu, new PasteAction());
        addActionToMenu(srcPopupMenu, new PasteTextAction());
        addActionToMenu(srcPopupMenu, new PasteOutlookEmailHTMLLinkAction());
        //addActionToMenu(editMenu, new PasteFormattedAction());

        srcPopupMenu.addSeparator();

        //
        // Divers
        //
        addActionToMenu(srcPopupMenu, new SelectAllAction());

        srcPopupMenu.addSeparator();

        addActionToMenu(srcPopupMenu, new FindReplaceAction(false, true));

    }

	// -------------------------------------------------------------------------------------------
	//
	// UI - Menus
	//
	// -------------------------------------------------------------------------------------------

    private void buildEditMenu() {

        //
        // create format menu
        //

        editMenu = new JMenu(i18n.str("edit"));

        //
        // Mode d'edition
        //
        actionWYSIWYG = (ChangeTabAction) addActionToMenu(editMenu, new ChangeTabAction(EditMode.WYSIWYG, false));
        actionWYSIWYG.setSelected(editor.getEditMode()==EditMode.WYSIWYG);

        actionHTML = (ChangeTabAction) addActionToMenu(editMenu, new ChangeTabAction(EditMode.HTML, false));
        actionHTML.setSelected(editor.getEditMode()==EditMode.HTML);

        editMenu.addSeparator();

        //
        // Undo/redo
        //
        addActionToMenu(editMenu, CompoundUndoManager.UNDO);
        addActionToMenu(editMenu, CompoundUndoManager.REDO);

        editMenu.addSeparator();

        //
        // Copy/Paste
        //
        addActionToMenu(editMenu, new CutAction());
        addActionToMenu(editMenu, new CopyAction());
        addActionToMenu(editMenu, new PasteAction());
        addActionToMenu(editMenu, new PasteTextAction());
        //addActionToMenu(editMenu, new PasteFormattedAction());

        editMenu.addSeparator();

        //
        // Divers
        //
        addActionToMenu(editMenu, new SelectAllAction());

        editMenu.addSeparator();

        addActionToMenu(editMenu, new FindReplaceAction(false, true));

    }

    private void buildFormatMenu() {

        //
        // create format menu
        //

        formatMenu = new JMenu(i18n.str("format"));

        //
        // Bloc fonte
        //
        addActionListToMenu(formatMenu, "size", HTMLEditorActionFactory.createFontSizeActionList());
        addActionListToMenu(formatMenu, "style", HTMLEditorActionFactory.createInlineActionList());
        addActionToMenu(formatMenu, new HTMLFontColorEditAction(0));
        addActionToMenu(formatMenu, new HTMLFontBackColorEditAction());
        addActionToMenu(formatMenu, new HTMLFontAction());
        addActionToMenu(formatMenu, new ClearStylesAction());

        formatMenu.addSeparator();

        //
        // Bloc indentation
        //

        addActionToMenu(formatMenu, new IndentAction(IndentAction.OUTDENT));
        addActionToMenu(formatMenu, new IndentAction(IndentAction.INDENT));

        formatMenu.addSeparator();

        //
        // Bloc paragraph + list
        //

        addActionListToMenu(formatMenu, "paragraph", HTMLEditorActionFactory.createBlockElementActionList());
        addActionListToMenu(formatMenu, "list", HTMLEditorActionFactory.createListElementActionList());

        formatMenu.addSeparator();

        //
        // Bloc alignement et tableau
        //

        addActionListToMenu(formatMenu,"align", HTMLEditorActionFactory.createAlignActionList());

        JMenu tableMenu = new JMenu(i18n.str("table"));
        formatMenu.add(tableMenu);

        addActionListToMenu(tableMenu,"insert", HTMLEditorActionFactory.createInsertTableElementActionList());
        addActionListToMenu(tableMenu,"delete", HTMLEditorActionFactory.createDeleteTableElementActionList());

        formatMenu.addSeparator();

        //
        // Bloc propriete et executer lien
        //

        editor.actionList.add(objectPropertiesAction);
        formatMenu.add(objectPropertiesAction);

        editor.actionList.add(executeLinksAction);
        formatMenu.add(executeLinksAction);

    }


	// -------------------------------------------------------------------------------------------
	//
	// UI - Outils pour creation Menus
	//
	// -------------------------------------------------------------------------------------------

    private void buildInsertMenu() {

        //create insert menu
        insertMenu = new JMenu(i18n.str("insert"));

        addActionToMenu(insertMenu, new HTMLLinkAction());
        addActionToMenu(insertMenu, new HTMLImageAction());
        addActionToMenu(insertMenu, new HTMLTableAction());

        insertMenu.addSeparator();

        addActionToMenu(insertMenu, new HTMLLineBreakAction());
        addActionToMenu(insertMenu, new HTMLHorizontalRuleAction());
        addActionToMenu(insertMenu, new SpecialCharAction());

        insertMenu.addSeparator();
    }

    public ActionList addActionListToMenu(JMenu menu, String name, ActionList lst) {
        editor.actionList.addAll(lst);
        menu.add(createMenu(lst, i18n.str(name)));;
        return lst;
    }

    private Action addActionToMenu(JPopupMenu menu, Action act) {
        editor.actionList.add(act);
        menu.add(ActionUIFactory.getInstance().createMenuItem(act));
        return act;
    }

    public Action addActionToMenu(JMenu menu, Action act) {
        editor.actionList.add(act);
        menu.add(ActionUIFactory.getInstance().createMenuItem(act));
        return act;
    }

    private JMenu createMenu(ActionList lst, String menuName)
    {
        JMenu m = ActionUIFactory.getInstance().createMenu(lst);
        m.setText(menuName);
        return m;
    }

    public JMenu getMenu(Shef40kMenuEnum menu) {
		switch (menu) {
		case EDIT:
			return editMenu;
		case FORMAT:
			return formatMenu;
		case INSERT:
			return insertMenu;
		}
		return null;
    }

    public void addCustomActionToMenuAndToolbar(Shef40kMenuEnum menuItem, Action act, boolean toolbar, boolean separator) {

    	JMenu menu = getMenu(menuItem);

    	if (separator) menu.addSeparator();

        editor.actionList.add(act);
        menu.add(act);

        if (toolbar) {
        	if (separator) formatToolBar.addSeparator();
        	addButtonToToolBar(formatToolBar, act);
        }

    }

	// -------------------------------------------------------------------------------------------
	//
	// UI - Barre d'outils
	//
	// -------------------------------------------------------------------------------------------


	private void buildToolBar()
    {
        Action act;
    	ActionList lst;

    	//
    	// Initialisation de la toolbar
    	//
        formatToolBar = new JToolBar();
        formatToolBar.setFloatable(false);
        formatToolBar.setFocusable(false);

        //
        // Remplissage de la toolbar
        //

        ChangeTabAction actTab = new ChangeTabAction(EditMode.HTML, true);
        actTab.putValue(Action.SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "text-html.png"));
        actTab.setSelected(editor.getEditMode()==EditMode.HTML);
        addToggleButtonToToolBar(formatToolBar, actTab);

        formatToolBar.addSeparator();

        //
        // Undo/redo
        //
        addButtonToToolBar(formatToolBar, CompoundUndoManager.UNDO);
        addButtonToToolBar(formatToolBar, CompoundUndoManager.REDO);

        formatToolBar.addSeparator();

        // Copier / coller

        addButtonToToolBar(formatToolBar, new CutAction());
        addButtonToToolBar(formatToolBar, new CopyAction());
        addButtonToToolBar(formatToolBar, new PasteAction());
        addButtonToToolBar(formatToolBar, new PasteTextAction());

        formatToolBar.addSeparator();

        // Format du bloc

        addButtonToToolBar(formatToolBar, new ClearAllStylesAction());

        act=new HTMLBlockAction(HTMLBlockAction.DIV);
        act.putValue(Action.SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "div.png"));
        addToggleButtonToToolBar(formatToolBar, act);

        act=new HTMLBlockAction(HTMLBlockAction.H1);
        act.putValue(Action.SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "h1.png"));
        addToggleButtonToToolBar(formatToolBar, act);

        act=new HTMLBlockAction(HTMLBlockAction.H2);
        act.putValue(Action.SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "h2.png"));
        addToggleButtonToToolBar(formatToolBar, act);

        act=new HTMLBlockAction(HTMLBlockAction.H3);
        act.putValue(Action.SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "h3.png"));
        addToggleButtonToToolBar(formatToolBar, act);


//        // Type de bloc
//        lst = HTMLEditorActionFactory.createBlockElementActionList();
//        paragraphCombo = new ParagraphCombo(lst);
//        editor.actionList.addAll(lst);
//        formatToolBar.add(paragraphCombo);

        formatToolBar.addSeparator();

//        // Fonte
//        fontFamilyCombo = new FontFamilyCombo();
//        fontFamilyCombo.addActionListener(fontChangeHandler);
//        formatToolBar.add(fontFamilyCombo);
//
//        formatToolBar.addSeparator();

//        final JButton fontSizeButton = new JButton(UIUtils.getIcon(UIUtils.X16, "fontsize.png"));
//        final JPopupMenu sizePopup = ActionUIFactory.getInstance().createPopupMenu(fontSizeActs);
//        ActionListener al = new ActionListener()
//        {
//            @Override
//			public void actionPerformed(ActionEvent e)
//            {
//                sizePopup.show(fontSizeButton, 0, fontSizeButton.getHeight());
//            }
//        };
//        fontSizeButton.addActionListener(al);
//        configToolbarButton(fontSizeButton);
//        formatToolBar.add(fontSizeButton);

        // Couleurs
        HTMLChangeColorAction actColor = new HTMLFontColorAction();
        editor.actionList.add(actColor);
        final JColorButton fontColorButton = new ColorActionButton(actColor);
        formatToolBar.add(fontColorButton);

        actColor = new HTMLFontBackColorAction();
        editor.actionList.add(actColor);
        final JColorButton fontBackColorButton = new ColorActionButton(actColor);
        formatToolBar.add(fontBackColorButton);

        formatToolBar.addSeparator();

        // Formats : gras, italique, ...
        addToggleButtonToToolBar(formatToolBar, new HTMLInlineAction(HTMLInlineAction.BOLD));
        addToggleButtonToToolBar(formatToolBar, new HTMLInlineAction(HTMLInlineAction.ITALIC));
        addToggleButtonToToolBar(formatToolBar, new HTMLInlineAction(HTMLInlineAction.UNDERLINE));
        addToggleButtonToToolBar(formatToolBar, new HTMLInlineAction(HTMLInlineAction.STRIKE));

        formatToolBar.addSeparator();

        // Indentation
        addButtonToToolBar(formatToolBar, new IndentAction(IndentAction.OUTDENT));
        addButtonToToolBar(formatToolBar, new IndentAction(IndentAction.INDENT));

        formatToolBar.addSeparator();

        // listes
        addToggleButtonToToolBar(formatToolBar, new HTMLBlockAction(HTMLBlockAction.UL));
        addToggleButtonToToolBar(formatToolBar, new HTMLBlockAction(HTMLBlockAction.OL));

        formatToolBar.addSeparator();

//        alst = HTMLEditorActionFactory.createAlignActionList();
//        for(Iterator<?> it = alst.iterator(); it.hasNext();)
//			  addToggleButtonToToolBar(formatToolBar, (Action)it.next());
//
//        formatToolBar.addSeparator();

        addButtonToToolBar(formatToolBar, new HTMLLinkAction());
        addButtonToToolBar(formatToolBar, new HTMLImageAction());
        addButtonToToolBar(formatToolBar, new HTMLTableAction());

        formatToolBar.addSeparator();

        addButtonToToolBar(formatToolBar, new HTMLHorizontalRuleAction());
        addButtonToToolBar(formatToolBar, new SpecialCharAction());


    }

    private Action addButtonToToolBar(JToolBar toolbar, Action act)
    {
        AbstractButton button = ActionUIFactory.getInstance().createButton(act);

        button.setText(null);
        button.setMnemonic(0);
        button.setMargin(new Insets(1, 1, 1, 1));
        button.setMaximumSize(new Dimension(22, 22));
        button.setMinimumSize(new Dimension(22, 22));
        button.setPreferredSize(new Dimension(22, 22));
        button.setFocusable(false);
        button.setFocusPainted(false);
        //button.setBorder(plainBorder);

        Action a = button.getAction();
        if(a != null)
            button.setToolTipText(a.getValue(Action.NAME).toString());

        editor.actionList.add(act);
        toolbar.add(button);
        return act;
    }

    private Action addToggleButtonToToolBar(JToolBar toolbar, Action act) {
    	act.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
    	return addButtonToToolBar(toolbar, act);
    }

	// -------------------------------------------------------------------------------------------
	//
	// UI - Listeners
	//
	// -------------------------------------------------------------------------------------------

	private class DblClickHandler extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getSource() == editor.wysEditor)
				if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1)
					executeLinkAction.executeLink(editor.wysEditor);
		}

	}

//    private class FontChangeHandler implements ActionListener
//    {
//        @Override
//		public void actionPerformed(ActionEvent e)
//        {
//            if(e.getSource() == fontFamilyCombo && editor.getEditMode() == EditMode.WYSIWYG )
//            {
//                //MutableAttributeSet tagAttrs = new SimpleAttributeSet();
//                HTMLDocument document = (HTMLDocument)editor.wysEditor.getDocument();
//                CompoundUndoManager.beginCompoundEdit(document);
//
//                if(fontFamilyCombo.getSelectedIndex() != 0)
//                {
//                    HTMLUtils.setFontFamily(editor.wysEditor, ((Font)fontFamilyCombo.getSelectedItem()).getFamily());
//
//                }
//                else
//                {
//                    HTMLUtils.setFontFamily(editor.wysEditor, null);
//                }
//                CompoundUndoManager.endCompoundEdit(document);
//            }
//        }
//
//        /* (non-Javadoc)
//         * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
//         */
//        @SuppressWarnings("unused")
//		public void itemStateChanged(ItemEvent e)
//        {
//
//
//        }
//    }

    private class PopupHandler extends MouseAdapter
    {
        @Override
		public void mousePressed(MouseEvent e)
        { checkForPopupTrigger(e); }

        @Override
		public void mouseReleased(MouseEvent e)
        { checkForPopupTrigger(e); }

        private void checkForPopupTrigger(MouseEvent e)
        {
            if(e.isPopupTrigger())
            {
                JPopupMenu p = null;
                if(e.getSource() == editor.wysEditor)
                    p =  wysPopupMenu;
                else if(e.getSource() == editor.srcEditor)
                    p = srcPopupMenu;
                else
                    return;
                p.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }

	// -------------------------------------------------------------------------------------------
	//
	// Actions
	//
	// -------------------------------------------------------------------------------------------


    private class ChangeTabAction extends DefaultAction
    {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        final EditMode editMode;
		private boolean switchOperation;

        public ChangeTabAction(EditMode editMode, boolean switchOperation)
        {
            super((editMode==EditMode.WYSIWYG) ? i18n.str("rich_text") : i18n.str("source"));
            this.editMode = editMode;
            this.switchOperation = switchOperation;
            putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_RADIO);
        }

        @Override
		protected void execute(ActionEvent e)
        {
        	if (switchOperation)
        		editor.setEditMode(editor.getEditMode() == EditMode.WYSIWYG ? EditMode.HTML:EditMode.WYSIWYG);
        	else
        		editor.setEditMode(editMode);
        }

        @Override
		protected void contextChanged()
        {
            setSelected(editor.getEditMode() == editMode);
        }
    }

}

package fr.dark40k.util.shef40k.shef;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.StringReader;
import java.util.EventObject;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.undo.UndoManager;

import org.bushe.swing.action.ActionList;
import org.jsoup.Jsoup;

import fr.dark40k.util.shef40k.ui.text.CompoundUndoManager;
import fr.dark40k.util.shef40k.ui.text.Entities;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;
import fr.dark40k.util.shef40k.ui.text.IndentationFilter;
import fr.dark40k.util.shef40k.ui.text.SourceCodeEditor;
import fr.dark40k.util.shef40k.ui.text.WysiwygHTMLEditorKit;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLTextEditAction;
import novaworx.syntax.SyntaxFactory;
import novaworx.textpane.SyntaxDocument;
import novaworx.textpane.SyntaxGutter;
import novaworx.textpane.SyntaxGutterBase;

/**
 *
 * @author Bob Tantlinger
 */
public class Shef40kEditPanel extends JPanel
{
	 /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final String INVALID_TAGS[] = {"html", "head", "body", "title"};

    public enum EditMode {
    	WYSIWYG("WYSIWYG"),
    	HTML("HTML");
    	private final String text;
    	EditMode(final String text) { this.text=text; }
        public static EditMode valueOfByCode(final String text) {
            for(EditMode text_val : values()) {
                if(text_val.text.equals(text)) {
                    return text_val;
                }
            }
            throw new IllegalArgumentException("Text n'est pas valide.");
        }
    };


    private EditMode editMode;

    protected final JEditorPane wysEditor = new JEditorPane();
    protected final SourceCodeEditor srcEditor = new SourceCodeEditor();

   //private JMenuBar menuBar;

    protected final ActionList actionList  = new ActionList("editor-actions");

    private FocusListener focusHandler = new FocusHandler();
    private DocumentListener textChangedHandler = new TextChangedHandler();
    private CaretListener caretHandler = new CaretHandler();

    private boolean isWysTextChanged;

	private CardLayout cardsLayout;

    // ======================================================================================
    //
    // Constructeur
    //
    // ======================================================================================

    public Shef40kEditPanel()
    {

    	initUI();

    	setEditMode(EditMode.WYSIWYG);

    }

    // ======================================================================================
    //
    // Methodes
    //
    // ======================================================================================

    public void setEditMode(EditMode editMode) {

    	this.editMode=editMode;

    	cardsLayout.show(this,editMode.text);

        if(editMode==EditMode.WYSIWYG)
        {
            String topText = removeInvalidTags(srcEditor.getText());
            wysEditor.setText("");
            insertHTML(wysEditor, topText, 0);
            CompoundUndoManager.discardAllEdits(wysEditor.getDocument());
        }
        else
        {
            if(isWysTextChanged || srcEditor.getText().equals(""))
            {
            	String t = prettyPrintHTML(removeInvalidTags(wysEditor.getText()));
                t = Entities.HTML40.unescapeUnknownEntities(t);
                srcEditor.setText(t);
            }
            CompoundUndoManager.discardAllEdits(srcEditor.getDocument());
        }

        isWysTextChanged = false;

        updateState();

    }

    public EditMode getEditMode() {
    	return editMode;
    }

    public JEditorPane getActiveEditor() {
    	return (editMode == EditMode.WYSIWYG)?wysEditor:srcEditor;
    }

    public void setCaretPosition(int pos)
    {
    	if(editMode==EditMode.WYSIWYG)
    	{
    		wysEditor.setCaretPosition(pos);
    		wysEditor.requestFocusInWindow();
    	}
    	else
    	{
    		srcEditor.setCaretPosition(pos);
    		srcEditor.requestFocusInWindow();
    	}
    }

    public void setText(String text)
    {
    	String t = removeInvalidTags(text);
        t = Entities.HTML40.unescapeUnknownEntities(t);
        srcEditor.setText(prettyPrintHTML(t));
        CompoundUndoManager.discardAllEdits(srcEditor.getDocument());

    	isWysTextChanged = false;

    	if(editMode==EditMode.WYSIWYG)
        {
            wysEditor.setText("");
            insertHTML(wysEditor, t, 0);
            CompoundUndoManager.discardAllEdits(wysEditor.getDocument());
        }

    }

    public String getText()
    {
    	String topText;
    	if(editMode==EditMode.WYSIWYG)
        {
           topText = removeInvalidTags(wysEditor.getText());

        }
        else
        {
            topText = removeInvalidTags(srcEditor.getText());
//            topText = deIndent(removeInvalidTags(topText));
            topText = Entities.HTML40.unescapeUnknownEntities(topText);
        }

    	return topText;
    }


    // ======================================================================================
    //
    // Initialisation de l'interface
    //
    // ======================================================================================

    private void initUI()
    {

    	//
    	// Initialisation du panneau principal
    	//
    	cardsLayout = new CardLayout();
        setLayout(cardsLayout);

        //
        // Creation de la carte pour editeur wyswig
        //

    	wysEditor.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
    	wysEditor.setFont(new Font("Arial", Font.PLAIN, 12));

    	wysEditor.setEditorKitForContentType("text/html", new WysiwygHTMLEditorKit());

    	wysEditor.setContentType("text/html");

        insertHTML(wysEditor, "<div></div>", 0);

        wysEditor.addCaretListener(caretHandler);
        wysEditor.addFocusListener(focusHandler);

        HTMLDocument document = (HTMLDocument)wysEditor.getDocument();
        CompoundUndoManager cuhWyswig = new CompoundUndoManager(document, new UndoManager());
        document.addUndoableEditListener(cuhWyswig);
        document.addDocumentListener(textChangedHandler);

        wysEditor.setCaret( new DefaultCaret() {
            // Always leave the selection highlighted
            @Override
			public void setSelectionVisible(boolean ignoredChoice) {
                super.setSelectionVisible(true);
            }
        });

        wysEditor.setSelectionColor(new Color( 128, 128, 128, 128));

        add(new JScrollPane(wysEditor),EditMode.WYSIWYG.text);

        //
        // Creation de la carte pour editeur source
        //

        SyntaxDocument doc = new SyntaxDocument();
        doc.setSyntax(SyntaxFactory.getSyntax("html"));
        CompoundUndoManager cuhSource = new CompoundUndoManager(doc, new UndoManager());
        doc.addUndoableEditListener(cuhSource);
        doc.setDocumentFilter(new IndentationFilter());
        doc.addDocumentListener(textChangedHandler);
        srcEditor.setDocument(doc);

        srcEditor.addFocusListener(focusHandler);
        srcEditor.addCaretListener(caretHandler);

        JScrollPane scrollPane = new JScrollPane(srcEditor);
        SyntaxGutter gutter = new SyntaxGutter(srcEditor);
        SyntaxGutterBase gutterBase = new SyntaxGutterBase(gutter);
        scrollPane.setRowHeaderView(gutter);
        scrollPane.setCorner(ScrollPaneConstants.LOWER_LEFT_CORNER, gutterBase);
        add(scrollPane,EditMode.HTML.text);

        //
        // Finalisation du panneau des cartes
        //

        cardsLayout.show(this,EditMode.WYSIWYG.text);
        editMode=EditMode.WYSIWYG;

    }


    //  inserts html into the wysiwyg editor
    //  potential improvement : remove JEditorPane parameter
    private void insertHTML(JEditorPane editor, String html, int location)
    {
        try
        {
            HTMLEditorKit kit = (HTMLEditorKit) editor.getEditorKit();
            Document doc = editor.getDocument();
            StringReader reader = new StringReader(HTMLUtils.jEditorPaneizeHTML(html));
            kit.read(reader, doc, location);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private String prettyPrintHTML(String rawHTML)
    {

    	org.jsoup.nodes.Document doc = Jsoup.parseBodyFragment(rawHTML);
//    	System.out.print(doc.outputSettings().prettyPrint());
//    	doc.outputSettings().prettyPrint(true);
    	doc.outputSettings().indentAmount(4);
    	org.jsoup.nodes.Element body = doc.body();

    	return body.html();
    }

    private String removeInvalidTags(String html)
    {
        for(int i = 0; i < INVALID_TAGS.length; i++)
        {
            html = deleteOccurance(html, '<' + INVALID_TAGS[i] + '>');
            html = deleteOccurance(html, "</" + INVALID_TAGS[i] + '>');
        }

        return html.trim();
    }

    private String deleteOccurance(String text, String word)
    {
        //if(text == null)return "";
        StringBuffer sb = new StringBuffer(text);
        int p;
        while((p = sb.toString().toLowerCase().indexOf(word.toLowerCase())) != -1)
        {
            sb.delete(p, p + word.length());
        }
        return sb.toString();
    }

    private void updateState()
    {

        actionList.putContextValueForAll(HTMLTextEditAction.EDITOR, getActiveEditor());
        actionList.updateEnabledForAll();

        fireUpdateStateEvent(this);

    }

    private class CaretHandler implements CaretListener
    {
        /* (non-Javadoc)
         * @see javax.swing.event.CaretListener#caretUpdate(javax.swing.event.CaretEvent)
         */
        @Override
		public void caretUpdate(CaretEvent e)
        {
            updateState();
        }
    }

    private class FocusHandler implements FocusListener
    {
        @Override
		public void focusGained(FocusEvent e)
        {
            if(e.getComponent() instanceof JEditorPane)
            {
                JEditorPane ed = (JEditorPane)e.getComponent();
                CompoundUndoManager.updateUndo(ed.getDocument());

                updateState();
               // updateEnabledStates();
            }
        }

        @Override
		public void focusLost(FocusEvent e) {}

    }

    private class TextChangedHandler implements DocumentListener
    {
        @Override
		public void insertUpdate(DocumentEvent e)
        {
            textChanged();
        }

        @Override
		public void removeUpdate(DocumentEvent e)
        {
            textChanged();
        }

        @Override
		public void changedUpdate(DocumentEvent e)
        {
            textChanged();
        }

        private void textChanged()
        {
            if(editMode==EditMode.WYSIWYG)
                isWysTextChanged = true;
            fireUpdateTextEvent(Shef40kEditPanel.this);
        }
    }


	// -------------------------------------------------------------------------------------------
	//
	// Change event
	//
	// -------------------------------------------------------------------------------------------

	public class Shef40kEditorEvent extends EventObject {
		public Shef40kEditorEvent(Object source) {
			super(source);
		}
	}

	public interface Shef40kEditorListener extends java.util.EventListener {
		public void updateText(Shef40kEditorEvent evt);
		public void updateState(Shef40kEditorEvent evt);
	}

	final EventListenerList listenerList = new EventListenerList();

	public void addUpdateHTMLTextListener(Shef40kEditorListener l) {
		listenerList.add(Shef40kEditorListener.class, l);
	}

	public void removeUpdateHTMLTextListener(Shef40kEditorListener l) {
		listenerList.remove(Shef40kEditorListener.class, l);
	}

	public void fireUpdateTextEvent(Object source) {
		Shef40kEditorEvent updateEvent = new Shef40kEditorEvent(source);
		Object[] listeners = listenerList.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2)
			if (listeners[i] == Shef40kEditorListener.class)
				((Shef40kEditorListener) listeners[i + 1]).updateText(updateEvent);
	}

	public void fireUpdateStateEvent(Object source) {
		Shef40kEditorEvent updateEvent = new Shef40kEditorEvent(source);
		Object[] listeners = listenerList.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2)
			if (listeners[i] == Shef40kEditorListener.class)
				((Shef40kEditorListener) listeners[i + 1]).updateState(updateEvent);
	}


}

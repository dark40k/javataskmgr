package fr.dark40k.util.shef40k.shef;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.function.Consumer;

public class Shef40kExecuteLinks {

    private final LinkedHashMap<String,Consumer<String>> customActions = new LinkedHashMap<String,Consumer<String>>();

    public void addHTMLLinkExecution(String prefix, Consumer<String> action) {
    	customActions.put(prefix,action);
    }

    public void runLink(String data) {

		String[] splitData = data.split(":",2);

		if (customActions.containsKey(splitData[0]))
			customActions.get(splitData[0]).accept(splitData[1]);
		else
			execCommand("cmd /C explorer \""+data+"\"");


    }

	private static boolean execCommand(String cmd) {
		boolean success=false;
//		LOGGER.info("Running ="+cmd);
		Runtime rt = Runtime.getRuntime();
		try {
			rt.exec(cmd);
			success=true;
		} catch (IOException e) {
//			LOGGER.warn("Execution Failed.",e);
		}
		return success;
	}

}

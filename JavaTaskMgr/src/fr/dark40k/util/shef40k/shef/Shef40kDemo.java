package fr.dark40k.util.shef40k.shef;

import java.io.IOException;
import java.io.InputStream;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import fr.dark40k.util.shef40k.io.IOUtils;

public class Shef40kDemo {

    public static void main(String args[]) {


        try {
            UIManager.setLookAndFeel(
                UIManager.getSystemLookAndFeelClassName());
        } catch(Exception ex){}

        JFrame frame = new JFrame();

       SwingUtilities.invokeLater(new Runnable() {

            @Override
			public void run() {

            	Shef40kEditor shef40kPanel;

            	shef40kPanel=new Shef40kEditor();

				try (InputStream in = Shef40kDemo.class.getResourceAsStream("/fr/dark40k/util/shef40k/shef/htmlsnip.txt")) {
					shef40kPanel.setEditorText(IOUtils.read(in),true);
				} catch (IOException ex) {
					ex.printStackTrace();
				}

                frame.setTitle("HTML Editor Demo");
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.setSize(800, 600);
                frame.getContentPane().add(shef40kPanel);
                frame.setVisible(true);


            }
        });


       do {
           try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

       } while (!frame.isVisible());


        do {
            try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

        } while (frame.isVisible());

//        System.out.println(shef40kPanel.editor.getText());

        System.exit(0);


    }

}

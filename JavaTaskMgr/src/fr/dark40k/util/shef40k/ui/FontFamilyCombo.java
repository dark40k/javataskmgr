package fr.dark40k.util.shef40k.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

public class FontFamilyCombo extends JComboBox<Font> {

	private final static Font comboFont = new Font("Dialog", Font.PLAIN, 12);

	public FontFamilyCombo() {

        ArrayList<String> fonts = new ArrayList<String>();
        fonts.add("Default");
        fonts.add("serif");
        fonts.add("sans-serif");
        fonts.add("monospaced");
        GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
        fonts.addAll(Arrays.asList(gEnv.getAvailableFontFamilyNames()));

        setPreferredSize(new Dimension(150, 22));
        setMinimumSize(new Dimension(150, 22));
        setMaximumSize(new Dimension(150, 22));
        setFont(comboFont);

        setRenderer(new DefaultListCellRenderer() {

            protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected,cellHasFocus);
                renderer.setFont((Font)value);
                renderer.setText(((Font)value).getFontName());
                setFont((Font)value);
                return renderer;
            }
        });

        for (String f : fonts) {
        	Font ft = new Font(f, Font.PLAIN, 12);
   			addItem(ft);
        }

	}

}

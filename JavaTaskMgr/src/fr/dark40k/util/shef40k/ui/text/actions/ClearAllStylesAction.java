/*
 * Created on Feb 28, 2005
 *
 */
package fr.dark40k.util.shef40k.ui.text.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.bushe.swing.action.ShouldBeEnabledDelegate;

import fr.dark40k.util.shef40k.ui.UIUtils;
import fr.dark40k.util.shef40k.ui.text.CompoundUndoManager;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;

/**
 * Action which clears inline text styles
 *
 * @author Bob Tantlinger
 *
 */
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class ClearAllStylesAction extends HTMLTextEditAction
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final static HTML.Tag CLEARTAG=HTML.Tag.DIV;

    public ClearAllStylesAction()
    {
        super("Effacer mise en forme");
//        putValue(MNEMONIC_KEY, new Integer(i18n.mnem("clear_styles")));
//        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl Y"));
		putValue(Action.SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "eraser.png"));

        addShouldBeEnabledDelegate(new ShouldBeEnabledDelegate()
        {
            @Override
			public boolean shouldBeEnabled(Action a)
            {
                return getEditMode() != SOURCE;
            }
        });
    }

    @Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor)
    {
        HTMLDocument document = (HTMLDocument)editor.getDocument();
        int caret = editor.getCaretPosition();
        CompoundUndoManager.beginCompoundEdit(document);
        try
        {
        	cleanSelectionStyle(editor, e);
            clearBlockType(editor, e);
            editor.setCaretPosition(caret);
        }
        catch(Exception awwCrap)
        {
            awwCrap.printStackTrace();
        }

        CompoundUndoManager.endCompoundEdit(document);
    }


    private void clearBlockType(JEditorPane editor, ActionEvent e)
    throws BadLocationException
    {
    	HTMLDocument doc = (HTMLDocument)editor.getDocument();
        Element curE = doc.getParagraphElement(editor.getSelectionStart());
        Element endE = doc.getParagraphElement(editor.getSelectionEnd());

        Element curTD = HTMLUtils.getParent(curE, HTML.Tag.TD);
        HTML.Tag tag = CLEARTAG;
        HTML.Tag rootTag = getRootTag(curE);
        String html = ""; //$NON-NLS-1$

        //a list to hold the elements we want to change
        List elToRemove = new ArrayList();
        elToRemove.add(curE);

        while(true)
        {
            html += HTMLUtils.createTag(tag, curE.getAttributes(), HTMLUtils.getElementHTML(curE, false));
            if(curE.getEndOffset() >= endE.getEndOffset() || curE.getEndOffset() >= doc.getLength())
                break;
            curE = doc.getParagraphElement(curE.getEndOffset() + 1);
            elToRemove.add(curE);

            //did we enter a (different) table cell?
            Element ckTD = HTMLUtils.getParent(curE, HTML.Tag.TD);
            if(ckTD != null && !ckTD.equals(curTD))
                break;//stop here so we don't mess up the table
        }

        //set the caret to the start of the last selected block element
        editor.setCaretPosition(curE.getStartOffset());

        //insert our changed block
        //we insert first and then remove, because of a bug in jdk 6.0
        insertHTML(html, CLEARTAG, rootTag, e);

        //now, remove the elements that were changed.
        for(Iterator it = elToRemove.iterator(); it.hasNext();)
        {
            Element c = (Element)it.next();
            HTMLUtils.removeElement(c);
        }
    }

    private HTML.Tag getRootTag(Element elem)
    {
        HTML.Tag root = HTML.Tag.BODY;
        if(HTMLUtils.getParent(elem, HTML.Tag.TD) != null)
            root = HTML.Tag.TD;
        return root;
    }

    private void insertHTML(String html, HTML.Tag tag, HTML.Tag root, ActionEvent e)
    {
        HTMLEditorKit.InsertHTMLTextAction a =
            new HTMLEditorKit.InsertHTMLTextAction("insertHTML", html, root, tag);             //$NON-NLS-1$
        a.actionPerformed(e);
    }

 	private void cleanSelectionStyle(JEditorPane editor, ActionEvent e) {

        HTMLDocument document = (HTMLDocument)editor.getDocument();
        HTMLEditorKit kit = (HTMLEditorKit)editor.getEditorKit();

        //Element el = document.getCharacterElement(editor.getCaretPosition());
        MutableAttributeSet attrs = new SimpleAttributeSet();
        attrs.addAttribute(StyleConstants.NameAttribute, HTML.Tag.CONTENT);

        //int cpos = editor.getCaretPosition();
        int selStart = editor.getSelectionStart();
        int selEnd = editor.getSelectionEnd();

        if(selEnd > selStart)
        {
            document.setCharacterAttributes(selStart, selEnd - selStart, attrs, true);
        }

        kit.getInputAttributes().removeAttributes(kit.getInputAttributes());
        kit.getInputAttributes().addAttributes(attrs);

    }

    /* (non-Javadoc)
     * @see fr.dark40k.util.shef40k.ui.text.actions.HTMLTextEditAction#sourceEditPerformed(java.awt.event.ActionEvent, javax.swing.JEditorPane)
     */
    @Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor)
    {

    }
}

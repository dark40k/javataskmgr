/*
 * Created on Jun 19, 2005
 *
 */
package fr.dark40k.util.shef40k.ui.text.actions;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.html.HTML;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bushe.swing.action.ActionManager;
import org.bushe.swing.action.ShouldBeEnabledDelegate;

import fr.dark40k.util.shef40k.ui.UIUtils;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;


public class PasteAction extends HTMLTextEditAction
{
	@SuppressWarnings("unused")
	private final static Logger LOGGER = LogManager.getLogger();

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public PasteAction()
    {
        super(i18n.str("paste"));
        putValue(MNEMONIC_KEY, Integer.valueOf(i18n.mnem("paste")));
        putValue(SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "paste.png"));
        putValue(ActionManager.LARGE_ICON, UIUtils.getIcon(UIUtils.X24, "paste.png"));
		putValue(ACCELERATOR_KEY,
			KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
        addShouldBeEnabledDelegate(new ShouldBeEnabledDelegate()
        {
            @Override
			public boolean shouldBeEnabled(Action a)
            {
                //return getCurrentEditor() != null &&
                //    Toolkit.getDefaultToolkit().getSystemClipboard().getContents(PasteAction.this) != null;
                return true;
            }
        });

        putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
    }

    @Override
	protected void updateWysiwygContextState(JEditorPane wysEditor)
    {
        this.updateEnabledState();
    }

    @Override
	protected void updateSourceContextState(JEditorPane srcEditor)
    {
        this.updateEnabledState();
    }

    /* (non-Javadoc)
     * @see fr.dark40k.util.shef40k.ui.text.actions.HTMLTextEditAction#sourceEditPerformed(java.awt.event.ActionEvent, javax.swing.JEditorPane)
     */
    @Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor)
    {
        editor.paste();
    }

    /* (non-Javadoc)
     * @see fr.dark40k.util.shef40k.ui.text.actions.HTMLTextEditAction#wysiwygEditPerformed(java.awt.event.ActionEvent, javax.swing.JEditorPane)
     */
    @Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor)
    {

    	Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
    	if (cb.isDataFlavorAvailable(DataFlavor.javaFileListFlavor)) {
    	    try {

    	        @SuppressWarnings("unchecked")
				List<File> files = (List<File>) cb.getData(DataFlavor.javaFileListFlavor);

    	        StringBuffer sb = new StringBuffer();

    	        for (File file : files) {
    	    		sb.append("<div><a href=\"");
    	    		sb.append(file.getPath());
    	    		sb.append("\">");
    	    		sb.append(file.getPath());
    	    		sb.append("</a></div>");
    	        }

    			editor.replaceSelection("");

    			HTMLUtils.insertHTML(sb.toString(), HTML.Tag.DIV, editor);

    	    } catch (UnsupportedFlavorException | IOException  ex) {
    	        LOGGER.warn(ex);
    	    }

    	} else
    	   	editor.paste();
    }
}

/*
 * Created on Feb 28, 2005
 *
 */
package fr.dark40k.util.shef40k.ui.text.actions;

import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JColorChooser;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.StyledEditorKit;

import fr.dark40k.util.shef40k.ui.UIUtils;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;


/**
 * Action which edits HTML font color
 *
 * @author Bob Tantlinger
 *
 */
@SuppressWarnings({"unused"})
public class HTMLFontColorEditAction extends HTMLTextEditAction
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Color COLORS[] = {
        	Color.BLACK,
        	Color.GRAY,
        	Color.RED,
        	Color.BLUE,
        	null
        };

    public static final String COLOR_NAMES[] = {
        	"Black",
        	"Gray",
        	"Red",
        	"Blue",
        	"Custom ..."
        };

    private int index;

    public HTMLFontColorEditAction(int index)
    {
        super(i18n.str("color_"));
        this.index=index;
        putValue(MNEMONIC_KEY, Integer.valueOf(i18n.mnem("color_")));
        this.putValue(SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "format-text-color.png"));
    }

    @Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor)
    {
        Color c = getColorFromUser(editor);
        if(c == null)
            return;

        String prefix = "<font color=" + HTMLUtils.colorToHex(c) + ">";
        String postfix = "</font>";
        String sel = editor.getSelectedText();
        if(sel == null)
        {
            editor.replaceSelection(prefix + postfix);

            int pos = editor.getCaretPosition() - postfix.length();
            if(pos >= 0)
            	editor.setCaretPosition(pos);
        }
        else
        {
            sel = prefix + sel + postfix;
            editor.replaceSelection(sel);
        }
    }

    @Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor)
    {
        Color color = getColorFromUser(editor);
		if(color != null)
		{
		    Action a = new StyledEditorKit.ForegroundAction("Color", color);
		    a.actionPerformed(e);
		}
    }

    private Color getColorFromUser(Component c)
    {
        Window win = SwingUtilities.getWindowAncestor(c);
        if(win != null)
            c = win;
        Color color =
			JColorChooser.showDialog(c, "Color", Color.black);	 //$NON-NLS-1$
		return color;
    }

}

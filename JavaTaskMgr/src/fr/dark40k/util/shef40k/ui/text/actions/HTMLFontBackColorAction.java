/*
 * Created on Feb 28, 2005
 *
 */
package fr.dark40k.util.shef40k.ui.text.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.UIManager;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit.StyledTextAction;
import javax.swing.text.html.HTML;

import fr.dark40k.util.shef40k.ui.UIUtils;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;


/**
 * Action which edits HTML font color
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLFontBackColorAction extends HTMLChangeColorAction
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public HTMLFontBackColorAction()
    {
        super(i18n.str("backcolor_"));
        putValue(MNEMONIC_KEY, Integer.valueOf(i18n.mnem("backcolor_")));
        this.putValue(SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "paintbrush.png"));
        setColor(Color.YELLOW);
    }

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor)
    {
        if(getColor() == null)
            return;

        String prefix = "<span style=\"background-color: " + HTMLUtils.colorToHex(getColor()) + "\">";
        String postfix = "</span>";
        String sel = editor.getSelectedText();
        if(sel == null)
        {
            editor.replaceSelection(prefix + postfix);

            int pos = editor.getCaretPosition() - postfix.length();
            if(pos >= 0)
            	editor.setCaretPosition(pos);
        }
        else
        {
            sel = prefix + sel + postfix;
            editor.replaceSelection(sel);
        }
    }

    @Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor)
    {

        if(getColor() == null)
            return;

	    Action a = new BackgroundAction("BackColor", getColor());
	    a.actionPerformed(e);

    }

    public static class BackgroundAction extends StyledTextAction {

		private static final long serialVersionUID = -7328762330694509207L;

		/**
         * Creates a new ForegroundAction.
         *
         * @param nm the action name
         * @param bg the foreground color
         */
        public BackgroundAction(String nm, Color bg) {
            super(nm);
            this.bg = bg;
        }

		/**
		 * Sets the foreground color.
		 *
		 * @param e the action event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			JEditorPane editor = getEditor(e);
			if (editor != null) {
				Color bg = this.bg;
				if ((e != null) && (e.getSource() == editor)) {
					String s = e.getActionCommand();
					try {
						bg = Color.decode(s);
					} catch (NumberFormatException nfe) {
					}
				}
				if (bg != null) {

					final MutableAttributeSet divAttributes = new SimpleAttributeSet();
					divAttributes.addAttribute(HTML.Attribute.STYLE, "background-color: " + HTMLUtils.colorToHex(bg));
					final MutableAttributeSet tagAttributes = new SimpleAttributeSet();
					tagAttributes.addAttribute(HTML.Tag.SPAN, divAttributes);
					setCharacterAttributes(editor, tagAttributes, false);

					MutableAttributeSet attr = new SimpleAttributeSet();
					StyleConstants.setBackground(attr, bg);
					setCharacterAttributes(editor, attr, false);

					editor.requestFocusInWindow();

				} else {
					UIManager.getLookAndFeel().provideErrorFeedback(editor);
				}
			}
		}

        private Color bg;
    }


}




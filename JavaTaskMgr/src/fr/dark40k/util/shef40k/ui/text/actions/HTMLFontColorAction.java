/*
 * Created on Feb 28, 2005
 *
 */
package fr.dark40k.util.shef40k.ui.text.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.StyledEditorKit;

import fr.dark40k.util.shef40k.ui.UIUtils;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;


/**
 * Action which edits HTML font color
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLFontColorAction extends HTMLChangeColorAction
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public HTMLFontColorAction()
    {
        super(i18n.str("color_"));
        putValue(MNEMONIC_KEY, Integer.valueOf(i18n.mnem("color_")));
        this.putValue(SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "format-text-color.png"));
        setColor(Color.RED);
    }

    @Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor)
    {
        if(getColor() == null)
            return;

        String prefix = "<font color=" + HTMLUtils.colorToHex(getColor()) + ">";
        String postfix = "</font>";
        String sel = editor.getSelectedText();
        if(sel == null)
        {
            editor.replaceSelection(prefix + postfix);

            int pos = editor.getCaretPosition() - postfix.length();
            if(pos >= 0)
            	editor.setCaretPosition(pos);
        }
        else
        {
            sel = prefix + sel + postfix;
            editor.replaceSelection(sel);
        }
    }

    @Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor)
    {
		if(getColor() != null)
		{
		    Action a = new StyledEditorKit.ForegroundAction("Color", getColor());
		    a.actionPerformed(e);
		}
    }

}

/*
 * Created on Jan 14, 2006
 *
 */
package fr.dark40k.util.shef40k.ui.text.actions;

import java.awt.event.ActionEvent;
import java.util.Enumeration;
//import java.util.Hashtable;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;

import org.bushe.swing.action.ShouldBeEnabledDelegate;

import fr.dark40k.util.shef40k.shef.Shef40kExecuteLinks;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;

@SuppressWarnings({"rawtypes"})
public class HTMLExecuteLinkAction extends BasicEditAction
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

	private final Shef40kExecuteLinks execLinks;

    public HTMLExecuteLinkAction(Shef40kExecuteLinks execLinks)
    {
        super(i18n.str("exec_link"));

        this.execLinks = execLinks;

        addShouldBeEnabledDelegate(new ShouldBeEnabledDelegate()
        {
            @Override
			public boolean shouldBeEnabled(Action a)
            {
                return getEditMode() != SOURCE && elementAtCaretPosition(getCurrentEditor()) != null;
            }
        });
    }

	private static Map getLinkAttributes(Element elem)
    {
        String link = HTMLUtils.getElementHTML(elem, true).trim();
        Map attribs = new HashMap();
        if(link.startsWith("<a"))
        {
            link = link.substring(0, link.indexOf('>'));
            link = link.substring(link.indexOf(' '), link.length()).trim();

            attribs = HTMLUtils.tagAttribsToMap(link);
        }

        return attribs;
    }

    /**
     * Computes the (inline or block) element at the focused editor's caret position
     * @return the element, or null of the element cant be retrieved
     */
    private static Element elementAtCaretPosition(JEditorPane ed)
    {
        if(ed == null)
        	return null;

    	HTMLDocument doc = (HTMLDocument)ed.getDocument();

    	// FBA : modifi� pour ne pas prendre la fin de la selection mais le d�but si c'est une selection
        int caret = (ed.getSelectedText()!=null) ? ed.getSelectionStart():ed.getCaretPosition();

        Element elem = doc.getParagraphElement(caret);
        HTMLDocument.BlockElement blockElem = (HTMLDocument.BlockElement)elem;
        return blockElem.positionToElement(caret);
    }

	@Override
	protected void doEdit(ActionEvent e, JEditorPane editor) {
		executeLink(editor);
	}

	public void executeLink(JEditorPane editor) {

        Element elem = elementAtCaretPosition(editor);

        AttributeSet att = elem.getAttributes();

		for (Enumeration ee = att.getAttributeNames(); ee.hasMoreElements();)
			if (ee.nextElement().toString().equals("a")) {

				Map linkData = getLinkAttributes(elem);

				String data = (String) linkData.get("href");

				execLinks.runLink(data);

				return;

			}

	}

}

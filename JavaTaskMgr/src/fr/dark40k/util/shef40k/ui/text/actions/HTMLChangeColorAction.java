package fr.dark40k.util.shef40k.ui.text.actions;

import java.awt.Color;
import java.util.EventListener;
import java.util.EventObject;

import javax.swing.event.EventListenerList;

public abstract class HTMLChangeColorAction extends HTMLTextEditAction {

	private Color activeColor;

	public HTMLChangeColorAction(String name) {
		super(name);
	}


    public Color getColor() {
		return activeColor;
	}

	public void setColor(Color newColor) {
		if (activeColor==newColor) return;

		if ((activeColor!=null)&&(activeColor.equals(newColor))) return;

		this.activeColor = newColor;

		fireColorChangedEvent();

	}

	//
	// Change event
	//

	private EventListenerList listenerList = new EventListenerList();

	public int listenersCount() { return listenerList.getListenerCount(); };

	public interface UpdateColorActionListener extends EventListener {
		public void colorChanged(UpdateColorActionEvent evt);
	}

	public class UpdateColorActionEvent extends EventObject {
		public UpdateColorActionEvent(Object source) {
			super(source);
		}
	}

	public void addColorActionListener(UpdateColorActionListener listener) {
		listenerList.add(UpdateColorActionListener.class, listener);
	}

	public void removeUpdateListener(UpdateColorActionListener listener) {
		listenerList.remove(UpdateColorActionListener.class, listener);
	}

	public void fireColorChangedEvent() {
		UpdateColorActionEvent evt=new UpdateColorActionEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateColorActionListener.class) {
				((UpdateColorActionListener) listeners[i+1]).colorChanged(evt);
			}
		}
	}


}

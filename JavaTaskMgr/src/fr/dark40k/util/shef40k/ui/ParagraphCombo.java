package fr.dark40k.util.shef40k.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;

import org.bushe.swing.action.ActionList;

public class ParagraphCombo extends JComboBox<Action> {

	private final static Font comboFont = new Font("Dialog", Font.PLAIN, 12);

    private ActionListener paragraphComboHandler = new ParagraphComboHandler();

	public ParagraphCombo(ActionList blockActs) {

		super(toArray(blockActs));

        PropertyChangeListener propLst = new PropertyChangeListener()
        {
            @Override
			public void propertyChange(PropertyChangeEvent evt)
            {
                if(evt.getPropertyName().equals("selected"))
                {
                    if(evt.getNewValue().equals(Boolean.TRUE))
                    {
                        removeActionListener(paragraphComboHandler);
                        setSelectedItem(evt.getSource());
                        addActionListener(paragraphComboHandler);
                    }
                }
            }
        };
        for(Iterator<?> it = blockActs.iterator(); it.hasNext();)
        {
            Object o = it.next();
            if(o instanceof DefaultAction)
                ((DefaultAction)o).addPropertyChangeListener(propLst);
        }
        setPreferredSize(new Dimension(120, 22));
        setMinimumSize(new Dimension(120, 22));
        setMaximumSize(new Dimension(120, 22));
        setFont(comboFont);
        setRenderer(new ParagraphComboRenderer());

	}

    /**
     * Converts an action list to an array.
     * Any of the null "separators" or sub ActionLists are ommited from the array.
     * @param lst
     * @return
     */
    private static Action[] toArray(ActionList lst)
    {
        List<Object> acts = new ArrayList<Object>();
        for(Iterator<?> it = lst.iterator(); it.hasNext();)
        {
            Object v = it.next();
            if(v != null && v instanceof Action)
                acts.add(v);
        }

        return acts.toArray(new Action[acts.size()]);
    }

    private class ParagraphComboRenderer extends DefaultListCellRenderer
    {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        @Override
		public Component getListCellRendererComponent(JList<?> list, Object value, int index,
            boolean isSelected, boolean cellHasFocus)
        {
            if(value instanceof Action)
            {
                value = ((Action)value).getValue(Action.NAME);
            }

            return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
    }

    private class ParagraphComboHandler implements ActionListener
    {
        @Override
		public void actionPerformed(ActionEvent e)
        {
            if(e.getSource() == ParagraphCombo.this)
            {
                Action a = (Action)(getSelectedItem());
                a.actionPerformed(e);
            }
        }
    }


}

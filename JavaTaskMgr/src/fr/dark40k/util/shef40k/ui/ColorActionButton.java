package fr.dark40k.util.shef40k.ui;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import fr.dark40k.util.shef40k.ui.text.actions.HTMLChangeColorAction;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLChangeColorAction.UpdateColorActionEvent;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLChangeColorAction.UpdateColorActionListener;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLFontBackColorAction;
import fr.dark40k.util.swing.JColorButton;

public class ColorActionButton extends JColorButton {

	final HTMLChangeColorAction actionColor;

	public ColorActionButton(HTMLChangeColorAction actColor) {

		super(actColor.getColor());

		actionColor=actColor;

		setIcon((ImageIcon) actColor.getValue(HTMLFontBackColorAction.SMALL_ICON));

		// Cas d'un click direct => executer l'action
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				doAction();
			}
		});

		// Cas d'un changement de couleur => changer la couleur de l'action puis executer l'action
		addColorButtonListener(new UpdateColorButtonListener() {
			@Override
			public void colorChanged(UpdateColorButtonEvent evt) {
				actionColor.setColor(getColor());
				doAction();
			}
		});

		// Cas d'un changement de couleur signal� par l'action => changer la couleur du bouton
		actColor.addColorActionListener(new UpdateColorActionListener() {
			@Override
			public void colorChanged(UpdateColorActionEvent evt) {
				setColor(actColor.getColor());
			}
		});


	}

	private void doAction() {
		try {
			ActionEvent act = new ActionEvent(this,ActionEvent.ACTION_PERFORMED, "Anything", System.currentTimeMillis(), 0);
			actionColor.execute(act);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

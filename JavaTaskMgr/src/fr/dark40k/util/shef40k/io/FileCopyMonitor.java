package fr.dark40k.util.shef40k.io;

import java.io.File;




public interface FileCopyMonitor extends CopyMonitor
{
    public void copyingFile(File f);
}

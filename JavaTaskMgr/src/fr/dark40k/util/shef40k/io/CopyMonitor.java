/*
 * Created on Feb 2, 2006
 *
 */
package fr.dark40k.util.shef40k.io;

public interface CopyMonitor
{
    public void bytesCopied(int numBytes);   
    
    public boolean isCopyAborted();    
}

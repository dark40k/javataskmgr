package fr.dark40k.util.properties;

import java.util.EventListener;

public interface PropertyUpdateListener extends EventListener {
	public void propertiesUpdated(PropertyUpdateEvent evt);
}

package fr.dark40k.util.properties;

import javax.swing.event.EventListenerList;

import fr.dark40k.util.xmlimportexport.XMLImportExport;

public abstract class Property implements XMLImportExport, PropertyUpdateListener {

	//
	// Evenement d'objets enfants
	//

	@Override
	public void propertiesUpdated(PropertyUpdateEvent evt) {
		fireUpdateEvent();
	}

	//
	// Change event
	//

	private EventListenerList listenerList = new EventListenerList();

	public void addUpdateListener(PropertyUpdateListener listener) {
		listenerList.add(PropertyUpdateListener.class, listener);
	}

	public void removeUpdateListener(PropertyUpdateListener listener) {
		listenerList.remove(PropertyUpdateListener.class, listener);
	}

	public void fireUpdateEvent() {
		PropertyUpdateEvent evt = new PropertyUpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i + 2) {
			if (listeners[i] == PropertyUpdateListener.class) {
				((PropertyUpdateListener) listeners[i + 1]).propertiesUpdated(evt);
			}
		}
	}

}

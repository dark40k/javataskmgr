package fr.dark40k.util.properties.dialog;

import java.lang.reflect.Field;

import javax.swing.JTextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.properties.Property;

public class EditableFieldString extends EditableField {

	private final static Logger LOGGER = LogManager.getLogger();

	protected JTextField textField = new JTextField();
	
	public EditableFieldString() {
		super();
		textField.setColumns(25);
		textField.setText("Edition");
		addEditorComponent(textField);
	}
	
	public EditableFieldString(Field field, Property property) {
		
		super(field,property);
				
		textField.setText((String)originalValue);
		
		addEditorComponent(textField);
		
	}
	
	@Override
	public boolean hasPendingChanges() {
		
		return !textField.getText().equals(originalValue);
		
	}
	
	@Override
	public boolean applyChanges() {
		
		if (!hasPendingChanges()) return false;
		
		try {
			field.set(property,textField.getText());
			return true;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName() + " => " + e.getMessage());
			throw new RuntimeException("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName(),e);
		}
		
	}

}

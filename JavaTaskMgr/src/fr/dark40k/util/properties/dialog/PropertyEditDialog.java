package fr.dark40k.util.properties.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.Duration;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.properties.Property;

public class PropertyEditDialog extends JDialog {

	private static final String NOSELECTION_TITLE_BORDER = "< pas de propriete selectionee >";

	private final static Logger LOGGER = LogManager.getLogger();

	protected final DefaultTreeModel treeModel = new DefaultTreeModel(null);
	protected final JTree tree = new JTree(treeModel);

	protected TitledBorder selectedNodePanelBorder;

	protected final JPanel contentPanel = new JPanel();
	protected final JSplitPane splitPane = new JSplitPane();
	protected final JPanel panelLeft = new JPanel();
	protected final JPanel selectedNodePanel = new JPanel();
	protected final JPanel selectedNodeContentPanel = new JPanel();
	protected final JPanel panel = new JPanel();
	protected final JButton btnDefaut = new JButton("Defaut");
	protected final JButton btnAppliquer = new JButton("Appliquer");

	private PropertyNode node;

	//
	// Methode d'utilsation de la boite de dialogue
	//

	public static void editProperty(Property property, boolean includeStatic) {

		PropertyEditDialog dialog = new PropertyEditDialog();
		dialog.setModal(true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setProperty(property, includeStatic);
		dialog.setVisible(true);

	}

	//
	// Constructeur
	//

	public PropertyEditDialog() {

		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

		tree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				PropertyNode node = (PropertyNode) tree.getLastSelectedPathComponent();
				setSelectedPropertyNode(node);
			}
		});

		initGUI();
	}

	//
	// Getters / Setters
	//

	public void setProperty(Property property, boolean includeStatic) {
		treeModel.setRoot(new PropertyNode(property, includeStatic));
	}

	public void setSelectedPropertyNode(PropertyNode node) {

		// stocke la nouvelle selection
		this.node=node;

		// nettoyage du panneau
		selectedNodeContentPanel.removeAll();
		if (node==null) {
			selectedNodePanelBorder.setTitle(NOSELECTION_TITLE_BORDER);
			selectedNodeContentPanel.revalidate();
			selectedNodeContentPanel.repaint();
			return;
		}

		// mise � jours du titre
		selectedNodePanelBorder.setTitle(node.toString());

		// mise � jours du contenu
		for (Field field : node.getFields()) {

			if (field.getType()==String.class) {
				selectedNodeContentPanel.add(new EditableFieldString(field, node.getProperty()));
				continue;
			}

			if ((field.getType()==Integer.class)||(field.getType()==Integer.TYPE)) {
				selectedNodeContentPanel.add(new EditableFieldInteger(field, node.getProperty()));
				continue;
			}

			if ((field.getType()==Boolean.class)||(field.getType()==Boolean.TYPE)) {
				selectedNodeContentPanel.add(new EditableFieldBoolean(field, node.getProperty()));
				continue;
			}

			if ((field.getType()==Duration.class)) {
				selectedNodeContentPanel.add(new EditableFieldDuration(field, node.getProperty()));
				continue;
			}

			LOGGER.warn("Type non g�r� "+ field.getType().getName()+" pour le parametre " + field.getName() + " pour la classe " + node.getProperty().getClass().getName());

		}

		// relance l'affichage
		selectedNodePanel.revalidate();
		selectedNodePanel.repaint();

	}

	public void apply() {

		for (Component comp : selectedNodeContentPanel.getComponents())
			((EditableField) comp).applyChanges();

		node.getProperty().fireUpdateEvent();

	}

	public void restore() {

		try {

			Object defaultProperty = node.getProperty().getClass().getDeclaredConstructor().newInstance();

			for (Field field : node.getFields())
				field.set(node.getProperty(), field.get(defaultProperty));

		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			LOGGER.warn("Echec copie parametres par d�faut "+node.getProperty().getClass().getName(),e);
			return;
		}

		node.getProperty().fireUpdateEvent();
		setSelectedPropertyNode(node);
	}

	//
	// Graphisme
	//

	private void initGUI() {
		setTitle("Propri\u00E9t\u00E9s");

		setBounds(100, 100, 638, 496);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setLayout(new BorderLayout(0, 0));
		contentPanel.add(splitPane);

		splitPane.setResizeWeight(0.2);
		splitPane.setLeftComponent(panelLeft);
		splitPane.setRightComponent(selectedNodePanel);

		panelLeft.setLayout(new BorderLayout(0, 0));
		panelLeft.add(tree);

		tree.setRootVisible(false);

		selectedNodePanelBorder = new TitledBorder(UIManager.getBorder("TitledBorder.border"), NOSELECTION_TITLE_BORDER, TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0));

		selectedNodePanel.setLayout(new BorderLayout(0, 0));
		selectedNodePanel.setBorder(selectedNodePanelBorder);
		final FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		selectedNodePanel.add(panel, BorderLayout.SOUTH);
		btnAppliquer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				apply();
			}
		});
		panel.add(btnAppliquer);
		btnDefaut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				restore();
			}
		});
		panel.add(btnDefaut);
		selectedNodePanel.add(selectedNodeContentPanel, BorderLayout.NORTH);

		selectedNodeContentPanel.setLayout(new BoxLayout(selectedNodeContentPanel, BoxLayout.Y_AXIS));
//		selectedNodePanel.add(selectedNodeContentPanel, BorderLayout.NORTH);


		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton closeButton = new JButton("Fermer");
				closeButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				buttonPane.add(closeButton);
			}
		}

	}

}

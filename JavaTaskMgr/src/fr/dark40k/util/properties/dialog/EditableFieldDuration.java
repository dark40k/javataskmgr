package fr.dark40k.util.properties.dialog;

import java.lang.reflect.Field;
import java.time.Duration;

import javax.swing.JTextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.FormattersString;
import fr.dark40k.util.properties.Property;

public class EditableFieldDuration extends EditableField {

	private final static Logger LOGGER = LogManager.getLogger();

	protected JTextField textField = new JTextField();
	
	public EditableFieldDuration(Field field, Property property) {
		
		super(field,property);
			
		textField.setText(FormattersString.format((Duration)originalValue,""));
		addEditorComponent(textField);
		
	}
	
	@Override
	public boolean hasPendingChanges() {
		
		if (getValue()==null) return false;

		return !getValue().equals(originalValue);
		
	}
	
	@Override
	public boolean applyChanges() {
		
		Duration newValue = getValue();
		if (newValue==null) return false;
		
		try {
			field.set(property,newValue);
			return true;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName() + " => " + e.getMessage());
			throw new RuntimeException("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName(),e);
		}
		
	}
	
	private Duration getValue() {
		return FormattersString.parseDuration(textField.getText(),(Duration)null);
	}

}

package fr.dark40k.util.properties.dialog;

import java.lang.reflect.Field;

import javax.swing.JCheckBox;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.properties.Property;

public class EditableFieldBoolean extends EditableField {

	private final static Logger LOGGER = LogManager.getLogger();

	protected JCheckBox checkBox = new JCheckBox();
	
	public EditableFieldBoolean(Field field, Property property) {
		
		super(field,property);
				
		checkBox.setSelected((Boolean)originalValue);
		
		addEditorComponent(checkBox);
		
	}
	
	@Override
	public boolean hasPendingChanges() {
		
		return !checkBox.isSelected()==((Boolean)originalValue).booleanValue();
		
	}
	
	@Override
	public boolean applyChanges() {
		
		if (!hasPendingChanges()) return false;
		
		try {
			field.set(property,checkBox.isSelected());
			return true;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName() + " => " + e.getMessage());
			throw new RuntimeException("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName(),e);
		}
		
	}

}

package fr.dark40k.util.properties.dialog;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.lang.reflect.Field;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.util.FormattersString;
import fr.dark40k.util.properties.Property;
import fr.dark40k.util.properties.PropertyFieldDescription;
import fr.dark40k.util.properties.PropertyFieldHelp;

public abstract class EditableField extends JPanel {

	private final static Logger LOGGER = LogManager.getLogger();

	protected final JLabel descriptionLabel = new JLabel();

	protected final Field field;
	protected final Property property;

	protected final Object originalValue;
	protected JTextField textField;
	protected JTextField textField_1;
	
	public EditableField() {
		this.field = null;
		this.property = null;
		this.originalValue = null;
		descriptionLabel.setText("<champ description>");
		initGUI();
	}

	public EditableField(Field field, Property property) {

		LOGGER.debug("Ajout parametre " + field.getName() + " pour la classe " + property.getClass().getName());
		
		this.field = field;
		this.property = property;

		initGUI();

		descriptionLabel.setText(
					(field.getAnnotation(PropertyFieldDescription.class) != null) 
				? 
					field.getAnnotation(PropertyFieldDescription.class).value() 
				: 
					field.getName()
			);

		setToolTipText(
				(field.getAnnotation(PropertyFieldHelp.class) != null) 
			? 
				FormattersString.textToHTML(field.getAnnotation(PropertyFieldHelp.class).value()) 
			: 
				null
			);
		
		try {
			originalValue = field.get(property);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.warn("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName() + " => " + e.getMessage());
			throw new IllegalArgumentException("Erreur d'acces au parametre " + field.getName() + " pour la classe " + property.getClass().getName(),e);
		}

	}
	
	public abstract boolean hasPendingChanges();

	public abstract boolean applyChanges();

	protected void addEditorComponent(Component comp) {
		GridBagConstraints gbc_editComponent = new GridBagConstraints();
		gbc_editComponent.insets = new Insets(3, 0, 3, 5);
		gbc_editComponent.fill = GridBagConstraints.HORIZONTAL;
		gbc_editComponent.anchor = GridBagConstraints.WEST;
		gbc_editComponent.gridx = 2;
		gbc_editComponent.gridy = 0;
		add(comp, gbc_editComponent);
	}
	
	private void initGUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);

		{
			GridBagConstraints gbc_descriptionLabel = new GridBagConstraints();
			gbc_descriptionLabel.anchor = GridBagConstraints.WEST;
			gbc_descriptionLabel.insets = new Insets(3, 5, 3, 5);
			gbc_descriptionLabel.gridx = 0;
			gbc_descriptionLabel.gridy = 0;
			add(descriptionLabel, gbc_descriptionLabel);
		}

		{
			final JLabel label = new JLabel(":");
			GridBagConstraints gbc_label = new GridBagConstraints();
			gbc_label.insets = new Insets(3, 0, 3, 5);
			gbc_label.anchor = GridBagConstraints.EAST;
			gbc_label.gridx = 1;
			gbc_label.gridy = 0;
			add(label, gbc_label);
		}

	}
	
}

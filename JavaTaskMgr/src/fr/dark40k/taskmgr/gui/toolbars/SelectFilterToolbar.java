package fr.dark40k.taskmgr.gui.toolbars;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.swing.JComboBox;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import org.jdom2.Element;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.taskmgr.database.NodeFilter;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeListener;
import fr.dark40k.util.swing.GUIUtilities;

public class SelectFilterToolbar extends JToolBar implements UpdateNodeListener {

//	private final static Logger LOGGER = LogManager.getLogger();

	public final static String XMLTAG="nodefilter";
	
	protected ArrayList<Integer> filterKeys;
	
	protected JComboBox<String> comboBox;
	
	protected Integer comboBoxSelIndex;

	private TaskMgrDB taskMgrDB;
	
	private TaskMgr taskMgr;
	
	private Integer lastSelectionKey=-1;
	
	// =============================================================================================================
	//
	// Constructeur
	//
	// =============================================================================================================

	public SelectFilterToolbar(TaskMgr taskMgr) {
		this.taskMgr=taskMgr;
		initGUI();
	}
	
	// =============================================================================================================
	//
	// Getters/Setters
	//
	// =============================================================================================================

	public void setDB(TaskMgrDB taskMgrDB) {
		
		this.taskMgrDB=taskMgrDB;

		updateCombo();

		comboBox.setEnabled(true);
	}
	
	public void updateDisplay() {
		updateCombo();
	}

	public void setStartFilter(int startKey) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				comboBox.setSelectedIndex(filterKeys.indexOf(getStartFilterIndex(startKey)) + 1);
			}
		});
	}

	private int getStartFilterIndex(int startKey) {
		if (startKey<0) return lastSelectionKey;
		if (startKey==0) return -1;
		return startKey;
	}
	
	// =============================================================================================================
	//
	// Import / Export XML
	//
	// =============================================================================================================
	
	public void importXML(Element fatherElt) {
		
		Element elt = fatherElt.getChild(XMLTAG);
		if (elt==null) return;
		
		lastSelectionKey = Integer.valueOf(elt.getAttributeValue("key", "-1"));
				
	}

	public Element exportXML() {

		Element elt=new Element(XMLTAG);

		elt.setAttribute("key", lastSelectionKey.toString());

		return elt;
	}

	// =============================================================================================================
	//
	// Interne
	//
	// =============================================================================================================
	
	public void updateCombo() {
		
		taskMgrDB.removeUpdateListener(this);
		comboBox.removeActionListener(comboBox_Listener);
		
		filterKeys=taskMgrDB.getRootFilter().getKeyAndChildrenKeys(false);
		filterKeys.remove(0);
		
		// recalculates combo list
		TreeSet<Integer> hiddenKeys = new TreeSet<Integer>();
		ArrayList<String> newComboList = new ArrayList<String>();
		
		newComboList.add("Afficher Tout");
		for (Integer filterKey : filterKeys) { 
			NodeFilter filterNode = (NodeFilter)taskMgrDB.getNode(filterKey);
			if ((!filterNode.isHidden()) && (filterNode.getFilter()!=null) )
				newComboList.add(filterNode.getStackedName(", "));
			else
				hiddenKeys.add(filterKey);
		}
		filterKeys.removeAll(hiddenKeys);
		
		// update combo
		if (updateComboIfNeeded(newComboList)) {
			comboBox.removeAllItems();
			for (String newComboLine : newComboList)
				comboBox.addItem(newComboLine);
		};
		
		// update combo selection
		if (filterKeys.contains(lastSelectionKey))
			comboBox.setSelectedIndex(filterKeys.indexOf(lastSelectionKey)+1);
		else {
			lastSelectionKey=-1;
			comboBox.setSelectedIndex(0);
		}

		// restarts listeners
		taskMgrDB.addUpdateListener(this);
		comboBox.addActionListener(comboBox_Listener);
		
	}
	
	private boolean updateComboIfNeeded(ArrayList<String> newComboList) {

		if (comboBox.getModel().getSize()!=newComboList.size()) return true;
		
		for (int i=0;i<newComboList.size();i++)
			if (!comboBox.getModel().getElementAt(i).equals(newComboList.get(i)))
				return true;

		return false;
	}
	
	// =============================================================================================================
	//
	// Listener pour combobox
	//
	// =============================================================================================================
	
	private ActionListener comboBox_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			doAction();
		}
	};

	private void doAction() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	comboChangedAction(comboBox.getSelectedIndex());
	        	}
	    };       
	    GUIUtilities.runEDT(doHighlight);
	}	

	private void comboChangedAction(Integer index) {
//    	LOGGER.debug("index="+index);
		if (index==0) {
			lastSelectionKey=-1;
			taskMgr.cmd_UpdateFilter(null);
		} else {
			lastSelectionKey=filterKeys.get(index-1);
			taskMgr.cmd_UpdateFilter((NodeFilter) taskMgrDB.getNode(lastSelectionKey));
//	    	LOGGER.debug("filter="+(NodeFilter) taskMgrDB.getNode(lastSelectionKey));
		}
	}


	// =============================================================================================================
	//
	// Listener pour base de donn�e
	//
	// =============================================================================================================
	
	@Override
	public void updateDataBase(UpdateNodeEvent evt) {
		updateCombo();
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		updateCombo();
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
		updateCombo();
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		updateCombo();
	}

	// =============================================================================================================
	//
	// Interface graphique
	//
	// =============================================================================================================
	
	private void initGUI() {
		comboBox = new JComboBox<String>();
		comboBox.addItem("No database selected");
		comboBox.setEnabled(false);
		add(comboBox);
	}

	
}

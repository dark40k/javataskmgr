package fr.dark40k.taskmgr.gui.toolbars;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import org.jdom2.Element;

import fr.dark40k.taskmgr.TaskMgr;

public class SearchToolbar extends JToolBar {

	private static final String XMLTAG = "searchtoolbar";
	private static final String XMLSUBTAG = "value";

	private static final int maxLinks = 25;
	
	protected JButton btnSearchPrevious;
	protected JButton btnSearchNext;

	private TaskMgr taskMgr;
	private JPanel panel;
	
	private JComboBox<String> comboBox;
	private DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>(new String[] {});

	private String text="";
	
	public SearchToolbar(TaskMgr taskMgr) {
		this.taskMgr=taskMgr;
		initGUI();
		
		
		final JTextComponent tc = (JTextComponent) comboBox.getEditor().getEditorComponent();
		tc.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				text = tc.getText();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				text = tc.getText();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {}
		});
		
	}
	
	private void initGUI() {
		
		btnSearchPrevious = new JButton();
		btnSearchPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (text.isEmpty()) return;
				updateComboTextList();
				taskMgr.cmd_SelectPreviousMatch(text);
			}
		});
		btnSearchPrevious.setIcon(new ImageIcon(SearchToolbar.class.getResource("/fr/dark40k/taskmgr/res/icon32/prevSearch.png")));
		btnSearchPrevious.setFocusable(false);
		add(btnSearchPrevious);
		
		btnSearchNext = new JButton();
		btnSearchNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (text.isEmpty()) return;
				updateComboTextList();
				taskMgr.cmd_SelectNextMatch(text);
			}
		});
		btnSearchNext.setIcon(new ImageIcon(SearchToolbar.class.getResource("/fr/dark40k/taskmgr/res/icon32/nextSearch.png")));
		btnSearchNext.setFocusable(false);
		add(btnSearchNext);
		
		panel = new JPanel();
		add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		comboBox = new JComboBox<String>(model);
		comboBox.setEditable(true);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 0;
		panel.add(comboBox, gbc_comboBox);
	}
	
	private void updateComboTextList() {
		
		String selectedText = text;
		
		int index = model.getIndexOf(selectedText);
		while (index>=0) {
			comboBox.removeItemAt(index);
			index = model.getIndexOf(selectedText);
		}
		
		comboBox.addItem(selectedText);
		comboBox.setSelectedItem(selectedText);
		
		while (model.getSize()>maxLinks) 
			comboBox.removeItemAt(0);

	}
	
	
	public void importXML(Element fatherElt) {
		
		Element elt = fatherElt.getChild(XMLTAG);
		if (elt==null) return;
		
		comboBox.removeAllItems();
		
		for (Element subElt : elt.getChildren(XMLSUBTAG))
			comboBox.addItem(subElt.getText());
		
	}
	
	public Element exportXML() {
		
		Element elt=new Element(XMLTAG);

		for (int index=0; index < model.getSize(); index++) {
			Element subElt = new Element(XMLSUBTAG);
			subElt.setText(model.getElementAt(index));
			elt.addContent(subElt);
		}
				
		return elt;
	}
	
}

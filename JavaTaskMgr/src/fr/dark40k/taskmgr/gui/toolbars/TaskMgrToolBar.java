package fr.dark40k.taskmgr.gui.toolbars;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.ToolTipManager;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.taskmgr.gui.TaskMgrFrame;

public class TaskMgrToolBar extends JToolBar {

	private TaskMgr taskMgr;

	public TaskMgrToolBar(TaskMgr taskMgr) {

		this.taskMgr=taskMgr;


		initGUI();
	}
	private void initGUI() {

		JButton btnSave = new JButton("");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrToolBar.this.taskMgr.cmd_Save();
			}
		});
		btnSave.setFocusable(false);
		btnSave.setToolTipText("Sauvegarder");
		btnSave.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/save.png")));
		add(btnSave);

		add(new JToolBar.Separator());

		JButton btnAddNewChild = new JButton("");
		btnAddNewChild.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrToolBar.this.taskMgr.cmd_NodeCreateNewChild();
			}
		});
		btnAddNewChild.setFocusable(false);
		btnAddNewChild.setToolTipText("Cr\u00E9er noeud fils");
		btnAddNewChild.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/AddChild.png")));
		add(btnAddNewChild);

		JButton btnAddNewBrother = new JButton("");
		btnAddNewBrother.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_NodeCreateNewBrother();
			}
		});
		btnAddNewBrother.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/AddBrother.png")));
		btnAddNewBrother.setToolTipText("Cr\u00E9er noeud frere");
		btnAddNewBrother.setFocusable(false);
		add(btnAddNewBrother);

		JToolBar.Separator tseparator_1 = new JToolBar.Separator();
		add(tseparator_1);

		JButton btnCopyNode = new JButton("");
		btnCopyNode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrToolBar.this.taskMgr.cmd_NodeCopy();
			}
		});

		JButton btnCutNode = new JButton("");
		btnCutNode.setFocusable(false);
		btnCutNode.setToolTipText("Couper noeud");
		btnCutNode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_Cut();
			}
		});
		btnCutNode.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/edit-cut-4.png")));
		add(btnCutNode);
		btnCopyNode.setFocusable(false);
		btnCopyNode.setToolTipText("Copier noeud");
		btnCopyNode.setIcon(new ImageIcon(TaskMgrFrame.class.getResource("/fr/dark40k/taskmgr/res/icon32/page_white_copy.png")));
		add(btnCopyNode);

		JButton btnPasteNode = new JButton("");
		btnPasteNode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_NodePaste();
			}
		});
		btnPasteNode.setFocusable(false);
		btnPasteNode.setToolTipText("Coller noeud");
		btnPasteNode.setIcon(new ImageIcon(TaskMgrFrame.class.getResource("/fr/dark40k/taskmgr/res/icon32/page_white_paste.png")));
		add(btnPasteNode);

		JButton btnPasteLink = new JButton("");
		btnPasteLink.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrToolBar.this.taskMgr.cmd_NodePasteLink();
			}
		});
		btnPasteLink.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/page_white_pastelink.png")));
		btnPasteLink.setToolTipText("Coller lien");
		btnPasteLink.setFocusable(false);
		add(btnPasteLink);

		JToolBar.Separator tseparator_2 = new JToolBar.Separator();
		add(tseparator_2);

		JButton btnDeleteNode = new JButton("");
		btnDeleteNode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_NodeDelete();
			}
		});
		btnDeleteNode.setFocusable(false);
		btnDeleteNode.setToolTipText("Supprimer noeud");
		btnDeleteNode.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/edit-delete-9.png")));
		add(btnDeleteNode);

		JToolBar.Separator tseparator_3 = new JToolBar.Separator();
		add(tseparator_3);

		JButton btnUndo = new JButton("") {
			@Override
			public String getToolTipText(MouseEvent event) {
				return TaskMgrToolBar.this.taskMgr.getUndoMgr().getUndoPresentationName();
			}
		};
		btnUndo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_Undo();
			}
		});
		btnUndo.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/edit-undo-4.png")));
		btnUndo.setFocusable(false);
		ToolTipManager.sharedInstance().registerComponent(btnUndo);
		add(btnUndo);

		JButton btnRedo = new JButton("") {
			@Override
			public String getToolTipText(MouseEvent event) {
				return TaskMgrToolBar.this.taskMgr.getUndoMgr().getRedoPresentationName();
			}
		};
		btnRedo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_Redo();
			}
		});
		btnRedo.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/edit-redo-4.png")));
		btnRedo.setFocusable(false);
		ToolTipManager.sharedInstance().registerComponent(btnRedo);
		add(btnRedo);

		JToolBar.Separator tseparator_4 = new JToolBar.Separator();
		add(tseparator_4);

		JButton btnMoveUp = new JButton("");
		btnMoveUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_MoveUp();
			}
		});
		btnMoveUp.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/page_white_get.png")));
		btnMoveUp.setToolTipText("Deplacer noeud (haut)");
		btnMoveUp.setFocusable(false);
		add(btnMoveUp);

		JButton btnMoveDown = new JButton("");
		btnMoveDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_MoveDown();
			}
		});
		btnMoveDown.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/page_white_put.png")));
		btnMoveDown.setToolTipText("D\u00E9placer noeud (bas)");
		btnMoveDown.setFocusable(false);
		add(btnMoveDown);

		JButton btnSwap = new JButton("");
		btnSwap.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_SwapLink();
			}
		});
		btnSwap.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/Permutter.png")));
		btnSwap.setToolTipText("D\u00E9placer noeud (bas)");
		btnSwap.setFocusable(false);
		add(btnSwap);

		JToolBar.Separator tseparator_5 = new JToolBar.Separator();
		add(tseparator_5);

		JButton btnPdfEditor = new JButton("");
		btnPdfEditor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrToolBar.this.taskMgr.cmd_ShowPDFImportFrame();
			}
		});
		btnPdfEditor.setIcon(new ImageIcon(TaskMgrToolBar.class.getResource("/fr/dark40k/taskmgr/res/icon32/application-pdf.png")));
		btnPdfEditor.setToolTipText("Ouvrir editeur PDF int\u00E9gr\u00E9");
		btnPdfEditor.setFocusable(false);
		add(btnPdfEditor);


	}

}

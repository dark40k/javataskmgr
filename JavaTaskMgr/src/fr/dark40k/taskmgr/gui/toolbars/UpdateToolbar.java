package fr.dark40k.taskmgr.gui.toolbars;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import fr.dark40k.taskmgr.TaskMgr;

public class UpdateToolbar extends JToolBar {

	protected JButton btnUpdate;
	
	private TaskMgr taskMgr;
	
	public UpdateToolbar(TaskMgr taskMgr) {
		this.taskMgr=taskMgr;
		initGUI();
	}
	
	private void initGUI() {
		btnUpdate = new JButton();
		btnUpdate.setIcon(new ImageIcon(UpdateToolbar.class.getResource("/fr/dark40k/taskmgr/res/icon32/update.png")));
		btnUpdate.setFocusable(false);
		btnUpdate.addActionListener(comboBox_Listener);
		add(btnUpdate);
	}
	
	//
	// Suivi de la comboBox
	//
	
	private ActionListener comboBox_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			doAction();
		}
	};

	private void doAction() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgr.cmd_Update();
	        	}
	    };       
	    SwingUtilities.invokeLater(doHighlight);
	}

}

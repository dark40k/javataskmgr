package fr.dark40k.taskmgr.gui.util;

import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Insets;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class MiddleDotRenderer extends DefaultTableCellRenderer {
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		// Determine the width available to render the text

		int availableWidth = table.getColumnModel().getColumn(column).getWidth();
		availableWidth -= table.getIntercellSpacing().getWidth();
		Insets borderInsets = getBorder().getBorderInsets(this);
		availableWidth -= (borderInsets.left + borderInsets.right);
		String cellText = getText();
		FontMetrics fm = getFontMetrics(getFont());

		// Not enough space so start rendering from the end of the string
		// until all the space is used up

		if (fm.stringWidth(cellText) > availableWidth) {

			String dots = "...";
			int dotWidth = fm.stringWidth(dots);

			String startText ="";
			String endText="";

			if (dotWidth < availableWidth) {

				int startWidth = (availableWidth - dotWidth)/3;
				int endWidth = availableWidth - dotWidth - startWidth;

				int i0=0;
				for (;(i0<cellText.length())&&(startWidth>0);i0++)
					startWidth -= fm.charWidth(cellText.charAt(i0));
				startText = cellText.substring(0, i0>1?i0-1:0);

				int i1 = cellText.length() - 1;
				for (; (i1 > 0)&&(endWidth>0); i1--)
					endWidth -= fm.charWidth(cellText.charAt(i1));
				endText = cellText.substring(i1 + 2);

			}

			setText(startText + dots + endText);
		}

		return this;
	}
}

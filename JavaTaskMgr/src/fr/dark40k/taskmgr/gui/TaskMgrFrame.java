package fr.dark40k.taskmgr.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedHashMap;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.gui.node.NodeEditPanel;
import fr.dark40k.taskmgr.gui.node.notes.PasteNodeHTMLLinkAction;
import fr.dark40k.taskmgr.gui.nodetable.NodeTableModel;
import fr.dark40k.taskmgr.gui.nodetable.NodeTableModel.ExpansionState;
import fr.dark40k.taskmgr.gui.nodetable.NodeTablePanel;
import fr.dark40k.taskmgr.gui.nodetable.RowAddress;
import fr.dark40k.taskmgr.gui.popup.NodeTaskPopupMenu;
import fr.dark40k.taskmgr.gui.toolbars.SearchToolbar;
import fr.dark40k.taskmgr.gui.toolbars.SelectFilterToolbar;
import fr.dark40k.taskmgr.gui.toolbars.TaskMgrToolBar;
import fr.dark40k.taskmgr.gui.toolbars.UpdateToolbar;
import fr.dark40k.util.shef40k.shef.Shef40kMenuEnum;
import fr.dark40k.util.swing.GUIUtilities;
import fr.dark40k.util.swing.ScreenSize;

public class TaskMgrFrame extends JFrame {

	private final static Logger LOGGER = LogManager.getLogger();

	public final static String XMLTAG="taskmgrframe";
	public final static String XMLTAG_WINDOW="window";

	private final TaskMgr taskMgr;

	public final NodeEditPanel nodeEditPanel;
	public final NodeTablePanel nodeListPanel;

	private JSplitPane splitPane;
	protected JPanel contentPane;

	protected JCheckBoxMenuItem chckbxmntmAdminMode;
	private JCheckBoxMenuItem chckbxmntmDatesDisplay;
	private JCheckBoxMenuItem chckbxmntmFileLinksDisplay;

	private NodeTaskPopupMenu nodeTaskPopupMenu;
	private SearchToolbar searchToolbar;
	public SelectFilterToolbar filterToolbar;

	private Node rootBaseNode;
	private Node rootTaskNode;

	/**
	 * Create the frame.
	 */
	public TaskMgrFrame(TaskMgr taskMgr) {

		// sauvegarde du taskMgr
		this.taskMgr=taskMgr;

		// initialisation des objets principaux
		nodeListPanel = new NodeTablePanel(taskMgr.getUndoMgr());
		nodeListPanel.setMinimumSize(new Dimension(250, 250));
		nodeEditPanel = new NodeEditPanel(taskMgr.getUndoMgr());

		// initialisation de l'affichage
		initGUI();

		// selection d'un noeud sur click d'un lien cod� sous la forme "node:<NodeKey>"
		nodeEditPanel.nodeEditTitlePanel.notesPanel.noteEditorPane.addHTMLLinkExecution(
				"node",
				(String data)->{
					Integer key = Integer.parseInt(data);
					NodeTableModel model = nodeListPanel.getTable().getModel();

					Node node = model.setExpanded(key,true);
					if (node==null) return;

					int row = model.getRow(node);
					if (row<0) return;

					nodeListPanel.setSelectedRow(row);
				});
		nodeEditPanel.nodeEditTitlePanel.notesPanel.noteEditorPane.addCustomActionToMenuAndToolbar(Shef40kMenuEnum.INSERT, new PasteNodeHTMLLinkAction(), true, false);

		// bouton de fermeture de la fenetre
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_Exit();
			}
		});

		// selection d'une ligne dans la liste des noeuds
		nodeListPanel.addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (!e.getValueIsAdjusting()) {
							nodeEditPanel.setNode(nodeListPanel.getSelectedNode(),chckbxmntmAdminMode.getState());
						}
					}
				}
				);

		chckbxmntmAdminMode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateDisplay();
			}
		});

		// popupMenu de la liste des noeuds
		nodeTaskPopupMenu = new NodeTaskPopupMenu(taskMgr);
		nodeListPanel.addNodeTableMouseListener(nodeTaskPopupMenu.getNodeTableMouseListener());

		// suivi de la resolution
		initResolutionTimer();

	}

	// =============================================================================================================
	//
	// Donn�es d'entr�e
	//
	// =============================================================================================================

	public void setDatabase(TaskMgrDB taskMgrDB) {

		LOGGER.info("Changement de base de donn�e.");

		jolieTimer.stop();

		filterToolbar.setDB(taskMgrDB);

		rootBaseNode = taskMgrDB.getRootBaseNode();
		rootTaskNode = taskMgrDB.getRootTask();

		nodeListPanel.setDisplayedRootNode(chckbxmntmAdminMode.getState() ? rootBaseNode : rootTaskNode);

		jolieTimer.start();

	}

	public void updateDisplay() {

		LOGGER.info("Mise � jours affichage.");

		// sauvegarde la ligne selectionn�e
		RowAddress rowAddress =  nodeListPanel.getSelectedRowAddress();

		// sauvegarde le depliement
		ExpansionState expState = nodeListPanel.getExpansionState();

		// force la mise � jours du filtre d'affichage
		filterToolbar.updateDisplay();

		// relance l'affichage de la liste
		nodeListPanel.setDisplayedRootNode(chckbxmntmAdminMode.getState() ? rootBaseNode : rootTaskNode);

		// restore le depliement
		nodeListPanel.setExpansionState(expState);

		// restore la ligne selectionnee
		if (rowAddress!=null) nodeListPanel.setSelectedRowAddress(rowAddress);

	}

	public boolean isAdminMode() {
		return chckbxmntmAdminMode.getState();
	}

	// =============================================================================================================
	//
	// Import / Export XML
	//
	// =============================================================================================================

	public void importXML(Element baseElt) {

		Element elt = baseElt.getChild(XMLTAG);
		if (elt==null) return;

		chckbxmntmAdminMode.setSelected(Boolean.TRUE.toString().equals(elt.getAttributeValue("admin", Boolean.FALSE.toString())));

		importXMLWindowPositions(elt);

		nodeListPanel.importXML(elt);

		nodeEditPanel.importXML(elt);

		searchToolbar.importXML(elt);

		filterToolbar.importXML(elt);

		chckbxmntmDatesDisplay.setSelected(nodeEditPanel.nodeEditTitlePanel.isDatesPanelVisible());
		chckbxmntmFileLinksDisplay.setSelected(nodeEditPanel.nodeEditTitlePanel.isNodeLinksPanelVisible());

//		updateDisplay();

	}

	public Element exportXML() {

		Element elt=new Element(XMLTAG);

		elt.setAttribute("admin", Boolean.toString(chckbxmntmAdminMode.isSelected()));

        elt.addContent(exportXMLWindowPositions());

        elt.addContent(nodeListPanel.exportXML());

        elt.addContent(nodeEditPanel.exportXML());

        elt.addContent(searchToolbar.exportXML());

        elt.addContent(filterToolbar.exportXML());

		return elt;
	}

	private int parseInt(String val, int def) {
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException e) {
			return def;
		}
	}

	// =============================================================================================================
	//
	// Divers
	//
	// =============================================================================================================

	/**
	 * Workaround for Maximize resize bug
	 * http://stackoverflow.com/questions/18724034/program-not-repainting-upon-jframe-height-maximizing
	 */
	@Override
	public void paint(Graphics g)
	{
	    checkResizeWindow(this);
	    super.paint(g);
	}

	public static void checkResizeWindow(Window window)
	{
	    if (!window.isShowing())
	    {
	        return;
	    }
	    Component[] components = window.getComponents();
	    if (components.length == 0)
	    {
	        return;
	    }
	    Component comp = components[0];
	    Insets insets = window.getInsets();
	    Dimension innerSize = window.getSize();
	    innerSize.width -= insets.left + insets.right;
	    innerSize.height -= insets.top + insets.bottom;
	    if (!innerSize.equals(comp.getSize()))
	    {
	        window.invalidate();
	        window.validate();
	    }
	}

	// =============================================================================================================
	//
	// Persistance des informations
	//
	// =============================================================================================================

	private Timer jolieTimer;
	private WindowPersistance currentDisplayConfig;
	private LinkedHashMap<String,WindowPersistance> windowPersistanceList = new LinkedHashMap<>();

	private void initResolutionTimer() {

		int delay = 1000; // milliseconds
		ActionListener taskPerformer = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				checkResolutionChange();
			}
		};
		setVisible(true);

		jolieTimer = new Timer(delay, taskPerformer);

	}

	private void checkResolutionChange() {

		String newResolution = calcElementName();

		LOGGER.trace("resolution ="+newResolution);

		if ((currentDisplayConfig==null) || (! newResolution.equals(currentDisplayConfig.storageElement.getName()))) {

			restoreDisplay();

		} else {

			currentDisplayConfig.saveSettings();

		}

	}

	private Element exportXMLWindowPositions() {

		saveDisplay();

		Element elt = new Element(XMLTAG_WINDOW);

		for (WindowPersistance persist : windowPersistanceList.values())
			elt.addContent(persist.exportXML());

		return elt;

	}

	public void importXMLWindowPositions(Element baseElt) {

		windowPersistanceList.clear();

		Element elt = baseElt.getChild(XMLTAG_WINDOW);
		if (elt==null) return;

		for (Element subElt : elt.getChildren())
			windowPersistanceList.put(subElt.getName(), new WindowPersistance(subElt));

		restoreDisplay();

	}

	public void saveDisplay() {

		String configName = calcElementName();
		currentDisplayConfig = windowPersistanceList.get(configName);

		if (currentDisplayConfig==null) {
			currentDisplayConfig = new WindowPersistance();
			windowPersistanceList.put(currentDisplayConfig.storageElement.getName(),currentDisplayConfig);
		}
		else
			currentDisplayConfig.saveSettings();

	}

	public void restoreDisplay() {

		String configName = calcElementName();

		LOGGER.debug("restore : "+configName);

		currentDisplayConfig = windowPersistanceList.get(configName);

		if (currentDisplayConfig == null) {
			LOGGER.info("No config found. Creating new one for : "+configName);
			saveDisplay();
		} else {
			GUIUtilities.runEDT(new Runnable() {
				@Override
				public void run() {
					currentDisplayConfig.applySettings();
				}
			});
		}

	}

	public String calcElementName() {
		 Rectangle virtualBounds=ScreenSize.getScreenBonds();
		 return XMLTAG_WINDOW+virtualBounds.width+"x"+virtualBounds.height;
	}

	private class WindowPersistance {

		private Element storageElement;

		public WindowPersistance() {
			saveSettings();
		}

		public WindowPersistance(Element elt) {
			storageElement = elt.clone();
		}

		public Element exportXML() {
			return storageElement.clone();
		}

		public void saveSettings() {

			storageElement = new Element(calcElementName());

			Rectangle virtualBounds=ScreenSize.getScreenBonds();
			storageElement.setAttribute("screen_height", Integer.toString(virtualBounds.height));
			storageElement.setAttribute("screen_width", Integer.toString(virtualBounds.width));

			storageElement.setAttribute("win_x", Integer.toString(getX()));
			storageElement.setAttribute("win_y", Integer.toString(getY()));
			storageElement.setAttribute("win_height", Integer.toString(getHeight()));
			storageElement.setAttribute("win_width", Integer.toString(getWidth()));

			storageElement.setAttribute("divider", Integer.toString(splitPane.getDividerLocation()));

		}

		public void applySettings() {

			LOGGER.info("start = "+storageElement.getName());

			String configName = calcElementName();
			if (!configName.equals(storageElement.getName())) {
				LOGGER.warn("Error: current resolution is = "+configName);
				return;
			}

			setBounds(
					parseInt(storageElement.getAttributeValue("win_x"), getX()),
					parseInt(storageElement.getAttributeValue("win_y"), getY()),
					parseInt(storageElement.getAttributeValue("win_width"), getWidth()),
					parseInt(storageElement.getAttributeValue("win_height"), getHeight()) );

			splitPane.setDividerLocation(parseInt(storageElement.getAttributeValue("divider"), splitPane.getDividerLocation()));

			revalidate();
			repaint();

		}


	}

	// =============================================================================================================
	//
	// Initialisation interface graphique
	//
	// =============================================================================================================

	private void initGUI() {

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 1058, 694);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		splitPane = new JSplitPane();
		contentPane.add(splitPane);

		splitPane.setLeftComponent(nodeListPanel);

		JPanel panel = new JPanel();
		splitPane.setRightComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));

		panel.add(nodeEditPanel);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{862, 250, 170, 0, 0};
		gbl_panel_1.rowHeights = new int[]{21, 41, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);

		JMenuBar menuBar = new JMenuBar();
		GridBagConstraints gbc_menuBar = new GridBagConstraints();
		gbc_menuBar.fill = GridBagConstraints.BOTH;
		gbc_menuBar.insets = new Insets(0, 0, 5, 0);
		gbc_menuBar.gridwidth = 4;
		gbc_menuBar.gridx = 0;
		gbc_menuBar.gridy = 0;
		panel_1.add(menuBar, gbc_menuBar);

		JMenu mnNewMenu = new JMenu("Fichier");
		menuBar.add(mnNewMenu);

		JMenuItem menuItem = new JMenuItem("Nouveau");
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_NewBase();
			}
		});
		mnNewMenu.add(menuItem);

		JMenuItem menuItem_1 = new JMenuItem("Ouvrir");
		menuItem_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_OpenDataBase();
			}
		});
		mnNewMenu.add(menuItem_1);

		JSeparator separator = new JSeparator();
		mnNewMenu.add(separator);

		JMenuItem menuItem_2 = new JMenuItem("Sauvegarder");
		menuItem_2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		menuItem_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_Save();
			}
		});
		mnNewMenu.add(menuItem_2);

		JMenuItem menuItem_3 = new JMenuItem("Sauvegarder sous ...");
		menuItem_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_SaveAs();
			}
		});
		mnNewMenu.add(menuItem_3);

		JSeparator separator_1 = new JSeparator();
		mnNewMenu.add(separator_1);

		JMenuItem menuItem_4 = new JMenuItem("Quitter");
		menuItem_4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_Exit();
			}
		});
		mnNewMenu.add(menuItem_4);

		JMenu mnEditer = new JMenu("Editer");
		menuBar.add(mnEditer);

		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Undo");
		mntmNewMenuItem_5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_Undo();
			}
		});
		mnEditer.add(mntmNewMenuItem_5);

		JMenuItem menuItem_5 = new JMenuItem("Redo");
		menuItem_5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_DOWN_MASK));
		menuItem_5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_Redo();
			}
		});
		mnEditer.add(menuItem_5);

		JSeparator separator_4 = new JSeparator();
		mnEditer.add(separator_4);

		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Couper");
		mntmNewMenuItem_4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_Cut();
			}
		});
		mnEditer.add(mntmNewMenuItem_4);

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Copier");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_NodeCopy();
			}
		});
		mntmNewMenuItem_1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
		mnEditer.add(mntmNewMenuItem_1);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Coller");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_NodePaste();
			}
		});
		mntmNewMenuItem_2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK));
		mnEditer.add(mntmNewMenuItem_2);

		JSeparator separator_3 = new JSeparator();
		mnEditer.add(separator_3);

		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Supprimer noeud");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_NodeDelete();
			}
		});
		mntmNewMenuItem_3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		mnEditer.add(mntmNewMenuItem_3);

		JMenu mnNodes = new JMenu("Noeuds");
		menuBar.add(mnNodes);

		chckbxmntmAdminMode = new JCheckBoxMenuItem("Mode administrateur");
		chckbxmntmAdminMode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_Update();
			}
		});


		JMenuItem mntmExpandNode = new JMenuItem("Developper Noeud");
		mntmExpandNode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_ExpandNode();
			}
		});

		JMenuItem mntmNextLink = new JMenuItem("Lien suivant");
		mntmNextLink.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0));
		mntmNextLink.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_SelectNextLinkNode();
			}
		});

		JMenuItem mntmLienPrecedent = new JMenuItem("Lien precedent");
		mntmLienPrecedent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_SelectPreviousLinkNode();
			}
		});

		JMenuItem mntmNewChild = new JMenuItem("Cr\u00E9er noeud fils");
		mnNodes.add(mntmNewChild);
		mntmNewChild.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TaskMgrFrame.this.taskMgr.cmd_NodeCreateNewChild();
			}
		});

		JMenuItem mntmNewBrother = new JMenuItem("Cr\u00E9er noeud fils");
		mnNodes.add(mntmNewBrother);
		mntmNewBrother.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_NodeCreateNewBrother();
			}
		});

		JSeparator separator_2 = new JSeparator();
		mnNodes.add(separator_2);
		mntmLienPrecedent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0));
		mnNodes.add(mntmLienPrecedent);
		mnNodes.add(mntmNextLink);

		JSeparator separator_6 = new JSeparator();
		mnNodes.add(separator_6);
		mnNodes.add(mntmExpandNode);

		JMenuItem mntmCollapseNode = new JMenuItem("R\u00E9duire Noeud");
		mntmCollapseNode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_CollapseNode();
			}
		});
		mnNodes.add(mntmCollapseNode);

		JSeparator separator_5 = new JSeparator();
		mnNodes.add(separator_5);

		JMenuItem mntmExpandAll = new JMenuItem("Developper Tout");
		mntmExpandAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_ExpandAllFromRoot();
			}
		});
		mnNodes.add(mntmExpandAll);

		JMenuItem mntmCollapseAll = new JMenuItem("R\u00E9duire Tout");
		mntmCollapseAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_CollapseAllFromRoot();
			}
		});
		mnNodes.add(mntmCollapseAll);

		JMenu mnAffichage = new JMenu("Affichage");
		menuBar.add(mnAffichage);

		JMenuItem mntmVisualiserPdf = new JMenuItem("Afficher Editeur PDF");
		mntmVisualiserPdf.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_ShowPDFImportFrame();
			}
		});

		chckbxmntmDatesDisplay = new JCheckBoxMenuItem("Afficher Bloc Dates");
		chckbxmntmDatesDisplay.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeEditPanel.nodeEditTitlePanel.setDatesPanelVisible(chckbxmntmDatesDisplay.isSelected());;
			}
		});
		mnAffichage.add(chckbxmntmDatesDisplay);

		chckbxmntmFileLinksDisplay = new JCheckBoxMenuItem("Afficher Bloc Liens");
		chckbxmntmFileLinksDisplay.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeEditPanel.nodeEditTitlePanel.setNodeLinksPanelVisible(chckbxmntmFileLinksDisplay.isSelected());;
			}
		});
		mnAffichage.add(chckbxmntmFileLinksDisplay);

		JSeparator separator_8 = new JSeparator();
		mnAffichage.add(separator_8);
		mnAffichage.add(mntmVisualiserPdf);

		JSeparator separator_7 = new JSeparator();
		mnAffichage.add(separator_7);
		mnAffichage.add(chckbxmntmAdminMode);

		JMenuItem mntmProperties = new JMenuItem("Preferences");
		mntmProperties.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_Properties();
			}
		});
		mnAffichage.add(mntmProperties);

		JMenuItem mntmConsole = new JMenuItem("Console");
		mntmConsole.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TaskMgrFrame.this.taskMgr.cmd_Console();
			}
		});
		mnAffichage.add(mntmConsole);

		TaskMgrToolBar toolBar = new TaskMgrToolBar(taskMgr);
		toolBar.setFocusable(false);
		toolBar.setFloatable(false);
		GridBagConstraints gbc_toolBar = new GridBagConstraints();
		gbc_toolBar.fill = GridBagConstraints.BOTH;
		gbc_toolBar.insets = new Insets(0, 0, 0, 5);
		gbc_toolBar.gridx = 0;
		gbc_toolBar.gridy = 1;
		panel_1.add(toolBar, gbc_toolBar);

		searchToolbar = new SearchToolbar(taskMgr);
		searchToolbar.setFloatable(false);
		GridBagConstraints gbc_searchToolbar = new GridBagConstraints();
		gbc_searchToolbar.fill = GridBagConstraints.HORIZONTAL;
		gbc_searchToolbar.insets = new Insets(0, 0, 0, 5);
		gbc_searchToolbar.gridx = 1;
		gbc_searchToolbar.gridy = 1;
		panel_1.add(searchToolbar, gbc_searchToolbar);

		filterToolbar = new SelectFilterToolbar(taskMgr);
		filterToolbar.setFocusable(false);
		filterToolbar.setFloatable(false);
		GridBagConstraints gbc_filterBar = new GridBagConstraints();
		gbc_filterBar.insets = new Insets(0, 0, 0, 5);
		gbc_filterBar.fill = GridBagConstraints.BOTH;
		gbc_filterBar.gridx = 2;
		gbc_filterBar.gridy = 1;
		panel_1.add(filterToolbar, gbc_filterBar);

		UpdateToolbar updateToolbar = new UpdateToolbar(taskMgr);
		updateToolbar.setFocusable(false);
		updateToolbar.setFloatable(false);
		GridBagConstraints gbc_updateToolbar = new GridBagConstraints();
		gbc_updateToolbar.gridx = 3;
		gbc_updateToolbar.gridy = 1;
		panel_1.add(updateToolbar, gbc_updateToolbar);

	}

}

package fr.dark40k.taskmgr.gui.node.notes;

import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.text.html.HTML;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLTextEditAction;

/**
 * Action which pastes a link to an outlook email
 *
 */
public class PasteNodeHTMLLinkAction extends HTMLTextEditAction {

	private static final long serialVersionUID = 1L;

	public PasteNodeHTMLLinkAction()
	    {
	        super("Inserer lien vers tache copi�e");

	        putValue(SMALL_ICON, new ImageIcon(PasteNodeHTMLLinkAction.class.getResource("/fr/dark40k/taskmgr/res/icon16/clipboard-paste-link.png")));
	        putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
	    }

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		String data = getHTMLLinksFromClipboard();
		if (data == null) return;

		editor.requestFocusInWindow();
		editor.replaceSelection(data);
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		String data = getHTMLLinksFromClipboard();
		if (data == null) return;

		editor.replaceSelection("");
		HTMLUtils.insertHTML(data, HTML.Tag.DIV, editor);
	}

	private String getHTMLLinksFromClipboard() {

		if (TaskMgr.copyNode==null) return null;

		int key = TaskMgr.copyNode.getNodeKey();
		if (key == TaskMgrDB.NO_NODE_KEY) return null;

		StringBuffer sb = new StringBuffer();

		sb.append("<div><a href=\"node:");
		sb.append(key);
		sb.append("\">Tache&rArr;");
		sb.append(TaskMgr.copyNode.getNodeName());
		sb.append("</a></div>");

		return sb.toString();

	}

}

package fr.dark40k.taskmgr.gui.node.notes;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.database.notes.Notes;
import fr.dark40k.taskmgr.database.notes.Notes.Note;
import fr.dark40k.taskmgr.util.OutlookConnector;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.shef40k.shef.Shef40kMenuEnum;
import fr.dark40k.util.shef40k.shef.Shef40kEditor;
import fr.dark40k.util.shef40k.shef.Shef40kEditor.UpdateHTMLTextEvent;
import fr.dark40k.util.shef40k.shef.Shef40kEditor.UpdateHTMLTextListener;
import fr.dark40k.util.swing.layout.WrapLayout;

public class NotesPanel extends JPanel implements TaskMgrDB.UpdateNodeListener {

	@SuppressWarnings("unused")
	private final static Logger LOGGER = LogManager.getLogger();

	private final static SimpleDateFormat NAME_DEFAULT_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

	private final TaskMgrUndoMgr taskMgrUndoMgr;

	private Node node;

	protected final JButton addNoteBtn = new JButton("+");
	protected final JPanel notesTabBar = new JPanel();
	public final Shef40kEditor noteEditorPane = new Shef40kEditor();

	public NotesPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		this.taskMgrUndoMgr = taskMgrUndoMgr;

		// initialisation de l'interface graphique
		initGUI();

		// ajout des liens specifiques
		noteEditorPane.addHTMLLinkExecution("outlook",(String data)->{OutlookConnector.openMail(data);});
		noteEditorPane.addCustomActionToMenuAndToolbar(Shef40kMenuEnum.INSERT, new PasteOutlookEmailHTMLLinkAction(), true, false);

    	// ajout du listener sur le bouton creation de notes
		addNoteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addNewNote(false);
			}
		});

	}

	//
	// Selection du noeud en cours
	//

	public void setNode(Node newNode) {

		// gere les modifications du noeud en direct => pas besoin de modifier si noeud inchang�
		if (newNode==node) return;

		// supprime les listeners
		if (node!=null) {
			noteEditorPane.removeUpdateHTMLTextListener(notesEditorPaneListener);
			node.getDataBase().removeUpdateListener(this);
		}

		// met a jours le noeud actuel et reinitialise la note selectionnee
		node=newNode;

		// reactive l'affichage
		updateDisplay();

		// stoppe la mise � jours si pas de nouveau noeud
		if (newNode==null) return;

		// reactive les listeners
		newNode.getDataBase().addUpdateListener(this);

	}

	//
	// Interface
	//

	protected void addNewNote(boolean dialog) {

		if (node==null) return;

		// calcule le nom par d�faut
		String newName = NAME_DEFAULT_DATE_FORMAT.format(new Date());
		if (node.getNotes().getFirstNote(newName)!=null) {
			String baseName = newName;
			int index=1;
			do {
				newName=baseName+" ("+Integer.toString(index++)+")";
			} while (node.getNotes().getFirstNote(newName)!=null);
		}

		// dialog pour creation de la note
		if (dialog) newName = (String) JOptionPane.showInputDialog(notesTabBar, "Nom de la note", "Creer note", JOptionPane.PLAIN_MESSAGE, null, null, newName);

		// cas ou cancel a ete utilis�
		if (newName == null) return;

		// cree la nouvelle note
		createNewNote(newName);

	}

	protected void editNoteName(NoteTab noteTab) {

		Note note = noteTab.getNote();

		// dialog pour l'edition de la note
		String s = (String) JOptionPane.showInputDialog(noteTab, "Nouveau nom de la note", "Editer note", JOptionPane.PLAIN_MESSAGE, null, null, note.getName());

		// cas ou cancel / pas de changement a ete utilis�
		if (s == null) return;
		if (s.equals(note.getName())) return;

		// modifie le nom
		changeName(noteTab,s);

	}

	//
	// Actions
	//

	protected void createNewNote(String newName) {

		// desactive le suivi des noeuds
		node.getDataBase().removeUpdateListener(this);

		// cree la nouvelle note et la selectionne
		taskMgrUndoMgr.addUndoableNodeEdit(node, "Ajout nouvelle note",false);
		Note newNote = node.getNotes().addNewNote(0,newName);
		newNote.setMain();

		// reactive le suivi des noeuds
		node.getDataBase().addUpdateListener(this);

		// update de l'affichage
		updateDisplay();

	}

	protected void changeName(NoteTab noteTab, String newName) {

		// desactive le suivi des noeuds
		node.getDataBase().removeUpdateListener(this);

		// modifie la nouvelle note et la selectionne
		taskMgrUndoMgr.addUndoableNodeEdit(node, "Edition titre note",false);
		noteTab.getNote().setName(newName);

		// reactive le suivi des noeuds
		node.getDataBase().addUpdateListener(this);

		// update de l'affichage
		noteTab.setText(newName);

	}

	protected void deleteNote(NoteTab noteTab) {

		// desactive le suivi des noeuds
		node.getDataBase().removeUpdateListener(this);

		// modifie la nouvelle note et la selectionne
		taskMgrUndoMgr.addUndoableNodeEdit(node, "Supprime note",false);
		noteTab.getNote().remove();

		// reactive le suivi des noeuds
		node.getDataBase().addUpdateListener(this);

		// update de l'affichage
		updateDisplay();

	}

	protected void moveNote(NoteTab noteTab, int newIndex) {

		// desactive le suivi des noeuds
		node.getDataBase().removeUpdateListener(this);

		// modifie la nouvelle note et la selectionne
		taskMgrUndoMgr.addUndoableNodeEdit(node, "Deplace note",false);
		noteTab.getNote().move(newIndex);

		// reactive le suivi des noeuds
		node.getDataBase().addUpdateListener(this);

		// update de l'affichage
		//updateDisplay();

	}

	//
	// Utilitaires
	//

	protected void updateDisplay() {

		// desactive le suivi de l'editeur de texte
		noteEditorPane.removeUpdateHTMLTextListener(notesEditorPaneListener);

		// supprime les tabs existants
		while (notesTabBar.getComponentCount()>0) notesTabBar.remove(0);

		// termine s'il n'y a pas de noeud selectionne
		if (node==null) return;

		// recree les tabs et reinitialise l'editeur de texte avec la mainNote
		for (Note note : node.getNotes()) {

			boolean isMain = note.isMain();

			addNewTab(note, isMain);

			// reactualise l'affichage de l'editeur de texte
			if (isMain)
				noteEditorPane.setEditorText(note.getContent(),true);

		}
		notesTabBar.add(addNoteBtn);
		notesTabBar.revalidate();

		// reactive le suivi de l'editeur de texte
		noteEditorPane.addUpdateHTMLTextListener(notesEditorPaneListener);

	}

	protected NoteTab addNewTab(Note note, boolean selected) {

		NoteTab newNoteTab = new NoteTab(this, note, selected );

		newNoteTab.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (note.isMain()) return;
				note.setMain();
				updateDisplay();
			}
		});

		notesTabBar.add(newNoteTab);

		return newNoteTab;
	}

	//
	// Description change event handling
	//

	private UpdateHTMLTextListener notesEditorPaneListener = new UpdateHTMLTextListener() {
		@Override
		public void updateHTMLText(UpdateHTMLTextEvent evt) {

        	String newNoteContent=noteEditorPane.getEditorText();

        	if (newNoteContent.replaceAll("\\<[^>]*>","").length()==0)
        		newNoteContent=Notes.EMPTY_NOTE_CONTENT;

			if (newNoteContent.equals(node.getNotes().getMainNote().getContent())) return;

        	node.getDataBase().removeUpdateListener(NotesPanel.this);

        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Edition note",true);

        	node.getNotes().getMainNote().setContent(newNoteContent);

        	node.getDataBase().addUpdateListener(NotesPanel.this);
		}
	};

	//
	// Node change handling
	//

	@Override
	public void updateDataBase(UpdateNodeEvent evt) {
		updateDisplay();
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) updateDisplay();
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) updateDisplay();
	}

	//
	// GUI
	//

	private void initGUI() {

		setLayout(new BorderLayout(0, 0));

//		noteEditorPane.getHTMLEditorPane().getActiveEditor().setOpaque(false);
//		noteEditorPane.getHTMLEditorPane().getActiveEditor().setText("<html>\r\n  <head>\r\n\r\n  </head>\r\n  <body>\r\n    <div>\r\n      \r\n    </div>\r\n  </body>\r\n</html>\r\n");

		// stoppe la propagation des evenements clavier (supr, Ctrl-C, ...)
		noteEditorPane.addKeyListener(new KeyListener() {
			@Override public void keyTyped(KeyEvent e) { e.consume();	}
			@Override public void keyPressed(KeyEvent e) { e.consume(); }
			@Override public void keyReleased(KeyEvent e) { e.consume(); }
		});

		add(noteEditorPane, BorderLayout.CENTER);

		add(notesTabBar, BorderLayout.NORTH);
		WrapLayout wl_notesTabBar = new WrapLayout();
		wl_notesTabBar.setAlignment(FlowLayout.LEFT);
		notesTabBar.setLayout(wl_notesTabBar);

	}


}

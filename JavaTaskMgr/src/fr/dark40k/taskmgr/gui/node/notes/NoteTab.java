package fr.dark40k.taskmgr.gui.node.notes;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.SoftBevelBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.taskmgr.database.notes.Notes.Note;
import fr.dark40k.util.swing.GUIUtilities;

public class NoteTab extends JButton {

	@SuppressWarnings("unused")
	private final static Logger LOGGER = LogManager.getLogger();

	private final static Font FONT = new Font("Tahoma", Font.PLAIN, 13);
	private final static Border BORDER = new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null);

	private final NotesPanel panel;
	private final Note note;

	private boolean selected;

	private final NotePopupMenu popup;

	public NoteTab(NotesPanel panel, Note note, boolean selected) {

		this.note = note;
		this.panel = panel;

		popup = new NotePopupMenu(panel, this);

		setText(note.getName());

		setFont(FONT);
		setBorder(BORDER);
		setContentAreaFilled(false);
		setOpaque(true);

		setSelected(selected);

		addMouseListener(new MouseClicksListener());

		MouseDNDNoteTabListener mouseListener = new MouseDNDNoteTabListener();
		addMouseListener(mouseListener);
		addMouseMotionListener(mouseListener);

	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
		update();
	}

	public void update() {
		GUIUtilities.runEDT(new Runnable() {
			@Override
			public void run() {
				if (selected) {
					setBackground(Color.WHITE);
					setForeground(Color.BLACK);
				} else {
					setBackground(Color.BLACK);
					setForeground(Color.WHITE);
				}
			}
		});
	}

	public Note getNote() {
		return note;
	}

	//
	// Reorder DND
	//
	class MouseDNDNoteTabListener extends MouseAdapter {

		Point startPoint = null;
		int startIndex = -1;
		int newIndex = -1;

		@Override
		public void mousePressed(MouseEvent e) {
			startPoint = e.getPoint();
			startIndex = getComponentIndex(NoteTab.this);
			newIndex = startIndex;
		}

		@Override
		public void mouseDragged(MouseEvent e) {

			Container parent = getParent();

			Point curPos = new Point(e.getPoint().x + NoteTab.this.getX(), e.getPoint().y + NoteTab.this.getY());

			Component c = getParent().getComponentAt(curPos);

			if (!(c instanceof NoteTab)) return;

			if (c==NoteTab.this) return;

			newIndex = getComponentIndex(c);

			parent.remove(NoteTab.this);

			parent.add(NoteTab.this, newIndex);

			parent.revalidate();

		}

		@Override
		public void mouseReleased(MouseEvent e) {

			if (startIndex==newIndex) return;

			panel.moveNote(NoteTab.this,newIndex);

		}

	}

	public static final int getComponentIndex(Component component) {
		if (component != null && component.getParent() != null) {
			Container c = component.getParent();
			for (int i = 0; i < c.getComponentCount(); i++) {
				if (c.getComponent(i) == component)
					return i;
			}
		}

		return -1;
	}

	//
	// Popup menu
	//

	class MouseClicksListener extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			checkPopup(e);
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				panel.editNoteName(NoteTab.this);
				return;
			}
			checkPopup(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			checkPopup(e);
		}

		private void checkPopup(MouseEvent e) {
			if (e.isPopupTrigger()) {
				popup.show(NoteTab.this, e.getX(), e.getY());
			}
		}
	}
}

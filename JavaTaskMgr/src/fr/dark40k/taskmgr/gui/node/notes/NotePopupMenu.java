package fr.dark40k.taskmgr.gui.node.notes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

public class NotePopupMenu extends JPopupMenu {

	protected final JSeparator separator = new JSeparator();
	protected final JMenuItem mntmNewNote = new JMenuItem("Nouvelle Note");
	protected final JMenuItem mntmRenommer = new JMenuItem("Renommer");
	protected final JMenuItem mntmSupprimer = new JMenuItem("Supprimer");

	private final NotesPanel panel;
	private final NoteTab noteTab;

	public NotePopupMenu(NotesPanel panel, NoteTab noteTab) {

		this.panel=panel;
		this.noteTab=noteTab;

		initGUI();

	}

	private void initGUI() {
		{
			mntmNewNote.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					panel.addNewNote(true);
				}
			});
			add(mntmNewNote);
		}
		{
			mntmRenommer.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					panel.editNoteName(noteTab);

				}
			});
			add(mntmRenommer);
		}
		{
			mntmSupprimer.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					panel.deleteNote(noteTab);
				}
			});
			add(mntmSupprimer);
		}
	}

}

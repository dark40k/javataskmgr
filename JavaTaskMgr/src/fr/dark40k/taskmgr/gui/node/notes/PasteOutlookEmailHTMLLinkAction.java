/*
 * Created on Feb 26, 2005
 *
 */
package fr.dark40k.taskmgr.gui.node.notes;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.html.HTML;

import fr.dark40k.taskmgr.util.OutlookConnector;
import fr.dark40k.taskmgr.util.OutlookConnector.Email;
import fr.dark40k.util.shef40k.ui.UIUtils;
import fr.dark40k.util.shef40k.ui.text.HTMLUtils;
import fr.dark40k.util.shef40k.ui.text.actions.HTMLTextEditAction;


/**
 * Action which pastes a link to an outlook email
 *
 */
public class PasteOutlookEmailHTMLLinkAction extends HTMLTextEditAction
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public PasteOutlookEmailHTMLLinkAction()
    {
        super("Inserer lien vers EMail dans Outlook");

        putValue(SMALL_ICON, UIUtils.getIcon(UIUtils.X16, "email.png"));
        putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
    }

    @Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor)
    {
    	String data = getHTMLLinksFromClipboard();
    	if (data.length()==0) return;

		editor.requestFocusInWindow();
		editor.replaceSelection(data);
    }

    @Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor)
    {
    	String data = getHTMLLinksFromClipboard();
    	if (data.length()==0) return;

		editor.replaceSelection("");
		HTMLUtils.insertHTML(data, HTML.Tag.DIV, editor);
    }

	private String getHTMLLinksFromClipboard() {

		ArrayList<OutlookConnector.Email> eMailList = OutlookConnector.ListSelectedEMails();

		if (eMailList==null)
			return "";

		if (eMailList.size() == 0)
			return "";

    	StringBuffer sb = new StringBuffer();

    	for (Email email  : eMailList) {

    		sb.append("<div><a href=\"outlook:");
    		sb.append(email.entryID);
    		sb.append("\">Email&rArr;");
    		sb.append(email.subject);
    		sb.append("</a></div>");

    	}

    	return sb.toString();

    }

}

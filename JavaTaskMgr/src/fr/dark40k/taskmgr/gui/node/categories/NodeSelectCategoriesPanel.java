package fr.dark40k.taskmgr.gui.node.categories;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeSelectCategoriesPanel extends JPanel {

	private JTable table;
	private NodeSelectCategoriesTableModel model;
	private Node node;
	
	
	/**
	 * Create the panel.
	 */
	public NodeSelectCategoriesPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		
		model = new NodeSelectCategoriesTableModel(taskMgrUndoMgr);
		table = new JTable(model);
		
		initGUI();
		
	}


	public void setNode(Node node) {
		this.node=node;
		model.setNode(node);
	}

	public void initGUI() {
		
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		table.getColumnModel().getColumn(0).setPreferredWidth(250);
		table.getColumnModel().getColumn(1).setMaxWidth(150);
		table.getColumnModel().getColumn(1).setPreferredWidth(75);
		scrollPane.setViewportView(table);
	}
	
	public Integer getSelectedCategoryIndex() {
		if (node==null) return -1;
		int row=table.getSelectedRow();
		if (row<0) return -1;
		return table.convertRowIndexToModel(row);
	}
	
}

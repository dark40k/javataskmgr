package fr.dark40k.taskmgr.gui.node.categories;

import javax.swing.table.AbstractTableModel;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.categories.Categories;
import fr.dark40k.taskmgr.database.categories.Categories.UpdateCategoryEvent;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeSelectCategoriesTableModel extends AbstractTableModel implements Categories.UpdateCategoryListener {

	private final TaskMgrUndoMgr taskMgrUndoMgr;
	
	private static final String[] columnNames = { "Cat�gorie", "Heritable" };

	private Node node;

	public NodeSelectCategoriesTableModel(TaskMgrUndoMgr taskMgrUndoMgr) {
		this.taskMgrUndoMgr=taskMgrUndoMgr;
	}
	
	public void setNode(Node node) {
		if (node!=null) node.getCategories().removeUpdateListener(this);
		this.node = node;
		if (node!=null) node.getCategories().addUpdateListener(this);
		fireTableDataChanged();
	}
	
	public void clearNode() {
		setNode(null);
	}
	
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		if (node==null) return 0;
		return node.getCategories().getCount();
	}

	@Override
	public Class<?> getColumnClass(int col) {
		switch (col) {
		case 0:
			return String.class;
		case 1:
			return Boolean.class;
		}
		return null;
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		if (node==null) return null;
		if (row>=node.getCategories().getCount()) return null;
		switch (col) {
		case 0:
			return node.getCategories().getCategory(row).name;
		case 1:
			return node.getCategories().getCategory(row).inheritable;
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return true;	
	}

	@Override
	public void setValueAt(Object value, int row, int col) {

		if (node==null) return;
		
		switch (col) {
		case 0:
			taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier nom cat�gorie",true);
			node.getCategories().getCategory(row).setName((String) value);
			break;
		case 1:
			taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier h�ritage cat�gorie",false);
			node.getCategories().getCategory(row).setInheritable((Boolean) value);
			break;
		}
		
		fireTableCellUpdated(row, col);
	}

	@Override
	public void categoriesUpdated(UpdateCategoryEvent evt) {
		fireTableDataChanged();
	}

}

package fr.dark40k.taskmgr.gui.node.categories;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeEditCategoriesPanel extends JPanel {

	public final static String XMLTAG="nodeeditcategoriespanel";
	
	private final TaskMgrUndoMgr taskMgrUndoMgr;

	private Node node;
	
	private NodeSelectCategoriesPanel nodeSelectCategoriesPanel;
	private NodeHeritedCategoriesPanel nodeHeritedCategoriesPanel;

	private JButton btnAjouter;
	private JButton btnDeplHaut;
	private JButton btnDeplBas;
	private JButton btnSupprimer;

	private JSplitPane splitPane;
	
	/**
	 * Create the panel.
	 */
	public NodeEditCategoriesPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		
		this.taskMgrUndoMgr=taskMgrUndoMgr;

		initGUI();
		setNode(null);
	}
	
	public void setNode(Node node) {
		
		this.node = node;

		btnAjouter.setEnabled(node != null);
		btnDeplHaut.setEnabled(node != null);
		btnDeplBas.setEnabled(node != null);
		btnSupprimer.setEnabled(node != null);
		
		nodeSelectCategoriesPanel.setNode(node);
		nodeHeritedCategoriesPanel.setNode(node);
		
	}
	
	protected void addCategory() {
		if (node==null) return;
    	taskMgrUndoMgr.addUndoableNodeEdit(node, "Ajouter catégorie",false);
		node.getCategories().insertNewCategory(nodeSelectCategoriesPanel.getSelectedCategoryIndex());
	}

	protected void removeCategory() {
		if (node==null) return;
		int index=nodeSelectCategoriesPanel.getSelectedCategoryIndex();
		if (index<0) return;
    	taskMgrUndoMgr.addUndoableNodeEdit(node, "Supprimer catégorie",false);
		node.getCategories().removeCategory(index);
	}

	protected void moveDownCategory() {
		if (node==null) return;
		int index=nodeSelectCategoriesPanel.getSelectedCategoryIndex();
		if (index<0 || index>node.getCategories().getCount()-2) return;
    	taskMgrUndoMgr.addUndoableNodeEdit(node, "Depl. haut catégorie",false);
		node.getCategories().moveDown(index);
	}

	protected void moveUpCategory() {
		if (node==null) return;
		int index=nodeSelectCategoriesPanel.getSelectedCategoryIndex();
		if (index<1 || index>node.getCategories().getCount()-1) return;
    	taskMgrUndoMgr.addUndoableNodeEdit(node, "Dépl. bas catégories",false);
		node.getCategories().moveUp(index);
	}
	
	
	private void initGUI() {

		setLayout(new BorderLayout(0, 0));

		splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.25);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		add(splitPane);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Categories propres",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		splitPane.setLeftComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));

		nodeSelectCategoriesPanel = new NodeSelectCategoriesPanel(taskMgrUndoMgr);
		panel.add(nodeSelectCategoriesPanel, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EmptyBorder(0, 5, 0, 5));
		panel.add(panel_1, BorderLayout.EAST);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 89, 0 };
		gbl_panel_1.rowHeights = new int[] { 23, 0, 0, 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addCategory();
			}
		});
		GridBagConstraints gbc_btnAjouter = new GridBagConstraints();
		gbc_btnAjouter.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAjouter.insets = new Insets(0, 0, 5, 0);
		gbc_btnAjouter.gridx = 0;
		gbc_btnAjouter.gridy = 0;
		panel_1.add(btnAjouter, gbc_btnAjouter);

		btnDeplHaut = new JButton("Depl. Haut");
		btnDeplHaut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				moveUpCategory();
			}
		});
		GridBagConstraints gbc_btnDeplHaut = new GridBagConstraints();
		gbc_btnDeplHaut.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDeplHaut.insets = new Insets(0, 0, 5, 0);
		gbc_btnDeplHaut.gridx = 0;
		gbc_btnDeplHaut.gridy = 1;
		panel_1.add(btnDeplHaut, gbc_btnDeplHaut);

		btnDeplBas = new JButton("Depl. Bas");
		btnDeplBas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				moveDownCategory();
			}
		});
		GridBagConstraints gbc_btnDeplBas = new GridBagConstraints();
		gbc_btnDeplBas.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDeplBas.insets = new Insets(0, 0, 5, 0);
		gbc_btnDeplBas.gridx = 0;
		gbc_btnDeplBas.gridy = 2;
		panel_1.add(btnDeplBas, gbc_btnDeplBas);

		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeCategory();
			}
		});
		GridBagConstraints gbc_btnSupprimer = new GridBagConstraints();
		gbc_btnSupprimer.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSupprimer.insets = new Insets(0, 0, 5, 0);
		gbc_btnSupprimer.gridx = 0;
		gbc_btnSupprimer.gridy = 3;
		panel_1.add(btnSupprimer, gbc_btnSupprimer);
		
		nodeHeritedCategoriesPanel = new NodeHeritedCategoriesPanel();
		nodeHeritedCategoriesPanel.setMinimumSize(new Dimension(100, 250));
		nodeHeritedCategoriesPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Cat\u00E9gories propres & Cat\u00E9gories h\u00E9rit\u00E9es", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		splitPane.setRightComponent(nodeHeritedCategoriesPanel);
	}
	
	//
	// Saving / Loading graphic configuration
	//
	
	public void importXML(Element baseElt) {

		Element elt = baseElt.getChild(XMLTAG); 
		if (elt==null) return;
		
		splitPane.setDividerLocation(parseInt(elt.getAttributeValue("divider"), splitPane.getDividerLocation()));
		
	}
	
	public Element exportXML() {
		
		Element elt=new Element(XMLTAG);
		
		elt.setAttribute("divider", Integer.toString(splitPane.getDividerLocation()));
        
		return elt;
	}
	
	private int parseInt(String val, int def) {
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException e) {
			return def;
		}
	}


}

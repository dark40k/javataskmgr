package fr.dark40k.taskmgr.gui.node.categories;

import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JList;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeListener;

public class NodeHeritedCategoriesPanel extends JPanel implements UpdateNodeListener {
	private final JScrollPane scrollPane = new JScrollPane();
	private final DefaultListModel<String> listModel = new DefaultListModel<>();
	private final JList<String> list = new JList<String>(listModel);

	/**
	 * Create the panel.
	 */
	public NodeHeritedCategoriesPanel() {

		initGUI();
	}
	
	private Node node;
	
	public void setNode(Node node) {
		if (node!=null) node.getDataBase().removeUpdateListener(this);
		this.node = node;
		updateList();
		if (node!=null) node.getDataBase().addUpdateListener(this);
	}
	
	public void clearNode() {
		setNode(null);
	}

	public void updateList(){
		listModel.clear();
		if (node==null) return;
		for (String catName : node.getRecursiveCategoriesNames())
			listModel.addElement(catName);
	}
	
	private void initGUI() {
		setLayout(new BorderLayout(0, 0));
		add(scrollPane, BorderLayout.CENTER);
		list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		scrollPane.setViewportView(list);
	}

	//
	// Node change handling
	//
	
	@Override
	public void updateDataBase(UpdateNodeEvent evt) {
		setNode(node);
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		setNode(node);
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
	}	

}

package fr.dark40k.taskmgr.gui.node;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.gui.node.datalinks.NodeLinksPanel;
import fr.dark40k.taskmgr.gui.node.datalinks.fastlinkspanel.FastLinkButttonListPanel;
import fr.dark40k.taskmgr.gui.node.notes.NotesPanel;
import fr.dark40k.taskmgr.gui.node.properties.NodeSelectStatusPanel;
import fr.dark40k.taskmgr.gui.node.properties.NodeSelectTemplatePanel;
import fr.dark40k.taskmgr.gui.popup.JPopupTextField;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.dragdrop.DropFilesHandler;
import fr.dark40k.util.dragdrop.DropFilesHandler.DropFileUser;

public class NodeEditTitlePanel extends JPanel implements TaskMgrDB.UpdateNodeListener {

	@SuppressWarnings("unused")
	private final static Logger LOGGER = LogManager.getLogger();

	public final static String XMLTAG="nodeedittitlepanel";

	private final TaskMgrUndoMgr taskMgrUndoMgr;

	private Node node;

	private DropFilesHandler dropFilesHandler;

	public JPopupTextField textField_Title;

	public final NotesPanel notesPanel;

	private NodeSelectTemplatePanel nodeSelectTemplatePanel;
	private NodeSelectStatusPanel nodeSelectStatusPanel;

	private final FastLinkButttonListPanel fastLinkButttonListPanel;
	private final NodeLinksPanel nodeLinksPanel;

	private TaskDatesPanel datesPanel;
	private JPanel panel_1;

	private JSplitPane splitPane;

	private boolean datesPanelVisible;
	private boolean nodeLinksPanelVisible;

	public NodeEditTitlePanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		this.taskMgrUndoMgr = taskMgrUndoMgr;

		fastLinkButttonListPanel = new FastLinkButttonListPanel(taskMgrUndoMgr);

		nodeLinksPanel = new NodeLinksPanel(taskMgrUndoMgr);

		notesPanel = new NotesPanel(taskMgrUndoMgr);

		dropFilesHandler = new DropFilesHandler(this,new DropFileUser() {
			@Override
			public void dropFiles(DropTargetDropEvent e, List<File> list) {

				NodeTask nodeTask = (NodeTask)node;

				taskMgrUndoMgr.addUndoableNodeEdit(node, "Ajouter liens ("+list.size()+")",false);

				for (File file : list) {
					nodeTask.getLinkList().addLink(new DataLink(nodeTask,file.getName(), file));
				}

			}
		});
		dropFilesHandler.setActive(false);

		initGUI();

	}

	public void setNode(Node newNode) {

		if (node!=null) {
			textField_Title.getDocument().removeDocumentListener(textField_Title_Listener);
			node.getDataBase().removeUpdateListener(this);
		}

		nodeLinksPanel.setNode(newNode);
		datesPanel.setNode(newNode);
		nodeSelectTemplatePanel.setNode(newNode);
		nodeSelectStatusPanel.setNode(newNode);
		nodeSelectTemplatePanel.setNode(newNode);

		fastLinkButttonListPanel.setNode(newNode);

		dropFilesHandler.setActive((newNode!=null)&&(newNode instanceof NodeTask));

		notesPanel.setNode(newNode);

		if (newNode!=null) {
			if (!equalStrings(textField_Title.getText(),newNode.getName())) textField_Title.setText(newNode.getName());
			textField_Title.setEditable(!newNode.isReadOnly());
		} else {
			textField_Title.setText(null);
			textField_Title.setEditable(false);
		}

		if (newNode!=null) {
			textField_Title.getDocument().addDocumentListener(textField_Title_Listener);
			newNode.getDataBase().addUpdateListener(this);
		}

		node=newNode;

	}

	private boolean equalStrings(String s1, String s2) {
		return (s1==null)?(s2==null):s1.equals(s2);
	}

	//
	// Saving / Loading graphic configuration
	//

	public void importXML(Element baseElt) {

		Element elt = baseElt.getChild(XMLTAG);
		if (elt == null)
			return;

		datesPanelVisible = elt.getAttributeValue("datesVisible", "true").equals("true");
		nodeLinksPanelVisible = elt.getAttributeValue("nodeLinksVisible", "true").equals("true");

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {

				splitPane.setDividerLocation(parseInt(elt.getAttributeValue("divider"), splitPane.getDividerLocation()));

				datesPanel.setVisible(datesPanelVisible);

				nodeLinksPanel.setVisible(nodeLinksPanelVisible);

			}
		});

	}

	public Element exportXML() {

		Element elt=new Element(XMLTAG);

		elt.setAttribute("divider", Integer.toString(splitPane.getDividerLocation()));

		elt.setAttribute("datesVisible", Boolean.toString(datesPanel.isVisible()));

		elt.setAttribute("nodeLinksVisible", Boolean.toString(nodeLinksPanel.isVisible()));

		return elt;
	}

	private int parseInt(String val, int def) {
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException e) {
			return def;
		}
	}

	//
	// Display handling
	//

	public void setDatesPanelVisible(boolean newstatus) {
		datesPanelVisible = newstatus;
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				datesPanel.setVisible(datesPanelVisible);
			}
		});
	}

	public boolean isDatesPanelVisible() {
		return datesPanelVisible;
	}

	public void setNodeLinksPanelVisible(boolean newstatus) {
		nodeLinksPanelVisible=newstatus;
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				nodeLinksPanel.setVisible(nodeLinksPanelVisible);
				if (nodeLinksPanelVisible)
					splitPane.resetToPreferredSizes();
			}
		});
	}

	public boolean isNodeLinksPanelVisible() {
		return nodeLinksPanelVisible;
	}

	//
	// Title change event handling
	//

	private DocumentListener textField_Title_Listener = new DocumentListener() {
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateTitleName();
		}
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateTitleName();
		}
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateTitleName();
		}
	};

	private void updateTitleName() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Edition titre",true);
	        	node.setName(textField_Title.getText());
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	//
	// Node change handling
	//

	@Override
	public void updateDataBase(UpdateNodeEvent evt) {
		setNode(node);
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) setNode(node);
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) setNode(node);
	}

	//
	// GUI
	//

	private void initGUI() {

		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBorder(null);
		add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 100, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JPanel panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_5.anchor = GridBagConstraints.NORTH;
		gbc_panel_5.gridx = 0;
		gbc_panel_5.gridy = 0;
		panel.add(panel_5, gbc_panel_5);
		panel_5.setBorder(new TitledBorder(null, "Titre", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagLayout gbl_panel_5 = new GridBagLayout();
		gbl_panel_5.columnWidths = new int[] { 75, 0 };
		gbl_panel_5.rowHeights = new int[] { 20, 0 };
		gbl_panel_5.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_5.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panel_5.setLayout(gbl_panel_5);

		textField_Title = new JPopupTextField();
		GridBagConstraints gbc_textField_Title = new GridBagConstraints();
		gbc_textField_Title.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_Title.anchor = GridBagConstraints.NORTH;
		gbc_textField_Title.gridx = 0;
		gbc_textField_Title.gridy = 0;
		panel_5.add(textField_Title, gbc_textField_Title);
		textField_Title.setColumns(10);

		panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		panel.add(panel_1, gbc_panel_1);
				GridBagLayout gbl_panel_1 = new GridBagLayout();
				gbl_panel_1.columnWidths = new int[]{0, 40, 40, 0};
				gbl_panel_1.rowHeights = new int[]{0, 0, 0};
				gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
				gbl_panel_1.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
				panel_1.setLayout(gbl_panel_1);

								GridBagConstraints gbc_panel_2 = new GridBagConstraints();
								gbc_panel_2.fill = GridBagConstraints.HORIZONTAL;
								gbc_panel_2.gridx = 0;
								gbc_panel_2.gridy = 0;
								panel_1.add(fastLinkButttonListPanel, gbc_panel_2);

								nodeSelectStatusPanel = new NodeSelectStatusPanel(taskMgrUndoMgr);
								GridBagConstraints gbc_nodeSelectStatusPanel = new GridBagConstraints();
								gbc_nodeSelectStatusPanel.fill = GridBagConstraints.HORIZONTAL;
								gbc_nodeSelectStatusPanel.gridx = 1;
								gbc_nodeSelectStatusPanel.gridy = 0;
								panel_1.add(nodeSelectStatusPanel, gbc_nodeSelectStatusPanel);
								nodeSelectStatusPanel.setBorder(new TitledBorder(null, "Status", TitledBorder.LEADING, TitledBorder.TOP, null, null));

						nodeSelectTemplatePanel = new NodeSelectTemplatePanel(taskMgrUndoMgr);
						GridBagConstraints gbc_nodeSelectTemplatePanel = new GridBagConstraints();
						gbc_nodeSelectTemplatePanel.fill = GridBagConstraints.HORIZONTAL;
						gbc_nodeSelectTemplatePanel.gridx = 2;
						gbc_nodeSelectTemplatePanel.gridy = 0;
						panel_1.add(nodeSelectTemplatePanel, gbc_nodeSelectTemplatePanel);
						nodeSelectTemplatePanel.setBorder(new TitledBorder(null, "Mod\u00E8le", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		datesPanel = new TaskDatesPanel(taskMgrUndoMgr);
		GridBagConstraints gbc_datesPanel = new GridBagConstraints();
		gbc_datesPanel.insets = new Insets(0, 0, 5, 0);
		gbc_datesPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_datesPanel.gridx = 0;
		gbc_datesPanel.gridy = 2;
		panel.add(datesPanel, gbc_datesPanel);

		splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		add(splitPane, BorderLayout.CENTER);

		splitPane.setLeftComponent(nodeLinksPanel);

		splitPane.setRightComponent(notesPanel);

		splitPane.setDividerLocation(150);

	}


}

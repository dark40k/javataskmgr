package fr.dark40k.taskmgr.gui.node;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.gui.node.categories.NodeEditCategoriesPanel;
import fr.dark40k.taskmgr.gui.node.display.NodeDisplayPanel;
import fr.dark40k.taskmgr.gui.node.executionslot.NodeEditExecutionSlotListPanel;
import fr.dark40k.taskmgr.gui.node.properties.NodePropertiesPanel;
import fr.dark40k.taskmgr.gui.node.scriptpanel.NodeScriptPanel;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeEditPanel extends JTabbedPane {

	public final static String XMLTAG="nodeeditpanel";

	public NodeEditTitlePanel nodeEditTitlePanel;

	private boolean modeAdmin;

	public final NodePropertiesPanel nodePropertiesPanel;
	public final NodeDisplayPanel nodeDisplayPanel;
	public final NodeEditCategoriesPanel categoriesPanel;
	public final NodeEditExecutionSlotListPanel timersPanel;
	public final NodeScriptPanel scriptPanel;

	/**
	 * Create the panel.
	 */
	public NodeEditPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		nodeEditTitlePanel = new NodeEditTitlePanel(taskMgrUndoMgr);
		addTab("Tache", null, nodeEditTitlePanel, null);


		JPanel parametersPanel = new JPanel();
		addTab("Param�tres", null, parametersPanel, null);

			GridBagLayout gbl_parametersPanel = new GridBagLayout();
			gbl_parametersPanel.rowHeights = new int[]{0, 0, 0};
			gbl_parametersPanel.rowWeights = new double[]{0.0, 0.0, 1.0};
			parametersPanel.setLayout(gbl_parametersPanel);

				nodePropertiesPanel = new NodePropertiesPanel(taskMgrUndoMgr);
				nodePropertiesPanel.setBorder(new TitledBorder(null, "Propri�t�s", TitledBorder.LEADING, TitledBorder.TOP, null, null));

				GridBagConstraints gbc_1 = new GridBagConstraints();
				gbc_1.gridx = 0;
				gbc_1.gridy = 0;
				gbc_1.insets = new Insets(0, 0, 5, 0);
				gbc_1.fill = GridBagConstraints.HORIZONTAL;
				gbc_1.anchor = GridBagConstraints.NORTH;
				gbc_1.gridwidth = GridBagConstraints.REMAINDER;
				gbc_1.weightx = 1.0;

			parametersPanel.add(nodePropertiesPanel, gbc_1);

				nodeDisplayPanel = new NodeDisplayPanel(taskMgrUndoMgr);
				nodeDisplayPanel.setBorder(new TitledBorder(null, "Param�tres d'affichage", TitledBorder.LEADING, TitledBorder.TOP, null, null));

				GridBagConstraints gbc_2 = new GridBagConstraints();
				gbc_2.insets = new Insets(0, 0, 5, 0);
				gbc_2.gridx = 0;
				gbc_2.gridy = 1;
				gbc_2.fill = GridBagConstraints.HORIZONTAL;
				gbc_2.anchor = GridBagConstraints.NORTH;
				gbc_2.gridwidth = GridBagConstraints.REMAINDER;
				gbc_2.weightx = 1.0;

			parametersPanel.add(nodeDisplayPanel, gbc_2);

		categoriesPanel = new NodeEditCategoriesPanel(taskMgrUndoMgr);
		addTab("Categories", null, categoriesPanel, null);

		timersPanel = new NodeEditExecutionSlotListPanel(taskMgrUndoMgr);
		addTab("Execution", null, timersPanel, null);

		scriptPanel = new NodeScriptPanel(taskMgrUndoMgr);
//		addTab("Scripts", null, scriptPanel, null);
		modeAdmin=false;
	}

	public void setNode(Node node, boolean admin) {

		nodeEditTitlePanel.setNode(node);
		nodeDisplayPanel.setNode(node);
		nodePropertiesPanel.setNode(node);
		categoriesPanel.setNode(node);
		timersPanel.setNode(node);

		// met � jour l'affichage en mode administrateur
		if (admin!=modeAdmin) {
			modeAdmin=admin;
			if (modeAdmin) {
				addTab("Scripts", null, scriptPanel, null);
			} else {
				remove(scriptPanel);
				scriptPanel.setNode(null);
			}
		}

		// quitte si on est pas en mode administrateur
		if (!modeAdmin) return;

		// mise � jours des panneaux administrateur
		scriptPanel.setNode(node);

	}

	public void importXML(Element baseElt) {

		Element elt = baseElt.getChild(XMLTAG);
		if (elt==null) return;

		nodeEditTitlePanel.importXML(elt);
		categoriesPanel.importXML(elt);
		timersPanel.importXML(elt);
		scriptPanel.importXML(elt);

	}

	public Element exportXML() {

		Element elt=new Element(XMLTAG);

        elt.addContent(nodeEditTitlePanel.exportXML());
        elt.addContent(categoriesPanel.exportXML());
        elt.addContent(timersPanel.exportXML());
        elt.addContent(scriptPanel.exportXML());

		return elt;
	}


}

package fr.dark40k.taskmgr.gui.node;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.FormattersString;

public class TaskDatesPanel extends JPanel {

	private final static String defaultCreationString = "<pas de valeur>";
	private final static String defaultModificationString = "<pas de valeur>";

	private final TaskMgrUndoMgr taskMgrUndoMgr;

	private NodeTask node;

	private DatePanel expectedStartPanel;
	private DatePanel expectedEndPanel;
	private DatePanel effectiveStartPanel;
	private DatePanel effectiveEndPanel;

	private JLabel labelCreationTime;
	private JLabel labelModificationTime;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeur
	//
	// -------------------------------------------------------------------------------------------

	public TaskDatesPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		setBorder(new TitledBorder("Dates"));
		this.taskMgrUndoMgr=taskMgrUndoMgr;
		initGUI();
		setNode(null);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Selection du noeud
	//
	// -------------------------------------------------------------------------------------------

	public void setNode(Node newNode) {

		clear();

		if (newNode==null) {
			return;
		}

		if (!(newNode instanceof NodeTask)) {
			return;
		}

		node = (NodeTask) newNode;

		expectedStartPanel.setDateData(node,node.getDates().getExpectedStart(),"date de d�but pr�vu");
		expectedEndPanel.setDateData(node,node.getDates().getExpectedEnd(),"date de fin pr�vue");
		effectiveStartPanel.setDateData(node,node.getDates().getEffectiveStart(),"date de d�but effectif");
		effectiveEndPanel.setDateData(node,node.getDates().getEffectiveEnd(),"date de fin effective");

		labelCreationTime.setText(FormattersString.format(node.getCreationDateTime(),defaultCreationString));
		labelModificationTime.setText(FormattersString.format(node.getModificationDateTime(),defaultModificationString));
	}

	public void clear() {
		node=null;
		expectedStartPanel.setDateData(null,null,null);
		expectedEndPanel.setDateData(null,null,null);
		effectiveStartPanel.setDateData(null,null,null);
		effectiveEndPanel.setDateData(null,null,null);

		labelCreationTime.setText(defaultCreationString);
		labelModificationTime.setText(defaultModificationString);
	}

	// -------------------------------------------------------------------------------------------
	//
	// GUI
	//
	// -------------------------------------------------------------------------------------------


	private void initGUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{100, 60, 100, 60, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);

				JLabel lblCreation = new JLabel("Creation :");
				GridBagConstraints gbc_lblCreation = new GridBagConstraints();
				gbc_lblCreation.anchor = GridBagConstraints.EAST;
				gbc_lblCreation.insets = new Insets(0, 0, 5, 5);
				gbc_lblCreation.gridx = 0;
				gbc_lblCreation.gridy = 0;
				add(lblCreation, gbc_lblCreation);

				labelCreationTime = new JLabel("< Creation Time >");
				GridBagConstraints gbc_label = new GridBagConstraints();
				gbc_label.anchor = GridBagConstraints.WEST;
				gbc_label.insets = new Insets(0, 0, 5, 5);
				gbc_label.gridx = 1;
				gbc_label.gridy = 0;
				add(labelCreationTime, gbc_label);

				JLabel lblModification = new JLabel("Modification :");
				GridBagConstraints gbc_lblModification = new GridBagConstraints();
				gbc_lblModification.anchor = GridBagConstraints.EAST;
				gbc_lblModification.insets = new Insets(0, 0, 5, 5);
				gbc_lblModification.gridx = 2;
				gbc_lblModification.gridy = 0;
				add(lblModification, gbc_lblModification);

				labelModificationTime = new JLabel("< Modification Time >");
				GridBagConstraints gbc_label_1 = new GridBagConstraints();
				gbc_label_1.anchor = GridBagConstraints.WEST;
				gbc_label_1.insets = new Insets(0, 0, 5, 0);
				gbc_label_1.gridx = 3;
				gbc_label_1.gridy = 0;
				add(labelModificationTime, gbc_label_1);

				JLabel lblDbutPrvuLe = new JLabel("D\u00E9but pr\u00E9vu le :");
				GridBagConstraints gbc_lblDbutPrvuLe = new GridBagConstraints();
				gbc_lblDbutPrvuLe.insets = new Insets(0, 0, 5, 5);
				gbc_lblDbutPrvuLe.anchor = GridBagConstraints.EAST;
				gbc_lblDbutPrvuLe.gridx = 0;
				gbc_lblDbutPrvuLe.gridy = 1;
				add(lblDbutPrvuLe, gbc_lblDbutPrvuLe);

				expectedStartPanel = new DatePanel(taskMgrUndoMgr);
				expectedStartPanel.setBorder(null);
				GridBagConstraints gbc_expectedStartPanel = new GridBagConstraints();
				gbc_expectedStartPanel.insets = new Insets(0, 0, 5, 5);
				gbc_expectedStartPanel.fill = GridBagConstraints.HORIZONTAL;
				gbc_expectedStartPanel.gridx = 1;
				gbc_expectedStartPanel.gridy = 1;
				add(expectedStartPanel, gbc_expectedStartPanel);

				JLabel lblFinPrvueLe = new JLabel("Fin pr\u00E9vue le :");
				GridBagConstraints gbc_lblFinPrvueLe = new GridBagConstraints();
				gbc_lblFinPrvueLe.insets = new Insets(0, 0, 5, 5);
				gbc_lblFinPrvueLe.anchor = GridBagConstraints.EAST;
				gbc_lblFinPrvueLe.gridx = 2;
				gbc_lblFinPrvueLe.gridy = 1;
				add(lblFinPrvueLe, gbc_lblFinPrvueLe);

				expectedEndPanel = new DatePanel(taskMgrUndoMgr);
				GridBagConstraints gbc_expectedEndPanel = new GridBagConstraints();
				gbc_expectedEndPanel.insets = new Insets(0, 0, 5, 0);
				gbc_expectedEndPanel.fill = GridBagConstraints.HORIZONTAL;
				gbc_expectedEndPanel.gridx = 3;
				gbc_expectedEndPanel.gridy = 1;
				add(expectedEndPanel, gbc_expectedEndPanel);

				JLabel lblDbutEffectifLe = new JLabel("D\u00E9but effectif le :");
				GridBagConstraints gbc_lblDbutEffectifLe = new GridBagConstraints();
				gbc_lblDbutEffectifLe.insets = new Insets(0, 0, 0, 5);
				gbc_lblDbutEffectifLe.anchor = GridBagConstraints.EAST;
				gbc_lblDbutEffectifLe.gridx = 0;
				gbc_lblDbutEffectifLe.gridy = 2;
				add(lblDbutEffectifLe, gbc_lblDbutEffectifLe);

				effectiveStartPanel = new DatePanel(taskMgrUndoMgr);
				GridBagConstraints gbc_effectiveStartPanel = new GridBagConstraints();
				gbc_effectiveStartPanel.insets = new Insets(0, 0, 0, 5);
				gbc_effectiveStartPanel.fill = GridBagConstraints.HORIZONTAL;
				gbc_effectiveStartPanel.gridx = 1;
				gbc_effectiveStartPanel.gridy = 2;
				add(effectiveStartPanel, gbc_effectiveStartPanel);

				JLabel lblFinEffectiveLe = new JLabel("Fin effective le :");
				GridBagConstraints gbc_lblFinEffectiveLe = new GridBagConstraints();
				gbc_lblFinEffectiveLe.insets = new Insets(0, 0, 0, 5);
				gbc_lblFinEffectiveLe.anchor = GridBagConstraints.EAST;
				gbc_lblFinEffectiveLe.gridx = 2;
				gbc_lblFinEffectiveLe.gridy = 2;
				add(lblFinEffectiveLe, gbc_lblFinEffectiveLe);

				effectiveEndPanel = new DatePanel(taskMgrUndoMgr);
				GridBagConstraints gbc_effectiveEndPanel = new GridBagConstraints();
				gbc_effectiveEndPanel.fill = GridBagConstraints.HORIZONTAL;
				gbc_effectiveEndPanel.gridx = 3;
				gbc_effectiveEndPanel.gridy = 2;
				add(effectiveEndPanel, gbc_effectiveEndPanel);

	}

}

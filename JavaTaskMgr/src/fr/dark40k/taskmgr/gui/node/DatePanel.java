package fr.dark40k.taskmgr.gui.node;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DateEditor;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.taskdates.TaskDateData;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.swing.DateDialog;

public class DatePanel extends JPanel {

	private JSpinner dateSpinner;
	private JButton selectDateButton;
	private JCheckBox chkDateCheckBox;

	private TaskDateData date;
	private final TaskMgrUndoMgr taskMgrUndoMgr;
	private Node node;
	
	private String modificationName;
	
	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------
	
	public DatePanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		this.taskMgrUndoMgr=taskMgrUndoMgr;
		initGUI();
	}

	// -------------------------------------------------------------------------------------------
	//
	// Commandes
	//
	// -------------------------------------------------------------------------------------------
	
	protected void changeDate() {
		date.setDate((Date) dateSpinner.getValue());
		updateDisplay();
	}

	public void switchDate() {
		
		if (date==null) return;
		
		if ((date.getLocalDate()==null) && (chkDateCheckBox.isSelected())) {
			taskMgrUndoMgr.addUndoableNodeEdit(node, "Edition "+modificationName,true);
			date.setLocalDate(LocalDate.now());
			updateDisplay();
			return;
		}
		
		if ((date.getLocalDate()!=null) && (!chkDateCheckBox.isSelected())) {
			taskMgrUndoMgr.addUndoableNodeEdit(node, "Edition "+modificationName,true);
			date.clearDate();
			updateDisplay();
			return;
		}
		
	}
	
	public void selectDate() {
		
		if (date==null) return;

		LocalDate actualDate = date.getLocalDate();
		
		DateDialog dialog = new DateDialog(selectDateButton,null);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocalDate((actualDate!=null)?actualDate:null);
		dialog.setVisible(true);
		
        //Make the renderer reappear.
		if (dialog.getExitStatus()) {
			taskMgrUndoMgr.addUndoableNodeEdit(node, "Edition "+modificationName,true);
			date.setLocalDate(dialog.getLocalDate());
			updateDisplay();
		}
	
	}
	
	public static Frame getOwningFrame(Component comp) {
		if (comp == null) {
			throw new IllegalArgumentException("null Component passed");
		}

		if (comp instanceof Frame) {
			return (Frame) comp;
		}
		return getOwningFrame(SwingUtilities.windowForComponent(comp));
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// GUI
	//
	// -------------------------------------------------------------------------------------------
	
	private void initGUI() {
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{21, 29, 0, 0};
		gridBagLayout.rowHeights = new int[]{21, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		chkDateCheckBox = new JCheckBox((String) null);
		chkDateCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchDate();
			}
		});
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.anchor = GridBagConstraints.WEST;
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxNewCheckBox.gridx = 0;
		gbc_chckbxNewCheckBox.gridy = 0;
		add(chkDateCheckBox, gbc_chckbxNewCheckBox);
		
		dateSpinner = new JSpinner(new SpinnerDateModel());
		dateSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				changeDate();
			}
		});
		dateSpinner.setEditor(new DateEditor(dateSpinner, "dd/MM/yyyy"));

		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinner.insets = new Insets(0, 0, 0, 5);
		gbc_spinner.gridx = 1;
		gbc_spinner.gridy = 0;
		add(dateSpinner, gbc_spinner);
		
		selectDateButton = new JButton((String) null);
		selectDateButton.setPreferredSize(new Dimension(21, 9));
		selectDateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectDate();
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton.gridx = 2;
		gbc_btnNewButton.gridy = 0;
		add(selectDateButton, gbc_btnNewButton);
	}

	
	public void updateDisplay() {
		
		if (date==null) {
			if (chkDateCheckBox.isEnabled()) chkDateCheckBox.setEnabled(false);
			if (dateSpinner.isEnabled() ) dateSpinner.setEnabled(false);
			if (selectDateButton.isEnabled()) selectDateButton.setEnabled(false);
			return;
		}
		
		if (!chkDateCheckBox.isEnabled()) chkDateCheckBox.setEnabled(true);
		if (!dateSpinner.isEnabled()) dateSpinner.setEnabled(true);
		if (!selectDateButton.isEnabled()) selectDateButton.setEnabled(true);
		
		if (date.getLocalDate()==null) {
			if (chkDateCheckBox.isSelected()) chkDateCheckBox.setSelected(false);
			if (dateSpinner.isEnabled()) dateSpinner.setEnabled(false);
			return;
		}
		
		if (!chkDateCheckBox.isSelected()) chkDateCheckBox.setSelected(true);
		if (!dateSpinner.isEnabled()) dateSpinner.setEnabled(true);
		
		dateSpinner.setValue(date.getDate());
		
	}
	
	public void setDateData(Node node, TaskDateData date, String modificationName) {
		
		this.node=node;
		this.date=date;
		this.modificationName=modificationName;
		
		updateDisplay();
	}
	
	
}

package fr.dark40k.taskmgr.gui.node.executionslot;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SortOrder;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableRowSorter;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlot;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.FormattersString;

public class ExecutionSlotListPanel extends JPanel {

	private final TaskMgrUndoMgr taskMgrUndoMgr;

	private final JTable table;
	private final ExecutionSlotListTableModel model;
	private final TableRowSorter<ExecutionSlotListTableModel> sorter;

	private NodeTask nodeTask;

	private JLabel lblNewLabel;
	private JButton btnAjouter1h;
	private JButton btnDeplHaut;
	private JButton btnDeplBas;
	private JButton btnSupprimer;

	/**
	 * Create the panel.
	 * @param pf 
	 */
	public ExecutionSlotListPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		
		this.taskMgrUndoMgr=taskMgrUndoMgr;
		
		model = new ExecutionSlotListTableModel(taskMgrUndoMgr);
		table = new JTable(model);
		sorter = new TableRowSorter<ExecutionSlotListTableModel>(model);
		
		initGUI();
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent lse) {
		        if (!lse.getValueIsAdjusting()) {
		        	updateDisplay();
		        }
		    }
		});
		
		table.getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent arg0) {
				updateDisplay();
			}
		});
		
		((DefaultCellEditor) table.getDefaultEditor(Object.class)).setClickCountToStart(2);
		table.setDefaultRenderer(LocalDateTime.class, new LocalDateTimeCellRenderer());
		table.setDefaultRenderer(Duration.class, new DurationCellRenderer());
		table.setDefaultEditor(LocalDateTime.class, new LocalDateTimeCellEditor());
		table.setDefaultEditor(Duration.class, new DurationCellEditor());
		
		table.setAutoCreateRowSorter(true);
		table.getTableHeader().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (table.getRowSorter() != null) {
					if (table.getRowSorter().getSortKeys().size() > 0)
						if (table.getRowSorter().getSortKeys().get(0).getSortOrder() == SortOrder.DESCENDING)
							table.setRowSorter(null);
				} else {
					table.setRowSorter(sorter);
				}
			}
		});
		
	}

	//
	// Getters / Setters
	//
	
	public void setNode(Node node) {
		
		this.nodeTask = (node instanceof NodeTask) ? (NodeTask) node : null;
		
		model.setNode(this.nodeTask);
		
		updateDisplay();
	}

	public Integer getSelectedCategoryIndex() {
		if (nodeTask==null) return -1;
		int row=table.getSelectedRow();
		if (row<0) return -1;
		return table.convertRowIndexToModel(row);
	}

	//
	// Mise � jours affichage
	// 
	
	public void updateDisplay() {
		
		btnAjouter1h.setEnabled((nodeTask != null));
		btnDeplHaut.setEnabled((nodeTask != null)&&(table.getSelectedRowCount()==1));
		btnDeplBas.setEnabled((nodeTask != null)&&(table.getSelectedRowCount()==1));
		btnSupprimer.setEnabled((nodeTask != null)&&(table.getSelectedRowCount()==1));
		
		if (nodeTask==null) {
			lblNewLabel.setText("Total = ");
			return;
		}
		
    	if (table.getSelectedRowCount()>0) {
    		Duration totalDuration = Duration.ZERO;
    		for (int row : table.getSelectedRows())
    			if (row<nodeTask.getExecutionSlotList().size())
    				totalDuration=totalDuration.plus(nodeTask.getExecutionSlotList().get(table.convertRowIndexToModel(row)).getDuration());
    		lblNewLabel.setText("Total (selection) = "+FormattersString.format(totalDuration,"<pas de valeur>"));
    	} else {
    		Duration totalDuration = Duration.ZERO;
    		for (ExecutionSlot executionSlot : nodeTask.getExecutionSlotList())
    			totalDuration=totalDuration.plus(executionSlot.getDuration());
    		lblNewLabel.setText("Total = "+FormattersString.format(totalDuration,"<pas de valeur>"));
    	}
	}
	
	//
	// Commandes
	//
	
	protected void removeExecutionSlot() {
		
		if (nodeTask==null) return;
		int index=getSelectedCategoryIndex();
		if (index<0) return;
    	taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Supprimer creneau execution",false);
    	nodeTask.getExecutionSlotList().removeExecutionSlot(index);
		
	}

	protected void moveDownExecutionSlot() {
		
		if (nodeTask==null) return;
		int index=getSelectedCategoryIndex();
		if (index<0 || index>nodeTask.getExecutionSlotList().size()-2) return;
    	taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "D�pl. haut creneau execution",false);
    	nodeTask.getExecutionSlotList().moveDown(index);
		
	}

	protected void moveUpExecutionSlot() {
		
		if (nodeTask==null) return;
		int index=getSelectedCategoryIndex();
		if (index<1 || index>nodeTask.getExecutionSlotList().size()-1) return;
    	taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "D�pl. bas creneau execution",false);
    	nodeTask.getExecutionSlotList().moveUp(index);

	}

	protected void addExecutionSlot1h() {
	   	taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter 1h",false);
		nodeTask.getExecutionSlotList().addExecutionSlot(new ExecutionSlot(Duration.ofHours(1L)));
	}
	
	
	//
	// GUI
	//
	
	public void initGUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{338, 100, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 20, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
				
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 5;
		gbc_scrollPane.insets = new Insets(3, 3, 3, 3);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		add(scrollPane, gbc_scrollPane);
		
				table.getColumnModel().getColumn(0).setPreferredWidth(100);
				table.getColumnModel().getColumn(1).setPreferredWidth(50);
				table.getColumnModel().getColumn(2).setPreferredWidth(200);
				
				scrollPane.setViewportView(table);
				
						btnAjouter1h = new JButton("Ajout 1h");
						GridBagConstraints gbc_btnAjouter1h = new GridBagConstraints();
						gbc_btnAjouter1h.fill = GridBagConstraints.HORIZONTAL;
						gbc_btnAjouter1h.anchor = GridBagConstraints.NORTH;
						gbc_btnAjouter1h.insets = new Insets(3, 3, 3, 3);
						gbc_btnAjouter1h.gridx = 1;
						gbc_btnAjouter1h.gridy = 0;
						add(btnAjouter1h, gbc_btnAjouter1h);
						btnAjouter1h.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								addExecutionSlot1h();
							}
						});
				
						btnDeplHaut = new JButton("Depl. Haut");
						GridBagConstraints gbc_btnDeplHaut = new GridBagConstraints();
						gbc_btnDeplHaut.fill = GridBagConstraints.HORIZONTAL;
						gbc_btnDeplHaut.insets = new Insets(3, 3, 3, 3);
						gbc_btnDeplHaut.gridx = 1;
						gbc_btnDeplHaut.gridy = 1;
						add(btnDeplHaut, gbc_btnDeplHaut);
						btnDeplHaut.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								moveUpExecutionSlot();
							}
						});
				
						btnDeplBas = new JButton("Depl. Bas");
						GridBagConstraints gbc_btnDeplBas = new GridBagConstraints();
						gbc_btnDeplBas.fill = GridBagConstraints.HORIZONTAL;
						gbc_btnDeplBas.insets = new Insets(3, 3, 3, 3);
						gbc_btnDeplBas.gridx = 1;
						gbc_btnDeplBas.gridy = 2;
						add(btnDeplBas, gbc_btnDeplBas);
						btnDeplBas.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								moveDownExecutionSlot();
							}
						});
		
				btnSupprimer = new JButton("Supprimer");
				GridBagConstraints gbc_btnSupprimer = new GridBagConstraints();
				gbc_btnSupprimer.fill = GridBagConstraints.HORIZONTAL;
				gbc_btnSupprimer.insets = new Insets(3, 3, 3, 3);
				gbc_btnSupprimer.gridx = 1;
				gbc_btnSupprimer.gridy = 4;
				add(btnSupprimer, gbc_btnSupprimer);
				btnSupprimer.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						removeExecutionSlot();
					}
				});
		
		
		lblNewLabel = new JLabel("Total = ");
		lblNewLabel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(3, 3, 3, 3);
		gbc_lblNewLabel.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 5;
		add(lblNewLabel, gbc_lblNewLabel);
	}
	

}

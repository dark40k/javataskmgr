package fr.dark40k.taskmgr.gui.node.executionslot;

import java.awt.Component;
import java.time.Duration;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.dark40k.util.FormattersString;

public class DurationCellRenderer extends DefaultTableCellRenderer {
	
	public DurationCellRenderer() {
	}
	
	@Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {
		return super.getTableCellRendererComponent(table, FormattersString.format((Duration)value,null), isSelected, hasFocus, rowIndex, columnIndex);
  	}

}

package fr.dark40k.taskmgr.gui.node.executionslot;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.time.Duration;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import fr.dark40k.util.FormattersString;

public class DurationCellEditor extends AbstractCellEditor implements TableCellEditor {
	
	private final JComponent component = new JTextField();
	
	private Duration defaultDuration;
	
	public DurationCellEditor() {}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int vColIndex) {
		defaultDuration = (Duration) value;
		((JTextField) component).setText(FormattersString.format(defaultDuration,null));
		return component;
	}

	public Object getCellEditorValue() {

		String newString = ((JTextField) component).getText();

		return FormattersString.parseDuration(newString, defaultDuration);
				
	}
	
	public boolean isCellEditable(EventObject anEvent) {
		if (anEvent instanceof MouseEvent) {
			return ((MouseEvent) anEvent).getClickCount() >= 2;
		}
		return true;
	}
}	
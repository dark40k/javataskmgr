package fr.dark40k.taskmgr.gui.node.executionslot;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.time.LocalDateTime;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import fr.dark40k.util.FormattersString;

public class LocalDateTimeCellEditor extends AbstractCellEditor implements TableCellEditor {
	
	private final JComponent component = new JTextField();
	
	private LocalDateTime defaultDateTime;
	
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int vColIndex) {
		defaultDateTime = (LocalDateTime) value;
		((JTextField) component).setText(FormattersString.format(defaultDateTime, "<vide>"));
		return component;
	}

	public Object getCellEditorValue() {

		String newString = ((JTextField) component).getText();

		return FormattersString.parseLocalDateTime(newString, defaultDateTime);
				
	}
	
	public boolean isCellEditable(EventObject anEvent) {
		if (anEvent instanceof MouseEvent) {
			return ((MouseEvent) anEvent).getClickCount() >= 2;
		}
		return true;
	}

}	
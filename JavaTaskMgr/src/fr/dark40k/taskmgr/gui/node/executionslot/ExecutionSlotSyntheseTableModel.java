package fr.dark40k.taskmgr.gui.node.executionslot;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlot;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlotList;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlotList.UpdateEvent;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class ExecutionSlotSyntheseTableModel extends AbstractTableModel implements ExecutionSlotList.UpdateListener {

	private static final String[] columnNames = { "Tache", "D�part", "Dur�e (h)", "Commentaire" };
	private static final Class<?>[] columnClasses = { String.class, LocalDateTime.class, Duration.class, String.class };
	
	private final TaskMgrUndoMgr taskMgrUndoMgr;
	
	private NodeTask nodeTask;
	private final static ArrayList<SyntheseLine> syntheseLinesArray = new ArrayList<SyntheseLine>();
	
	private class SyntheseLine {
		NodeTask nodeTask;
		ExecutionSlot executionSlot;
		public SyntheseLine(NodeTask nodeTask,ExecutionSlot executionSlot) { this.nodeTask=nodeTask; this.executionSlot=executionSlot; }
	}
	
	ExecutionSlotSyntheseTableModel(TaskMgrUndoMgr taskMgrUndoMgr) {
		this.taskMgrUndoMgr=taskMgrUndoMgr;
	}
	
	public void setNode(NodeTask nodeTask) {
		this.nodeTask=nodeTask;
		buildSyntheseTable(nodeTask);
	}
	
	
	private void buildSyntheseTable(NodeTask baseNode) {
		
		clearSyntheseTable();
		
		if (baseNode==null) {
			fireTableDataChanged();
			return;
		}
		
		ArrayDeque<NodeTask> stack = new ArrayDeque<NodeTask>();
		stack.push(baseNode);
		
		while (!stack.isEmpty()) {

			NodeTask node = stack.pollLast();

			node.getExecutionSlotList().addUpdateListener(this);

			for (ExecutionSlot executionSlot : node.getExecutionSlotList())
				syntheseLinesArray.add(new SyntheseLine(node, executionSlot));

			for (Node subNode : node.getChildren())
				stack.push((NodeTask) subNode);

		}
	
	fireTableDataChanged();
		
	}

	public void clearSyntheseTable() {
		for (SyntheseLine line : syntheseLinesArray) line.nodeTask.getExecutionSlotList().removeUpdateListener(this);
		syntheseLinesArray.clear();
	}
	
	public void clearNode() {
		nodeTask=null;
		clearSyntheseTable();
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Class<?> getColumnClass(int col) {
		return columnClasses[col];
	}

	@Override
	public int getRowCount() {
		return syntheseLinesArray.size();
	}

	@Override
	public Object getValueAt(int row, int col) {

		switch (col) {
			case 0 : return syntheseLinesArray.get(row).nodeTask.getName();
			case 1 : return syntheseLinesArray.get(row).executionSlot.getStart();
			case 2 : return syntheseLinesArray.get(row).executionSlot.getDuration();
			case 3 : return syntheseLinesArray.get(row).executionSlot.getComment();
			default : throw new IllegalArgumentException("wrong column number : " + col);
		}
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {
		return (col!=0);	
	}

	@Override
	public void setValueAt(Object value, int row, int col) {

		if (nodeTask==null) return;
		
		switch (col) {
		case 1:
			taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Modifier date execution tache", true);
			syntheseLinesArray.get(row).executionSlot.setStart((LocalDateTime) value);;
			break;
		case 2:
			taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Modifier duree execution tache", true);
			syntheseLinesArray.get(row).executionSlot.setDuration((Duration) value);;
			break;
		case 3:
			taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Modifier commentaire execution tache", true);
			syntheseLinesArray.get(row).executionSlot.setComment((String) value);
			break;
		default:
			throw new IllegalArgumentException("Unsuported column:"+col);
		}
		
	}

	@Override
	public void executionSlotListUpdated(UpdateEvent evt) {
		buildSyntheseTable(nodeTask);
		fireTableDataChanged();
	}

	@Override
	public void executionSlotUpdated(UpdateEvent evt) {
		buildSyntheseTable(nodeTask);
		fireTableDataChanged();
	}

	public Duration getDuration(int row) { 
		SyntheseLine line;
		try {
			line = syntheseLinesArray.get(row);
		} catch (IndexOutOfBoundsException e) {
			return Duration.ZERO;
		}
		return line.executionSlot.getDuration();
	}

	public Duration getTotalDuration() {
		Duration duration = Duration.ZERO;
		for (SyntheseLine line : syntheseLinesArray) duration=duration.plus(line.executionSlot.getDuration());
		return duration;
	}

}

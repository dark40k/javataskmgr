package fr.dark40k.taskmgr.gui.node.executionslot;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.swing.table.AbstractTableModel;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlot;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlotList;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlotList.UpdateEvent;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
 
public class ExecutionSlotListTableModel extends AbstractTableModel implements ExecutionSlotList.UpdateListener {

	private static final String[] columnNames = { "Start", "Dur�e (h)", "Commentaires" };
	private static final Class<?>[] columnClasses = { LocalDateTime.class, Duration.class, String.class };

	private final TaskMgrUndoMgr taskMgrUndoMgr;
	
	private NodeTask nodeTask;
	
	ExecutionSlotListTableModel(TaskMgrUndoMgr taskMgrUndoMgr) {
		this.taskMgrUndoMgr=taskMgrUndoMgr;
	}
	
	public void setNode(NodeTask nodeTask) {
		if (nodeTask!=null) nodeTask.getExecutionSlotList().removeUpdateListener(this);
		this.nodeTask = nodeTask;
		if (nodeTask!=null) nodeTask.getExecutionSlotList().addUpdateListener(this);
		fireTableDataChanged();
	}
	
	public void clearNode() {
		setNode(null);
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Class<?> getColumnClass(int col) {
		return columnClasses[col];
	}

	@Override
	public int getRowCount() {
		if (nodeTask==null) return 0;
		return nodeTask.getExecutionSlotList().size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		if (nodeTask==null) return null;
		ExecutionSlot executionSlot = nodeTask.getExecutionSlotList().get(row);
		if (executionSlot==null) return null;
		switch (col) {
			case 0 : return executionSlot.getStart();
			case 1 : return executionSlot.getDuration();
			case 2 : return executionSlot.getComment();
			default : throw new IllegalArgumentException("wrong column number : " + col);
		}
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {
		return true;	
	}

	@Override
	public void setValueAt(Object value, int row, int col) {

		if (nodeTask==null) return;

		switch (col) {
		case 0:
			taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Modifier date execution tache", true);
			nodeTask.getExecutionSlotList().get(row).setStart((LocalDateTime) value);;
			break;
		case 1:
			taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Modifier duree execution tache", true);
			nodeTask.getExecutionSlotList().get(row).setDuration((Duration) value);;
			break;
		case 2:
			taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Modifier commentaire execution tache", true);
			nodeTask.getExecutionSlotList().get(row).setComment((String) value);
			break;
		default:
			throw new IllegalArgumentException("Unsuported column:"+col);
		}
		
	}

	@Override
	public void executionSlotListUpdated(UpdateEvent evt) {
		fireTableDataChanged();
	}

	@Override
	public void executionSlotUpdated(UpdateEvent evt) {
		fireTableDataChanged();
	}

}

package fr.dark40k.taskmgr.gui.node.executionslot;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.swing.DefaultCellEditor;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SortOrder;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableRowSorter;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.FormattersString;

public class ExecutionSlotSynthesePanel extends JPanel {

	private final JTable table;
	private final ExecutionSlotSyntheseTableModel model;
	private final TableRowSorter<ExecutionSlotSyntheseTableModel> sorter;
	
	private NodeTask nodeTask;
	
	private JLabel lblNewLabel;
	

	/**
	 * Create the panel.
	 * @param pf2 
	 */
	public ExecutionSlotSynthesePanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		
		model = new ExecutionSlotSyntheseTableModel(taskMgrUndoMgr);
		table = new JTable(model);
		sorter = new TableRowSorter<ExecutionSlotSyntheseTableModel>(model);
		
		initGUI();
				
		table.getColumnModel().getColumn(0).setPreferredWidth(100);
		table.getColumnModel().getColumn(1).setPreferredWidth(100);
		table.getColumnModel().getColumn(2).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setPreferredWidth(100);

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent lse) {
		        if (!lse.getValueIsAdjusting()) {
		        	updateTotal();
		        }
		    }
		});
		
		table.getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent arg0) {
				updateTotal();
			}
		});
		
		((DefaultCellEditor) table.getDefaultEditor(Object.class)).setClickCountToStart(2);
		table.setDefaultRenderer(LocalDateTime.class, new LocalDateTimeCellRenderer());
		table.setDefaultRenderer(Duration.class, new DurationCellRenderer());
		table.setDefaultEditor(LocalDateTime.class, new LocalDateTimeCellEditor());
		table.setDefaultEditor(Duration.class, new DurationCellEditor());
		table.getTableHeader().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (table.getRowSorter() != null) {
					if (table.getRowSorter().getSortKeys().size() > 0)
						if (table.getRowSorter().getSortKeys().get(0).getSortOrder() == SortOrder.DESCENDING)
							table.setRowSorter(null);
				} else {
					table.setRowSorter(sorter);
				}
			}
		});
		
	}

	public void setNode(Node node) {
		
		this.nodeTask = (node instanceof NodeTask) ? (NodeTask) node : null;
		
		model.setNode(this.nodeTask);
		
		updateTotal();
	}

	public void updateTotal() {
		if (nodeTask==null) {
			lblNewLabel.setText("Total = ");
			return;
		}
    	if (table.getSelectedRowCount()>0) {
    		Duration totalDuration = Duration.ZERO;
    		for (int row : table.getSelectedRows())
    			totalDuration=totalDuration.plus(model.getDuration(table.convertRowIndexToModel(row)));
    		lblNewLabel.setText("Total (selection) = "+ FormattersString.format(totalDuration,"<pas de valeur>"));
    	} else {
    		lblNewLabel.setText("Total = "+FormattersString.format(model.getTotalDuration(),"<pas de valeur>"));
    	}
	}
	
	
	public void initGUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 100, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(3, 3, 3, 3);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		add(scrollPane, gbc_scrollPane);
		
		scrollPane.setViewportView(table);
		
		lblNewLabel = new JLabel("Total =");
		lblNewLabel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(3, 3, 3, 3);
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		add(lblNewLabel, gbc_lblNewLabel);
	}
	
	public Integer getSelectedCategoryIndex() {
		if (nodeTask==null) return -1;
		int row=table.getSelectedRow();
		if (row<0) return -1;
		return table.convertRowIndexToModel(row);
	}


}

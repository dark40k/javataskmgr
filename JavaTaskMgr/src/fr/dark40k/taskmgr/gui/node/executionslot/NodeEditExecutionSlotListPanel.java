package fr.dark40k.taskmgr.gui.node.executionslot;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeEditExecutionSlotListPanel extends JPanel {

	public final static String XMLTAG="nodeedittimerspanel";
	
	private final TaskMgrUndoMgr taskMgrUndoMgr;

	private JSplitPane splitPane;
	private JPanel panel_2;
	
	private ExecutionSlotListPanel nodeExecutionSlotListPanel;
	private ExecutionSlotSynthesePanel nodeExecutionSlotSynthesePanel;
	
	/**
	 * Create the panel.
	 */
	public NodeEditExecutionSlotListPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		
		this.taskMgrUndoMgr=taskMgrUndoMgr;

		initGUI();
		
		setNode(null);
	}
	
	public void setNode(Node node) {
		nodeExecutionSlotListPanel.setNode(node);
		nodeExecutionSlotSynthesePanel.setNode(node);
	}
	
	private void initGUI() {

		setLayout(new BorderLayout(0, 0));

		splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.25);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		add(splitPane);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Execution directe", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		splitPane.setLeftComponent(panel);
		panel.setLayout(new BorderLayout(0, 2));

		
		nodeExecutionSlotListPanel = new ExecutionSlotListPanel(taskMgrUndoMgr);
		panel.add(nodeExecutionSlotListPanel, BorderLayout.CENTER);
		
		panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Execution totale (incluant descendants)", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		splitPane.setRightComponent(panel_2);
		panel_2.setLayout(new BorderLayout(0, 2));
		
		nodeExecutionSlotSynthesePanel = new ExecutionSlotSynthesePanel(taskMgrUndoMgr);
		panel_2.add(nodeExecutionSlotSynthesePanel, BorderLayout.CENTER);
		
	}

	//
	// Saving / Loading graphic configuration
	//
	
	public void importXML(Element baseElt) {

		Element elt = baseElt.getChild(XMLTAG); 
		if (elt==null) return;
		
		splitPane.setDividerLocation(parseInt(elt.getAttributeValue("divider"), splitPane.getDividerLocation()));
		
	}
	
	public Element exportXML() {
		
		Element elt=new Element(XMLTAG);
		
		elt.setAttribute("divider", Integer.toString(splitPane.getDividerLocation()));
        
		return elt;
	}
	
	private int parseInt(String val, int def) {
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException e) {
			return def;
		}
	}
	
}

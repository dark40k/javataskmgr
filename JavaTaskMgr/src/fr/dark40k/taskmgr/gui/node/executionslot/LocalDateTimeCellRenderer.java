package fr.dark40k.taskmgr.gui.node.executionslot;

import java.awt.Component;
import java.time.LocalDateTime;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.dark40k.util.FormattersString;

public class LocalDateTimeCellRenderer extends DefaultTableCellRenderer {
		
	@Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {
		return super.getTableCellRendererComponent(table, FormattersString.format((LocalDateTime)value, "<vide>"), isSelected, hasFocus, rowIndex, columnIndex);
  	}
	
}


package fr.dark40k.taskmgr.gui.node.properties;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.SwingUtilities;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.interfaces.NodeHiddenInterface;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class HiddenPropPanel extends PropPanel {

	private Node node;

	private JCheckBox chckbxHidden;

	private TaskMgrUndoMgr	taskMgrUndoMgr;

	public HiddenPropPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		this.taskMgrUndoMgr=taskMgrUndoMgr;

		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(0);
		flowLayout.setHgap(0);

		chckbxHidden = new JCheckBox("Cach�");
		add(chckbxHidden);
	}


	@Override
	public void setNode(Node newNode) {

		if (node!=null) clearNode();

		node = newNode;

		chckbxHidden.setSelected( ((NodeHiddenInterface) newNode).isHidden());

		chckbxHidden.addItemListener(chckbxHidden_Listener);

	}

	public void clearNode() {
		chckbxHidden.removeItemListener(chckbxHidden_Listener);
		node=null;
	}

	// StrikeThrough

	private ItemListener chckbxHidden_Listener = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (((NodeHiddenInterface) node).isHidden() != chckbxHidden.isSelected()) updateHidden();
		}
	};

	private void updateHidden() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier param�tres",false);
	        	((NodeHiddenInterface) node).setHidden(chckbxHidden.isSelected());
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}


}

package fr.dark40k.taskmgr.gui.node.properties;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import fr.dark40k.taskmgr.database.NodeFilter;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeEditFilterPanel extends JPanel implements TaskMgrDB.UpdateNodeListener {

	private NodeFilter nodeFilter;
	private JTextField filterTextField;
	private JButton btnEditer;

	private TaskMgrUndoMgr taskMgrUndoMgr;

	/**
	 * Create the panel.
	 */
	public NodeEditFilterPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		this.taskMgrUndoMgr=taskMgrUndoMgr;

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);

		JLabel lblFiltre = new JLabel("Filtre :");
		GridBagConstraints gbc_lblFiltre = new GridBagConstraints();
		gbc_lblFiltre.insets = new Insets(0, 0, 0, 5);
		gbc_lblFiltre.anchor = GridBagConstraints.EAST;
		gbc_lblFiltre.gridx = 0;
		gbc_lblFiltre.gridy = 0;
		add(lblFiltre, gbc_lblFiltre);

		filterTextField = new JTextField();
		filterTextField.setText("OR( \"Toujours Visible\", \"Action\" )");
		filterTextField.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		filterTextField.setEditable(false);
		GridBagConstraints gbc_filterTextField = new GridBagConstraints();
		gbc_filterTextField.insets = new Insets(0, 0, 0, 5);
		gbc_filterTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_filterTextField.gridx = 1;
		gbc_filterTextField.gridy = 0;
		add(filterTextField, gbc_filterTextField);
		filterTextField.setColumns(10);

		btnEditer = new JButton("Editer");
		btnEditer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				cmdEditFilter();
			}
		});
		btnEditer.setEnabled(false);
		GridBagConstraints gbc_btnEditer = new GridBagConstraints();
		gbc_btnEditer.gridx = 2;
		gbc_btnEditer.gridy = 0;
		add(btnEditer, gbc_btnEditer);

	}

	public void setNode(NodeFilter newNodeFilter) {

		if (nodeFilter!=null) clearNode();

		nodeFilter = newNodeFilter;

		updateTextField();

		btnEditer.setEnabled(!newNodeFilter.isReadOnly());
		newNodeFilter.getDataBase().addUpdateListener(this);

	}

	public void clearNode() {
		if (nodeFilter!=null) nodeFilter.getDataBase().removeUpdateListener(this);

		nodeFilter=null;

		updateTextField();

		btnEditer.setEnabled(false);
	}

	private void cmdEditFilter() {
		String newFilterText = (String)JOptionPane.showInputDialog(
                this,
                "Entrer le nouveau filtre :\nFonctions OR, AND, NOR, NAND\nExemple:\n OR(cat00,AND(cat01,cat02),NOR(cat03))",
                "Edition filtre - "+nodeFilter.getName(),
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                nodeFilter.getFilterText());

		if (newFilterText==null) return;

		newFilterText=newFilterText.replaceAll("\\s+", "");
		if (newFilterText.length()==0) newFilterText=null;

		nodeFilter.getDataBase().removeUpdateListener(this);
		taskMgrUndoMgr.addUndoableNodeEdit(nodeFilter, "Edition du filtre", true);
		nodeFilter.setFilterText(newFilterText);
		nodeFilter.getDataBase().addUpdateListener(this);

		updateTextField();
	}

	private void updateTextField() {

		if (nodeFilter==null) {
			filterTextField.setText(null);
			filterTextField.setBackground(SystemColor.control);
			return;
		}

		if (nodeFilter.getFilterText()==null) {
			filterTextField.setText("<No filter defined>");
			filterTextField.setBackground(SystemColor.control);
			return;
		}

		filterTextField.setText(nodeFilter.getFilterText());
		filterTextField.setBackground(nodeFilter.isFilterValid()?Color.GREEN:Color.RED);
	}

	//
	// Node change handling
	//

	@Override
	public void updateDataBase(UpdateNodeEvent evt) {
		setNode(nodeFilter);
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==nodeFilter.getKey()) setNode(nodeFilter);
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==nodeFilter.getKey()) setNode(nodeFilter);
	}



}

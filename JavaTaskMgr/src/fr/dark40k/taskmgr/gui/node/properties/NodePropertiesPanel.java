package fr.dark40k.taskmgr.gui.node.properties;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeFilter;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.interfaces.NodeDefaultTemplateStatusInterface;
import fr.dark40k.taskmgr.database.interfaces.NodeHiddenInterface;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodePropertiesPanel extends JPanel {
	
	private ReadOnlyPropPanel readOnlyPropPanel;

	private JPanel	templatePanel;
	private JLabel lblTemplate;
	private NodeSelectTemplatePanel	nodeSelectTemplatePanel;

	private JPanel	statusPanel;
	private JLabel lblStatus;
	private NodeSelectStatusPanel	nodeSelectStatusPanel;

	private HiddenPropPanel	hiddenPropPanel;

	private JPanel	defaultTemplatePanel;
	private JLabel	lblDefaultTemplate;
	private NodeSelectDefaultTemplatePanel	nodeSelectDefaultTemplatePanel;
	
	private JPanel	defaultStatusPanel;
	private JLabel	lblDefaultStatus;
	private NodeSelectDefaultStatusPanel	nodeSelectDefaultStatusPanel;
	
	private NodeEditFilterPanel nodeEditFilterPanel;

	public NodePropertiesPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		gridBagLayout.columnWeights = new double[]{0.0};
		setLayout(gridBagLayout);
		
		//
		// ReadOnly
		//
			GridBagConstraints gbc_1 = new GridBagConstraints();
			gbc_1.insets = new Insets(0, 0, 5, 0);
			gbc_1.gridx = 0;
			gbc_1.gridy = 0;
			gbc_1.anchor = GridBagConstraints.WEST;
			gbc_1.gridwidth = GridBagConstraints.REMAINDER;
			gbc_1.weightx = 1.0;
			
			readOnlyPropPanel=new ReadOnlyPropPanel(taskMgrUndoMgr);
			
		add(readOnlyPropPanel, gbc_1);
		
		//
		// Template
		//
			GridBagConstraints gbc_templatePanel = new GridBagConstraints();
			gbc_templatePanel.insets = new Insets(0, 0, 5, 0);
			gbc_templatePanel.gridx = 0;
			gbc_templatePanel.gridy = 1;
			gbc_templatePanel.anchor = GridBagConstraints.WEST;
			gbc_templatePanel.gridwidth = GridBagConstraints.REMAINDER;
			gbc_templatePanel.weightx = 1.0;
			
			templatePanel = new JPanel();
			lblTemplate = new JLabel("Mod\u00E8le :");
			templatePanel.add(lblTemplate);
			nodeSelectTemplatePanel = new NodeSelectTemplatePanel(taskMgrUndoMgr);
			templatePanel.add(nodeSelectTemplatePanel);
			
		add(templatePanel, gbc_templatePanel);
		
		//
		// Status
		//
			GridBagConstraints gbc_statusPanel = new GridBagConstraints();
			gbc_statusPanel.insets = new Insets(0, 0, 5, 0);
			gbc_statusPanel.gridx = 0;
			gbc_statusPanel.gridy = 2;
			gbc_statusPanel.anchor = GridBagConstraints.WEST;
			gbc_statusPanel.gridwidth = GridBagConstraints.REMAINDER;
			gbc_statusPanel.weightx = 1.0;
			
			statusPanel = new JPanel();
			lblStatus = new JLabel("Status :");
			statusPanel.add(lblStatus);
			nodeSelectStatusPanel = new NodeSelectStatusPanel(taskMgrUndoMgr);
			statusPanel.add(nodeSelectStatusPanel);
			
		add(statusPanel,gbc_statusPanel);
		
		//
		// Hidden
		//
			GridBagConstraints gbc_4 = new GridBagConstraints();
			gbc_4.insets = new Insets(0, 0, 5, 0);
			gbc_4.gridx = 0;
			gbc_4.gridy = 3;
			gbc_4.anchor = GridBagConstraints.WEST;
			gbc_4.gridwidth = GridBagConstraints.REMAINDER;
			gbc_4.weightx = 1.0;
			
			hiddenPropPanel=new HiddenPropPanel(taskMgrUndoMgr);
			
		add(hiddenPropPanel,gbc_4);
	
		//
		// Default Template
		//
			GridBagConstraints gbc_defaultTemplatePanel = new GridBagConstraints();
			gbc_defaultTemplatePanel.insets = new Insets(0, 0, 5, 0);
			gbc_defaultTemplatePanel.gridx = 0;
			gbc_defaultTemplatePanel.gridy = 4;
			gbc_defaultTemplatePanel.anchor = GridBagConstraints.WEST;
			gbc_defaultTemplatePanel.gridwidth = GridBagConstraints.REMAINDER;
			gbc_defaultTemplatePanel.weightx = 1.0;
			
			defaultTemplatePanel = new JPanel();
			lblDefaultTemplate = new JLabel("Mod\u00E8le pour les enfants :");
			defaultTemplatePanel.add(lblDefaultTemplate);
			nodeSelectDefaultTemplatePanel = new NodeSelectDefaultTemplatePanel(taskMgrUndoMgr);
			defaultTemplatePanel.add(nodeSelectDefaultTemplatePanel);
			
		add(defaultTemplatePanel, gbc_defaultTemplatePanel);

		//
		// Default Status
		//
			GridBagConstraints gbc_defaultStatusPanel = new GridBagConstraints();
			gbc_defaultStatusPanel.insets = new Insets(0, 0, 5, 0);
			gbc_defaultStatusPanel.gridx = 0;
			gbc_defaultStatusPanel.gridy = 5;
			gbc_defaultStatusPanel.anchor = GridBagConstraints.WEST;
			gbc_defaultStatusPanel.gridwidth = GridBagConstraints.REMAINDER;
			gbc_defaultStatusPanel.weightx = 1.0;
			
			defaultStatusPanel = new JPanel();
			lblDefaultStatus = new JLabel("Status pour les enfants :");
			defaultStatusPanel.add(lblDefaultStatus);
			nodeSelectDefaultStatusPanel = new NodeSelectDefaultStatusPanel(taskMgrUndoMgr);
			defaultStatusPanel.add(nodeSelectDefaultStatusPanel);
			
		add(defaultStatusPanel, gbc_defaultStatusPanel);

		nodeEditFilterPanel = new NodeEditFilterPanel(taskMgrUndoMgr);
		GridBagConstraints gbc_nodeEditFilterPanel = new GridBagConstraints();
		gbc_nodeEditFilterPanel.fill = GridBagConstraints.BOTH;
		gbc_nodeEditFilterPanel.gridx = 0;
		gbc_nodeEditFilterPanel.gridy = 6;
		add(nodeEditFilterPanel, gbc_nodeEditFilterPanel);
		
		//
		// Fin initialisation
		//
		
		clearNode();
	
	}
	
	
	public void setNode(Node node) {
		
		clearNode();
		
		if (node==null) return;
		
		readOnlyPropPanel.setNode(node);
		
		if (node instanceof NodeTask) {
			templatePanel.setVisible(true);
			nodeSelectTemplatePanel.setNode(node);
			
			statusPanel.setVisible(true);
			nodeSelectStatusPanel.setNode(node);
			
		} 		
		
		if (node instanceof NodeHiddenInterface) {
			hiddenPropPanel.setVisible(true);
			hiddenPropPanel.setNode(node);
		}
		
		if (node instanceof NodeDefaultTemplateStatusInterface) {
			defaultTemplatePanel.setVisible(true);
			nodeSelectDefaultTemplatePanel.setNode(node);
			defaultStatusPanel.setVisible(true);
			nodeSelectDefaultStatusPanel.setNode(node);
		}
		
		if (node instanceof NodeFilter) {
			hiddenPropPanel.setVisible(true);
			hiddenPropPanel.setNode(node);
			nodeEditFilterPanel.setVisible(true);
			nodeEditFilterPanel.setNode((NodeFilter) node);
		}
		
	}

	private void clearNode() {
		readOnlyPropPanel.clearNode();
		
		templatePanel.setVisible(false);
		nodeSelectTemplatePanel.setNode(null);
		
		statusPanel.setVisible(false);
		nodeSelectStatusPanel.setNode(null);
		
		hiddenPropPanel.setVisible(false);
		hiddenPropPanel.clearNode();
		
		defaultTemplatePanel.setVisible(false);
		nodeSelectDefaultTemplatePanel.clearNode();
		
		defaultStatusPanel.setVisible(false);
		nodeSelectDefaultStatusPanel.clearNode();
		
		nodeEditFilterPanel.setVisible(false);
		nodeEditFilterPanel.clearNode();
		
		
	}
	
	
}

package fr.dark40k.taskmgr.gui.node.properties;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTemplate;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.database.interfaces.NodeDefaultTemplateStatusInterface;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeSelectDefaultStatusPanel extends NodeSelectPanelBase implements TaskMgrDB.UpdateNodeListener {

	Node node;
	NodeDefaultTemplateStatusInterface nodeTemplateDefaultStatus;

	private TaskMgrUndoMgr	taskMgrUndoMgr;
	
	public NodeSelectDefaultStatusPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		super();
		this.taskMgrUndoMgr=taskMgrUndoMgr;
	}

	public void setNode(Node newNode) {
		
		clearNode();
		
		if (!(newNode instanceof NodeDefaultTemplateStatusInterface)) return;

		this.node=newNode;

		nodeTemplateDefaultStatus=(NodeDefaultTemplateStatusInterface) newNode;
		
		NodeTemplate nodeTemplate = (NodeTemplate) newNode.getDataBase().getNode(nodeTemplateDefaultStatus.getChildDefaultTemplateKey());
		if (nodeTemplate!=null)
			initComboList(nodeTemplateDefaultStatus.getChildDefaultStatusKey(), newNode.getDataBase().getRootStatus(), nodeTemplate.getRecursiveCategoriesNames(), true);
		else
			initComboList(nodeTemplateDefaultStatus.getChildDefaultStatusKey(), newNode.getDataBase().getRootStatus(), null, true);
		
		comboBox.setEnabled(!newNode.isReadOnly());
		node.getDataBase().addUpdateListener(this);
	}
	
	public void clearNode() {
		if (node!=null) node.getDataBase().removeUpdateListener(this);
		super.clearComboList();
	}
	
	//
	// Suivi de la comboBox
	//
	
	protected void comboChangedAction(Integer newKey) {
		node.getDataBase().removeUpdateListener(this);
		taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier mod�le",false);
		nodeTemplateDefaultStatus.setChildDefaultStatusKey(newKey);
		node.getDataBase().addUpdateListener(this);
	}

	
	
	//
	// Node change handling
	//
	
	@Override
	public void updateDataBase(UpdateNodeEvent evt) {
		setNode(node);
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) setNode(node);
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) setNode(node);
	}	
	
}

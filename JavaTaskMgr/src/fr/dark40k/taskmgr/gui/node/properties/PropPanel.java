package fr.dark40k.taskmgr.gui.node.properties;

import javax.swing.JPanel;

import fr.dark40k.taskmgr.database.Node;

public abstract class PropPanel extends JPanel {

	public abstract void setNode(Node node);
	
}

package fr.dark40k.taskmgr.gui.node.properties;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.SwingUtilities;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class ReadOnlyPropPanel extends PropPanel {

	private Node node;

	private JCheckBox chckbxReadOnly;

	private TaskMgrUndoMgr	taskMgrUndoMgr;

	public ReadOnlyPropPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		this.taskMgrUndoMgr=taskMgrUndoMgr;

		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(0);
		flowLayout.setHgap(0);

		chckbxReadOnly = new JCheckBox("Lecture seule");
		add(chckbxReadOnly);
	}


	@Override
	public void setNode(Node newNode) {

		if (node!=null) clearNode();

		node = newNode;

		chckbxReadOnly.setSelected(newNode.isReadOnly());

		chckbxReadOnly.addItemListener(chckbxReadOnly_Listener);

	}

	public void clearNode() {
		chckbxReadOnly.removeItemListener(chckbxReadOnly_Listener);
		node=null;
	}

	// StrikeThrough

	private ItemListener chckbxReadOnly_Listener = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (node.isReadOnly() != chckbxReadOnly.isSelected()) updateReadOnly();
		}
	};

	private void updateReadOnly() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier paramètres",false);
	        	node.setReadOnly(chckbxReadOnly.isSelected());
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}


}

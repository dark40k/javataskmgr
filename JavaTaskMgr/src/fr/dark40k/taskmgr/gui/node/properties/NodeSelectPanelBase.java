package fr.dark40k.taskmgr.gui.node.properties;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JComboBox;
import javax.swing.SwingUtilities;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeFilter;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.interfaces.NodeHiddenInterface;
import fr.dark40k.taskmgr.util.StringFormulaFilter;

public abstract class NodeSelectPanelBase extends PropPanel {

	protected ArrayList<Integer> subKeysList;
	
	protected JComboBox<String> comboBox;
	
	protected Integer comboBoxSelIndex;
	
	/**
	 * Create the panel.
	 * @param string2 
	 * @param string 
	 */
	public NodeSelectPanelBase() {
		
		setLayout(new BorderLayout(0, 0));
		
		comboBox = new JComboBox<String>();
		add(comboBox, BorderLayout.CENTER);
		comboBox.setEnabled(false);
	}

	protected void initComboList(Integer selectedKey, Node listRootNode, Set<String> categoriesList, boolean noNode) {

		if (listRootNode==null) {
			clearComboList();
			return;
		}
			
		if (subKeysList!=null)
			comboBox.removeActionListener(comboBox_Listener);
		
		comboBox.setEnabled(true);
		
		// extrait la table des liens pour la combo
		subKeysList=listRootNode.getKeyAndChildrenKeys(false);

		// supprime les liens cach�s (en partant du dernier)
		for (int i=subKeysList.size()-1; i>=0; i--)
			if ( ( ( (NodeHiddenInterface) listRootNode.getDataBase().getNode(subKeysList.get(i)) ).isHidden() ) && (subKeysList.get(i) != selectedKey))
				subKeysList.remove(i);

		// supprime les liens qui ne passent pas le filtre (en partant du dernier)
		if (listRootNode instanceof NodeFilter) {
			for (int i = subKeysList.size() - 1; i >= 0; i--) {
				StringFormulaFilter filter = ((NodeFilter) listRootNode.getDataBase().getNode(subKeysList.get(i))).getFilter();
				if (filter != null)
					if ((subKeysList.get(i) != selectedKey) && (!filter.checkStringSet(categoriesList)))
						subKeysList.remove(i);
			}
		}

		// recalcule les lignes de la combo
		ArrayList<String> newComboList = new ArrayList<String>();
		
		for (Integer subKey : subKeysList) 
			newComboList.add(listRootNode.getDataBase().getNode(subKey).getStackedName(", "));

		if (noNode) {
			subKeysList.add(TaskMgrDB.NO_NODE_KEY);
			newComboList.add("<Pas de selection>");
		}

		if (!subKeysList.contains(selectedKey)) {
			subKeysList.add(selectedKey);
			newComboList.add("<Invalid key = "+selectedKey+">");
		}
		
		// verifie et met a jour le contenu de la combo si necessaire
		if (comboContentHasChanges(newComboList)) {
			comboBox.removeAllItems();
			for (String newComboLine : newComboList)
				comboBox.addItem(newComboLine);
		}
		
		// met a jour la selection
		comboBoxSelIndex=subKeysList.indexOf(selectedKey);
		if (comboBoxSelIndex<0) throw  new IndexOutOfBoundsException("selectedKey is not child of listRootNode");

		comboBox.setSelectedIndex(comboBoxSelIndex);
		
		// remet en place le listener
		comboBox.addActionListener(comboBox_Listener);

	}
	
	// verifie si la boite de dialog a besoin d'etre mise � jours
	private boolean comboContentHasChanges(ArrayList<String> newComboList) {
		
		if ( comboBox.getModel().getSize() !=  newComboList.size() ) return true;
		
		for (int i=0;i<newComboList.size();i++)
			if (!comboBox.getModel().getElementAt(i).equals(newComboList.get(i)))
				return true;
		
		return false;
	}
	
	protected void clearComboList() {
		if (subKeysList==null) return;
		this.subKeysList=null;
		comboBox.removeActionListener(comboBox_Listener);
		comboBox.removeAllItems();
		comboBox.setEnabled(false);
	}
	
	//
	// Suivi de la comboBox
	//
	
	private ActionListener comboBox_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (comboBoxSelIndex!=comboBox.getSelectedIndex()) doAction();
		}
	};

	private void doAction() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {    	
	        	comboBoxSelIndex=comboBox.getSelectedIndex();
	        	comboChangedAction(subKeysList.get(comboBoxSelIndex));
	        	}
	    };       
	    SwingUtilities.invokeLater(doHighlight);
	}	

	protected abstract void comboChangedAction(Integer newKey);

}

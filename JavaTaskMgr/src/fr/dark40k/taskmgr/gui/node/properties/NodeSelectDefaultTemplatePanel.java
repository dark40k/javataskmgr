package fr.dark40k.taskmgr.gui.node.properties;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.database.interfaces.NodeDefaultTemplateStatusInterface;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeSelectDefaultTemplatePanel extends NodeSelectPanelBase implements TaskMgrDB.UpdateNodeListener {

	Node node;
	NodeDefaultTemplateStatusInterface nodeDefaultTemplate;
	
	private TaskMgrUndoMgr	taskMgrUndoMgr;
	
	public NodeSelectDefaultTemplatePanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		super();
		this.taskMgrUndoMgr=taskMgrUndoMgr;
	}

	public void setNode(Node newNode) {
		
		clearNode();
		
		if (!(newNode instanceof NodeDefaultTemplateStatusInterface)) return;
		
		this.node=newNode;
		
		nodeDefaultTemplate=(NodeDefaultTemplateStatusInterface) newNode;
		initComboList(nodeDefaultTemplate.getChildDefaultTemplateKey(), newNode.getDataBase().getRootTemplate(), null, true);
		
		comboBox.setEnabled(!newNode.isReadOnly());
		node.getDataBase().addUpdateListener(this);
	}
	
	public void clearNode() {
		if (node!=null) node.getDataBase().removeUpdateListener(this);
		super.clearComboList();
	}
	
	//
	// Suivi de la comboBox
	//
	
	protected void comboChangedAction(Integer newKey) {
		node.getDataBase().removeUpdateListener(this);
		taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier mod�le",false);
		nodeDefaultTemplate.setChildDefaultTemplateKey(newKey);
		node.getDataBase().addUpdateListener(this);
	}

	
	
	//
	// Node change handling
	//
	
	@Override
	public void updateDataBase(UpdateNodeEvent evt) {
		setNode(node);
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) setNode(node);
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) setNode(node);
	}	
	
}

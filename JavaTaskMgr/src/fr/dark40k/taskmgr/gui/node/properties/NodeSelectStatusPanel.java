package fr.dark40k.taskmgr.gui.node.properties;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeSelectStatusPanel extends NodeSelectPanelBase implements TaskMgrDB.UpdateNodeListener {

	NodeTask node;
	private TaskMgrUndoMgr	taskMgrUndoMgr;
	
	public NodeSelectStatusPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		super();
		this.taskMgrUndoMgr=taskMgrUndoMgr;
	}

	public void setNode(Node newNode) {

		if (node!=null) node.getDataBase().removeUpdateListener(this);

		if (!(newNode instanceof NodeTask)) {
			super.clearComboList();
			return;
		}
		
		NodeTask newNodeTask=(NodeTask) newNode;
		initComboList(newNodeTask.getStatusKey(), newNodeTask.getDataBase().getRootStatus(),newNodeTask.getTemplate().getRecursiveCategoriesNames(), false);
		this.node=newNodeTask;
		
		comboBox.setEnabled(!newNode.isReadOnly());
		node.getDataBase().addUpdateListener(this);

	}
	
	//
	// Suivi de la comboBox
	//
	
	protected void comboChangedAction(Integer newKey) {
		node.getDataBase().removeUpdateListener(this);
		taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier Status",false);
    	node.setStatusKey(newKey);
		node.getDataBase().addUpdateListener(this);
	}
	
	//
	// Node change handling
	//
	
	@Override
	public void updateDataBase(UpdateNodeEvent evt) {
		setNode(node);
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) setNode(node);
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==node.getKey()) setNode(node);
	}	
	
}

package fr.dark40k.taskmgr.gui.node.display;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.display.attr.Attr.UpdateEvent;
import fr.dark40k.taskmgr.database.display.attr.Attr.UpdateListener;
import fr.dark40k.taskmgr.database.display.attr.AttrIcon;
import fr.dark40k.taskmgr.database.nodeicons.NodeIconDB;
import fr.dark40k.taskmgr.gui.dialogs.NodeIconSelectDialog;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeAttrIconPanel extends JPanel implements UpdateListener {

	private JSpinner spinner_priority;

	private JTextField textField;

	private JButton buttonFileSelect;
	private JButton buttonFileClear;

	private AttrIcon	attrIcon;

	private TaskMgrUndoMgr	taskMgrUndoMgr;

	private Node	node;

	/**
	 * Create the panel.
	 */
	public NodeAttrIconPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		this.taskMgrUndoMgr=taskMgrUndoMgr;

		setAlignmentY(Component.TOP_ALIGNMENT);
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		spinner_priority = new JSpinner();
		spinner_priority.setMinimumSize(new Dimension(50, 20));
		spinner_priority.setModel(new SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(-1), null, Integer.valueOf(1)));
		spinner_priority.setPreferredSize(new Dimension(50, 20));
		add(spinner_priority);

		textField = new JTextField();
		textField.setColumns(30);
		textField.setEditable(false);
		add(textField);

		buttonFileSelect = new JButton("");
		buttonFileSelect.setIcon(new ImageIcon(NodeAttrIconPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/folder-yellow_open.png")));
		add(buttonFileSelect);

		buttonFileClear = new JButton("");
		buttonFileClear.setIcon(new ImageIcon(NodeAttrIconPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-delete-6.png")));
		add(buttonFileClear);

	}

	//
	// Connection de la forme
	//


	public void setAttrIcon(Node node, AttrIcon attrIcon) {

		clearAttrColor();

		this.attrIcon=attrIcon;
		this.node=node;

		spinner_priority.setValue(attrIcon.getPriority());

		textField.setText((attrIcon.getNodeIcon()!=null) ? attrIcon.getNodeIcon().getName() : "<Pas d'icone selectionnée>");

		spinner_priority.addChangeListener(spinner_priority_Listener);
		buttonFileSelect.addActionListener(buttonFileSelect_Action_Listener);
		buttonFileClear.addActionListener(buttonFileClear_Action_Listener);

		attrIcon.addUpdateListener(this);

	}

	public void clearAttrColor() {

		if (attrIcon!=null) attrIcon.removeUpdateListener(this);

		attrIcon=null;
		node=null;

		spinner_priority.removeChangeListener(spinner_priority_Listener);
		buttonFileSelect.removeActionListener(buttonFileSelect_Action_Listener);
		buttonFileClear.removeActionListener(buttonFileClear_Action_Listener);
		textField.setText("");

	}

	//
	// gestion de la priorite
	//

	private ChangeListener spinner_priority_Listener = new ChangeListener(){
		@Override
		public void stateChanged(ChangeEvent e) {
			updatePriority();
		}
	};

	private void updatePriority() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage priorite icone", true);
	        	attrIcon.removeUpdateListener(NodeAttrIconPanel.this);
	        	attrIcon.setPriority((Integer)spinner_priority.getValue());
	        	attrIcon.addUpdateListener(NodeAttrIconPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

//	// Gestion du bouton de selection de fichier
//	private ActionListener buttonFileSelect_Action_Listener = new ActionListener() {
//		public void actionPerformed(ActionEvent arg0) {
//
//			JFileChooser chooser = new JFileChooser();
//			chooser.setCurrentDirectory(TaskMgr.currentDirectory);
//			chooser.setFileFilter(iconFileFilter);
//
//			int res = chooser.showOpenDialog(null);
//			if (res != JFileChooser.APPROVE_OPTION) return;
//
//			File sgFile = chooser.getSelectedFile();
//			if (sgFile == null) {
//				System.err.println("No file selected");
//				return;
//			}
//
//
//			updateIcon(sgFile);
//
//		}
//	};

	// Gestion du bouton de selection de fichier
	private ActionListener buttonFileSelect_Action_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {

			NodeIconSelectDialog chooser = new NodeIconSelectDialog();

			Integer res = chooser.showSelectDialog(NodeAttrIconPanel.this, taskMgrUndoMgr ,node.getDataBase().getNodeIconsDB(), (attrIcon.getNodeIcon()!=null) ? attrIcon.getNodeIcon().getKey() : NodeIconDB.NO_ICON_KEY);

			if (res != NodeIconSelectDialog.APPROVE_OPTION) return;

			updateIcon(chooser.getSelectedKey());

		}
	};


	private void updateIcon(final Integer newKey) {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage icone",false);
	        	attrIcon.removeUpdateListener(NodeAttrIconPanel.this);
	        	attrIcon.setNodeIcon(node.getDataBase().getNodeIconsDB().getNodeIcon(newKey));
	    		textField.setText((attrIcon.getNodeIcon()!=null) ? attrIcon.getNodeIcon().getName() : "<Pas d'icone selectionnée>");
	        	attrIcon.addUpdateListener(NodeAttrIconPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	// Gestion du bouton de selection de fichier
	private ActionListener buttonFileClear_Action_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {

			updateIcon(NodeIconDB.NO_ICON_KEY);

		}
	};


	//
	// Surveillance de attrIcon
	//

	@Override
	public void displayAttrUpdate(UpdateEvent evt) {
//		setAttrIcon(node, attrIcon);
	}

}

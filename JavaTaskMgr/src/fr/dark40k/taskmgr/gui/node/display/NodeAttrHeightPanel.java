package fr.dark40k.taskmgr.gui.node.display;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.display.attr.Attr.UpdateEvent;
import fr.dark40k.taskmgr.database.display.attr.Attr.UpdateListener;
import fr.dark40k.taskmgr.database.display.attr.AttrHeight;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeAttrHeightPanel extends JPanel implements UpdateListener {

	private JSpinner spinner_priority;
	private JSpinner spinner_height;
	private JSpinner spinner_endheight;

	private AttrHeight attrHeight;
	private TaskMgrUndoMgr	taskMgrUndoMgr;
	private Node	node;

	/**
	 * Create the panel.
	 */
	public NodeAttrHeightPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		this.taskMgrUndoMgr=taskMgrUndoMgr;

		setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

		spinner_priority = new JSpinner();
		spinner_priority.setMinimumSize(new Dimension(50, 20));
		spinner_priority.setModel(new SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(-1), null, Integer.valueOf(1)));
		spinner_priority.setPreferredSize(new Dimension(50, 20));
		add(spinner_priority);

		spinner_height = new JSpinner();
		spinner_height.setModel(new SpinnerNumberModel(Integer.valueOf(AttrHeight.DEFAULT_HEIGHT), Integer.valueOf(AttrHeight.MIN_HEIGHT), null, Integer.valueOf(1)));
		spinner_height.setPreferredSize(new Dimension(100, 20));
		add(spinner_height);

		spinner_endheight = new JSpinner();
		spinner_endheight.setModel(new SpinnerNumberModel(Integer.valueOf(AttrHeight.DEFAULT_END_HEIGHT), Integer.valueOf(AttrHeight.MIN_HEIGHT), null, Integer.valueOf(1)));
		spinner_endheight.setPreferredSize(new Dimension(100, 20));
		add(spinner_endheight);

	}


	//
	// Connection de la forme
	//


	public void setAttrHeight(Node node, AttrHeight attrHeight) {

		clearAttrHeight();

		this.attrHeight=attrHeight;
		this.node=node;

		spinner_priority.setValue(attrHeight.getPriority());
		spinner_height.setValue(attrHeight.getHeight());
		spinner_endheight.setValue(attrHeight.getEndHeight());

		spinner_priority.addChangeListener(spinner_priority_Listener);
		spinner_height.addChangeListener(spinner_height_Listener);
		spinner_endheight.addChangeListener(spinner_endheight_Listener);
		attrHeight.addUpdateListener(this);

	}

	public void clearAttrHeight() {

		if (attrHeight!=null) attrHeight.removeUpdateListener(this);

		attrHeight=null;
		node=null;

		spinner_priority.removeChangeListener(spinner_priority_Listener);
		spinner_height.removeChangeListener(spinner_height_Listener);
		spinner_endheight.removeChangeListener(spinner_endheight_Listener);

	}

	//
	// gestion de la priorite
	//

	private ChangeListener spinner_priority_Listener = new ChangeListener(){
		@Override
		public void stateChanged(ChangeEvent e) {
			updatePriority();
		}
	};

	private void updatePriority() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage priorite hauteur",true);
	        	attrHeight.removeUpdateListener(NodeAttrHeightPanel.this);
	        	attrHeight.setPriority((Integer)spinner_priority.getValue());
	        	attrHeight.addUpdateListener(NodeAttrHeightPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	//
	// Gestion des valeurs
	//

	private ChangeListener spinner_height_Listener = new ChangeListener(){
		@Override
		public void stateChanged(ChangeEvent e) {
			updateHeight();
		}
	};

	private void updateHeight() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage hauteur",true);
	        	attrHeight.removeUpdateListener(NodeAttrHeightPanel.this);
	        	attrHeight.setHeight((Integer)spinner_height.getValue());
	        	attrHeight.addUpdateListener(NodeAttrHeightPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	private ChangeListener spinner_endheight_Listener = new ChangeListener(){
		@Override
		public void stateChanged(ChangeEvent e) {
			updateEndHeight();
		}
	};

	private void updateEndHeight() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage hauteur fin",true);
	        	attrHeight.removeUpdateListener(NodeAttrHeightPanel.this);
	        	attrHeight.setEndHeight((Integer)spinner_endheight.getValue());
	        	attrHeight.addUpdateListener(NodeAttrHeightPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	//
	// Utility
	//

	public static int indexOfIntArray(int[] array, int key) {
	    int returnvalue = -1;
	    for (int i = 0; i < array.length; ++i) {
	        if (key == array[i]) {
	            returnvalue = i;
	            break;
	        }
	    }
	    return returnvalue;
	}



	//
	// Surveillance de AttrHeight
	//
	@Override
	public void displayAttrUpdate(UpdateEvent evt) {
		setAttrHeight(node, attrHeight);
	}
}

package fr.dark40k.taskmgr.gui.node.display;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.display.attr.Attr.UpdateEvent;
import fr.dark40k.taskmgr.database.display.attr.Attr.UpdateListener;
import fr.dark40k.taskmgr.database.display.attr.AttrFont;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeAttrFontPanel extends JPanel implements UpdateListener {

	private final static String[] styleList = new String[] { "Normal", "Gras", "Italique", "Gras + Italique" };
	private final static int[] styleValue = new int[] { Font.PLAIN, Font.BOLD, Font.ITALIC, Font.BOLD + Font.ITALIC  };

	private final static String[] sizeList = new String[] { "8", "10", "12", "14", "16", "18", "20", "22", "24" };

	private JSpinner spinner_priority;

	private JComboBox<String> comboBox_FontName;
	private JComboBox<String> comboBox_FontStyle;
	private JComboBox<String> comboBox_FontSize;

	private JButton btnStrikeThrough;
	private JButton btnUnderlined;

	private AttrFont	attrFont;

	private Node	node;
	private TaskMgrUndoMgr	taskMgrUndoMgr;

	/**
	 * Create the panel.
	 */
	public NodeAttrFontPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		this.taskMgrUndoMgr=taskMgrUndoMgr;

		setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

		spinner_priority = new JSpinner();
		spinner_priority.setMinimumSize(new Dimension(50, 20));
		spinner_priority.setModel(new SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(-1), null, Integer.valueOf(1)));
		spinner_priority.setPreferredSize(new Dimension(50, 20));
		add(spinner_priority);

		comboBox_FontName = new JComboBox<String>();
		comboBox_FontName.setModel(new DefaultComboBoxModel<String>(GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames()));
		add(comboBox_FontName);

		comboBox_FontStyle = new JComboBox<String>();
		comboBox_FontStyle.setModel(new DefaultComboBoxModel<String>(styleList));
		add(comboBox_FontStyle);

		comboBox_FontSize = new JComboBox<String>();
		comboBox_FontSize.setModel(new DefaultComboBoxModel<String>(sizeList));
		add(comboBox_FontSize);

		btnStrikeThrough = new JButton("");
		btnStrikeThrough.setIcon(new ImageIcon(NodeAttrFontPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/format-text-strikethrough-5.png")));
		add(btnStrikeThrough);

		btnUnderlined = new JButton("");
		btnUnderlined.setIcon(new ImageIcon(NodeAttrFontPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/format-text-underline-6.png")));
		add(btnUnderlined);

	}


	//
	// Connection de la forme
	//


	public void setAttrFont(Node node, AttrFont attrFont) {

		clearAttrFont();

		this.attrFont=attrFont;
		this.node=node;

		spinner_priority.setValue(attrFont.getPriority());

		comboBox_FontName.setSelectedItem(attrFont.getFontName());

		comboBox_FontStyle.setSelectedIndex(indexOfIntArray(styleValue,attrFont.getFontStyle()));
		comboBox_FontSize.setSelectedItem(Integer.toString(attrFont.getFontSize()));

		btnStrikeThrough.setSelected(attrFont.isStrikeThroughOut());
		btnUnderlined.setSelected(attrFont.isUnderlined());

		spinner_priority.addChangeListener(spinner_priority_Listener);

		comboBox_FontName.addActionListener(comboBox_FontName_Listener);
		comboBox_FontSize.addActionListener(comboBox_FontSize_Listener);
		comboBox_FontStyle.addActionListener(comboBox_FontStyle_Listener);

		btnStrikeThrough.addActionListener(chckbxStrikeThrough_Listener);
		btnUnderlined.addActionListener(chckbxUnderlined_Listener);

		attrFont.addUpdateListener(this);

	}

	public void clearAttrFont() {

		if (attrFont!=null) attrFont.removeUpdateListener(this);

		attrFont=null;
		node=null;

		spinner_priority.removeChangeListener(spinner_priority_Listener);
		comboBox_FontName.removeActionListener(comboBox_FontName_Listener);
		comboBox_FontSize.removeActionListener(comboBox_FontSize_Listener);
		comboBox_FontStyle.removeActionListener(comboBox_FontStyle_Listener);
		btnStrikeThrough.removeActionListener(chckbxStrikeThrough_Listener);
		btnUnderlined.removeActionListener(chckbxUnderlined_Listener);
	}

	//
	// gestion de la priorite
	//

	private ChangeListener spinner_priority_Listener = new ChangeListener(){
		@Override
		public void stateChanged(ChangeEvent e) {
			updatePriority();
		}
	};

	private void updatePriority() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage",true);
	        	attrFont.removeUpdateListener(NodeAttrFontPanel.this);
	        	attrFont.setPriority((Integer)spinner_priority.getValue());
	        	attrFont.addUpdateListener(NodeAttrFontPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	//
	// Gestion de la fonte
	//

	// Font name

	private ActionListener comboBox_FontName_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (!attrFont.getFontName().equals(comboBox_FontName.getSelectedItem())) updateFontName();
		}
	};

	private void updateFontName() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage",true);
	        	attrFont.removeUpdateListener(NodeAttrFontPanel.this);
	        	attrFont.setFontName((String)comboBox_FontName.getSelectedItem());
	        	attrFont.addUpdateListener(NodeAttrFontPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	// Font size

	private ActionListener comboBox_FontSize_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (attrFont.getFontSize()!=Integer.valueOf((String) comboBox_FontSize.getSelectedItem())) updateFontSize();
		}
	};

	private void updateFontSize() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage",true);
	        	attrFont.removeUpdateListener(NodeAttrFontPanel.this);
	        	attrFont.setFontSize(Integer.valueOf((String) comboBox_FontSize.getSelectedItem()));
	        	attrFont.addUpdateListener(NodeAttrFontPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	// FontStyle

	private ActionListener comboBox_FontStyle_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (attrFont.getFontStyle()!=styleValue[comboBox_FontStyle.getSelectedIndex()]) updateFontStyle();
		}
	};

	private void updateFontStyle() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage",true);
	        	attrFont.removeUpdateListener(NodeAttrFontPanel.this);
	        	attrFont.setFontStyle(styleValue[comboBox_FontStyle.getSelectedIndex()]);
	        	attrFont.addUpdateListener(NodeAttrFontPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}


	// StrikeThrough

	private ActionListener chckbxStrikeThrough_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (attrFont.isStrikeThroughOut() == btnStrikeThrough.isSelected()) updateStrikeThrough();
		}
	};

	private void updateStrikeThrough() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage",true);
	        	boolean newval = !btnStrikeThrough.isSelected();
	        	attrFont.removeUpdateListener(NodeAttrFontPanel.this);
	        	btnStrikeThrough.setSelected(newval);
	        	attrFont.setStrikeThrough(newval);
	        	attrFont.addUpdateListener(NodeAttrFontPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	// Underlined

	private ActionListener chckbxUnderlined_Listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (attrFont.isUnderlined() == btnUnderlined.isSelected()) updateUnderlined();
		}
	};

	private void updateUnderlined() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage",true);
	        	attrFont.removeUpdateListener(NodeAttrFontPanel.this);
	        	boolean newval = !btnUnderlined.isSelected();
	        	btnUnderlined.setSelected(newval);
	        	attrFont.setUnderlined(newval);
	        	attrFont.addUpdateListener(NodeAttrFontPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}

	//
	// Utility
	//

	public static int indexOfIntArray(int[] array, int key) {
	    int returnvalue = -1;
	    for (int i = 0; i < array.length; ++i) {
	        if (key == array[i]) {
	            returnvalue = i;
	            break;
	        }
	    }
	    return returnvalue;
	}



	//
	// Surveillance AttrFont
	//
	@Override
	public void displayAttrUpdate(UpdateEvent evt) {
//		setAttrFont(node, attrFont);
	}
}

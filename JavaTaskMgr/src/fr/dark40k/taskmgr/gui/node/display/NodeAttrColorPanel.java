package fr.dark40k.taskmgr.gui.node.display;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.display.attr.Attr.UpdateEvent;
import fr.dark40k.taskmgr.database.display.attr.Attr.UpdateListener;
import fr.dark40k.taskmgr.database.display.attr.AttrColor;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;


public class NodeAttrColorPanel extends JPanel implements UpdateListener {

	private static final Color defaultColor = Color.yellow;

	private Color btnColor;

	private JSpinner spinner_priority;

	private JButton	btn;

	private AttrColor attrColor;

	private TaskMgrUndoMgr	taskMgrUndoMgr;

	private Node	node;


	/**
	 * @wbp.parser.constructor
	 */
	public NodeAttrColorPanel(TaskMgrUndoMgr taskMgrUndoMgr, String defaultText) {
		this(defaultText, 0, defaultColor);
		this.taskMgrUndoMgr=taskMgrUndoMgr;
	}

	public NodeAttrColorPanel(String defaultText, Integer priority, Color startColor) {

		btnColor = startColor;
		setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

		spinner_priority = new JSpinner();
		spinner_priority.setMinimumSize(new Dimension(50, 20));
		spinner_priority.setModel(new SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(-1), null, Integer.valueOf(1)));
		spinner_priority.setPreferredSize(new Dimension(50, 20));
		spinner_priority.setValue(priority);
		add(spinner_priority);

		JPanel panelBtn = new JPanel();
		panelBtn.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelBtn.setLayout(new BorderLayout(0, 0));

		btn = new JButton();
		btn.setText(defaultText);
		btn.setBackground(btnColor);
		btn.setContentAreaFilled(false);
		btn.setOpaque(true);
		panelBtn.add(btn);
		add(panelBtn);

		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doActionButton();
			}
		});

		this.setSize(this.getPreferredSize());

		updateDisplay();

	}

	public void setText(String text) {
		btn.setText(text);
	}
	public String getText() {
		return btn.getText();
	}

	public void setColor(Color color) {
		btnColor = color;
		updateDisplay();
	}

	public Color getColor() {
		return btnColor;
	}

	private void doActionButton() {

        Color c;
        c = JColorChooser.showDialog(this, "Selectioner la couleur", (attrColor.getColor()!=null)?attrColor.getColor():defaultColor);
        if (c!=null) {
        	btnColor = c;
        	if (attrColor!=null) {
        		taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage", true);
        		attrColor.removeUpdateListener(this);
        		attrColor.setColor(c);
        		attrColor.addUpdateListener(this);
        	}
        	updateDisplay();
        }

	}

	private void updateDisplay() {
			btn.setBackground(btnColor);
			btn.setForeground(OpposeColor(btnColor));
	}

	private static Color OpposeColor(Color ColorToInvert)
	{
		if (ColorToInvert==null) return Color.BLACK;

		float[] hsv = new float[3];
		Color.RGBtoHSB(ColorToInvert.getRed(), ColorToInvert.getGreen(), ColorToInvert.getBlue(), hsv);

		if (hsv[2] > 0.5)
			return Color.BLACK;
		else
			return Color.white;

	}

	//
	// Connection de la forme
	//


	public void setAttrColor(Node node, AttrColor attrColor) {

		clearAttrColor();

		this.attrColor=attrColor;
		this.node=node;

		spinner_priority.setValue(attrColor.getPriority());
		setColor(attrColor.getColor());

		spinner_priority.addChangeListener(spinner_priority_Listener);

		attrColor.addUpdateListener(this);

	}

	public void clearAttrColor() {

		if (this.attrColor!=null) attrColor.removeUpdateListener(this);

		spinner_priority.removeChangeListener(spinner_priority_Listener);

		attrColor=null;
		node=null;
	}


	//
	// gestion de la priorite
	//

	private ChangeListener spinner_priority_Listener = new ChangeListener(){
		@Override
		public void stateChanged(ChangeEvent e) {
			updatePriority();
		}
	};

	private void updatePriority() {
	    Runnable doHighlight = new Runnable() {
	        @Override
	        public void run() {
	        	taskMgrUndoMgr.addUndoableNodeEdit(node, "Modifier affichage", true);
	        	attrColor.removeUpdateListener(NodeAttrColorPanel.this);
	        	attrColor.setPriority((Integer)spinner_priority.getValue());
	        	attrColor.addUpdateListener(NodeAttrColorPanel.this);
	        }
	    };
	    SwingUtilities.invokeLater(doHighlight);
	}



	//
	// Surveillance de l'attrColor
	//

	@Override
	public void displayAttrUpdate(UpdateEvent evt) {
		setAttrColor(node, attrColor);
	}

}

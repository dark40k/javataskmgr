package fr.dark40k.taskmgr.gui.node.display;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeDisplayPanel extends JPanel {

	private NodeAttrIconPanel nodeAttrIconPanel_1;
	private NodeAttrIconPanel nodeAttrIconPanel_2;
	private NodeAttrColorPanel nodeAttrColorPanel_Foreground;
	private NodeAttrColorPanel nodeAttrColorPanel_BackgroundTop;
	private NodeAttrColorPanel nodeAttrColorPanel_BackgroundMiddle;
	private NodeAttrColorPanel nodeAttrColorPanel_BackgroundBottom;
	private NodeAttrFontPanel  nodeAttrFontPanel;
	private NodeAttrHeightPanel  nodeAttrHeightPanel;
	
	/**
	 * Create the panel.
	 */
	public NodeDisplayPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{1.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0};
		setLayout(gridBagLayout);
		
		nodeAttrIconPanel_1 = new NodeAttrIconPanel(taskMgrUndoMgr);
		nodeAttrIconPanel_1.setAlignmentX(Component.CENTER_ALIGNMENT);
		nodeAttrIconPanel_1.setBorder(new TitledBorder(null, "Chemin icone 1", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_nodeAttrIconPanel_1 = new GridBagConstraints();
		gbc_nodeAttrIconPanel_1.anchor = GridBagConstraints.NORTH;
		gbc_nodeAttrIconPanel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodeAttrIconPanel_1.insets = new Insets(0, 0, 5, 0);
		gbc_nodeAttrIconPanel_1.gridx = 0;
		gbc_nodeAttrIconPanel_1.gridy = 0;
		add(nodeAttrIconPanel_1, gbc_nodeAttrIconPanel_1);
		
		nodeAttrIconPanel_2 = new NodeAttrIconPanel(taskMgrUndoMgr);
		nodeAttrIconPanel_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		nodeAttrIconPanel_2.setBorder(new TitledBorder(null, "Chemin icone 2", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_nodeAttrIconPanel_2 = new GridBagConstraints();
		gbc_nodeAttrIconPanel_2.anchor = GridBagConstraints.NORTH;
		gbc_nodeAttrIconPanel_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodeAttrIconPanel_2.insets = new Insets(0, 0, 5, 0);
		gbc_nodeAttrIconPanel_2.gridx = 0;
		gbc_nodeAttrIconPanel_2.gridy = 1;
		add(nodeAttrIconPanel_2, gbc_nodeAttrIconPanel_2);
		
		nodeAttrColorPanel_Foreground = new NodeAttrColorPanel(taskMgrUndoMgr, (String) null);
		nodeAttrColorPanel_Foreground.setBorder(new TitledBorder(null, "Couleur du texte", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		nodeAttrColorPanel_Foreground.setText("Foreground");
		GridBagConstraints gbc_nodeAttrColorPanel_Foreground = new GridBagConstraints();
		gbc_nodeAttrColorPanel_Foreground.anchor = GridBagConstraints.NORTH;
		gbc_nodeAttrColorPanel_Foreground.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodeAttrColorPanel_Foreground.insets = new Insets(0, 0, 5, 0);
		gbc_nodeAttrColorPanel_Foreground.gridx = 0;
		gbc_nodeAttrColorPanel_Foreground.gridy = 2;
		add(nodeAttrColorPanel_Foreground, gbc_nodeAttrColorPanel_Foreground);
		
		nodeAttrColorPanel_BackgroundTop = new NodeAttrColorPanel(taskMgrUndoMgr, (String) null);
		nodeAttrColorPanel_BackgroundTop.setBorder(new TitledBorder(null, "Couleur de fond Haut", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		nodeAttrColorPanel_BackgroundTop.setText("Background Haut");
		GridBagConstraints gbc_nodeAttrColorPanel_BackgroundTop = new GridBagConstraints();
		gbc_nodeAttrColorPanel_BackgroundTop.anchor = GridBagConstraints.NORTH;
		gbc_nodeAttrColorPanel_BackgroundTop.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodeAttrColorPanel_BackgroundTop.insets = new Insets(0, 0, 5, 0);
		gbc_nodeAttrColorPanel_BackgroundTop.gridx = 0;
		gbc_nodeAttrColorPanel_BackgroundTop.gridy = 3;
		add(nodeAttrColorPanel_BackgroundTop, gbc_nodeAttrColorPanel_BackgroundTop);
		
		nodeAttrColorPanel_BackgroundMiddle = new NodeAttrColorPanel(taskMgrUndoMgr, (String) null);
		nodeAttrColorPanel_BackgroundMiddle.setBorder(new TitledBorder(null, "Couleur de fond Milieu", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		nodeAttrColorPanel_BackgroundMiddle.setText("Background Milieu");
		GridBagConstraints gbc_nodeAttrColorPanel_BackgroundMiddle = new GridBagConstraints();
		gbc_nodeAttrColorPanel_BackgroundMiddle.anchor = GridBagConstraints.NORTH;
		gbc_nodeAttrColorPanel_BackgroundMiddle.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodeAttrColorPanel_BackgroundMiddle.insets = new Insets(0, 0, 5, 0);
		gbc_nodeAttrColorPanel_BackgroundMiddle.gridx = 0;
		gbc_nodeAttrColorPanel_BackgroundMiddle.gridy = 4;
		add(nodeAttrColorPanel_BackgroundMiddle, gbc_nodeAttrColorPanel_BackgroundMiddle);
		
		nodeAttrColorPanel_BackgroundBottom = new NodeAttrColorPanel(taskMgrUndoMgr, (String) null);
		nodeAttrColorPanel_BackgroundBottom.setBorder(new TitledBorder(null, "Couleur de fond Bas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		nodeAttrColorPanel_BackgroundBottom.setText("Background Bas");
		GridBagConstraints gbc_nodeAttrColorPanel_BackgroundBottom = new GridBagConstraints();
		gbc_nodeAttrColorPanel_BackgroundBottom.anchor = GridBagConstraints.NORTH;
		gbc_nodeAttrColorPanel_BackgroundBottom.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodeAttrColorPanel_BackgroundBottom.insets = new Insets(0, 0, 5, 0);
		gbc_nodeAttrColorPanel_BackgroundBottom.gridx = 0;
		gbc_nodeAttrColorPanel_BackgroundBottom.gridy = 5;
		add(nodeAttrColorPanel_BackgroundBottom, gbc_nodeAttrColorPanel_BackgroundBottom);
		
		nodeAttrFontPanel = new NodeAttrFontPanel(taskMgrUndoMgr);
		nodeAttrFontPanel.setBorder(new TitledBorder(null, "Style de texte", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_nodeAttrFontPanel = new GridBagConstraints();
		gbc_nodeAttrFontPanel.anchor = GridBagConstraints.NORTH;
		gbc_nodeAttrFontPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodeAttrFontPanel.insets = new Insets(0, 0, 5, 0);
		gbc_nodeAttrFontPanel.gridx = 0;
		gbc_nodeAttrFontPanel.gridy = 6;
		add(nodeAttrFontPanel, gbc_nodeAttrFontPanel);
		
		nodeAttrHeightPanel = new NodeAttrHeightPanel(taskMgrUndoMgr);
		nodeAttrHeightPanel.setBorder(new TitledBorder(null, "Hauteur de ligne", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_nodeAttrHeightPanel = new GridBagConstraints();
		gbc_nodeAttrHeightPanel.anchor = GridBagConstraints.NORTH;
		gbc_nodeAttrHeightPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_nodeAttrHeightPanel.gridx = 0;
		gbc_nodeAttrHeightPanel.gridy = 7;
		add(nodeAttrHeightPanel, gbc_nodeAttrHeightPanel);

	}

	public void setNode(Node node) {
		if (node==null) {
			clearNode();
			return;
		}
		nodeAttrIconPanel_1.setAttrIcon(node, node.getDisplay().getIcon1());
		nodeAttrIconPanel_2.setAttrIcon(node, node.getDisplay().getIcon2());
		nodeAttrColorPanel_Foreground.setAttrColor(node, node.getDisplay().getFrontColor());
		nodeAttrColorPanel_BackgroundTop.setAttrColor(node, node.getDisplay().getBackColorTop());
		nodeAttrColorPanel_BackgroundMiddle.setAttrColor(node, node.getDisplay().getBackColorMiddle());
		nodeAttrColorPanel_BackgroundBottom.setAttrColor(node, node.getDisplay().getBackColorBottom());
		nodeAttrFontPanel.setAttrFont(node, node.getDisplay().getFont());
		nodeAttrHeightPanel.setAttrHeight(node, node.getDisplay().getHeight());
	}

	private void clearNode() {
		nodeAttrIconPanel_1.clearAttrColor();
		nodeAttrIconPanel_2.clearAttrColor();
		nodeAttrColorPanel_Foreground.clearAttrColor();
		nodeAttrColorPanel_BackgroundTop.clearAttrColor();
		nodeAttrColorPanel_BackgroundMiddle.clearAttrColor();
		nodeAttrColorPanel_BackgroundBottom.clearAttrColor();
		nodeAttrFontPanel.clearAttrFont();
		nodeAttrHeightPanel.clearAttrHeight();
	}

}

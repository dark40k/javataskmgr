package fr.dark40k.taskmgr.gui.node.scriptpanel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;

import org.fife.rsta.ac.LanguageSupportFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.FormattersString;
import fr.dark40k.util.TextFile;

public class NodeScriptPanel extends JPanel {

	private static final String XMLTAG = "scriptpanel";

	private static final ImageIcon VALID_SCRIPT_ICON = new ImageIcon(NodeScriptPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/dialog-ok-apply-4.png"));
	private static final ImageIcon INVALID_SCRIPT_ICON = new ImageIcon(NodeScriptPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-delete-6.png"));

	private static final String HELP_TEXT = TextFile.read(NodeScriptPanel.class.getResourceAsStream("help.txt"));

	private static final String EMPTY_SCRIPT_TEXT = "";

	@SuppressWarnings("unused")
	private final TaskMgrUndoMgr taskMgrUndoMgr;

	private Node editNode;

	protected JPanel panel;
	protected JPanel panel_1;
	protected RSyntaxTextArea textArea;
	protected JPanel panel_2;
	protected JButton btnApply;
	protected JPanel panel_3;
	protected JScrollPane scrollPane;
	protected JTextPane txtpnAppeleeLors;
	protected JSplitPane splitPane;
	protected JPanel panel_4;

	protected JLabel lblShortStatus;
	protected JLabel lblLongStatus;

	public NodeScriptPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		this.taskMgrUndoMgr = taskMgrUndoMgr;
		initGUI();
		update();
	}

	public void setNode(Node node) {

		// Sauvegarde du script pour le noeud pr�c�dent
		if (editNode!=null) applyScript();

		if (node == editNode) return;

		editNode = node;
		update();
	}

	private void update() {

		if (editNode == null) {
			textArea.setText(null);
			textArea.setEditable(false);
			btnApply.setEnabled(false);
			lblShortStatus.setIcon(VALID_SCRIPT_ICON);
			lblShortStatus.setToolTipText(null);
			lblLongStatus.setText(EMPTY_SCRIPT_TEXT);
			return;
		}

		textArea.setText(editNode.getScript().getSource());
		textArea.setEditable(!editNode.isReadOnly());
		btnApply.setEnabled(!editNode.isReadOnly());

		lblShortStatus.setIcon((editNode.getScript().isValid()) ? VALID_SCRIPT_ICON : INVALID_SCRIPT_ICON);
		lblShortStatus.setToolTipText(FormattersString.textToHTML(editNode.getScript().getErrorMsg()));
		lblLongStatus.setText(FormattersString.textToHTML(editNode.getScript().getErrorMsg()));
	}

	protected void applyScript() {
		editNode.getScript().setSource(textArea.getText());
		update();
	}

	//
	// Import/Export de l'affichage
	//

	public void importXML(Element baseElt) {

		// recupere les donn�es d'affichage persistantes et quitte si pas trouv�es
		Element elt = baseElt.getChild(XMLTAG);
		if (elt == null) return;

		// emplacement pour ajouter la r�cup�ration de donn�es
		// TODO A completer si utile
	}

	public Element exportXML() {
		Element elt = new Element(XMLTAG);
		return elt;
	}

	//
	// Autocompletion
	//

//	/**
//	 * Create a simple provider that adds some Java-related completions.
//	 */
//	private CompletionProvider createCompletionProvider() {
//
//		// A DefaultCompletionProvider is the simplest concrete implementation
//		// of CompletionProvider. This provider has no understanding of
//		// language semantics. It simply checks the text entered up to the
//		// caret position for a match against known completions. This is all
//		// that is needed in the majority of cases.
//		DefaultCompletionProvider provider = new DefaultCompletionProvider();
//
//		// Add completions for all Java keywords. A BasicCompletion is just
//		// a straightforward word completion.
//		provider.addCompletion(new BasicCompletion(provider, "abstract"));
//		provider.addCompletion(new BasicCompletion(provider, "assert"));
//		provider.addCompletion(new BasicCompletion(provider, "break"));
//		provider.addCompletion(new BasicCompletion(provider, "case"));
//		// ... etc ...
//		provider.addCompletion(new BasicCompletion(provider, "transient"));
//		provider.addCompletion(new BasicCompletion(provider, "try"));
//		provider.addCompletion(new BasicCompletion(provider, "void"));
//		provider.addCompletion(new BasicCompletion(provider, "volatile"));
//		provider.addCompletion(new BasicCompletion(provider, "while"));
//
//		provider.addCompletion(new JavaCompletionProvider());
//
//		// Add a couple of "shorthand" completions. These completions don't
//		// require the input text to be the same thing as the replacement text.
//		provider.addCompletion(new ShorthandCompletion(provider, "sysout", "System.out.println(", "System.out.println("));
//		provider.addCompletion(new ShorthandCompletion(provider, "syserr", "System.err.println(", "System.err.println("));
//
//		return provider;
//
//	}


	//
	// Interface graphique
	//

	private void initGUI() {
				setLayout(new BorderLayout(0, 0));

				panel_2 = new JPanel();
				add(panel_2, BorderLayout.NORTH);
				GridBagLayout gbl_panel_2 = new GridBagLayout();
				gbl_panel_2.columnWidths = new int[] { 77, 0, 0, 0 };
				gbl_panel_2.rowHeights = new int[] { 23, 0 };
				gbl_panel_2.columnWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
				gbl_panel_2.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
				panel_2.setLayout(gbl_panel_2);

						btnApply = new JButton("Appliquer");
						btnApply.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								applyScript();
							}
						});
						GridBagConstraints gbc_btnApply = new GridBagConstraints();
						gbc_btnApply.insets = new Insets(0, 0, 0, 5);
						gbc_btnApply.anchor = GridBagConstraints.NORTHWEST;
						gbc_btnApply.gridx = 0;
						gbc_btnApply.gridy = 0;
						panel_2.add(btnApply, gbc_btnApply);

								lblShortStatus = new JLabel((String) null);
								lblShortStatus.setIcon(INVALID_SCRIPT_ICON);
								GridBagConstraints gbc_lblStatus = new GridBagConstraints();
								gbc_lblStatus.gridx = 2;
								gbc_lblStatus.gridy = 0;
								panel_2.add(lblShortStatus, gbc_lblStatus);

		splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.8);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		add(splitPane, BorderLayout.CENTER);

		panel_3 = new JPanel();
		splitPane.setRightComponent(panel_3);
		panel_3.setBorder(new TitledBorder(null, "Aide", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		panel_3.add(scrollPane, BorderLayout.CENTER);

		txtpnAppeleeLors = new JTextPane();
		txtpnAppeleeLors.setEditable(false);
		txtpnAppeleeLors.setText(HELP_TEXT);
		scrollPane.setViewportView(txtpnAppeleeLors);

				panel = new JPanel();
				splitPane.setLeftComponent(panel);
				panel.setBorder(new TitledBorder(null, "Code", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				panel.setLayout(new BorderLayout(0, 0));

						textArea = new RSyntaxTextArea(20, 60);
						textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
						textArea.setCodeFoldingEnabled(true);

//						CompletionProvider provider = createCompletionProvider();
//						AutoCompletion ac = new AutoCompletion(provider);
//						ac.install(textArea);

						LanguageSupportFactory.get().register(textArea);

						RTextScrollPane sp = new RTextScrollPane(textArea);
						panel.add(sp);

						panel_4 = new JPanel();
						panel_4.setBorder(new TitledBorder(null, "Status compilation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
						panel.add(panel_4, BorderLayout.SOUTH);
						panel_4.setLayout(new BorderLayout(0, 0));

						lblLongStatus = new JLabel("Pas de status disponible.");
						panel_4.add(lblLongStatus);;
	}

}

package fr.dark40k.taskmgr.gui.node.datalinks.fastlinkspanel;

import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.datalinks.DataLinkList;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class FastLinkButttonListPanel extends JPanel {

	private TaskMgrUndoMgr taskMgrUndoMgr;

	private ArrayList<FastLinkButton> buttonList = new ArrayList<>();

	private NodeTask nodeTask;

	protected JPanel panel;
	protected JButton btnNewAddButton;
	protected JButton btnNewRemoveButton;

	public FastLinkButttonListPanel(TaskMgrUndoMgr taskMgrUndoMgr) {
		
		this.taskMgrUndoMgr=taskMgrUndoMgr;
		
		panel = new JPanel();
		add(panel);
		panel.setLayout(new GridLayout(2, 0, 0, 0));
		
		btnNewAddButton = new JButton("+");
		btnNewAddButton.setMargin(new Insets(0, 0, 0, 0));
		panel.add(btnNewAddButton);
		
		btnNewAddButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (nodeTask==null) return;
				taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter emplacement lien rapide",false);
				nodeTask.getLinkList().addFastLink();
			}
		});
		
		btnNewRemoveButton = new JButton("-");
		btnNewRemoveButton.setMargin(new Insets(0, 0, 0, 0));
		panel.add(btnNewRemoveButton);
		
		btnNewRemoveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (nodeTask==null) return;
				taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter emplacement lien rapide",false);
				nodeTask.getLinkList().removeFastLink();
			}
		});
		
		for (int index =0; index < DataLinkList.MIN_FASTLINK_NBR; index++) {
			FastLinkButton button = new FastLinkButton(taskMgrUndoMgr,index);
			buttonList.add(button);
			add(button);
		}
		
		
		
	}

	public void setNode(Node node) {
		
		// cas ou le noeud n'est pas utilisable
		if ((node==null) || (!(node instanceof NodeTask))) {
			if (nodeTask!=null) clearButtons();
			return;
		}
		
		// recuperation du noeud
		nodeTask = (NodeTask) node;

		// met � jours les bouttons
		updateButtons();
	}

	
	public void clearButtons() {
		
		nodeTask=null;
		
		for (int index = 0; index < buttonList.size(); index++ ) {
			FastLinkButton button = buttonList.get(index);
			button.setVisible(index<DataLinkList.MIN_FASTLINK_NBR);
			button.setNode(null);
		}
		
		btnNewAddButton.setToolTipText(null);
		btnNewRemoveButton.setToolTipText(null);
	}
	
	public void updateButtons() {
		
		// recuperation du nombre de lien
		int nodeLinkCount = nodeTask.getLinkList().getHeritedFastLinkCount();
		
		// creation des boutons manquants (s'il y a lieu)
		while (buttonList.size()<nodeLinkCount){
			FastLinkButton button = new FastLinkButton(taskMgrUndoMgr,buttonList.size());
			buttonList.add(button);
			add(button);
		}

		// initialise + affiche/masque les boutons
		for (int index = 0; index < buttonList.size(); index++ ) {
			FastLinkButton button = buttonList.get(index);
			if (index<nodeLinkCount) {
				button.setVisible(true);
				button.setNode(nodeTask);
			} else {
				button.setVisible(false);
				button.setNode(null);
			}
		}
		
		btnNewAddButton.setToolTipText("Augmenter liens, nbr ="+nodeTask.getLinkList().getFastLinkCount());
		btnNewRemoveButton.setToolTipText("Reduire liens, nbr ="+nodeTask.getLinkList().getFastLinkCount());
		
		
		// verifie le nombre de liens
		btnNewRemoveButton.setEnabled(nodeTask.getLinkList().getFastLinkCount()>DataLinkList.MIN_FASTLINK_NBR);
		
	}

}

package fr.dark40k.taskmgr.gui.node.datalinks.popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.gui.node.datalinks.LinkTableModel;
import fr.dark40k.taskmgr.gui.node.datalinks.NodeLinksPanel;

public class CreateLinkPopupMenu extends JPopupMenu {

	private static final ImageIcon CREATE_MANUAL_LINK_ICON = new ImageIcon(CreateLinkPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/bookmark-new-2.png"));
	private static final ImageIcon CREATE_FOLDER_OUTLOOK_LINK_ICON = new ImageIcon(CreateLinkPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/emailfolder.png"));
	private static final ImageIcon CREATE_MAIL_OUTLOOK_LINK_ICON = new ImageIcon(CreateLinkPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/email.png"));

	@SuppressWarnings("unused")
	public CreateLinkPopupMenu(NodeLinksPanel filePanel, NodeTask nodeTask, LinkTableModel model){

		boolean isReadOnly = nodeTask.isReadOnly();

		{
			JMenuItem item = new JMenuItem("Cr�ation manuelle");
			add(item);
			item.setIcon(CREATE_MANUAL_LINK_ICON);
			if (!isReadOnly) {
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						filePanel.addLink();
					}
				});
			} else {
				item.setEnabled(false);
			}
		}

		{
			JMenuItem item = new JMenuItem("Import mail Outlook");
			add(item);
			item.setIcon(CREATE_MAIL_OUTLOOK_LINK_ICON);
			if (!isReadOnly) {
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						filePanel.addMailFromOutlook();
					}
				});
			} else {
				item.setEnabled(false);
			}
		}

		{
			JMenuItem item = new JMenuItem("Import repertoire Outlook");
			add(item);
			item.setIcon(CREATE_FOLDER_OUTLOOK_LINK_ICON);
			if (!isReadOnly) {
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						filePanel.addFolderFromOutlook();
					}
				});
			} else {
				item.setEnabled(false);
			}
		}

	}



}

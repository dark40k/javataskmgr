package fr.dark40k.taskmgr.gui.node.datalinks.popup;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import fr.dark40k.pdfeditor.PdfEditor;
import fr.dark40k.pdfeditor.data.PdfEditorClipboard;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.database.datalinks.DataLinkList;
import fr.dark40k.taskmgr.database.datalinks.DataLinkType;
import fr.dark40k.taskmgr.gui.node.datalinks.LinkTableModel;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class LinkPopupMenu extends JPopupMenu {

	// private final static Logger LOGGER = LogManager.getLogger();

	private static final ImageIcon EDIT_LINK_ICON = new ImageIcon(LinkPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/bookmark-new-list-4.png"));
	private static final ImageIcon REMOVE_LINK_ICON = new ImageIcon(LinkPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-delete-6.png"));

	public LinkPopupMenu(Component parent, TaskMgrUndoMgr taskMgrUndoMgr, NodeTask nodeTask, LinkTableModel model, int modelRowIndex){

		boolean isReadOnly = nodeTask.isReadOnly();

		DataLinkList linkList = nodeTask.getLinkList();
		DataLink link = model.getLink(modelRowIndex);

		boolean isHerited = model.isHeritedLink(modelRowIndex);

		{
			JMenuItem item = new JMenuItem();
			add(item);

			item.setForeground(Color.black);
			item.setBackground(Color.white);
			item.setOpaque(true);

			if (link!=null) {
				item.setText(link.getDescription());
				item.setIcon(nodeTask.getDataBase().getNodeIconsDB().getNodeIcon(link.getIcon16Key(true)).getIcon());
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						link.run();
					}
				});
			}
			else {
				item.setText("<No Link>");
				item.setEnabled(false);
			}
		}

		add(new JSeparator());

		{
			JMenuItem item = new JMenuItem("Editer lien");
			item.setIcon(EDIT_LINK_ICON);
			add(item);
			if ((link != null) && (!isReadOnly) && (!isHerited)) {
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {

						taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Editer lien", false);
						model.editLink(parent, modelRowIndex);

					}
				});
			} else {
				item.setEnabled(false);
			}
		}

		{
			JMenuItem item = new JMenuItem("Supprimer lien");
			item.setIcon(REMOVE_LINK_ICON);
			add(item);
			if ((link != null) && (!isReadOnly) && (!isHerited)) {
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (JOptionPane.showConfirmDialog(parent,
								"Confirmer suppression du lien. \n(le fichier n'est pas d�truit)", "WARNING",
								JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							model.removeLink(modelRowIndex);
					}
				});
			} else {
				item.setEnabled(false);
			}
		}

		add(new JSeparator());

		add(new SelectFastLinkButtonListMenu(taskMgrUndoMgr, nodeTask, linkList,link,this));

		if ((link != null) &&(link.getType()==DataLinkType.PDFFILE)) {

			add(new JSeparator());

			{
				int count = PdfEditorClipboard.getClipboardCount();

				JMenuItem item = new JMenuItem("Coller pages PDF (" + count + ")");
				item.setToolTipText("Ajouter pages PDF selectionn�es");
				add(item);

				if ((!isReadOnly) && (!isHerited) && (count > 0)) {
					item.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							File file = new File(link.getData());
							boolean status = PdfEditorClipboard.pastePages(file);
							if (!status) JOptionPane.showMessageDialog(LinkPopupMenu.this, "Echec collage\n" + file.getAbsolutePath(), "Warning",
									JOptionPane.ERROR_MESSAGE);
						}
					});
				} else {
					item.setEnabled(false);
				}
			}

			{
				JMenuItem item = new JMenuItem("Editer PDF");
				item.setToolTipText("Ouvrir document dans editeur PDF int�gr�.");
				add(item);

				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						File file = new File(link.getData());
						PdfEditor.showPdfEditor(file);
					}
				});

			}


		}
	}

	//
	// Commandes menu
	//

}

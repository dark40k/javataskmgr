package fr.dark40k.taskmgr.gui.node.datalinks;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.gui.node.datalinks.popup.CreateLinkPopupMenu;
import fr.dark40k.taskmgr.gui.node.datalinks.popup.LinkPopupMenu;
import fr.dark40k.taskmgr.gui.util.MiddleDotRenderer;
import fr.dark40k.taskmgr.util.OutlookConnector;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeLinksPanel extends JPanel {

	public final static ImageIcon FILE_ICON = new ImageIcon(
			NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/document_empty.png"));
	public final static ImageIcon DIRECTORY_ICON = new ImageIcon(
			NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/folder_vertical_document.png"));
	public final static ImageIcon MAIL_ICON = new ImageIcon(
			NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/email.png"));
	public final static ImageIcon FOLDER_ICON = new ImageIcon(
			NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/emailfolder.png"));

	public final static Color DIRECT_LINK_BACKGROUND = Color.WHITE;
	public final static Color HERITED_LINK_BACKGROUND = new Color(230, 230, 230); // gris clair

	private JScrollPane scrollPane;
	private final JTable table;
	private final LinkTableModel model;

	private NodeTask curNodeTask;

	//
	// Constructeur
	//

	public NodeLinksPanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		setBorder(new TitledBorder("Fichiers et repertoires associ\u00E9s"));

		// $hide>>$ remove from WindowBuilder parsing (error)
		model = new LinkTableModel(taskMgrUndoMgr);
		table = new JTable(model) {
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				if (table.getSelectedRow() != row)
					c.setBackground(model.getRowColour(row));
				return c;
			}
		};
		// $hide<<$

		intializeGUI();

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);

		table.getColumnModel().getColumn(0).setPreferredWidth(26);
		table.getColumnModel().getColumn(0).setMaxWidth(26);

		table.getColumnModel().getColumn(1).setPreferredWidth(24);
		table.getColumnModel().getColumn(1).setMaxWidth(24);
		table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

		table.getColumnModel().getColumn(2).setPreferredWidth(100);

		table.getColumnModel().getColumn(3).setPreferredWidth(150);
		table.getColumnModel().getColumn(3).setCellRenderer(new MiddleDotRenderer());

		table.setFont(new Font(table.getFont().getName(), table.getFont().getStyle(), 12));
		table.setRowHeight(20);

		table.addMouseListener(mouseListener);

		scrollPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if ((!e.isPopupTrigger()) || (curNodeTask == null))
					return;
				CreateLinkPopupMenu popup = new CreateLinkPopupMenu(NodeLinksPanel.this, curNodeTask, model);
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if ((!e.isPopupTrigger()) || (curNodeTask == null))
					return;
				int popupRow = table.rowAtPoint(e.getPoint());
				if (popupRow < 0)
					return;
				LinkPopupMenu popup = new LinkPopupMenu(NodeLinksPanel.this, taskMgrUndoMgr, curNodeTask, model,
						table.convertRowIndexToModel(popupRow));
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});

	}

	//
	// Selection du Node actif
	//
	public void setNode(Node newNode) {

		if ((newNode == null) || !(newNode instanceof NodeTask)) {
			curNodeTask = null;
			model.setNode(null);
			return;
		}

		curNodeTask = (NodeTask) newNode;
		model.setNode(curNodeTask);

	}

	//
	// DblClick listener
	//

	private MouseAdapter mouseListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() != 2)
				return;

			Integer selRow = table.getSelectedRow();
			if (selRow < 0)
				return;

			model.runLink(selRow);

		}
	};
	//
	// Commandes
	//

	public void addMailFromOutlook() {

		if (curNodeTask == null)
			return;

		ArrayList<OutlookConnector.Email> eMailList = OutlookConnector.ListSelectedEMails();
		if (eMailList.size() == 0)
			return;

		model.addOutlookMailList(eMailList);

	}

	public void addFolderFromOutlook() {

		if (curNodeTask == null)
			return;

		OutlookConnector.Folder mailFolder = OutlookConnector.getSelectedFolder();
		if (mailFolder == null)
			return;

		model.addOutlookFolder(mailFolder);

	}

	public void addLink() {

		if (curNodeTask == null)
			return;

		model.addLink(this);

	}

	public void editLink() {

		if (curNodeTask == null)
			return;

		Integer selRow = table.getSelectedRow();
		if (selRow < 0)
			return;

		model.editLink(this, selRow);

	}

	public void removeLink() {

		if (curNodeTask == null)
			return;

		Integer selRow = table.getSelectedRow();
		if (selRow < 0)
			return;

		model.removeLink(selRow);

	}

	public void moveLinkUp() {

		if (curNodeTask == null)
			return;

		Integer selRow = table.getSelectedRow();
		if (selRow <= 0)
			return;

		boolean ret = model.moveLinkUp(selRow);
		if (ret)
			table.getSelectionModel().setSelectionInterval(selRow - 1, selRow - 1);
	}

	public void moveLinkDown() {

		if (curNodeTask == null)
			return;

		Integer selRow = table.getSelectedRow();
		if (selRow < 0)
			return;
		if (selRow >= table.getRowCount() - 1)
			return;

		boolean ret = model.moveLinkDown(selRow);
		if (ret)
			table.getSelectionModel().setSelectionInterval(selRow + 1, selRow + 1);
	}

	private void intializeGUI() {

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 300, 49, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEADING);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTH;
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridwidth = 2;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);

		JButton btnAddOutlookFolder = new JButton((String) null);
		panel.add(btnAddOutlookFolder);
		btnAddOutlookFolder.setIcon(
				new ImageIcon(NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/emailfolder.png")));
		btnAddOutlookFolder.setToolTipText("Cr\u00E9er lien repertoire Outlook");

		JButton btnAddOutlookMsg = new JButton((String) null);
		panel.add(btnAddOutlookMsg);
		btnAddOutlookMsg
				.setIcon(new ImageIcon(NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/email.png")));
		btnAddOutlookMsg.setToolTipText("Cr\u00E9er lien mail Outlook");

		JButton btnAdd = new JButton((String) null);
		panel.add(btnAdd);
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				addLink();
			}
		});

		btnAdd.setToolTipText("Cr\u00E9er lien");
		btnAdd.setFocusable(false);
		btnAdd.setIcon(
				new ImageIcon(NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/bookmark-new-2.png")));

		JButton btnEdit = new JButton((String) null);
		panel.add(btnEdit);
		btnEdit.setToolTipText("Editer lien");
		btnEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editLink();
			}
		});
		btnEdit.setIcon(new ImageIcon(
				NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/bookmark-new-list-4.png")));

		JButton btnDel = new JButton((String) null);
		panel.add(btnDel);
		btnDel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				removeLink();
			}
		});
		btnDel.setToolTipText("Supprimer lien");
		btnDel.setFocusable(false);
		btnDel.setIcon(
				new ImageIcon(NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-delete-6.png")));
		btnAddOutlookMsg.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addMailFromOutlook();
			}
		});
		btnAddOutlookFolder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addFolderFromOutlook();
			}
		});

		scrollPane = new JScrollPane();
		scrollPane.setViewportView(table);

		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		add(scrollPane, gbc_scrollPane);

		JPanel panelTools = new JPanel();
		GridBagConstraints gbc_panelTools = new GridBagConstraints();
		gbc_panelTools.anchor = GridBagConstraints.WEST;
		gbc_panelTools.fill = GridBagConstraints.VERTICAL;
		gbc_panelTools.gridx = 1;
		gbc_panelTools.gridy = 1;
		add(panelTools, gbc_panelTools);
		GridBagLayout gbl_panelTools = new GridBagLayout();
		gbl_panelTools.columnWidths = new int[] { 45, 0 };
		gbl_panelTools.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_panelTools.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelTools.rowWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		panelTools.setLayout(gbl_panelTools);

		JButton btnUp = new JButton((String) null);
		btnUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				moveLinkUp();
			}
		});
		btnUp.setToolTipText("Deplacer lien vers le haut");
		btnUp.setFocusable(false);
		btnUp.setIcon(
				new ImageIcon(NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/move_task_up.png")));
		GridBagConstraints gbc_btnUp = new GridBagConstraints();
		gbc_btnUp.anchor = GridBagConstraints.NORTH;
		gbc_btnUp.insets = new Insets(0, 0, 5, 0);
		gbc_btnUp.gridx = 0;
		gbc_btnUp.gridy = 0;
		panelTools.add(btnUp, gbc_btnUp);

		JButton btnDwn = new JButton((String) null);
		btnDwn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				moveLinkDown();
			}
		});

		JPanel panel_1 = new JPanel();
		panel_1.setMinimumSize(new Dimension(10, 50));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		panelTools.add(panel_1, gbc_panel_1);
		btnDwn.setToolTipText("D\u00E9placer lien vers le bas");
		btnDwn.setFocusable(false);
		btnDwn.setIcon(
				new ImageIcon(NodeLinksPanel.class.getResource("/fr/dark40k/taskmgr/res/icon16/move_task_down.png")));
		GridBagConstraints gbc_btnDwn = new GridBagConstraints();
		gbc_btnDwn.anchor = GridBagConstraints.NORTH;
		gbc_btnDwn.gridx = 0;
		gbc_btnDwn.gridy = 2;
		panelTools.add(btnDwn, gbc_btnDwn);

	}

}

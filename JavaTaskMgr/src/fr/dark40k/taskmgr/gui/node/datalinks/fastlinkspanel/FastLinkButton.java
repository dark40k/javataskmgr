package fr.dark40k.taskmgr.gui.node.datalinks.fastlinkspanel;

import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.multipdf.PDFMergerUtility;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.database.datalinks.DataLinkType;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.icons.StackedIcon;

public class FastLinkButton extends JButton {

	private final static Logger LOGGER = LogManager.getLogger();

	@SuppressWarnings("unused")
	private final TaskMgrUndoMgr taskMgrUndoMgr;
	
	private final int linkIndex;
	
	private final static ImageIcon NULL_ICON = new ImageIcon(FastLinkButton.class.getResource("/fr/dark40k/taskmgr/res/icon32/list-add-2.png"));
	private final static ImageIcon INDIRECT_ICON = new ImageIcon(FastLinkButton.class.getResource("/fr/dark40k/taskmgr/res/icon32/link.png"));
	
	private DataLink link;
	
	private final DropTarget dropTarget;

	private NodeTask nodeTask;
	
	public FastLinkButton(TaskMgrUndoMgr taskMgrUndoMgr, int linkIndex) {
		
		this.taskMgrUndoMgr=taskMgrUndoMgr;
		this.linkIndex=linkIndex;
		this.nodeTask=null;
		
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				link.run();
			}
		});
		
		dropTarget = new DropTarget(this, new DropTargetListener() {
			public void dragEnter(DropTargetDragEvent e) {}
			public void dragExit(DropTargetEvent e) {}
			public void dragOver(DropTargetDragEvent e) {}
			public void dropActionChanged(DropTargetDragEvent e) {}
			@SuppressWarnings("unchecked")
			public void drop(DropTargetDropEvent e) {
				try {
					// Accept the drop first, important!
					e.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);

					// Get the files that are dropped as java.util.List
					List<File> list = (List<File>) e.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
					
					// Now get the first file from the list,
					dropFiles(list);

				} catch (Exception ex) {
				}
			}
		});

		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				checkPopup(e);
			}

			public void mouseClicked(MouseEvent e) {
				checkPopup(e);
			}

			public void mouseReleased(MouseEvent e) {
				checkPopup(e);
			}

			private void checkPopup(MouseEvent e) {
				if ((e.isPopupTrigger()) && (nodeTask!=null)) {
					FastLinkButtonPopupMenu popup = new FastLinkButtonPopupMenu(taskMgrUndoMgr,nodeTask,linkIndex);
					popup.show(FastLinkButton.this, e.getX(), e.getY());
				}
			}
		});
		
		resetButton(false);
	}

	protected void dropFiles(List<File> list) {
		
		// check if there's an active node
		if (nodeTask==null) return;
		
		if (list.size()>1) {
			LOGGER.warn("Too many files dropped, cancelling.");
			return;
		}
		
		File droppedFile = list.get(0);
		
		LOGGER.info("File dropped : "+droppedFile.getPath());

		
		// verifie si un lien existe dej� avec cette destination
		DataLink foundLink = null;
		
		for (DataLink link : nodeTask.getLinkList())
			if (link.getData().equals(droppedFile.getAbsolutePath()))
				foundLink = link;
		
		// teste si le lien existe
		if (foundLink != null) {
			
			// quitte si c'est le lien existant
			if (foundLink == link) return;
			
		} else {
		
			// cree le lien s'il nexiste pas
			
			// calcul le type de lien � cr�er
			DataLinkType dropType = DataLinkType.getDefaultFileLinkType(droppedFile);
			
			// cas ou le type de lien n'a pas pu �tre calcul�
			if (dropType==null) {
				LOGGER.warn("Data type not found, file ="+ droppedFile.getAbsolutePath()+", cancelling.");
				return;
			}
			
			// cas ou il s'agit d'un document PDF dropp� sur un doc pdf
			if ((dropType==DataLinkType.PDFFILE)&&(link!=null)&&(link.getType()==DataLinkType.PDFFILE)) {
				
				Object[] options = {"Fusionner", "Remplacer", "Annuler"};
				
			    int n = JOptionPane.showOptionDialog(getTopLevelAncestor(),
			            "Options:\nFusion : Fusionner le fichier � la fin du fichier existant\nRemplacer : remplacer le fichier existant (le lien est conserv�)\n Annuler : quitte sans rien faire",
			            "Import fichier PDF",
			            JOptionPane.YES_NO_CANCEL_OPTION,
			            JOptionPane.QUESTION_MESSAGE,
			            null,
			            options,
			            options[0]);

			    // Option = Fusionner
			    if (n==0) {
			    	importAllPages(new File(link.getData()), droppedFile);
			    	return;
			    } 
			    
			    // Option != Importer => Cancel ou equivalent
			    if (n!=1) return;
				
			}
			
			foundLink = new DataLink(nodeTask,droppedFile.getName(), droppedFile.getAbsolutePath(), dropType);
			nodeTask.getLinkList().addLink(foundLink);
		}
	
		//associe le lien au bouton
		nodeTask.getLinkList().setFastLink(linkIndex, foundLink);
		
	}

	public void setNode(NodeTask nodeTask) {
				
		this.nodeTask=nodeTask;
		
		// cas ou il n'y a pas de noeud
		if (nodeTask==null) {
			resetButton(false);
			return;
		}
		
		// recupere le lien
		link = nodeTask.getLinkList().getHeritedFastLink(linkIndex);
		
		// verifie si le lien n'est pas vide
		if (link==null) {
			resetButton(true);
			return;
		}
		
		// recupere l'icone de base
		Icon icon = nodeTask.getDataBase().getNodeIconsDB().getNodeIcon(link.getIcon32Key(true)).getIcon();
		
		// verifie le type de lien
		if (!nodeTask.getLinkList().isDirectFastLink(linkIndex))
			icon = new StackedIcon(INDIRECT_ICON, icon);
		
		// affiche
		setIcon(icon);
		setToolTipText(link.getDescription());
		setEnabled(true);
		dropTarget.setActive(true);
		
	}
	
	private void resetButton(boolean dropable) {
		link=null;
		dropTarget.setActive(dropable);
		setIcon(NULL_ICON);
		setEnabled(false);
	}
	
	private void importAllPages(File destinationFile, File importFile) {
		
//		if (JOptionPane.showConfirmDialog(this, "Confirmer import des pages du fichier. \n"+importFile.getPath()+"\n a la fin de \n"+destinationFile.getPath(), "Import fichier PDF", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
//			return;

		PDFMergerUtility ut = new PDFMergerUtility();
		try {
			ut.addSource(destinationFile);
			ut.addSource(importFile);
			ut.setDestinationFileName(destinationFile.getPath());
			ut.mergeDocuments(null);
		} catch (IOException e) {
			LOGGER.info("Warning : Echec de la fusion des documents PDF. Motif = "+e.getMessage());
			JOptionPane.showMessageDialog(this, e.getMessage(), "Warning", JOptionPane.ERROR_MESSAGE);
		}
		
	}

}

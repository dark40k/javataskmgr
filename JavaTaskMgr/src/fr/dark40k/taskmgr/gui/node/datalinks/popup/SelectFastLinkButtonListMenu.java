package fr.dark40k.taskmgr.gui.node.datalinks.popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.database.datalinks.DataLinkList;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class SelectFastLinkButtonListMenu extends JMenu {
	
	public SelectFastLinkButtonListMenu(TaskMgrUndoMgr taskMgrUndoMgr, NodeTask nodeTask, DataLinkList linkList, DataLink link, LinkPopupMenu linkPopupMenu) {
		
		setText("Lien rapide");
		
		for (int index = 0; index < linkList.getFastLinkCount(); index++ )
			add(new SetFastLinkButtonMenu(taskMgrUndoMgr, nodeTask, index,linkList, link, linkPopupMenu));
			
		
		add(new JSeparator());

		{
			JMenuItem item = new JMenuItem("Ajouter emplacement lien rapide");
			add(item);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter emplacement lien rapide",false);
					linkList.addFastLink();
				}
			});

		}
		
		{
			JMenuItem item = new JMenuItem("Supprimer emplacement lien rapide");
			add(item);
			item.setEnabled(linkList.getFastLinkCount()>DataLinkList.MIN_FASTLINK_NBR);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter emplacement lien rapide",false);
					linkList.removeFastLink();
				}
			});
		}
		
	}

}

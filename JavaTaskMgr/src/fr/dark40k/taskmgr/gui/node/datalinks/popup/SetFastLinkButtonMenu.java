package fr.dark40k.taskmgr.gui.node.datalinks.popup;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.database.datalinks.DataLinkList;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class SetFastLinkButtonMenu extends JMenu {
	
	private static final ImageIcon FAST_LINK_ICON = new ImageIcon(LinkPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/dialog-ok-apply-4.png"));

	public SetFastLinkButtonMenu(TaskMgrUndoMgr taskMgrUndoMgr, NodeTask nodeTask, int fastLinkIndex, DataLinkList linkList, DataLink link, LinkPopupMenu linkPopupMenu) {
		
		DataLink fastLink = linkList.getFastLink(fastLinkIndex);
				
		setText(Integer.toString(fastLinkIndex+1)+" => "+ ((fastLink!=null)?fastLink.getDescription():"<vide>"));

		setForeground(Color.black);
		setBackground(Color.white);
		setOpaque(true);

		if (linkList.getFastLink(fastLinkIndex) == link)
			setIcon(FAST_LINK_ICON);
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getSource() != null) {
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Definir lien rapide",false);
					linkList.setFastLink(fastLinkIndex, link);
					
					linkPopupMenu.setVisible(false);
					
				}
				
			}
		});
		
		{
			JMenuItem item = new JMenuItem("Definir lien rapide");
			add(item);
			item.setEnabled(fastLink!=link);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Definir lien rapide",false);
					linkList.setFastLink(fastLinkIndex, link);
				}
			});

		}
		
		{
			JMenuItem item = new JMenuItem("Effacer lien rapide");
			add(item);
			item.setEnabled(fastLink!=null);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Effacer lien rapide",false);
					linkList.setFastLink(fastLinkIndex, null);
				}
			});
		}
		
		{
			JMenuItem item = new JMenuItem("Monter lien rapide");
			add(item);
			item.setEnabled((fastLink!=null)&&(fastLinkIndex>0));
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Monter lien rapide",false);
					linkList.moveUpFastLink(fastLinkIndex);
				}
			});
		}
		
		{
			JMenuItem item = new JMenuItem("Descendre lien rapide");
			add(item);
			item.setEnabled((fastLink!=null)&&(fastLinkIndex<(linkList.getFastLinkCount()-1)));
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Descendre lien rapide",false);
					linkList.moveDownFastLink(fastLinkIndex);
				}
			});
		}
		
		
		
	}
	

}

package fr.dark40k.taskmgr.gui.node.datalinks;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.database.datalinks.DataLinkType;
import fr.dark40k.taskmgr.database.nodeicons.NodeIconDB;
import fr.dark40k.taskmgr.gui.dialogs.NodeIconSelectDialog;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class EditLinkDialog extends JDialog {
	
	private final static Logger LOGGER = LogManager.getLogger();

	//
	// Dialogues utilisateurs
	//
	
	public static void editFileLink(Component parent, TaskMgrUndoMgr taskMgrUndoMgr, NodeIconDB nodeIconDB, DataLink link) {
		EditLinkDialog newDialog = new  EditLinkDialog(link, taskMgrUndoMgr, nodeIconDB);
		newDialog.setLocationRelativeTo(parent);
		newDialog.setVisible(true);
	}
	
	public static DataLink newFileLink(Component parent, TaskMgrUndoMgr taskMgrUndoMgr, NodeIconDB nodeIconDB, NodeTask node) {
		EditLinkDialog newDialog = new  EditLinkDialog(new DataLink(node), taskMgrUndoMgr, nodeIconDB);
		newDialog.setLocationRelativeTo(parent);
		newDialog.setVisible(true);
		return newDialog.link;
	}
	
	//
	// Parties internes
	//
	
	private NodeIconDB nodeIconDB;
	private TaskMgrUndoMgr	taskMgrUndoMgr;

	private final JPanel contentPanel = new JPanel();
	private JTextField descriptionTextField;
	private JTextField pathTextField;
	private JLabel lblIcon32;
	private JLabel lblIcon16;

	private DataLink link;
	private JComboBox<DataLinkType> dataLinkComboBox;
	
	private Integer icon16key;
	private Integer icon32key;
	
	private EditLinkDialog(DataLink link, TaskMgrUndoMgr taskMgrUndoMgr, NodeIconDB nodeIconDB) {
		
		this.nodeIconDB=nodeIconDB;
		this.taskMgrUndoMgr=taskMgrUndoMgr;

		initializeGUI();
		
		this.link = link;
		descriptionTextField.setText(link.getDescription());
		pathTextField.setText(link.getData());
		dataLinkComboBox.setSelectedItem(link.getType());
		icon16key=link.getIcon16Key(false);
		icon32key=link.getIcon32Key(false);
		
		dataLinkComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateDisplay();
			}
		});
		
		updateDisplay();
	}

	protected void cmdClearIcon16() {
		icon16key=null;
		updateDisplay();
	}

	protected void cmdOpenIcon16() {
		
		NodeIconSelectDialog chooser = new NodeIconSelectDialog();
		Integer res = chooser.showSelectDialog(this, taskMgrUndoMgr, nodeIconDB, (icon16key!=null) ? icon16key : NodeIconDB.NO_ICON_KEY);
		if (res != NodeIconSelectDialog.APPROVE_OPTION) return;

		icon16key=chooser.getSelectedKey();
		
		updateDisplay();
		
	}
	
	protected void cmdClearIcon32() {
		icon32key=null;
		updateDisplay();
	}

	protected void cmdOpenIcon32() {
		
		NodeIconSelectDialog chooser = new NodeIconSelectDialog();
		Integer res = chooser.showSelectDialog(this, taskMgrUndoMgr, nodeIconDB, (icon32key!=null) ? icon32key : NodeIconDB.NO_ICON_KEY);
		if (res != NodeIconSelectDialog.APPROVE_OPTION) return;

		icon32key=chooser.getSelectedKey();
		
		updateDisplay();
	}

	private void cmdCancel() {
		link=null;
		this.setVisible(false);
	}
	
	private void cmdOk() {
		
		if (!pathTextField.getText().equals(link.getData()))
			link.setData(pathTextField.getText());
		
		if (!descriptionTextField.getText().equals(link.getDescription()))
			link.setDescription(descriptionTextField.getText());
		
		if (!((DataLinkType)dataLinkComboBox.getSelectedItem()==link.getType()))
			link.setType((DataLinkType)dataLinkComboBox.getSelectedItem());
		
		if (link.getIcon16Key(false)!=icon16key)
			link.setIcon16Key(icon16key);

		if (link.getIcon32Key(false)!=icon32key)
			link.setIcon32Key(icon32key);

		this.setVisible(false);
		
	}
	
	private void cmdSelectFileDialog() {

		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		int res = chooser.showOpenDialog(this);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			LOGGER.error("No file selected");
			return;
		}

		if (descriptionTextField.getText().isEmpty()) 
			descriptionTextField.setText(sgFile.getName());
		pathTextField.setText(sgFile.getAbsolutePath());
		dataLinkComboBox.setSelectedItem(sgFile.isDirectory()?DataLinkType.DIRECTORY:DataLinkType.COMMAND);
	}
	
	private void updateDisplay() {
		
		if (icon16key!=null) {
			lblIcon16.setText("Manuel="+icon16key.toString());
			lblIcon16.setIcon(nodeIconDB.getNodeIcon(icon16key).getIcon());
		} else {
			Integer iconKey = ((DataLinkType)dataLinkComboBox.getSelectedItem()).icon16Key;
			lblIcon16.setText("Auto="+iconKey.toString());
			lblIcon16.setIcon(nodeIconDB.getNodeIcon(iconKey).getIcon());
		}
			
		if (icon32key!=null) {
			lblIcon32.setText("Manuel="+icon32key.toString());
			lblIcon32.setIcon(nodeIconDB.getNodeIcon(icon32key).getIcon());
		} else {
			Integer iconKey = ((DataLinkType)dataLinkComboBox.getSelectedItem()).icon32Key;
			lblIcon32.setText("Auto="+iconKey.toString());
			lblIcon32.setIcon(nodeIconDB.getNodeIcon(iconKey).getIcon());
		}
		
	}
	
	
	private void initializeGUI() {
		
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		
		setTitle("Editer un lien");
		setBounds(100, 100, 523, 366);
		getContentPane().setLayout(new BorderLayout());
		
		contentPanel.setBorder(new TitledBorder(null, "Param\u00E8tres du lien", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{32, 32, 32, 32, 40, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblDescription = new JLabel("Description :");
			GridBagConstraints gbc_lblDescription = new GridBagConstraints();
			gbc_lblDescription.anchor = GridBagConstraints.EAST;
			gbc_lblDescription.insets = new Insets(0, 0, 5, 5);
			gbc_lblDescription.gridx = 0;
			gbc_lblDescription.gridy = 0;
			contentPanel.add(lblDescription, gbc_lblDescription);
		}
		{
			descriptionTextField = new JTextField();
			GridBagConstraints gbc_descriptionTextField = new GridBagConstraints();
			gbc_descriptionTextField.gridwidth = 3;
			gbc_descriptionTextField.insets = new Insets(0, 0, 5, 5);
			gbc_descriptionTextField.fill = GridBagConstraints.HORIZONTAL;
			gbc_descriptionTextField.gridx = 1;
			gbc_descriptionTextField.gridy = 0;
			contentPanel.add(descriptionTextField, gbc_descriptionTextField);
			descriptionTextField.setColumns(10);
		}
		{
			JLabel lblData = new JLabel("Param\u00E8tres :");
			GridBagConstraints gbc_lblData = new GridBagConstraints();
			gbc_lblData.insets = new Insets(0, 0, 5, 5);
			gbc_lblData.anchor = GridBagConstraints.EAST;
			gbc_lblData.gridx = 0;
			gbc_lblData.gridy = 1;
			contentPanel.add(lblData, gbc_lblData);
		}
		{
			pathTextField = new JTextField();
			GridBagConstraints gbc_pathTextField = new GridBagConstraints();
			gbc_pathTextField.gridwidth = 2;
			gbc_pathTextField.insets = new Insets(0, 0, 5, 5);
			gbc_pathTextField.fill = GridBagConstraints.HORIZONTAL;
			gbc_pathTextField.gridx = 1;
			gbc_pathTextField.gridy = 1;
			contentPanel.add(pathTextField, gbc_pathTextField);
			pathTextField.setColumns(10);
		}
		{
			JButton fileDialogSelectionButton = new JButton("...");
			fileDialogSelectionButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cmdSelectFileDialog();
				}
			});
			GridBagConstraints gbc_fileDialogSelectionButton = new GridBagConstraints();
			gbc_fileDialogSelectionButton.insets = new Insets(0, 0, 5, 0);
			gbc_fileDialogSelectionButton.gridx = 3;
			gbc_fileDialogSelectionButton.gridy = 1;
			contentPanel.add(fileDialogSelectionButton, gbc_fileDialogSelectionButton);
		}
		{
			JLabel lblType = new JLabel("Type :");
			GridBagConstraints gbc_lblType = new GridBagConstraints();
			gbc_lblType.anchor = GridBagConstraints.EAST;
			gbc_lblType.insets = new Insets(0, 0, 5, 5);
			gbc_lblType.gridx = 0;
			gbc_lblType.gridy = 2;
			contentPanel.add(lblType, gbc_lblType);
		}
		{
			dataLinkComboBox = new JComboBox<DataLinkType>();
			dataLinkComboBox.setModel(new DefaultComboBoxModel<DataLinkType>(DataLinkType.values()));
			GridBagConstraints gbc_dataLinkComboBox = new GridBagConstraints();
			gbc_dataLinkComboBox.gridwidth = 3;
			gbc_dataLinkComboBox.insets = new Insets(0, 0, 5, 5);
			gbc_dataLinkComboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_dataLinkComboBox.gridx = 1;
			gbc_dataLinkComboBox.gridy = 2;
			contentPanel.add(dataLinkComboBox, gbc_dataLinkComboBox);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdOk();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdCancel();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		{
			JLabel lblIcone = new JLabel("Icone 16 :");
			GridBagConstraints gbc_lblIcone = new GridBagConstraints();
			gbc_lblIcone.anchor = GridBagConstraints.EAST;
			gbc_lblIcone.insets = new Insets(0, 0, 5, 5);
			gbc_lblIcone.gridx = 0;
			gbc_lblIcone.gridy = 3;
			contentPanel.add(lblIcone, gbc_lblIcone);
		}
		{
			lblIcon16 = new JLabel("<Auto>");
			GridBagConstraints gbc_lblIcon16 = new GridBagConstraints();
			gbc_lblIcon16.anchor = GridBagConstraints.WEST;
			gbc_lblIcon16.insets = new Insets(0, 0, 5, 5);
			gbc_lblIcon16.gridx = 1;
			gbc_lblIcon16.gridy = 3;
			contentPanel.add(lblIcon16, gbc_lblIcon16);
		}
		{
			JButton btnNewButton = new JButton("");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cmdOpenIcon16();
				}
			});
			btnNewButton.setIcon(new ImageIcon(EditLinkDialog.class.getResource("/fr/dark40k/taskmgr/res/icon16/folder-yellow_open.png")));
			GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
			gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
			gbc_btnNewButton.gridx = 2;
			gbc_btnNewButton.gridy = 3;
			contentPanel.add(btnNewButton, gbc_btnNewButton);
		}
		{
			JButton button = new JButton("");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cmdClearIcon16();
				}
			});
			button.setIcon(new ImageIcon(EditLinkDialog.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-delete-6.png")));
			GridBagConstraints gbc_button = new GridBagConstraints();
			gbc_button.insets = new Insets(0, 0, 5, 0);
			gbc_button.gridx = 3;
			gbc_button.gridy = 3;
			contentPanel.add(button, gbc_button);
		}
		{
			JLabel label = new JLabel("Icone 32 :");
			GridBagConstraints gbc_label = new GridBagConstraints();
			gbc_label.anchor = GridBagConstraints.EAST;
			gbc_label.insets = new Insets(0, 0, 0, 5);
			gbc_label.gridx = 0;
			gbc_label.gridy = 4;
			contentPanel.add(label, gbc_label);
		}
		{
			lblIcon32 = new JLabel("<Auto>");
			GridBagConstraints gbc_lblIcon32 = new GridBagConstraints();
			gbc_lblIcon32.anchor = GridBagConstraints.WEST;
			gbc_lblIcon32.insets = new Insets(0, 0, 0, 5);
			gbc_lblIcon32.gridx = 1;
			gbc_lblIcon32.gridy = 4;
			contentPanel.add(lblIcon32, gbc_lblIcon32);
		}
		{
			JButton button = new JButton("");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cmdOpenIcon32();
				}
			});
			button.setIcon(new ImageIcon(EditLinkDialog.class.getResource("/fr/dark40k/taskmgr/res/icon16/folder-yellow_open.png")));
			GridBagConstraints gbc_button = new GridBagConstraints();
			gbc_button.insets = new Insets(0, 0, 0, 5);
			gbc_button.gridx = 2;
			gbc_button.gridy = 4;
			contentPanel.add(button, gbc_button);
		}
		{
			JButton button = new JButton("");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cmdClearIcon32();
				}
			});
			button.setIcon(new ImageIcon(EditLinkDialog.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-delete-6.png")));
			GridBagConstraints gbc_button = new GridBagConstraints();
			gbc_button.gridx = 3;
			gbc_button.gridy = 4;
			contentPanel.add(button, gbc_button);
		}

	}
	
}

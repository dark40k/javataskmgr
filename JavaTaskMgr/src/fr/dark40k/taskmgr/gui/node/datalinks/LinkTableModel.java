package fr.dark40k.taskmgr.gui.node.datalinks;

import static fr.dark40k.taskmgr.gui.node.datalinks.NodeLinksPanel.DIRECT_LINK_BACKGROUND;
import static fr.dark40k.taskmgr.gui.node.datalinks.NodeLinksPanel.HERITED_LINK_BACKGROUND;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.Icon;
import javax.swing.table.AbstractTableModel;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.NodeTaskLink;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.database.datalinks.DataLinkList;
import fr.dark40k.taskmgr.database.datalinks.DataLinkType;
import fr.dark40k.taskmgr.util.OutlookConnector;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.taskmgr.util.OutlookConnector.Folder;

public class LinkTableModel extends AbstractTableModel {

	private final static String[] columnNames = { "Tp.", "Bt.", "Description", "Chemin" };

	private final TaskMgrUndoMgr taskMgrUndoMgr;
	
	private CascadeLinkList cascadeLinkList;
	private NodeTask nodeTask;
	
	private static class CascadeLinkList {
		
		private DataLinkList linkList;
		private CascadeLinkList nextList;
		private boolean isSubList;
		
		private int size;
		
		private CascadeLinkList(NodeTask nodeTask, boolean isSubList) {
			linkList = nodeTask.getLinkList();
			size = linkList.size();
			this.isSubList=isSubList;
			Node father = (nodeTask instanceof NodeTaskLink) ? ((NodeTaskLink)nodeTask).getLinkNode().getFather() : nodeTask.getFather();
			if (father instanceof NodeTask) {
				nextList = new CascadeLinkList((NodeTask) father,true);
				size += nextList.size;
			}
		}

		public static CascadeLinkList buildCascadeLinkList(NodeTask nodeTask) {
			return new CascadeLinkList(nodeTask,false);
		}
		
		public int size() {
			return size;
		}
		
		public DataLink get(int index) {
			if (index<linkList.size()) return linkList.get(index);
			if (nextList==null) throw new IndexOutOfBoundsException();
			return nextList.get(index-linkList.size());
		}

		public Integer getFastLinkPos(int index) {
			if (index<linkList.size()) {
				return linkList.getFastLinkIndexOf(linkList.get(index));
			}
			if (nextList==null) throw new IndexOutOfBoundsException();
			return nextList.getFastLinkPos(index-linkList.size());
		}
		
		public boolean isSubList(int index) {
			if (isSubList) return true;
			return (index>=linkList.size());
		}

		public void addLink(DataLink newLink) {
			if (isSubList) throw new RuntimeException("CascadeLinkList is readonly");
			linkList.addLink(newLink);
			size = linkList.size() + ( (nextList!=null) ? nextList.size : 0 );
		}

		public void removeLink(Integer rowIndex) {
			if (isSubList) throw new RuntimeException("CascadeLinkList is readonly");
			if (rowIndex>=size) throw new RuntimeException("Index >= size of 1st LinkList which can be edited");
			linkList.removeLink(rowIndex);
			size = linkList.size() + ( (nextList!=null) ? nextList.size : 0 );
		}

		public void moveUp(Integer rowIndex) {
			if (isSubList) throw new RuntimeException("CascadeLinkList is readonly");
			linkList.moveUp(rowIndex);
		}

		public void moveDown(Integer rowIndex) {
			if (isSubList) throw new RuntimeException("CascadeLinkList is readonly");
			linkList.moveDown(rowIndex);
		}
		
		public int topListSize() {
			return linkList.size();
		}
		
	}
	
	
	//
	// Constructeur
	//
	
	public LinkTableModel(TaskMgrUndoMgr taskMgrUndoMgr) {
		this.taskMgrUndoMgr=taskMgrUndoMgr;
	}

	//
	// Designation du noeud � visualiser
	//
	
	public void setNode(NodeTask nodeTask) {
		this.nodeTask = nodeTask;
		if (nodeTask!=null)
			this.cascadeLinkList = CascadeLinkList.buildCascadeLinkList(nodeTask);
		else
			this.cascadeLinkList = null;
		fireTableDataChanged();
	}

	//
	// Methods from AbstractTableModel that need to be Overriden
	//

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		if (cascadeLinkList == null)
			return 0;
		return cascadeLinkList.size();
	}

	
    public Color getRowColour(int row) {
        return (cascadeLinkList.isSubList(row)) ? HERITED_LINK_BACKGROUND : DIRECT_LINK_BACKGROUND ;
    }

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		if (cascadeLinkList == null)
			return null;
		if (rowIndex > cascadeLinkList.size())
			return null;

		switch (columnIndex) {
		case 0:
			return nodeTask.getDataBase().getNodeIconsDB().getNodeIcon(cascadeLinkList.get(rowIndex).getIcon16Key(true)).getIcon();
		case 1:
			int fastLinkPos = cascadeLinkList.getFastLinkPos(rowIndex);
			return (fastLinkPos>=0)?fastLinkPos+1:null;
		case 2:
			return cascadeLinkList.get(rowIndex).getDescription();
		case 3:
			return cascadeLinkList.get(rowIndex).getData();
		}

		return null;

	}
	
	@Override
	public Class<?> getColumnClass(int c) {
		switch (c) {
		case 0:
			return Icon.class;
		case 1:
			return String.class;
		case 2:
			return String.class;
		case 3:
			return String.class;
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		switch (col) {
//			case 1 : linkList.get(row).setDescription((String) value); break;
			default : throw new IllegalArgumentException("Col "+col+" cannot be edited");
		}
	}	

	//
	// Recuperation des donn�es
	//
	public DataLink getLink(int rowIndex) {
		return cascadeLinkList.get(rowIndex);
	}
	
	public boolean isHeritedLink(int rowIndex) {
		return cascadeLinkList.isSubList(rowIndex);
	}
	
    //
    // Ajout de liens dans la FileLinkList
    //

	public void addLink(Component parent) {

		DataLink newLink = EditLinkDialog.newFileLink(parent, taskMgrUndoMgr, nodeTask.getDataBase().getNodeIconsDB(), nodeTask);

		if (newLink == null)
			return;

		taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter lien",false);
		cascadeLinkList.addLink(newLink);
		
		fireTableRowsInserted(cascadeLinkList.size(), cascadeLinkList.size());
	}
	
	public void addOutlookMailList(List<OutlookConnector.Email> eMailList) {

		if (eMailList == null) return;
		if (eMailList.size() == 0) return;

		taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter lien sur Mail Outlook",false);

		for (OutlookConnector.Email email : eMailList) {
			cascadeLinkList.addLink(new DataLink(nodeTask,email.subject, email.entryID , DataLinkType.OUTLOOKEMAIL));
			fireTableRowsInserted(cascadeLinkList.size(), cascadeLinkList.size());
		}

	}

	public void addOutlookFolder(Folder mailFolder) {
		if (mailFolder == null) return;

		taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter lien sur Repertoire Outlook",false);
		
		cascadeLinkList.addLink(new DataLink(nodeTask,mailFolder.name, mailFolder.entryID , DataLinkType.OUTLOOKFOLDER));
		fireTableRowsInserted(cascadeLinkList.size(), cascadeLinkList.size());
		
	}

   //
    // Manipulation des donnes de la FileLinkList
    //

	public void editLink(Component parent, Integer rowIndex) {
		
		if (cascadeLinkList.isSubList(rowIndex)) return;
		
		taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Editer lien",false);
		EditLinkDialog.editFileLink(parent, taskMgrUndoMgr, nodeTask.getDataBase().getNodeIconsDB() ,cascadeLinkList.get(rowIndex));
		fireTableRowsUpdated(rowIndex, rowIndex);
	}

	public void removeLink(Integer rowIndex) {
		
		if (cascadeLinkList.isSubList(rowIndex)) return;
		
		taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Supprimer lien",false);
		cascadeLinkList.removeLink(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}

	public boolean moveLinkUp(Integer rowIndex) {
		
		if (rowIndex <= 0) return false;
		if (cascadeLinkList.isSubList(rowIndex)) return false;
		
		taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "D�placer lien",false);
		cascadeLinkList.moveUp(rowIndex);
		fireTableRowsUpdated(rowIndex - 1, rowIndex);
		
		return true;
	}

	public boolean moveLinkDown(Integer rowIndex) {
		
		if (rowIndex >= cascadeLinkList.topListSize()-1) return false;
		if (cascadeLinkList.isSubList(rowIndex)) return false;
		
		taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "D�placer lien",false);
		cascadeLinkList.moveDown(rowIndex);
		fireTableRowsUpdated(rowIndex, rowIndex + 1);
		
		return true;
	}

	public void runLink(int rowIndex) {
		cascadeLinkList.get(rowIndex).run();
	}

    
	
}

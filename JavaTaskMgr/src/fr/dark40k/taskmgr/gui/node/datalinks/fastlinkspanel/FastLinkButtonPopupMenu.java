package fr.dark40k.taskmgr.gui.node.datalinks.fastlinkspanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import fr.dark40k.pdfeditor.PdfEditor;
import fr.dark40k.pdfeditor.data.PdfEditorClipboard;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.database.datalinks.DataLinkType;
import fr.dark40k.taskmgr.gui.node.datalinks.EditLinkDialog;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class FastLinkButtonPopupMenu extends JPopupMenu {

//	private final static Logger LOGGER = LogManager.getLogger();
	
	private static final ImageIcon EDIT_LINK_ICON = new ImageIcon(FastLinkButtonPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/bookmark-new-list-4.png"));
	private static final ImageIcon REMOVE_LINK_ICON = new ImageIcon(FastLinkButtonPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-delete-6.png"));
	
	public FastLinkButtonPopupMenu(TaskMgrUndoMgr taskMgrUndoMgr, NodeTask nodeTask, int linkIndex){

		DataLink fastLink = nodeTask.getLinkList().getHeritedFastLink(linkIndex);
		
		{
			JMenuItem item = new JMenuItem((fastLink != null) ? fastLink.getData() : "<empty>");
			add(item);
			
			item.setForeground(Color.black);
			item.setBackground(Color.white);
			item.setOpaque(true);

			if (fastLink!=null) {
				item.setText(fastLink.getDescription());
				item.setIcon(nodeTask.getDataBase().getNodeIconsDB().getNodeIcon(fastLink.getIcon16Key(true)).getIcon());
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						fastLink.run();
					}
				});
			}
			else {
				item.setText("<No Link>");
				item.setEnabled(false);
			}
			
		}
		
		if (fastLink==null) return;
		
		NodeTask nodeTaskLink = fastLink.getNodeTask();
		boolean isEditable = (!nodeTaskLink.isReadOnly());
		boolean isDirectLink = nodeTask.getLinkList().isDirectFastLink(linkIndex);
		
		add(new JSeparator());

		{
			JMenuItem item = new JMenuItem("Editer lien");
			add(item);
			item.setIcon(EDIT_LINK_ICON);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTaskLink, "Editer lien",false);
					EditLinkDialog.editFileLink(getTopLevelAncestor(), taskMgrUndoMgr, nodeTaskLink.getDataBase().getNodeIconsDB() ,fastLink);
				}
			});
			item.setEnabled(isEditable);
		}
		
		{
			JMenuItem item = new JMenuItem("Supprimer lien");
			add(item);
			item.setIcon(REMOVE_LINK_ICON);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (JOptionPane.showConfirmDialog(getTopLevelAncestor(),
							"Confirmer effacement du lien. \n(le fichier n'est pas d�truit)", "WARNING",
							JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
						return;
					taskMgrUndoMgr.addUndoableNodeEdit(nodeTaskLink, "Supprimer lien",false);
					nodeTaskLink.getLinkList().setFastLink(linkIndex, null);
				}
			});
			item.setEnabled(isEditable && isDirectLink);
		}
		
		if (fastLink.getType()==DataLinkType.PDFFILE) {
			
			add(new JSeparator());

//			{
//				JMenuItem item = new JMenuItem("Importer doc PDF");
//				add(item);
//
//				item.addActionListener(new ActionListener() {
//					@Override
//					public void actionPerformed(ActionEvent e) {
//						PdfImportDialog.importPagesUsingDialog((Frame) getTopLevelAncestor(), new File(fastLink.getData()));
//					}
//				});
//
//				item.setEnabled(isEditable);
//			}			

			{
				int count = PdfEditorClipboard.getClipboardCount();
				
				JMenuItem item = new JMenuItem("Coller pages PDF ("+count+")");
				item.setToolTipText("Ajouter pages PDF selectionn�es");
				add(item);

				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						File file = new File(fastLink.getData());
						boolean status = PdfEditorClipboard.pastePages(file);
						if (!status) JOptionPane.showMessageDialog(FastLinkButtonPopupMenu.this, "Echec collage\n"+file.getAbsolutePath(), "Warning", JOptionPane.ERROR_MESSAGE);
					}
				});

				item.setEnabled(isEditable&&(count>0));
			}
			
			{
				JMenuItem item = new JMenuItem("Editer PDF");
				item.setToolTipText("Ouvrir document dans editeur PDF int�gr�.");
				add(item);

				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						File file = new File(fastLink.getData());
						PdfEditor.showPdfEditor(file); 
					}
				});

				item.setEnabled(isEditable);
			}
		}
		
	}
	
	
	
}

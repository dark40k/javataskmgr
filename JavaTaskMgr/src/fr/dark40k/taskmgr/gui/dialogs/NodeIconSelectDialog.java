package fr.dark40k.taskmgr.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.taskmgr.database.nodeicons.NodeIcon;
import fr.dark40k.taskmgr.database.nodeicons.NodeIconDB;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr.UndoableNodeDatabaseEdit;
import fr.dark40k.util.swing.layout.ModifiedFlowLayout;

public class NodeIconSelectDialog extends JDialog implements ActionListener {

	private final static Logger LOGGER = LogManager.getLogger();

	private static final FileFilter	iconFileFilter	= new FileNameExtensionFilter("Icon File (png)","png");

	public static final int	APPROVE_OPTION	= 1;
	public static final int	CANCEL_OPTION	= 0;

	private final JPanel	contentPanel	= new JPanel();
	private JPanel userIconsPanel;
	private JPanel defaultIconPanel;

	private HashMap<Integer, JButton> buttonList;
	private Integer selectedKey;

	private NodeIconDB nodeIconDB;
	private TaskMgrUndoMgr	taskMgrUndoMgr;

	private Integer res=CANCEL_OPTION;



	public NodeIconSelectDialog() {

		setTitle("Selectionner une icone");
		setBounds(100, 100, 721, 507);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		setModalityType(ModalityType.APPLICATION_MODAL);

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			GridBagConstraints gbc_scrollPane = new GridBagConstraints();
			gbc_scrollPane.gridheight = 3;
			gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
			gbc_scrollPane.fill = GridBagConstraints.BOTH;
			gbc_scrollPane.gridx = 0;
			gbc_scrollPane.gridy = 0;
			contentPanel.add(scrollPane, gbc_scrollPane);
			{
				JPanel panel = new JPanel();
				scrollPane.setViewportView(panel);
				panel.setLayout(new BorderLayout(0, 0));
				{
					defaultIconPanel = new JPanel();
					defaultIconPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Icones standard", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
					panel.add(defaultIconPanel, BorderLayout.NORTH);
					defaultIconPanel.setLayout(new ModifiedFlowLayout(FlowLayout.LEFT, 5, 5));
				}
				{
					userIconsPanel = new JPanel();
					panel.add(userIconsPanel, BorderLayout.CENTER);
					userIconsPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Icones utilisateur", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
					userIconsPanel.setLayout(new ModifiedFlowLayout(FlowLayout.LEFT, 5, 5));
				}
			}
		}
		{
			JButton btnAddIcon = new JButton("Importer");
			btnAddIcon.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					dlgImport();
				}
			});
			GridBagConstraints gbc_btnAddIcon = new GridBagConstraints();
			gbc_btnAddIcon.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnAddIcon.insets = new Insets(0, 0, 5, 0);
			gbc_btnAddIcon.gridx = 1;
			gbc_btnAddIcon.gridy = 0;
			contentPanel.add(btnAddIcon, gbc_btnAddIcon);
		}
		{
			JButton btnSuprimer = new JButton("Supprimer");
			btnSuprimer.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					dlgRemove();
				}
			});
			GridBagConstraints gbc_btnSuprimer = new GridBagConstraints();
			gbc_btnSuprimer.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnSuprimer.insets = new Insets(0, 0, 5, 0);
			gbc_btnSuprimer.gridx = 1;
			gbc_btnSuprimer.gridy = 1;
			contentPanel.add(btnSuprimer, gbc_btnSuprimer);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						dlgOK();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						dlgCancel();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

	}

	private void addButton(Integer iconKey) {
		JButton newButton = new JButton();
		NodeIcon nodeIcon = nodeIconDB.getNodeIcon(iconKey);
		newButton.setIcon(nodeIcon.getIcon());
		newButton.setFocusable(false);
		newButton.setToolTipText(nodeIcon.getKey()+" > "+nodeIcon.getName());
		newButton.setActionCommand(iconKey.toString());
		newButton.addActionListener(this);
		buttonList.put(iconKey, newButton);

		if (iconKey>0) {
			userIconsPanel.add(newButton);
			userIconsPanel.validate();
		} else {
			defaultIconPanel.add(newButton);
			defaultIconPanel.validate();
		}
	}

	private void removeButton(Integer removeKey) {

		JButton button = buttonList.get(removeKey);

		button.removeActionListener(this);
		userIconsPanel.remove(button);
		buttonList.remove(removeKey);

		userIconsPanel.validate();

	}

	@SuppressWarnings("unused")
	public Integer showSelectDialog(Component parent, TaskMgrUndoMgr taskMgrUndoMgr, NodeIconDB nodeIconDB, Integer selKey) {

		this.nodeIconDB=nodeIconDB;
		this.taskMgrUndoMgr=taskMgrUndoMgr;

		selectedKey = NodeIconDB.NO_ICON_KEY;

		buttonList = new HashMap<Integer, JButton>();
		for (Integer iconKey : new TreeSet<Integer>(nodeIconDB.keySet()))
//			if (iconKey>0)
				addButton(iconKey);

		setSelectedKey( buttonList.containsKey(selKey) ? selKey : NodeIconDB.NO_ICON_KEY );

		res=CANCEL_OPTION;

		setVisible(true);

		userIconsPanel.removeAll();
		buttonList = null;

		return res;
	}

	protected void dlgRemove() {

		if (selectedKey==null) return;

		if (selectedKey<0) {
			JOptionPane.showMessageDialog(this, "Supression interdite pour les icones internes.","Avertissement",JOptionPane.WARNING_MESSAGE);
			return;
		}

		Integer removeKey = selectedKey;
		setSelectedKey(NodeIconDB.NO_ICON_KEY);

		int n = JOptionPane.showConfirmDialog( this, "Confirmer la supression ?", "Supprimer icone", JOptionPane.YES_NO_OPTION);
		if (n!=JOptionPane.YES_OPTION) return;

		UndoableNodeDatabaseEdit undo = taskMgrUndoMgr.createUndoableGlobalEdit("Suppression icone");

		if (nodeIconDB.removeIcon(removeKey)) {
			removeButton(removeKey);
			taskMgrUndoMgr.addEdit(undo);
		}
		else
			JOptionPane.showMessageDialog(this, "Attention: Cette icone est utilis�e. \n Supression annul�e.","Avertissement", JOptionPane.WARNING_MESSAGE);

	}

	protected void dlgImport() {

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(TaskMgr.currentDirectory);
		chooser.setFileFilter(iconFileFilter);

		int res = chooser.showOpenDialog(null);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			LOGGER.error("No file selected");
			return;
		}

		UndoableNodeDatabaseEdit undo = taskMgrUndoMgr.createUndoableGlobalEdit("Importer icone");
		Integer iconKey=nodeIconDB.addNewIcon(sgFile).getKey();
		addButton(iconKey);
		taskMgrUndoMgr.addEdit(undo);
		this.repaint();
	}

	protected void dlgCancel() {
		res=CANCEL_OPTION;
		setVisible(false);
	}

	protected void dlgOK() {
		res=APPROVE_OPTION;
		setVisible(false);
	}

	public Integer getSelectedKey() {
		return selectedKey;
	}

	private void setSelectedKey(Integer newKey) {
		if (selectedKey!=NodeIconDB.NO_ICON_KEY) buttonList.get(selectedKey).setSelected(false);
		selectedKey = newKey;
		if (selectedKey!=NodeIconDB.NO_ICON_KEY) buttonList.get(selectedKey).setSelected(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Integer iconKey = Integer.valueOf(e.getActionCommand());
		setSelectedKey( (iconKey == selectedKey) ? NodeIconDB.NO_ICON_KEY : iconKey );
	}

}

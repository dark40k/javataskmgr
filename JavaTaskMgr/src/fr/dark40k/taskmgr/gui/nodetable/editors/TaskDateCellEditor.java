package fr.dark40k.taskmgr.gui.nodetable.editors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import fr.dark40k.taskmgr.database.taskdates.TaskDateData;
import fr.dark40k.util.swing.DateDialog;

public class TaskDateCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener  {

	protected static final String EDIT = "edit";

	private JButton button;

	private LocalDate currentValue;

	private String colTitle;

	public TaskDateCellEditor() {
        //Set up the editor (from the table's point of view),
        //which is a button.
        //This button brings up the color chooser dialog,
        //which is the editor from the user's point of view.
        button = new JButton();
        button.setActionCommand(EDIT);
        button.addActionListener(this);
        button.setBorderPainted(false);
	}

    /**
     * Handles events from the editor button and from
     * the dialog's OK button.
     */
    @Override
	public void actionPerformed(ActionEvent e) {
        if (EDIT.equals(e.getActionCommand())) {
            //The user has clicked the cell, so
            //bring up the dialog.
    		DateDialog dialog = new DateDialog(button,"Edition "+colTitle);
    		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    		dialog.setLocalDate(currentValue);
    		dialog.setVisible(true);

            //Make the renderer reappear.
    		if (dialog.getExitStatus()) {
    			currentValue=dialog.getLocalDate();
    			fireEditingStopped();
    		} else {
    			fireEditingCanceled();
    		}
        }
    }

    //Implement the one CellEditor method that AbstractCellEditor doesn't.
    @Override
	public Object getCellEditorValue() {
        return currentValue;
    }

    //Implement the one method defined by TableCellEditor.
    @Override
	public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {
    	currentValue = ((TaskDateData)value).getLocalDate();
    	colTitle = (String) table.getColumnModel().getColumn(column).getHeaderValue();
        return button;
    }

    //Need double click to start editing
	@Override
	public boolean isCellEditable(EventObject anEvent) {
		if (anEvent instanceof MouseEvent) {
			return ((MouseEvent) anEvent).getClickCount() >= 2;
		}
		return true;
	}
}

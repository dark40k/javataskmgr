package fr.dark40k.taskmgr.gui.nodetable.editors;

import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.expansionHandleWidth;
import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.linkIcon;
import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.nestingWidth;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.taskmgr.gui.popup.JPopupTextField;
import fr.dark40k.util.icons.CompoundIcon;

public class NodeCellEditor extends AbstractCellEditor implements TableCellEditor {

	JIconTextField textField = new JIconTextField();
	

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int vColIndex) {

		NodeRowData nodeRowData = (NodeRowData) value;
		Node node = nodeRowData.getNode();
		
		textField.setText(node.getName());
		textField.setFont(node.getEffectiveDisplay().getFont().getEffectiveFont());
		textField.setForeground(node.getEffectiveDisplay().getFrontColor().getColor());
		textField.setBackground(node.getEffectiveDisplay().getBackColorTop().getColor());

		Icon icon = new CompoundIcon(new Icon[] { (nodeRowData.isLink()) ? linkIcon : null, node.getEffectiveDisplay().getIcon1().getIcon(),
				node.getEffectiveDisplay().getIcon2().getIcon() });
		
		textField.setIcon(icon, expansionHandleWidth + nodeRowData.getNodeDepth() * nestingWidth);
		
		textField.requestFocus();

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				textField.selectAll();
			}
		});

		return textField;
	}

	@Override
	public Object getCellEditorValue() {
		return ((JTextField) textField).getText();
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		if (anEvent instanceof MouseEvent) {
			return ((MouseEvent) anEvent).getClickCount() >= 2;
		}
		return true;
	}
	
	private static class JIconTextField extends JPopupTextField {
		
		Icon icon;
		int offset1;
		private Insets dummyInsets;
		
	    public JIconTextField(){
	        super();
	        this.icon = null;
	 
	        Border border = UIManager.getBorder("TextField.border");
	        JTextField dummy = new JTextField();
	        this.dummyInsets = border.getBorderInsets(dummy);
	    }
	    
	    public void setIcon(Icon icon, int offset1) {
			this.icon=icon;
			this.offset1=offset1;
			setMargin(new Insets(2, offset1+icon.getIconWidth()+4, 2, 2));
		}
		
	    @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	 
	        if(this.icon!=null){
	            icon.paintIcon(this, g, dummyInsets.left -2+ offset1, (this.getHeight() - icon.getIconHeight())/2);
	        }
	 
	    }		
	}
	
}

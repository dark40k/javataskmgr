package fr.dark40k.taskmgr.gui.nodetable;

import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.expansionHandleWidth;
import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.nestingWidth;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.basic.BasicTableUI;

import fr.dark40k.taskmgr.database.Node;

public class NodeTableUI extends BasicTableUI {
    
    private boolean draggingRow = false;
 
    private Node nodeExpanded;
    
    boolean moveAsChild;
    boolean newChild;
    
	private int toDepth;
	
    private int fromRow;
	private int toRow;
	
	private NodeRowData fromRowData;
	private NodeRowData toRowDataPrevious;

   protected MouseInputListener createMouseInputListener() {
       return new DragDropRowMouseInputHandler();
   }
   
   public void paint(Graphics g, JComponent c) {
	   
        super.paint(g, c);
        
        if (draggingRow) {
        	
           	Rectangle fromRect = table.getCellRect(fromRow, table.convertColumnIndexToView(0), false);
           	Rectangle toRect = table.getCellRect(toRow, table.convertColumnIndexToView(0), false);
           	
           	if (moveAsChild) {
	        	g.setColor(Color.WHITE);
	        	g.fillRect(0, toRect.y+toRect.height-4, table.getWidth(), 8);
	        	g.fillRect(fromRect.x+nestingWidth*toDepth-2, toRect.y+toRect.height-2, expansionHandleWidth/4+4, fromRect.height/2+4);
	        	if (newChild) g.fillRect(fromRect.x+nestingWidth*toDepth-2, toRect.y+fromRect.height/2+toRect.height-2, expansionHandleWidth/2+4, 8);
	
	        	g.setColor(Color.RED);
	        	g.fillRect(0, toRect.y+toRect.height-2, table.getWidth(), 4);
	        	g.fillRect(fromRect.x+nestingWidth*toDepth, toRect.y+toRect.height, expansionHandleWidth/4, fromRect.height/2);
	        	if (newChild) g.fillRect(fromRect.x+nestingWidth*toDepth, toRect.y+fromRect.height/2+toRect.height, expansionHandleWidth/2, 4);
           	} else {
	        	g.setColor(Color.WHITE);
	        	g.fillRect(0, toRect.y+toRect.height-4, table.getWidth(), 8);
	        	g.fillRect(fromRect.x+nestingWidth*toDepth-2, toRect.y+toRect.height-fromRect.height/2-2, expansionHandleWidth/4+4, fromRect.height+4);
	
	        	g.setColor(Color.RED);
	        	g.fillRect(0, toRect.y+toRect.height-2, table.getWidth(), 4);
	        	g.fillRect(fromRect.x+nestingWidth*toDepth, toRect.y+toRect.height-fromRect.height/2, expansionHandleWidth/4, fromRect.height);
           	}
         }
   }
   
	class DragDropRowMouseInputHandler extends MouseInputHandler {

		private static final int onmask = MouseEvent.BUTTON1_DOWN_MASK;
		private static final int offmask =  MouseEvent.BUTTON2_DOWN_MASK +  MouseEvent.BUTTON3_DOWN_MASK;

		public void mouseDragged(MouseEvent evt) {

			// verifie que seul le bouton gauche de la souris est enfonc� pendant le drag
			if ((evt.getModifiersEx() & (onmask | offmask)) != onmask) {
				draggingRow=false;
				table.repaint();
				return;
			}
			
			// recupere la ligne a d�placer, quitte si pas trouv�e
			fromRow = table.getSelectedRow();
			
			if (fromRow<0) {
				draggingRow=false;
				table.repaint();
				return;
			}
			
			fromRowData = (NodeRowData) table.getValueAt(fromRow, table.convertColumnIndexToView(0));
			
			// annule le deplacement si noeud readonly
			if (fromRowData.getNode().isReadOnly()) {
				draggingRow=false;
				table.repaint();
				return;
			}
				
			// calcule la ligne de destination
			toRow = table.rowAtPoint(evt.getPoint());

			// annule le d�placement si la ligne de destination n'existe pas ou si pas de changement
			if ((toRow<0)||(fromRow==toRow)) {
				draggingRow=false;
				table.repaint();
				return;
			}

			// collapse la ligne � deplacer si elle est ouverte et termine pour relancer
			if ((fromRowData.canExpand()) && (((NodeTableModel) table.getModel()).isExpanded(fromRowData.getNode()))) {
				nodeExpanded = fromRowData.getNode();
				((NodeTableModel) table.getModel()).setExpanded(nodeExpanded, false);
				((JNodeTable)table).setSelectedRowAddress(fromRowData.getRowAddress());
				draggingRow=false;
				table.repaint();
				return;
			}
			
			// verifie la position de la souris par rapport au milieu de la colonne de titre puis defini l'offset:
			// => move comme fils immediat si souris � droite 
			// => move comme frere suivant si souris � gauche)
			Rectangle mouseOverCell = table.getCellRect(toRow, table.columnAtPoint(evt.getPoint()), false);
			moveAsChild=(evt.getPoint().x > (mouseOverCell.x+mouseOverCell.width/2));


			draggingRow=true;

			// recupere les donn�es de la ligne au-dessus de la ligne de destination
			toRowDataPrevious = (NodeRowData) table.getValueAt(toRow, table.convertColumnIndexToView(0));
			toDepth=toRowDataPrevious.getNodeDepth();

			// force le mode fils si la ligne pr�c�dente est une ligne expans�e (donc le noeud suivant est un fils)
			if (toRowDataPrevious.canExpand() && ((NodeTableModel) table.getModel()).isExpanded(toRowDataPrevious.getNode())) {
				moveAsChild=true;
			}
			
			// interdit le mode fils si la ligne pr�c�dente est une ligne repli�e (donc le noeud suivant n'est pas un fils)
			if (toRowDataPrevious.canExpand() && !(((NodeTableModel) table.getModel()).isExpanded(toRowDataPrevious.getNode()))) {
				moveAsChild=false;
			}
					
			// interdit le mode fils si la ligne pr�c�dente est une ligne de cloture (donc fin des fils) 
			if (toRowDataPrevious.isBottomLine()) {
				moveAsChild=false;
			}
			
			// corrige l'offset et la ligne d'affichage pour suivant le mode d'insertion (frere/fils)
			if (moveAsChild) {
				toDepth+=1;
			}

			newChild=moveAsChild && ((!toRowDataPrevious.canExpand()) || (!((NodeTableModel) table.getModel()).isExpanded(toRowDataPrevious.getNode())));
			
			
			// lance le r�affichage
			table.repaint();
			
		}

		public void mouseReleased(MouseEvent e) {
			super.mouseReleased(e);

			if (draggingRow) {
				
				// verifie et annule si mouvement conduit � aucune modification
				if (moveAsChild) {
					if ((toRowDataPrevious.getNode().getChildren().getChildCount() == 0)
							|| (toRowDataPrevious.getNode().getChildren().getChild(0).getKey() != fromRowData.getNode().getKey())) {

						((JNodeTable) table).moveNodeAsFirstChild(fromRowData.getNode(), toRowDataPrevious.getNode());
						
						((JNodeTable)table).setSelectedRowAddress(new RowAddress(toRowDataPrevious.getRowAddress(),fromRowData.getNode()));
						
					}
				} else {
					if ((fromRowData.getRow() - toRowDataPrevious.getRow() != 1)
							|| (fromRowData.getNode().getFather().getKey() != toRowDataPrevious.getNode().getFather().getKey())) {
						
						((JNodeTable) table).moveNodeAsYoungerBrother(fromRowData.getNode(), toRowDataPrevious.getNode());
						
						((JNodeTable)table).setSelectedRowAddress(new RowAddress(toRowDataPrevious.getRowAddress().getFatherRowAddress(),fromRowData.getNode()));
						
					}
				}
				
			}

			if (nodeExpanded != null) {
				RowAddress selected = ((JNodeTable)table).getSelectedRowAddress();
				((NodeTableModel) table.getModel()).setExpanded(nodeExpanded, true);
				nodeExpanded = null;
				((JNodeTable)table).setSelectedRowAddress(selected);
			}
			
			draggingRow = false;
			table.repaint();

		}
	}
}
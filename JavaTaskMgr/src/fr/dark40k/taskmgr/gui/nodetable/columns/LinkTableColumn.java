package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.taskmgr.gui.nodetable.renderers.IconCellRenderer;

public class LinkTableColumn extends NodeTableColumn {

	private final static ImageIcon icon = new ImageIcon(NodeTableColumn.class.getResource("res/link.png"));
	
	public LinkTableColumn() {
		super(null, icon, SwingConstants.CENTER, "Liens");
		cellRenderer = new IconCellRenderer();
		setPreferredWidth(25);
		setResizable(false);
	}
	
	@Override
	public Class<?> getColumnClass() {
		return Icon.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		return (nodeRowData.getNode() instanceof NodeTask) ? ((((NodeTask) nodeRowData.getNode()).getLinkList().size() > 0) ? icon : null) : null;
	}

}

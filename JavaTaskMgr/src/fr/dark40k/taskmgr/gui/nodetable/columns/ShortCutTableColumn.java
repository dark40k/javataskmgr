package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.taskmgr.gui.nodetable.renderers.IconCellRenderer;
import fr.dark40k.util.icons.StackedIcon;

public class ShortCutTableColumn extends NodeTableColumn {

	private final static ImageIcon INDIRECT_ICON = new ImageIcon(ShortCutTableColumn.class.getResource("/fr/dark40k/taskmgr/res/icon16/link.png"));
	
	private int linkNbr=0;
	
	public ShortCutTableColumn(int index) {
		super(Integer.toString(index), null, SwingConstants.CENTER, "Racourci rapide "+Integer.toString(index));
		this.linkNbr=index;
		cellRenderer = new IconCellRenderer();
		setPreferredWidth(25);
		setResizable(false);
	}

	@Override
	public Class<?> getColumnClass() {
		return Icon.class;
	}
	
	@Override
	public Object getValue(NodeRowData nodeRowData) {
		
		if (nodeRowData.isBottomLine()) return null;
		if (!(nodeRowData.getNode() instanceof NodeTask)) return null;
		
		NodeTask nodeTask = (NodeTask) nodeRowData.getNode();
		
		DataLink link = nodeTask.getLinkList().getHeritedFastLink(linkNbr);
		if (link!=null) {
			Icon icon = nodeTask.getDataBase().getNodeIconsDB().getNodeIcon(link.getIcon16Key(true)).getIcon();
			if (!nodeTask.getLinkList().isDirectFastLink(linkNbr)) icon = new StackedIcon(INDIRECT_ICON, icon);
			return icon;
		}
		
		return null;
	}

	@Override
	public boolean dblClick(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return false;
		if (!(nodeRowData.getNode() instanceof NodeTask)) return false;
		
		DataLink link = ((NodeTask) nodeRowData.getNode()).getLinkList().getHeritedFastLink(linkNbr);
		if (link==null) return false;
			
		link.run();
		
		return true;
	}
	
	@Override
	public String getToolTipText(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		if (!(nodeRowData.getNode() instanceof NodeTask)) return null;
		DataLink link = ((NodeTask) nodeRowData.getNode()).getLinkList().getHeritedFastLink(linkNbr);
		return (link!=null)?link.getDescription():null;
	}

}

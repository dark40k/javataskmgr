package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.database.interfaces.NodeLinkInterface;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;

public class KeyTableColumn extends NodeTableColumn {

	public KeyTableColumn() {
		super("Key", null, SwingConstants.CENTER, "Key");
		minWidth = 50;
		reset();
	}
	
	public void reset() {
		setPreferredWidth(50);
	}
	

	@Override
	public Class<?> getColumnClass() {
		return String.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		if (nodeRowData.getNode() instanceof NodeLinkInterface)
			return nodeRowData.getNode().getKey() + " (" + ((NodeLinkInterface) nodeRowData.getNode()).getLinkKey() + ")";
		return nodeRowData.getNode().getKey();
	}

}

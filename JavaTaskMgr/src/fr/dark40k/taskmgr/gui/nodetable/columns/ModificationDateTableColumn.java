package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.util.FormattersString;

public class ModificationDateTableColumn extends NodeTableColumn {

	public ModificationDateTableColumn() {
		super("Modification", null, SwingConstants.CENTER, "Date de modification");
		minWidth = 50;
		reset();
	}
	
	public void reset() {
		setPreferredWidth(50);
	}
	

	@Override
	public Class<?> getColumnClass() {
		return String.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		return FormattersString.format(nodeRowData.getNode().getModificationDateTime(),"<pas de valeur>");
	}

}

package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.taskmgr.gui.nodetable.editors.NodeCellEditor;
import fr.dark40k.taskmgr.gui.nodetable.renderers.NodeCellRenderer;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeNameTableColumn extends NodeTableColumn {

	public NodeNameTableColumn() {
		
		super("Tache",null,SwingConstants.LEFT,"Tache");
		
		cellEditor = new NodeCellEditor();
		cellRenderer = new NodeCellRenderer();

		minWidth = 100;
		reset();
		
	}
	
	public void reset() {
		setPreferredWidth(450);
	}
	

	@Override
	public Class<?> getColumnClass() {
		return Node.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		return nodeRowData;
	}

	@Override
	public boolean isEditable(NodeRowData nodeRowData) {
		return !nodeRowData.getNode().isReadOnly();
	}

	@Override
	public void setValue(TaskMgrUndoMgr taskMgrUndoMgr, NodeRowData nodeRowData, Object value) {
		if (!(value instanceof String))
			throw new RuntimeException("Unexpectd type=" + Object.class.toString() + " for column" + headerValue);
		taskMgrUndoMgr.addUndoableNodeEdit(nodeRowData.getNode(), "Edition titre", true);
		nodeRowData.getNode().setName((String) value);
	}
	
	
}

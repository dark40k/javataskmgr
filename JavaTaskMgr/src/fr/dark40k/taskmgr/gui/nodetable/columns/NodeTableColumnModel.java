package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

import org.jdom2.Element;

public class NodeTableColumnModel extends DefaultTableColumnModel {

	private static final String XMLTAG = "columns";
	private static final String XMLSUBTAG = "column";
	
	public final NodeTableColumn[] nodeColumns;
		
	//
	// Constructeur
	//
	
	public NodeTableColumnModel() {
		
		// creation des colonnes
		nodeColumns = new NodeTableColumn[] {
				
			   	// NODE
				new NodeNameTableColumn(),

		    	// DESCRIPTION
				new DescriptionTableColumn(),

		    	// LINK
				new LinkTableColumn(),

		    	// Raccourci 1
				new ShortCutTableColumn(0),

		    	// Raccourci 2
				new ShortCutTableColumn(1),

		    	// Raccourci 3
				new ShortCutTableColumn(2),

		    	// EXPECTEDSTART
				new TaskDateTableColumn("D�but pr�vu", null, "D�but pr�vu", (dates) -> {return dates.getExpectedStart();}),

				// EXPECTEDEND
				new TaskDateTableColumn("Fin pr�vue", null, "Fin pr�vue", (dates) -> {return dates.getExpectedEnd();}),

		    	// EXPECTEDSTART
				new TaskDateTableColumn("D�but eff.", null, "D�but eff.", (dates) -> {return dates.getEffectiveStart();}),

				// EXPECTEDEND
				new TaskDateTableColumn("Fin eff.", null, "Fin eff.", (dates) -> {return dates.getEffectiveEnd();}),

		    	// KEY
				new KeyTableColumn(),

		    	// CATEGORIES
				new CategoriesTableColumn(),
				
				// LECTURE SEULE
				new ReadOnlyTableColumn(),
				
				// CACHE
				new HiddenTableColumn(),
				
				// SCRIPT
				new JavaScriptTableColumn(),
				
				// CREATION
				new CreationDateTableColumn(),
				
				// Edition
				new ModificationDateTableColumn(),
				
		};
		
		// numerote et insere les colonnes dans la table
		for (int index=0; index<nodeColumns.length; index++) {
			nodeColumns[index].setModelIndex(index);
			addColumn(nodeColumns[index]);
		}
		
	}

	public void reset() {
		
		// enleve les colonnes existantes
		while (getColumnCount()>0) removeColumn(getColumn(0));
		
		// reinitialise et recr�e les colonnes
		for (int index=0; index<nodeColumns.length; index++) {
			nodeColumns[index].reset();
			addColumn(nodeColumns[index]);
		}
		
	}
	
	//
	// Import / Export XML
	//
	
	public Element exportXML() {
		
		Element elt = new Element(XMLTAG);
		
		for (int viewIndex=0; viewIndex<getColumnCount(); viewIndex++) {
			
			Element subElt = new Element(XMLSUBTAG);
			
			NodeTableColumn column = getColumn(viewIndex);
			
			subElt.setAttribute("index",Integer.toString(column.getModelIndex()));
			
			if (column.getResizable())
				subElt.setAttribute("width",Integer.toString(column.getPreferredWidth()));
			
			elt.addContent(subElt);
			
		}
		
		return elt;
	}

	public void importXML(Element elt) {

		// laisse tel quel si pas de donn�es
		if (elt==null) return;
		
		Element columnsElt = elt.getChild(XMLTAG);
		if (columnsElt==null) return;
		
		// enleve les colonnes existantes
		while (getColumnCount()>0) removeColumn(getColumn(0));

		// charge les colonnes sauvegard�es et recr�e les colonnes
		for (Element subElt : columnsElt.getChildren(XMLSUBTAG)) {

			// recupere l'index 
			int modelIndex = Integer.parseInt(subElt.getAttributeValue("index"));

			// reinitialise la colonne et applique les parametres pesistants
			nodeColumns[modelIndex].reset();

			// affiche la colonne
			addColumn(nodeColumns[modelIndex]);

			// recupere la largeur si redimensionnable
			if (nodeColumns[modelIndex].getResizable()) {
				String width = subElt.getAttributeValue("width");
				if (width!=null) 
					nodeColumns[modelIndex].setPreferredWidth(Integer.parseInt(width));
			}
			
		}
		
	}
	
	//
	// Getter / Setters
	//
	
	@Override
	public NodeTableColumn getColumn(int index) {
		return (NodeTableColumn) super.getColumn(index);
	}
	
	//
	// Manipulation des colonnes
	//
	
	@Override
	@Deprecated
	public void addColumn(TableColumn aColumn) {
		throw new UnsupportedOperationException("Command deactivated.");
	}
	
	public void addColumn(NodeTableColumn aColumn) {
		super.addColumn(aColumn);
	}

}

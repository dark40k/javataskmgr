package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.database.categories.Category;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;

public class CategoriesTableColumn extends NodeTableColumn {

	public CategoriesTableColumn() {
		super("Categories", null, SwingConstants.CENTER, "Categories");
		minWidth = 50;
		reset();
	}

	public void reset() {
		setPreferredWidth(100);
	}
	
	@Override
	public Class<?> getColumnClass() {
		return String.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		String res = null;
		for (Category cat : nodeRowData.getNode().getCategories()) {
			res = (res == null) ? cat.name : res + ", " + cat.name;
		}
		return res;
	}

}

package fr.dark40k.taskmgr.gui.nodetable.columns;

import java.time.LocalDate;
import java.util.function.Function;

import javax.swing.Icon;
import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.taskdates.TaskDateData;
import fr.dark40k.taskmgr.database.taskdates.TaskDates;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.taskmgr.gui.nodetable.editors.TaskDateCellEditor;
import fr.dark40k.taskmgr.gui.nodetable.renderers.TaskDateCellRenderer;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class TaskDateTableColumn extends NodeTableColumn {

	private final Function<TaskDates,TaskDateData> getTaskDateData;

	public TaskDateTableColumn(String name, Icon icon, String tooltiptext, Function<TaskDates,TaskDateData> getTaskDateData) {
		super(name, icon, SwingConstants.CENTER, tooltiptext);
		this.getTaskDateData=getTaskDateData;
		cellRenderer = new TaskDateCellRenderer();
		cellEditor = new TaskDateCellEditor();
		minWidth = 75;
		reset();
	}

	@Override
	public void reset() {
		setPreferredWidth(100);
	}

	@Override
	public Class<?> getColumnClass() {
		return TaskDateData.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		if (!(nodeRowData.getNode() instanceof NodeTask)) return null;
		return getTaskDateData.apply(((NodeTask) nodeRowData.getNode()).getDates());
	}

	@Override
	public boolean isEditable(NodeRowData nodeRowData) {
		return (nodeRowData.getNode() instanceof NodeTask);
	}

	@Override
	public void setValue(TaskMgrUndoMgr taskMgrUndoMgr, NodeRowData nodeRowData, Object value) {

		if (!(value instanceof LocalDate) && (value!=null))
			throw new RuntimeException("Unexpectd type=" + Object.class.toString() + " for column" + headerValue);

		taskMgrUndoMgr.addUndoableNodeEdit(nodeRowData.getNode(), "Edition de "+headerValue+" pr�vue",false);

		getTaskDateData.apply(((NodeTask) nodeRowData.getNode()).getDates()).setLocalDate((LocalDate)value);

	}


}

package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.Icon;
import javax.swing.table.TableColumn;

import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.taskmgr.gui.nodetable.renderers.HeaderCellRenderer;
import fr.dark40k.taskmgr.gui.nodetable.renderers.NodeDefaultCellRenderer;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public abstract class NodeTableColumn extends TableColumn {

	private String tooltiptext;
	private Icon icon;

	public NodeTableColumn(String name, Icon icon, Integer alignment, String tooltiptext) {

		this.icon=icon;
		this.tooltiptext = tooltiptext;

		headerValue = name;
		headerRenderer = new HeaderCellRenderer(icon, alignment, tooltiptext);

		cellRenderer = new NodeDefaultCellRenderer(alignment);

	}

	public void reset() {};

	public abstract Class<?> getColumnClass();

	public abstract Object getValue(NodeRowData nodeRowData);

	@SuppressWarnings("unused")
	public boolean isEditable(NodeRowData nodeRowData) {
		return false;
	};

	@SuppressWarnings("unused")
	public void setValue(TaskMgrUndoMgr taskMgrUndoMgr, NodeRowData nodeRowData, Object value) {
		throw new UnsupportedOperationException("Column is not editable:" + headerValue);
	}

	/**
	 * @param nodeRowData ligne selectionnee
	 * @return true si une action a ete r�alisee, false si pas d'action associ�e
	 */
	public boolean dblClick(NodeRowData nodeRowData) {
		return false;
	}

	public String getHeaderToolTipText() {
		return tooltiptext;
	}

	public Icon getHeaderIcon() {
		return icon;
	}

	@SuppressWarnings("unused")
	public String getToolTipText(NodeRowData nodeRowData) {
		return null;
	}

	@Override
	public String getHeaderValue() {
		return (String) headerValue;
	}

	@Override
	public Object getIdentifier() {
		return super.getIdentifier();
	}

	@Override
	public void setIdentifier(Object identifier) {
		if (!(identifier instanceof Integer)) throw new UnsupportedOperationException("Wrong type of data : "+identifier.getClass().getName());
		super.setIdentifier(identifier);
	}

}
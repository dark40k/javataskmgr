package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.util.FormattersString;

public class CreationDateTableColumn extends NodeTableColumn {

	public CreationDateTableColumn() {
		super("Creation", null, SwingConstants.CENTER, "Date de creation");
		minWidth = 50;
		reset();
	}
	
	public void reset() {
		setPreferredWidth(50);
	}
	

	@Override
	public Class<?> getColumnClass() {
		return String.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		return FormattersString.format(nodeRowData.getNode().getCreationDateTime(),"<pas de valeur>");
	}

}

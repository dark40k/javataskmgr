package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeFilter;
import fr.dark40k.taskmgr.database.NodeStatus;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.NodeTemplate;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.taskmgr.gui.nodetable.renderers.IconCellRenderer;

public class HiddenTableColumn extends NodeTableColumn {

	private final static ImageIcon icon = new ImageIcon(HiddenTableColumn.class.getResource("res/eye-crossed.png"));
	
	public HiddenTableColumn() {
		super(null, icon, SwingConstants.CENTER, "Cach�");
		cellRenderer = new IconCellRenderer();
		setPreferredWidth(25);
		setResizable(false);
	}
	
	@Override
	public Class<?> getColumnClass() {
		return Icon.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		
		Node node = nodeRowData.getNode();
		
		// teste en 1er le plus courant
		if (node instanceof NodeTask) return null;
		
		if (node instanceof NodeFilter) return ((NodeFilter) node).isHidden() ? icon : null;
		if (node instanceof NodeStatus) return ((NodeStatus) node).isHidden() ? icon : null;
		if (node instanceof NodeTemplate) return ((NodeTemplate) node).isHidden() ? icon : null;
		
		return null;
	}

}

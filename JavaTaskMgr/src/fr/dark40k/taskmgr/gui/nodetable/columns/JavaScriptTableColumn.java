package fr.dark40k.taskmgr.gui.nodetable.columns;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;
import fr.dark40k.taskmgr.gui.nodetable.renderers.IconCellRenderer;

public class JavaScriptTableColumn extends NodeTableColumn {

	private final static ImageIcon icon = new ImageIcon(NodeTableColumn.class.getResource("res/javascript.png"));
	private static final ImageIcon VALID_SCRIPT_ICON = new ImageIcon(JavaScriptTableColumn.class.getResource("res/dialog-ok-apply-4.png"));
	private static final ImageIcon INVALID_SCRIPT_ICON = new ImageIcon(JavaScriptTableColumn.class.getResource("res/edit-delete-6.png"));
	
	public JavaScriptTableColumn() {
		super(null, icon, SwingConstants.CENTER, "Macros javascript");
		cellRenderer = new IconCellRenderer();
		setPreferredWidth(25);
		setResizable(false);
	}
	
	@Override
	public Class<?> getColumnClass() {
		return Icon.class;
	}

	@Override
	public Object getValue(NodeRowData nodeRowData) {
		if (nodeRowData.isBottomLine()) return null;
		
		Node node = nodeRowData.getNode();
		
		if (!node.getScript().sourceExists()) return null;
		
		return (node.getScript().isValid()) ? VALID_SCRIPT_ICON : INVALID_SCRIPT_ICON;
		
	}

}

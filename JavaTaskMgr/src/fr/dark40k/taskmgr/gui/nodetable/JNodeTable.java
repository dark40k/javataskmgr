package fr.dark40k.taskmgr.gui.nodetable;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.NodeTaskLink;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.gui.nodetable.renderers.BottomLineCellRenderer;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.FormattersString;
import fr.dark40k.util.dragdrop.DropFilesHandler;
import fr.dark40k.util.dragdrop.DropFilesHandler.DropFileUser;

public class JNodeTable extends JTable {

	private final static Logger LOGGER = LogManager.getLogger();

	public static final Color LINKED_SELECTION_BACKGROUND = new Color(127,201,255);

	public final static Icon expandedIcon = UIManager.getIcon("Tree.expandedIcon");
	public final static Icon collapsedIcon = UIManager.getIcon("Tree.collapsedIcon");
	public final static Icon linkIcon = new ImageIcon(JNodeTable.class.getResource("res/emblem-symbolic-link-3.png"));

	public static int expansionHandleWidth = 16; // FBT force la largeur du bouton (plus grand que l'icone utilis�e)
	public static int nestingWidth = 16; // FBT Ajout� pour contr�ler le pas
	public static int expansionHandleHeight = 16; // FBT Ajout� pour contr�ler le pas

	private BottomLineCellRenderer bottomLineCellRenderer = new BottomLineCellRenderer();

	protected NodeTableModel nodeTableModel;

	private TaskMgrUndoMgr taskMgrUndoMgr;

	@SuppressWarnings("unused")
	private final DropFilesHandler dropFilesHandler;

	// ===============================================================================================================
	//
	// Constructeur
	//
	// ===============================================================================================================

	public JNodeTable(TaskMgrUndoMgr taskMgrUndoMgr, NodeTableModel tableModel) {

		super(tableModel, tableModel.columnModel);

		this.taskMgrUndoMgr=taskMgrUndoMgr;
		this.nodeTableModel=tableModel;

		// affichage grille
		setShowGrid(false);
		setIntercellSpacing(new Dimension(0, 0));

		// charge le popupmenu des en-t�tes
		HeaderPopupMenu.registerListener(this);

		// touches clavier sp�cifiques
		setKeyBindings();

		// drag/drop pour manipuler les lignes
	    setUI(new NodeTableUI());

	    // drag/drop des fichier sur les lignes
		dropFilesHandler = new DropFilesHandler(this,new DropFileUser() {
			@Override
			public void dropFiles(DropTargetDropEvent e, List<File> list) {

				// recupere la ligne et la colonne cliqu�e
		        int row = rowAtPoint(e.getLocation());

		        // recupere la donnee sur laquelle on a dropp�
			    NodeRowData nodeRowData=nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(row));

			    LOGGER.info("Drop node ="+nodeRowData.getNode());

			    // verifie si le noeud est de type nodeTask
			    if (!(nodeRowData.getNode() instanceof NodeTask)) {
			    	LOGGER.info("Cancelling : Destination node type incorrect ="+nodeRowData.getNode().getClass().getName());
			    	return;
			    }

				NodeTask nodeTask = (NodeTask) nodeRowData.getNode();

				taskMgrUndoMgr.addUndoableNodeEdit(nodeTask, "Ajouter lien ("+list.size()+")",false);

				for (File file : list) {
					nodeTask.getLinkList().addLink(new DataLink(nodeTask,file.getName(), file));
				}

				changeSelection(nodeRowData.getRow(), 0, false, false);

			}

		});


	}

	// ===============================================================================================================
	//
	// Surcharges fonctionnement general
	//
	// ===============================================================================================================

	@Override
	public void tableChanged(TableModelEvent e) {
		super.tableChanged(e);

		//
		// Met � jours les hauteurs de ligne
		//

		for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++) {

			// recupere la ligne
			NodeRowData nodeRowData = nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(rowIndex));

			// recuperation du noeud de la ligne en cours
			Node node = nodeRowData.getNode();

			// hauteur de ligne
			int newRowHeight =
					(nodeRowData.isBottomLine())
					?
					node.getEffectiveDisplay().getHeight().getEndHeight()
					:
					node.getEffectiveDisplay().getHeight().getHeight();

			// reinitialise la hauteur si necessaire
			if (getRowHeight(rowIndex) != newRowHeight) {
//				System.out.println("row "+rowIndex+" => "+newRowHeight+" pts. / "+ getRowHeight(rowIndex));
				setRowHeight(rowIndex, newRowHeight);
			}

		}

	}

	@Override
	@Deprecated
	public void setModel(TableModel model) {
		if (!(model instanceof NodeTableModel)) throw new IllegalArgumentException();
		nodeTableModel=(NodeTableModel)model;
		super.setModel(model);
	}

	@Override
	public NodeTableModel getModel() {
		return (NodeTableModel) dataModel;
	}

	// ===============================================================================================================
	//
	// Methodes d'information
	//
	// ===============================================================================================================

	public Node getNodeAtDisplayedRow(int displayedRow) {
		if (displayedRow<0) return null;
		return nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(displayedRow)).getNode();
	}

	public RowAddress getRowAddressAtDisplayedRow(int displayedRow) {
		if (displayedRow<0) return null;
		return nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(displayedRow)).getRowAddress();
	}

	private int getNextBrotherRow(int displayedRow) {
		if ( (displayedRow<0) || (displayedRow>=getRowCount()) ) return -1;

		NodeRowData rowData = nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(displayedRow));

		displayedRow += (rowData.isExpanded()) ? rowData.getNodeSubLines()+2 : 1;

		NodeRowData fatherRowData = rowData.getFather();
		if (fatherRowData!=null) {
			if (displayedRow>(fatherRowData.getRow()+fatherRowData.getNodeSubLines()) ) displayedRow = fatherRowData.getRow()+1;
		}
		else {
			if (displayedRow >= getRowCount()) displayedRow = 0;
		}

		return displayedRow;
	}

	// ===============================================================================================================
	//
	// Methodes d'information sur la selection
	//
	// ===============================================================================================================

	public NodeRowData getSelectedNodeRowData() {
		int row = getSelectedRow();
		if (row<0) return null;
		return nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(row));
	}

	public RowAddress getSelectedRowAddress() {
		NodeRowData nodeRowData = getSelectedNodeRowData();
		if (nodeRowData==null) return null;
		return nodeRowData.getRowAddress();
	}

	public Node getSelectedNode() {
		int row = getSelectedRow();
		if (row<0) return null;
		return getNodeAtDisplayedRow(row);
	}

	public Node getSelectedNodeFather() {
		int row = getSelectedRow();
		if (row<0) return null;
		NodeRowData rowData = nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(row)).getFather();
		if (rowData==null) return ((NodeTableModel) dataModel).getRoot();
		return rowData.getNode();
	}

	// ===============================================================================================================
	//
	// Methodes pour modifier la selection
	//
	// ===============================================================================================================

//	public void setSelectedNode(Node node) {
//		for (int row=0; row<getRowCount();row++) {
//			if (getNodeAtDisplayedRow(row)==node) {
//				changeSelection(row, 0, false, false);
//				return;
//			}
//		}
//		clearSelection();
//	}

	public void setSelectedRowAddress(RowAddress rowAddress) {

		if (rowAddress==null) {
			clearSelection();
			return;
		}

		for (int row=0; row<getRowCount();row++) {
			if (getRowAddressAtDisplayedRow(row).equals(rowAddress)) {
//				LOGGER.warn("Found row="+row);
				changeSelection(row, 0, false, false);
				return;
			}
		}

//		LOGGER.warn("Not found="+rowAddress);
		clearSelection();
	}

	public void selectFatherNode(int displayedRow) {

		if ( (displayedRow<0) || (displayedRow>=getRowCount()) ) return;

		NodeRowData rowData = nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(displayedRow));
		NodeRowData fatherRowData = rowData.getFather();

		if (fatherRowData==null) return;

		changeSelection(rowData.getFather().getRow(), 0, false, false);
	}

	public void selectFirstChildNode(int displayedRow) {

		if ( (displayedRow<0) || (displayedRow>=getRowCount()) ) return;

		NodeRowData rowData = nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(displayedRow));

		if (!rowData.canExpand()) return;

		if (!rowData.isExpanded()) getModel().setAllExpanded(rowData.getNode());

		changeSelection(rowData.getRow()+1, 0, false, false);

	}

	public void selectPreviousLinkNode(int displayedRow) {

		if ( (displayedRow<0) || (displayedRow>=getRowCount()) ) return;

		Node node = getNodeAtDisplayedRow(displayedRow);;

		int searchKey = (node instanceof NodeTaskLink)?((NodeTaskLink)node).getLinkKey():node.getKey();

		for (int offset=1; offset<getRowCount(); offset++) {

			int searchRow = Math.floorMod(displayedRow-offset, getRowCount());

			Node rowNode = getNodeAtDisplayedRow(searchRow);
			if (rowNode.getKey()==node.getKey()) continue;

			int rowKey = (rowNode instanceof NodeTaskLink)?((NodeTaskLink)rowNode).getLinkKey():rowNode.getKey();

			if (rowKey==searchKey) {
				changeSelection(searchRow, 0, false, false);
				return;
			}
		}

	}

	public void selectNextLinkNode(int displayedRow) {

		if ( (displayedRow<0) || (displayedRow>=getRowCount()) ) return;

		Node node = getNodeAtDisplayedRow(displayedRow);;

		int searchKey = (node instanceof NodeTaskLink)?((NodeTaskLink)node).getLinkKey():node.getKey();

		for (int offset=1; offset<getRowCount(); offset++) {

			int searchRow = Math.floorMod(displayedRow+offset, getRowCount());

			Node rowNode = getNodeAtDisplayedRow(searchRow);
			if (rowNode.getKey()==node.getKey()) continue;

			int rowKey = (rowNode instanceof NodeTaskLink)?((NodeTaskLink)rowNode).getLinkKey():rowNode.getKey();

			if (rowKey==searchKey) {
				changeSelection(searchRow, 0, false, false);
				return;
			}
		}

	}

	public void selectNextBrotherNode(int displayedRow) {
		int newRow = getNextBrotherRow(displayedRow);
		changeSelection(newRow, 0, false, false);
	}

	public void selectPreviousBrotherNode(int displayedRow) {

		if ( (displayedRow<0) || (displayedRow>=getRowCount()) ) return;

		int previousRow = -1;
		int nextRow = displayedRow;

		do {
			previousRow = nextRow;
			nextRow = getNextBrotherRow(nextRow);
		} while (nextRow != displayedRow);

		changeSelection(previousRow, 0, false, false);

	}

	public void selectPreviousMatch(String searchText) {

		searchText = cleanString(searchText);
		if (searchText.isEmpty()) return;

		int startRow = getSelectedRow();

		int selectionKey = TaskMgrDB.NO_NODE_KEY;

		if (startRow>=0) {
			selectionKey = getNodeAtDisplayedRow(startRow).getKey();
			startRow = (startRow > 0) ? getRowCount() + startRow - 1 : getRowCount();
		} else {
			startRow = getRowCount()-1;
		}

		for (int offset=0; offset<getRowCount(); offset++) {

			int row = (startRow-offset) % getRowCount();

			NodeRowData rowData = (nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(row)));

			if (rowData.isBottomLine()) continue;
			if (rowData.getNode().getKey()==selectionKey) continue;

			if ((!rowData.getNode().getName().isEmpty()) && (cleanString(rowData.getNode().getName()).contains(searchText.toLowerCase()))) {
				changeSelection(row, 0, false, false);
				return;
			}

		}
	}

	public void selectNextMatch(String searchText) {

		searchText = cleanString(searchText);
		if (searchText.isEmpty()) return;

		int startRow = getSelectedRow();

		int selectionKey = TaskMgrDB.NO_NODE_KEY;

		if (startRow>=0) {
			selectionKey = getNodeAtDisplayedRow(startRow).getKey();
			startRow = startRow +1;
		} else {
			startRow = 0;
		}

		for (int offset=0; offset<getRowCount(); offset++) {

			int row = (startRow+offset) % getRowCount();

			NodeRowData rowData = (nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(row)));

			if (rowData.isBottomLine()) continue;
			if (rowData.getNode().getKey()==selectionKey) continue;

			if ((!rowData.getNode().getName().isEmpty()) && (cleanString(rowData.getNode().getName()).contains(searchText.toLowerCase()))) {
				changeSelection(row, 0, false, false);
				return;
			}

		}
	}

	// ===============================================================================================================
	//
	// Methodes pour modifier les noeuds
	//
	// ===============================================================================================================

	public void moveNodeAsFirstChild(Node nodeToMove, Node father) {

		// check if copy/paste would be acceptable
		String cantMoveMsg=nodeToMove.getDataBase().canMoveAsChild(nodeToMove,father);;
		if (cantMoveMsg!=null) {
			JOptionPane.showMessageDialog(null, "Action interdite: "+cantMoveMsg,"Paste error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		LOGGER.info("Moving {"+nodeToMove+"} as 1st child of {"+father+"}");

		// do move
		taskMgrUndoMgr.addUndoableGlobalEdit("D�placer noeud");
		nodeToMove.getDataBase().moveNodeAsFirstChild(nodeToMove,father);
	}

	public void moveNodeAsYoungerBrother(Node nodeToMove, Node nodeUpperBrother) {

		// check if copy/paste would be acceptable
		String cantMoveMsg=nodeToMove.getDataBase().canMoveAsChild(nodeToMove,nodeUpperBrother.getFather());;
		if (cantMoveMsg!=null) {
			JOptionPane.showMessageDialog(null, "Action interdite: "+cantMoveMsg,"Paste error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		LOGGER.info("Moving {"+nodeToMove+"} as next brother of {"+nodeUpperBrother+"}");

		// do move
		taskMgrUndoMgr.addUndoableGlobalEdit("D�placer noeud");
		nodeToMove.getDataBase().moveNodeAsYoungerBrother(nodeToMove,nodeUpperBrother);
	}

	// ===============================================================================================================
	//
	// Outils internes
	//
	// ===============================================================================================================

	private static String cleanString(String string) {
	    return Normalizer.normalize(string, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();
	}

	// ===============================================================================================================
	//
	// Surcharges pour affichages
	//
	// ===============================================================================================================

	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {

		if ((convertColumnIndexToModel(column)>0) && nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(row)).isBottomLine())
			return bottomLineCellRenderer;

		return super.getCellRenderer(row, column);
	}

	@Override
    public String getToolTipText(MouseEvent e) {
        String tip = null;
        java.awt.Point p = e.getPoint();
        int rowIndex = rowAtPoint(p);
        int colIndex = columnAtPoint(p);

		tip = ((NodeTableModel) dataModel).getToolTipText(convertRowIndexToModel(rowIndex), colIndex);

		if (tip!=null) {
			if (tip.length()>0)
				tip=FormattersString.textToHTML(tip);
			else
				tip = null;
		}

        return tip;
    }

	// ===============================================================================================================
	//
	// Surcharge pour gestion du clavier
	//
	// ===============================================================================================================

	@Override
    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
    	if (interceptKey(ks)) return super.processKeyBinding(ks, e, condition, pressed);
    	return false;
   }

    private boolean interceptKey(KeyStroke ks) {
    	if (isEditing()) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0))) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0))) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, KeyEvent.SHIFT_DOWN_MASK))) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, KeyEvent.SHIFT_DOWN_MASK))) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0))) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0))) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.SHIFT_DOWN_MASK))) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.SHIFT_DOWN_MASK))) return true;
    	if (ks.equals(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0))) return true;
    	return false;
    }

	private void setKeyBindings() {

		// remove ENTER default action ("selectNextRowCell")
		getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
	    getActionMap().put("Enter", new AbstractAction() {
	        @Override
	        public void actionPerformed(ActionEvent ae) {
	        	if (convertColumnIndexToView(0)<0) return;
	        	if (isEditing())
	        		getCellEditor().stopCellEditing();
	        }
	    });

		// overwrite RIGHT key => select next link
		getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "SelectNextLink");
	    getActionMap().put("SelectNextLink", new AbstractAction() {
	        @Override
	        public void actionPerformed(ActionEvent ae) {
	        	selectNextLinkNode(getSelectedRow());
	        }
	    });

		// overwrite SHIFT+LEFT key => select previous link
		getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, KeyEvent.SHIFT_DOWN_MASK), "SelectPreviousLink");
	    getActionMap().put("SelectPreviousLink", new AbstractAction() {
	        @Override
	        public void actionPerformed(ActionEvent ae) {
	        	selectPreviousLinkNode(getSelectedRow());
	        }
	    });

		// overwrite SHIFT + RIGHT key => select next link
		getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, KeyEvent.SHIFT_DOWN_MASK), "SelectChild");
	    getActionMap().put("SelectChild", new AbstractAction() {
	        @Override
	        public void actionPerformed(ActionEvent ae) {
	        	selectFirstChildNode(getSelectedRow());
	        }
	    });

		// overwrite LEFT key => select previous link
		getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "SelectFather");
	    getActionMap().put("SelectFather", new AbstractAction() {
	        @Override
	        public void actionPerformed(ActionEvent ae) {
	        	selectFatherNode(getSelectedRow());
	        }
	    });

		// overwrite DOWN key => select next brother
		getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "SelectNextBrother");
	    getActionMap().put("SelectNextBrother", new AbstractAction() {
	        @Override
	        public void actionPerformed(ActionEvent ae) {
	        	selectNextBrotherNode(getSelectedRow());
	        }
	    });

		// overwrite UP key => select previous brother
		getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "SelectPreviousBrother");
	    getActionMap().put("SelectPreviousBrother", new AbstractAction() {
	        @Override
	        public void actionPerformed(ActionEvent ae) {
	        	selectPreviousBrotherNode(getSelectedRow());
	        }
	    });

	}

	// ===============================================================================================================
	//
	// Surcharge pour gestion de la souris
	//
	// ===============================================================================================================

	@Override
	protected void processMouseEvent(MouseEvent e){

		if (e.getID() == MouseEvent.MOUSE_PRESSED)
			if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) != 0)
				if (mousePressedExpansionMgt(e))
					return;

		if (e.getID() == MouseEvent.MOUSE_CLICKED)
			if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) != 0)
				if (e.getClickCount()>=2)
					if (mouseDblClickedMgt(e))
						return;

		super.processMouseEvent(e);

	}

	private boolean mouseDblClickedMgt(MouseEvent evt) {

		// recupere la ligne et la colonne cliqu�e
        int row = rowAtPoint(evt.getPoint());
        int col = columnAtPoint(evt.getPoint());

        // recupere la ligne de donn�e
        NodeRowData nodeRowData=nodeTableModel.getNodeRowDataAtModelRow(convertRowIndexToModel(row));

        // execute l'action correspondant � la colonne
        return nodeTableModel.columnModel.getColumn(col).dblClick(nodeRowData);

	}

	private boolean mousePressedExpansionMgt(MouseEvent evt) {

		// recupere la ligne et la colonne cliqu�e
        int row = rowAtPoint(evt.getPoint());
        int col = columnAtPoint(evt.getPoint());

        // quitte si ce n'est pas la colonne 0 (titre) qui a �t� cliqu�e
        if ((row<0)||(col<0)) return false;
        if (convertColumnIndexToModel(col)!=0) return false;

        // recupere le RowData et la profondeur d'indentation correspondant � la ligne cliqu�e
        NodeRowData nodeRowData=(NodeRowData) getValueAt(row, col);
        int nd =  ((NodeTableModel) dataModel).nestingDepth(convertRowIndexToModel(row));

        // calcule la position relative horizontale par rapport au d�but de la cellule
        int posX = evt.getX()-getInsets().left-getCellRect(row, col, false).x;

        // si la position est negative, on est dans la bordure => quitte
        if (posX<0) return false;

        // si la position relative est inf�rieure � l'indentation max, on est dans les colonnes
        if (posX < nd * nestingWidth) {

        	// calcule le nombre de g�n�ration vers l'ancetre
        	int n = nd - posX / nestingWidth;

        	// cherche le rowData correspondant � l'ancetre
        	NodeRowData selRowData = nodeRowData;
        	for (int i=0; i<n;i++) selRowData=selRowData.getFather();

        	// selectionne l'ancetre
        	setSelectedRowAddress(selRowData.getRowAddress());

        	// quitte en bloquant les traitements suivants
        	return true;

        }

        // verifie si le noeud n'a pas de bouton d'expansion (ne peut pas s'etendre) => quitte
        Node node = (nodeRowData).getNode();
        if (!nodeRowData.canExpand()) return false;

    	// teste si on est audela du bouton d'expansion => quitte
    	if (posX - nd * nestingWidth > expansionHandleWidth) return false;

        // save selection
        RowAddress selection = getSelectedRowAddress();

        // update display
        ((NodeTableModel) dataModel).setExpanded(node, !(((NodeTableModel) dataModel).isExpanded(node)));

        // restore selection if needed
        setSelectedRowAddress(selection);

        return true;
	}


}

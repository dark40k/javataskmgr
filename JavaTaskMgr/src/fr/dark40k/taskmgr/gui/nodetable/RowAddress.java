package fr.dark40k.taskmgr.gui.nodetable;

import fr.dark40k.taskmgr.database.Node;

public class RowAddress {
	
	private Node node;
	private RowAddress fatherRowAddress;
	
	public RowAddress(NodeTableModel tableModel, int modelRow) {
		this(tableModel.getNodeRowDataAtModelRow(modelRow));
	}
	
	public RowAddress(NodeRowData nodeRowData) {
		node = nodeRowData.getNode();
		NodeRowData fatherNodeRowData = nodeRowData.getFather();
		if (fatherNodeRowData!=null) fatherRowAddress = new RowAddress(fatherNodeRowData);
	}

	public RowAddress(RowAddress fatherRowAddress, Node child) {
		this.node=child;
		this.fatherRowAddress=fatherRowAddress;
	}
	
	public Node getNode() {
		return node;
	}
	
	public RowAddress getFatherRowAddress() {
		return fatherRowAddress;
	}
	
	public int getModelRow(NodeTableModel tableModel) {
		
		int searchRow = (fatherRowAddress==null)?0:fatherRowAddress.getModelRow(tableModel);
		
		do {
			
			NodeRowData searchNodeRowData = tableModel.getNodeRowDataAtModelRow(searchRow);
		
			if (searchNodeRowData.getNode().equals(node)) return searchRow;
		
			searchRow += searchNodeRowData.getNodeSubLines();
			
		} while (searchRow<tableModel.getRowCount());
		
		return -1;
	}
	
	public NodeRowData getNodeRowData(NodeTableModel tableModel) {
		
		int searchRow = getModelRow(tableModel);

		if (searchRow<0) return null;
		
		return tableModel.getNodeRowDataAtModelRow(searchRow);
	}
	
	public boolean equals(Object object) {
		
		if (object==null) return false;
		if (!(object instanceof RowAddress)) return false;
		
		RowAddress nodeChainedSelection = (RowAddress) object;
		
		if (!(node.equals(nodeChainedSelection.node))) return false;
		
		if ((fatherRowAddress==null) || (nodeChainedSelection.fatherRowAddress==null)) return ((fatherRowAddress==null) && (nodeChainedSelection.fatherRowAddress==null));
		
		return fatherRowAddress.equals(nodeChainedSelection.fatherRowAddress);
	}
	
	public final static RowAddress getBrotherRowAddress(RowAddress rowAddress, Node brother) {
		if (!brother.getFather().equals(rowAddress.node.getFather())) throw new IllegalArgumentException("Node "+brother.getKey()+" is not brother.");
		return new RowAddress(rowAddress.fatherRowAddress,brother);
	}
	
	public String toString() {
		if (fatherRowAddress!=null) return "[ "+node.toString()+", "+fatherRowAddress.toString()+" ]";
		return "[ "+node.toString()+" ]";
	}
	
}

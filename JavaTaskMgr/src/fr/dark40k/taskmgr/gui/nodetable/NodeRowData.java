package fr.dark40k.taskmgr.gui.nodetable;

import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.linkIcon;

import javax.swing.Icon;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.interfaces.NodeLinkInterface;
import fr.dark40k.util.icons.CompoundIcon;

public class NodeRowData {
	
	private int row;
	
	private Node node;
	private Integer linkedKey;
	
	private NodeRowData fatherRow;					
	
	private NodeRowData topRow;		// si different de null => ligne de rappel
	
	private RowAddress rowAddress;
	
	private boolean canExpand = false;
	private boolean isExpanded = false;
	
	private boolean isLink = false;
	private int nd;
	private int subLines = 0;

	private CompoundIcon icon = null;
	private Icon icon1 = null;
	private Icon icon2 = null;


	public NodeRowData(int row, Node node, NodeRowData fatherRow, int nd, NodeRowData topRow) {
		
		this.row=row;
		
		this.node = node;
		this.nd = nd;
		this.fatherRow=fatherRow;
		
		this.rowAddress = new RowAddress((fatherRow!=null)?fatherRow.rowAddress:null,node);
		
		this.isLink=(node instanceof NodeLinkInterface);
		this.linkedKey=(this.isLink)?((NodeLinkInterface)node).getLinkKey():node.getKey();
				
		this.topRow=topRow;
		
		update();
	}

	//
	// Getters
	//
	
	public int getRow() {
		return row;
	}
	
	public RowAddress getRowAddress() {
		return rowAddress;
	}
	
	public boolean canExpand() {
		return canExpand;
	}
	
	public boolean isExpanded() {
		return isExpanded;
	}
		
	public boolean isBottomLine() {
		return (topRow!=null);
	}

	public NodeRowData getTopRow() {
		return (topRow!=null)?topRow:this;
	}
	
	public boolean isLink() {
		return isLink;
	}
	
	public int getLinkedKey() {
		return linkedKey;
	}
	
	public Node getNode() {
		return node;
	}
	
	public NodeRowData getFather() {
		return fatherRow;
	}
	
	public int getNodeDepth() {
		return nd;
	}
	
	public int getNodeSubLines() {
		return subLines;
	}

	public Icon getIcon() {
		return icon;
	}
	
	//
	// Methodes / Setters
	//
	
	public void setCanExpand(boolean canExpand, boolean isExpanded) {
		this.canExpand=canExpand;
		this.isExpanded=isExpanded;
	}
	
	public void addSubLines(int n) {
		subLines+=n;
	}
	
	public void update() {
		
		if (topRow == null)
			if ((icon1 != node.getEffectiveDisplay().getIcon1().getIcon()) || (icon2 != node.getEffectiveDisplay().getIcon2().getIcon())) {
				icon1 = node.getEffectiveDisplay().getIcon1().getIcon();
				icon2 = node.getEffectiveDisplay().getIcon2().getIcon();
				icon = new CompoundIcon(new Icon[] { (this.isLink()) ? linkIcon : null, icon1, icon2 });
			}

	}
	
}

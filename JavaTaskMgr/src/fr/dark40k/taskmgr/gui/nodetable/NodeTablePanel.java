package fr.dark40k.taskmgr.gui.nodetable;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeFilter;
import fr.dark40k.taskmgr.gui.nodetable.NodeTableModel.ExpansionState;
import fr.dark40k.taskmgr.gui.nodetable.columns.NodeTableColumnModel;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.util.swing.SmoothViewport;

public class NodeTablePanel extends JPanel   {

	public static final String	XMLTAG	= "nodelist";

	private final static int DX_PER_FRAME = 10; // pixel / frame
	private final static int DY_PER_FRAME = 10; // pixel / frame
	private final static int MSEC_PER_FRAME = 20; // msec / frame
	private final static double ACC_PER_FRAME = 1.15;

	private final JNodeTable table;
	private final NodeTableModel tableModel;
	private final NodeTableColumnModel columnModel;

	public NodeTablePanel(TaskMgrUndoMgr taskMgrUndoMgr) {

		super();

		this.setLayout(new BorderLayout());

		// initialisation table et modeles
		columnModel = new NodeTableColumnModel();
		tableModel = new NodeTableModel(taskMgrUndoMgr, columnModel);
		table = new JNodeTable(taskMgrUndoMgr, tableModel);

		// affichage lisse
		SmoothViewport viewport = new SmoothViewport(DX_PER_FRAME, DY_PER_FRAME, MSEC_PER_FRAME, ACC_PER_FRAME);
		viewport.setView(table);

		// scrollpane
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewport(viewport);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.add(scrollPane, BorderLayout.CENTER);

		// parametres comportement table
		table.setAutoscrolls(false);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		// gestion dynamique du scrollpane pour afficher la zone selectionn�e
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    @Override
			public void valueChanged(ListSelectionEvent lse) {
		        if (!lse.getValueIsAdjusting()) {

		        	NodeRowData rowData=table.getSelectedNodeRowData();

		        	if (rowData!=null) {

		        		NodeRowData topRowData = rowData.getTopRow();

						Rectangle rect = table.getCellRect(topRowData.getRow(), 0, true);

						int rowNbr = rowData.getNodeSubLines();
						if (rowNbr>0) rect=rect.union(table.getCellRect(topRowData.getRow()+rowNbr+1, 0, true));

			        	// System.out.println("updating position1." + rect.y + " / " + rect.height);

			        	if (rect.height>scrollPane.getViewport().getHeight()) rect.height=scrollPane.getViewport().getHeight();

			        	// System.out.println("updating position2." + rect.y + " / " + rect.height);

			        	table.scrollRectToVisible(rect);
		        	}

		            table.repaint();
		        }
		    }
		});

	}


	//
	// Acces � la table
	//

	public JNodeTable getTable() {
		return table;
	}

	public void addListSelectionListener(ListSelectionListener listener) {
		table.getSelectionModel().addListSelectionListener(listener);
	}

	public void addNodeTableMouseListener(MouseListener listener) {
		table.addMouseListener(listener);
	}


	//
	// Sauvergarder/Backup de la configuration
	//

	public Element exportXML() {

		Element elt=new Element(XMLTAG);

		elt.addContent(columnModel.exportXML());

		elt.addContent(tableModel.exportXML());

		return elt;
	}

	public void importXML(Element baseElt) {

		Element elt = baseElt.getChild(XMLTAG);
		if (elt==null) return;

		columnModel.importXML(elt);

		tableModel.importXML(elt);

	}


	//
	// Mise � jours de l'affichage
	//
	public void updateDisplay() {
		tableModel.updateDisplay();
	}

	//
	// Root node
	//

	public void setDisplayedRootNode(Node node) {
		tableModel.setRoot(node);
	}

	public Node getDisplayedRootNode() {
		return tableModel.getRoot();
	}

	//
	// Display filter mgt
	//

	public void setFilter(NodeFilter node) {
		tableModel.setFilter(node);
	}


	//
	// Selection mgt
	//

	public Node getSelectedNode() {
		return table.getSelectedNode();
	}

	public Node getSelectedNodeFather() {
		return table.getSelectedNodeFather();
	}

	public RowAddress getSelectedRowAddress() {
		return table.getSelectedRowAddress();
	}

	public RowAddress getRowAddress(int displayRow) {
		if ((displayRow<0) || (displayRow>=table.getRowCount())) return null;
		return table.getRowAddressAtDisplayedRow(displayRow);
	}

	public int getSelectedRow() {
		return table.getSelectedRow();
	}

	public void setSelectedKey(int key) {
		Node node=tableModel.setExpanded(key,true);
		if (node==null) return;
		setSelectedRow(tableModel.getRow(node));
	}

	public void setSelectedRow(int displayRow) {
		if ((displayRow<0) || (displayRow>=table.getRowCount())) return;
		table.changeSelection(displayRow,0,false,false);
	}

	public void setSelectedRowAddress(RowAddress rowAddress) {
		table.setSelectedRowAddress(rowAddress);
	}

	public void clearSelection() {
		table.clearSelection();
	}

	public void selectPreviousLinkNode(int displayRow) {
		table.selectPreviousLinkNode(displayRow);
	}

	public void selectNextLinkNode(int displayRow) {
		table.selectNextLinkNode(displayRow);
	}

	public void selectPreviousMatch(String text) {
		table.selectPreviousMatch(text);
	}

	public void selectNextMatch(String text) {
		table.selectNextMatch(text);
	}

	//
	// Expansion / collapse mgt of nodes
	//

	public void setExpanded(Node node) {
		RowAddress selection  = table.getSelectedRowAddress();
		tableModel.setExpanded(node, true);
		table.setSelectedRowAddress(selection);
	}

	public void collapseNode(Node node) {
		RowAddress selection  = table.getSelectedRowAddress();
		tableModel.setExpanded(node, false);
		table.setSelectedRowAddress(selection);
	}

	public void setAllExpanded(Node node) {
		RowAddress selection  = table.getSelectedRowAddress();
		tableModel.setAllExpanded(node);
		table.setSelectedRowAddress(selection);
	}

	public void setAllCollapsed(Node node) {
		RowAddress selection  = table.getSelectedRowAddress();
		tableModel.setAllCollapsed(node);
		table.setSelectedRowAddress(selection);
	}

	public ExpansionState getExpansionState() {
		return tableModel.getExpansionState();
	}

	public ExpansionState getExpansionState(Node node) {
		return tableModel.getExpansionState(node);
	}

	public void setExpansionState(ExpansionState state) {
		RowAddress selection  = table.getSelectedRowAddress();
		tableModel.setExpansionState(state);
		table.setSelectedRowAddress(selection);
	}




}

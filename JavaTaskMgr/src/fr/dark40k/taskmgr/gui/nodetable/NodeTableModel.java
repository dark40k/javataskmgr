package fr.dark40k.taskmgr.gui.nodetable;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeFilter;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeListener;
import fr.dark40k.taskmgr.gui.nodetable.columns.NodeTableColumnModel;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;

public class NodeTableModel extends AbstractTableModel implements UpdateNodeListener {

	private final static Logger LOGGER = LogManager.getLogger();

	public static final String	XMLTAG_TABLEMODEL	= "tablemodel";

	private final TaskMgrUndoMgr taskMgrUndoMgr;

	final NodeTableColumnModel columnModel;

	private TaskMgrDB database;
	private Node displayRootNode;

	private ArrayList<NodeRowData> rowList = new ArrayList<>();

	private final CollapsedKeySet collapsedSet = new CollapsedKeySet();

	private NodeFilter nodeFilter = null;
	private NodeFilteredChildren nodeFilteredChildren = null;


	// =====================================================================
	//
	// Constructeur
	//
	// =====================================================================

	public NodeTableModel(TaskMgrUndoMgr taskMgrUndoMgr, NodeTableColumnModel columnModel) {
		this.taskMgrUndoMgr = taskMgrUndoMgr;
		this.columnModel = columnModel;
	}

	// =====================================================================
	//
	// Informations
	//
	// =====================================================================

	public Node getRoot() {
		return displayRootNode;
	}

	public int getRow(Node node) {
		for (int row = 0;row<rowList.size();row++)
			if (rowList.get(row).getNode()==node)
				return row;
		return -1;
	}

	public int nestingDepth(int row) {
		return rowList.get(row).getNodeDepth();
	}

	public NodeRowData getNodeRowDataAtModelRow(int row) {
		return rowList.get(row);
	}

	// =====================================================================
	//
	// Mise � jours de la racine ou du filtre, calcul du contenu
	//
	// =====================================================================

	public void setRoot(Node node) {

//		LOGGER.debug("node="+node);

		displayRootNode=node;

		// met � jour la base si besoin
		if (database!=node.getDataBase()) {

			if (database!=null) database.removeUpdateListener(this);

			database=node.getDataBase();
			database.addUpdateListener(this);

			// reinitialise le filtre
			nodeFilter=null;

		}

		// recalcule l'affichage
		updateDisplay();

	}

	public void setFilter(NodeFilter nodeFilter) {

		// met a jour le filtre
		this.nodeFilter=nodeFilter;

		// recalcule l'affichage
		updateDisplay();

	}

	public void updateDisplay() {

		// recalcule la liste de noeuds visibles
		if ((nodeFilter==null)||(displayRootNode==null))
			nodeFilteredChildren = null;
		else
			nodeFilteredChildren = new NodeFilteredChildren(displayRootNode.getDataBase(), nodeFilter.getFilter());

		// recalcule les lignes d'affichage
		buildRowList();

		// fire event for drawing update
		fireTableDataChanged();

	}

	// =====================================================================
	//
	// Calcul des lignes d'affichage
	//
	// =====================================================================

	private void buildRowList() {

		// vide la table des noeuds
		rowList.clear();

		// cas ou il n'y a pas de rootnode.
		if (displayRootNode==null) return;

		// lance la construction de la liste
		int row=0;
		for (Node child : displayRootNode.getChildren())
			if ((nodeFilteredChildren == null) || (nodeFilteredChildren.getVisibility(child)))
				row+=addLine(row,child,null,0);

	}

	private int addLine(int row, Node node, NodeRowData father, int nd) {

		// ligne d'ouverture
		NodeRowData rowData = new NodeRowData(row, node, father, nd, (NodeRowData) null);
		rowList.add(row,rowData);

		// lignes intermediaires
		for (Node child : node.getChildren()) {
			if ((nodeFilteredChildren == null) || (nodeFilteredChildren.getVisibility(child))) {
				boolean isExpanded = isExpanded(node);
				rowData.setCanExpand(true, isExpanded);
				if (isExpanded)
					rowData.addSubLines(addLine(row+rowData.getNodeSubLines()+1,child,rowData,nd+1));
				else
					return 1;
			}
		}

		// quite si le noeud est repli� ou ne peut s'etendre (i.e. pas de ligne de cloture)
		if (!rowData.canExpand()) return 1;

		// ligne de cloture
		int newRow=row+rowData.getNodeSubLines()+1;
		rowList.add(newRow,new NodeRowData(newRow,node,father,nd,rowData));

		// nombre de lignes ajout�es
		return rowData.getNodeSubLines()+2;
	}

	// =====================================================================
	//
	// Gestion de l'expansion des noeuds
	//
	// =====================================================================

	public boolean isExpanded(Node node) {
		return !collapsedSet.contains(node.getKey());
	}

	public boolean canExpand(int row) {
		return rowList.get(row).canExpand();
	}

	public void setExpanded(Node node, boolean expand) {

		assert node!=null : "cannot expand/collapse null node";

		if (expand == isExpanded(node)) return;

//		Version optimis�e - supprim�e car potentiellement pas propre : buildRowList peut modifier plus de choses que calcul�
//
//		if (expand) {
//			collapsedSet.remove(node);
//			buildRowList();
//			int nodeRow=getRow(node);
//			fireTableRowsInserted(nodeRow+1, nodeRow+rowList.get(nodeRow).getNodeSubLines());
//
//		} else {
//			collapsedSet.add(node);
//			int nodeRow=getRow(node);
//			int firstRow=nodeRow+1;
//			int lastRow=nodeRow+rowList.get(nodeRow).getNodeSubLines();
//			buildRowList();
//			fireTableRowsDeleted(firstRow, lastRow);
//		}

		if (expand)
			collapsedSet.remove(node.getKey());
		else
			collapsedSet.add(node.getKey());

		buildRowList();
		fireTableDataChanged();

	}

	public Node setExpanded(Integer key, boolean expand) {
		Node node=database.getNode(key);
		if (node==null) return null;
		setExpanded(node, expand);
		return node;
	}

	public void setAllExpanded(Node node) {

		ArrayDeque<Node> stack = new ArrayDeque<>();
		stack.push(node);

		while (!stack.isEmpty()) {
			Node stackNode = stack.pollLast();
			for (Node child : stackNode.getChildren()) stack.push(child);
			collapsedSet.remove(stackNode.getKey());
		}
		buildRowList();
		fireTableDataChanged();
	}

	public void setAllCollapsed(Node node) {

		ArrayDeque<Node> stack = new ArrayDeque<>();
		stack.push(node);

		while (!stack.isEmpty()) {
			Node stackNode = stack.pollLast();
			for (Node child : stackNode.getChildren()) stack.push(child);
			if (node!=null) collapsedSet.add(stackNode.getKey());
		}
		buildRowList();
		fireTableDataChanged();
	}

	public ExpansionState getExpansionState() {
		return new ExpansionState(displayRootNode);
	}

	public ExpansionState getExpansionState(Node node) {
		return new ExpansionState(node);
	}

	protected void setExpansionState(ExpansionState state) {

		state.updateCollapsedSet();

		buildRowList();

		fireTableDataChanged();
	}

	public class ExpansionState {

		private HashMap<Integer, Boolean> nodeCollapsedStateMap = new HashMap<>();

		public ExpansionState(Node node) {
			ArrayDeque<Node> stack = new ArrayDeque<>();
			stack.push(node);
			while (!stack.isEmpty()) {
				Node stackNode = stack.pollLast();
				for (Node child : stackNode.getChildren()) stack.push(child);
				nodeCollapsedStateMap.put(stackNode.getKey(), collapsedSet.contains(stackNode.getKey()));
			}
		}

		protected void updateCollapsedSet() {

			ArrayDeque<Node> stack = new ArrayDeque<>();
			stack.push(displayRootNode);

			while (!stack.isEmpty()) {
				Node stackNode = stack.pollLast();
				for (Node child : stackNode.getChildren()) stack.push(child);

				Boolean nodeCollapsedState = nodeCollapsedStateMap.get(stackNode.getKey());
				if (nodeCollapsedState != null) {
					if (nodeCollapsedState == true)
						collapsedSet.add(stackNode.getKey());
					else if (nodeCollapsedState == false)
						collapsedSet.remove(stackNode.getKey());
				}
			}

		}

	}

	public void importXML(Element rootElement) {

		Element elt = rootElement.getChild(XMLTAG_TABLEMODEL);
		if (elt==null) return;

		collapsedSet.importXML(elt);

		for (Integer key : collapsedSet)
			if (database.getNode(key)==null) {
				LOGGER.warn("Collapsed node does not exist, key="+key);
				collapsedSet.remove(key);
			}

		buildRowList();

		fireTableDataChanged();
	}

	public Element exportXML() {
		Element elt = new Element(XMLTAG_TABLEMODEL);
		collapsedSet.exportXML(elt);
		return elt;
	}


	// =====================================================================
	//
	// Listeners des modifications de la base de donn�e
	//
	// =====================================================================

	@Override
	public void updateDataBase(UpdateNodeEvent evt) {

		// recharge le rootnode via la clef
		if (displayRootNode!=null) displayRootNode=database.getNode(displayRootNode.getKey());

		// reinitialise la liste d'affichage
		buildRowList();

		//signale la mise � jours
		fireTableDataChanged();
	}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		int row = getRow(evt.getNode());
		rowList.get(row).update();
		fireTableRowsUpdated(row, row);
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
		int row = getRow(evt.getNode());
		rowList.get(row).update();
		fireTableRowsUpdated(row, row);
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		buildRowList();
		fireTableDataChanged();
	}

	// =====================================================================
	//
	// Interface vers la Jtable
	//
	// =====================================================================

	@Override
	public int getColumnCount() {
		return columnModel.nodeColumns.length;
	}

	@Override
	public int getRowCount() {
		return rowList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return columnModel.nodeColumns[columnIndex].getValue(rowList.get(rowIndex));
	}

	public String getToolTipText(int rowIndex, int columnIndex) {
		return columnModel.nodeColumns[columnIndex].getToolTipText(rowList.get(rowIndex));
	}

	@Override
    public String getColumnName(int col) {
        return columnModel.nodeColumns[col].getHeaderValue();
    }

	@Override
    public Class<?> getColumnClass(int col) {
		return columnModel.nodeColumns[col].getColumnClass();
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return !rowList.get(row).isBottomLine() && columnModel.nodeColumns[col].isEditable(rowList.get(row));
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		columnModel.nodeColumns[col].setValue(taskMgrUndoMgr, rowList.get(row), value);
	}

}

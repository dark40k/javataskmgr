package fr.dark40k.taskmgr.gui.nodetable;

import java.util.HashMap;
import java.util.TreeSet;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.NodeTaskLink;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.util.StringFormulaFilter;

public class NodeFilteredChildren {
	
	private HashMap<Node,VisibilityData> nodeVisibilityMap= new HashMap<Node,VisibilityData>();
		
	public NodeFilteredChildren(TaskMgrDB taskMgrDB,StringFormulaFilter filter) {
		for (Node node : taskMgrDB) 
			calculateVisibilityData(node,filter);
	}
	
	public boolean getVisibility(Node node) {
		VisibilityData visData = nodeVisibilityMap.get(node);
		if (visData==null) return true; 
		return visData.isVisible;
	}
	
	public Integer getVisibleChildCount(Node node) {
		
		int count=0;
		for (Node child : node.getChildren())
			if (getVisibility(child))
				count++;
		return count;
	}
	
	public Node getVisibleChild(Node parent, int index) {
		int pos=-1;
		for (Node child : parent.getChildren())
			if (getVisibility(child)) {
				pos++;
				if (pos==index) return child;
			}
		throw new RuntimeException("Child not found");
	}
	
	public int getIndexOfVisibleChild(Node parent, Node searchChild) {
		int pos=-1;
		for (Node child : parent.getChildren())
			if (getVisibility(child)) {
				pos++;
				if (child == searchChild) return pos;
			}
		return -1;
	}
	
	private VisibilityData calculateVisibilityData(Node node, StringFormulaFilter filter) {
		
		VisibilityData visData = nodeVisibilityMap.get(node);
		if (visData!=null) return visData;
		
		visData = new VisibilityData(node);
		nodeVisibilityMap.put(node,visData);
		
		if (node.hasFather()) {
			visData.categories.addAll(calculateVisibilityData(node.getFather(),filter).inheritedCategories);
			visData.inheritedCategories.addAll(calculateVisibilityData(node.getFather(),filter).inheritedCategories);
		}

		if (!(node instanceof NodeTask)) {
			visData.isVisible = true;
			return visData;
		}
		
        if (node instanceof NodeTaskLink) {
            visData.isVisible = calculateVisibilityData(((NodeTaskLink)node).getLinkNode(),filter).isVisible;
            if (visData.isVisible) propagateVisibilityToAscendents(node);
            return visData;
        }
        
		NodeTask nodeTask = (NodeTask) node;
		
		visData.categories.addAll(calculateVisibilityData(nodeTask.getDataBase().getNode(nodeTask.getStatusKey()),filter).categories);

		visData.categories.addAll(calculateVisibilityData(nodeTask.getDataBase().getNode(nodeTask.getTemplateKey()),filter).categories);
		
		visData.isVisible = filter.checkStringSet(visData.categories);
		
		if (visData.isVisible) propagateVisibilityToAscendents(node);

		return visData;
	}
	
	private void propagateVisibilityToAscendents(Node node) {
		Node father = node.getFather();
		while (father != null) {
			if (nodeVisibilityMap.get(father).isVisible == true) break;
			nodeVisibilityMap.get(father).isVisible = true;
			father = father.getFather();
		}
	}
	
	
	private class VisibilityData {
		public boolean isVisible = false;
		public TreeSet<String> categories;
		public TreeSet<String> inheritedCategories;
		
		public VisibilityData(Node node) {
			categories = node.getCategories().listCategoriesNames(false);
			inheritedCategories = node.getCategories().listCategoriesNames(true);
		}
		
		public String toString() {
			String ret="";
			for (String cat : categories)
				ret=ret+" , "+cat;
			for (String cat : inheritedCategories)
				ret=ret+" , "+cat+"*";
			return "[ "+((ret.length()>0)?ret.substring(3):"")+" ]";
		}
		 

	}
	
}

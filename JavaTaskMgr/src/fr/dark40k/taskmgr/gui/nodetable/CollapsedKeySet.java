package fr.dark40k.taskmgr.gui.nodetable;

import java.util.HashSet;
import java.util.Iterator;

import org.jdom2.Element;

public class CollapsedKeySet implements Iterable<Integer> {

	private HashSet<Integer> set = new HashSet<>();

	public boolean contains(Integer integer) {
		return set.contains(integer);
	}
	
	public void add(Integer integer) {
		set.add(integer);
	}
	
	public void remove(Integer integer) {
		set.remove(integer);
	}

	@Override
	public Iterator<Integer> iterator() {
		
		@SuppressWarnings("unchecked")
		HashSet<Integer> setCopy = (HashSet<Integer>) set.clone();
		
		return setCopy.iterator();
		
	}
	
	public void importXML(Element elt) {
		for (Element child : elt.getChildren("collapsed")) {
			Integer key = Integer.valueOf(child.getAttributeValue("key"));
			set.add(key);
		}
	}

	public void exportXML(Element elt) {
		for (Object key : set.toArray())
			elt.addContent(new Element("collapsed").setAttribute("key",Integer.toString((Integer)key)));	
	}
	
}

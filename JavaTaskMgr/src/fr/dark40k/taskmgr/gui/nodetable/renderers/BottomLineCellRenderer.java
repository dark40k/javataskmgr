package fr.dark40k.taskmgr.gui.nodetable.renderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

public class BottomLineCellRenderer extends NodeDefaultCellRenderer {

	private static final Border expansionBorder = new ExpansionHandleBorder();

	/**
	 * Overridden to combine the expansion border (whose insets determine how
	 * much a child tree node is shifted to the right relative to the ancestor
	 * root node) with whatever border is set, as a CompoundBorder. The
	 * expansion border is also responsible for drawing the expansion icon.
	 * 
	 * @param b
	 *            the border to be rendered for this component
	 */
	@Override
	public final void setBorder(Border b) {
		if (b == expansionBorder) {
			super.setBorder(b);
		} else {
			super.setBorder(BorderFactory.createCompoundBorder(b, expansionBorder));
		}
	}

	private static class ExpansionHandleBorder implements Border {

		private Insets insets = new Insets(0, 0, 0, 0);

		@Override
		public Insets getBorderInsets(Component c) {
			insets.left = 1;
			insets.top = 1;
			insets.right = 1;
			insets.bottom = 1;
			return insets;
		}

		@Override
		public boolean isBorderOpaque() {
			return false;
		}

		@Override
		public void paintBorder(Component c, java.awt.Graphics g, int x, int y, int width, int height) {

			BottomLineCellRenderer ren = (BottomLineCellRenderer) c;
			
			// sauvegarde de la couleur de trac�
			Color curColor = g.getColor();

			// trac�
			g.setColor(ren.getForeground());
			drawHorizontalLine(g, height - height / 2, 0, width+1);

			// restauration de la couleur de trac�
			g.setColor(curColor);

		}

	}
	
	private static void drawHorizontalLine(Graphics g, int y, int x1, int x2) {
		g.drawLine(x1, y, x2, y);
	}
	
	
}

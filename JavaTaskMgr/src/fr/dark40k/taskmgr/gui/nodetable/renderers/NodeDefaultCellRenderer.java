package fr.dark40k.taskmgr.gui.nodetable.renderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.gui.nodetable.JNodeTable;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;

public class NodeDefaultCellRenderer extends DefaultTableCellRenderer {

	protected JNodeTable jNodeTable;
	protected NodeRowData nodeRowData;
	protected NodeRowData selRowData;
	protected Node node;
	
	// Les deux couleurs principales des lignes :
	private Color colorTop;
	private Color colorBottom;
	
	// Initialisation
	public NodeDefaultCellRenderer() {
		this(SwingConstants.CENTER); // Au centre par d�faut
	}
	
	public NodeDefaultCellRenderer(int alignment) {
		
		super();
		
		setOpaque(false); // Car on va dessiner le fond nous m�me
		
		setHorizontalAlignment(alignment);
		
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {
		
		// recuperation table
		jNodeTable = (JNodeTable) table;
		
		// recuperation des lignes critiques (en cours et selectionn�e)
		nodeRowData = ((NodeRowData) jNodeTable.getModel().getValueAt(jNodeTable.convertRowIndexToModel(rowIndex), 0));
		selRowData = jNodeTable.getSelectedNodeRowData();

		// recuperation du noeud de la ligne en cours
		node = nodeRowData.getNode();
		
		// calcule l'objet
		Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, rowIndex, columnIndex);
		
		// fond de la cellule
		calculateBackgroundColors();
		
		// couleur de texte
		setForeground(node.getEffectiveDisplay().getFrontColor().getColor());
				
		return c;
	}
	
	private void calculateBackgroundColors() {
		
		// une ligne est selectionn�e
		if (selRowData!=null) {
			
			// la ligne est directement selectionn�e
			if (selRowData.getTopRow()==nodeRowData.getTopRow()) {
				colorTop = jNodeTable.getSelectionBackground();
				colorBottom = colorTop;
				return;
			}
			
			// la ligne est la meme que la ligne selectionn�e
			if (nodeRowData.getLinkedKey()==selRowData.getLinkedKey()) {
				colorTop = JNodeTable.LINKED_SELECTION_BACKGROUND;
				colorBottom = colorTop;
				return;
			}
			
		}
		
		// la ligne est une ligne de fond
		if (nodeRowData.isBottomLine()) {
			colorTop = node.getEffectiveDisplay().getBackColorMiddle().getColor();
			colorBottom = node.getEffectiveDisplay().getBackColorBottom().getColor();
			return;
		}
		
		// la ligne est une ligne de d�but etendue
		if (nodeRowData.canExpand() && nodeRowData.isExpanded()) {
			colorTop = node.getEffectiveDisplay().getBackColorTop().getColor();
			colorBottom = node.getEffectiveDisplay().getBackColorMiddle().getColor();
			return;
		}
		
		// ligne individuelle (par d�faut)
		colorTop = node.getEffectiveDisplay().getBackColorTop().getColor();
		colorBottom = node.getEffectiveDisplay().getBackColorBottom().getColor();
		
	}
	
	@Override
	protected void paintComponent(Graphics g) {

		// recupere le graphique 2D
		Graphics2D g2 = (Graphics2D) g;

		// programme le gradient
		g2.setPaint(new GradientPaint(0, 0, colorTop, 0, getHeight(), colorBottom));
		
		// On dessine le fond (en d�grad�)
		g.fillRect(0, 0, getWidth(), getHeight());

		// Et on dessine le composant de mani�re standard :
		super.paintComponent(g);

	}

}

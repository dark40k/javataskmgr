package fr.dark40k.taskmgr.gui.nodetable.renderers;

import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.collapsedIcon;
import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.expandedIcon;
import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.expansionHandleWidth;
import static fr.dark40k.taskmgr.gui.nodetable.JNodeTable.nestingWidth;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import fr.dark40k.taskmgr.database.display.Display;
import fr.dark40k.taskmgr.gui.nodetable.JNodeTable;
import fr.dark40k.taskmgr.gui.nodetable.NodeRowData;

public class NodeCellRenderer extends NodeDefaultCellRenderer {

	private static final Border expansionBorder = new ExpansionHandleBorder();
	
	public NodeCellRenderer() {
		super();
		setHorizontalAlignment(SwingConstants.LEFT); // alignement � gauche
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {

		//
		// Get node to display (Rendered object is Node)
		//
		Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, rowIndex, columnIndex);

		// applique la fonte
		setFont(node.getEffectiveDisplay().getFont().getEffectiveFont());
		
		//
		// if bottom line => hide text and icons
		//
		if (!nodeRowData.isBottomLine()) {
			setText(nodeRowData.getNode().getName());
			setIcon(nodeRowData.getIcon());
		} else {
			setText(null);
			setIcon(null);
		}

		return c;
	}

	/**
	 * Overridden to combine the expansion border (whose insets determine how
	 * much a child tree node is shifted to the right relative to the ancestor
	 * root node) with whatever border is set, as a CompoundBorder. The
	 * expansion border is also responsible for drawing the expansion icon.
	 * 
	 * @param b
	 *            the border to be rendered for this component
	 */
	@Override
	public final void setBorder(Border b) {
		if (b == expansionBorder) {
			super.setBorder(b);
		} else {
			super.setBorder(BorderFactory.createCompoundBorder(b, expansionBorder));
		}
	}

	private static class ExpansionHandleBorder implements Border {

		private Insets insets = new Insets(0, 0, 0, 0);

		@Override
		public Insets getBorderInsets(Component c) {

			NodeCellRenderer ren = (NodeCellRenderer) c;

			insets.left = expansionHandleWidth + (ren.nodeRowData.getNodeDepth() * nestingWidth);
			insets.top = 1;
			insets.right = 1;
			insets.bottom = 1;
			return insets;
		}

		@Override
		public boolean isBorderOpaque() {
			return false;
		}

		@Override
		public void paintBorder(Component c, java.awt.Graphics g, int x, int y, int width, int height) {
			
			// recupere le graphique 2D
			Graphics2D g2 = (Graphics2D) g;
			
			// Recuperation du renderer et donn�es associ�es
			NodeCellRenderer ren = (NodeCellRenderer) c;
			
			int nd = ren.nodeRowData.getNodeDepth();

			// dimension de la zone de dessin
			Rectangle paintBounds = g2.getClipBounds();
			int middleHeight = height - height/2; 
			// sauvegarde de la couleur de trac�
			Color curColor = g2.getColor();

			
			//
			// Trac� du fond pour le noeud en cours
			//
			Display display = ren.nodeRowData.getNode().getEffectiveDisplay();

			if ((ren.selRowData != null)&&(ren.selRowData.getTopRow() == ren.nodeRowData.getTopRow())) {
				g2.setColor(ren.jNodeTable.getSelectionBackground());
				g2.fillRect(nd * nestingWidth, paintBounds.y, nestingWidth, paintBounds.height);
			}
			
			// selection indirecte
			else if ((ren.selRowData != null) && (ren.selRowData.getLinkedKey() == ren.nodeRowData.getLinkedKey())) {
				g2.setColor(JNodeTable.LINKED_SELECTION_BACKGROUND);
				g2.fillRect(nd * nestingWidth, paintBounds.y, nestingWidth, paintBounds.height);
			}

			// autres cas
			else {
				int red = display.getBackColorBottom().getColor().getRed();
				int green = display.getBackColorBottom().getColor().getGreen();
				int blue = display.getBackColorBottom().getColor().getBlue();
				
				g2.setPaint(
						new GradientPaint(
								nd * nestingWidth, paintBounds.y, new Color(red,green,blue,255), 
								(nd + 1) * nestingWidth, paintBounds.y, new Color(red,green,blue,0)
						)
				);
				g2.fillRect(nd * nestingWidth, paintBounds.y, nestingWidth, paintBounds.height);
			}											
			
			//
			// Trac� des lignes de suivi pour les noeuds parents
			//
			NodeRowData curNodeRowData = ren.nodeRowData.getFather();
			while (curNodeRowData != null) {

				int curNd = curNodeRowData.getNodeDepth();
				
				Display curDisplay = curNodeRowData.getNode().getEffectiveDisplay();
				
				// Dessin du fond

				// selection directe
				if ((ren.selRowData != null)&&(ren.selRowData.getTopRow() == curNodeRowData.getTopRow())) {
					g2.setColor(ren.jNodeTable.getSelectionBackground());
					g2.fillRect(curNd * nestingWidth, paintBounds.y, nestingWidth, paintBounds.height);
				}
				
				// selection indirecte
				else if ((ren.selRowData != null) && (ren.selRowData.getLinkedKey() == curNodeRowData.getLinkedKey())) {
					g2.setColor(JNodeTable.LINKED_SELECTION_BACKGROUND);
					g2.fillRect(curNd * nestingWidth, paintBounds.y, nestingWidth, paintBounds.height);
				}

				// autres cas
				else {
					g2.setColor(curDisplay.getBackColorMiddle().getColor());
					g2.fillRect(curNd * nestingWidth, paintBounds.y, nestingWidth, paintBounds.height);
					
					int red = curDisplay.getBackColorBottom().getColor().getRed();
					int green = curDisplay.getBackColorBottom().getColor().getGreen();
					int blue = curDisplay.getBackColorBottom().getColor().getBlue();
					
					g2.setPaint(
							new GradientPaint(
									curNd * nestingWidth, paintBounds.y, new Color(red,green,blue,255), 
									(curNd + 1) * nestingWidth, paintBounds.y, new Color(red,green,blue,0)
							)
					);
					g2.fillRect(curNd * nestingWidth, paintBounds.y, nestingWidth, paintBounds.height);
				}											
				
				// Lignes verticales
				g2.setColor(curDisplay.getFrontColor().getColor());
				drawVerticalLine(g2, curNd * nestingWidth + expansionHandleWidth / 2, paintBounds.y, paintBounds.y + paintBounds.height);
				
				// previous parent
				curNodeRowData=curNodeRowData.getFather();
			}
			
			
			//
			// Trac� de l'objet pour la ligne
			//
			
			// couleur de trac� identique � celle du noeud
			g2.setColor(ren.getForeground());
			
			if (ren.nodeRowData.isBottomLine()) {
				drawVerticalLine(g2, (nd) * nestingWidth + expansionHandleWidth / 2, paintBounds.y, middleHeight);
				drawHorizontalLine(g2, middleHeight, nd * nestingWidth + expansionHandleWidth / 2, width+1);
			}

			if (ren.nodeRowData.canExpand()) {
				
				drawHorizontalLine(g2, middleHeight, nd * nestingWidth + expansionHandleWidth / 2, (nd + 1) * nestingWidth);

				Icon icon = ren.nodeRowData.isExpanded() ? expandedIcon : collapsedIcon;

				int iconX = nd * nestingWidth + expansionHandleWidth / 2 - icon.getIconWidth() / 2;
				int iconY = (icon.getIconHeight() < height) ? (middleHeight) - (icon.getIconHeight() / 2) : 0;

				if (ren.nodeRowData.isExpanded())
					drawVerticalLine(g2, nd * nestingWidth + expansionHandleWidth / 2, middleHeight, paintBounds.y + paintBounds.height);
				
				icon.paintIcon(c, g2, iconX, iconY);
			}
			
			// restauration de la couleur de trac�
			g2.setColor(curColor);
			
		}

	}

	private static void drawHorizontalLine(Graphics g, int y, int x1, int x2) {
		g.drawLine(x1, y, x2, y);
	}
	
	private static void drawVerticalLine(Graphics g, int x, int y1, int y2) {
		g.drawLine(x, y1, x, y2);
	}
	
}

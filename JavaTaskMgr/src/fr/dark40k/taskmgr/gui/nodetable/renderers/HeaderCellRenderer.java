package fr.dark40k.taskmgr.gui.nodetable.renderers;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class HeaderCellRenderer implements TableCellRenderer {

	private Icon icon;
	private Integer alignment;
	private String tooltiptext;

	public HeaderCellRenderer(Icon icon, Integer alignment, String tooltiptext) {
		this.icon=icon;
		this.alignment=alignment;
		this.tooltiptext=tooltiptext;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

		TableCellRenderer renderer = table.getTableHeader().getDefaultRenderer();

		JLabel label = (JLabel) renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

		if (icon!=null) label.setIcon(icon);

		if (alignment!=null) label.setHorizontalAlignment(alignment);

		if (tooltiptext!=null) label.setToolTipText(tooltiptext);

		return label;

	}

}

package fr.dark40k.taskmgr.gui.nodetable.renderers;

import java.awt.Component;
import java.time.LocalDate;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;

import fr.dark40k.taskmgr.database.taskdates.TaskDateData;
import fr.dark40k.util.FormattersString;

public class TaskDateCellRenderer extends NodeDefaultCellRenderer {

	private final static Icon warningIcon = new ImageIcon(TaskDateCellRenderer.class.getResource("res/week.png"));
	private final static Icon alarmIcon = new ImageIcon(TaskDateCellRenderer.class.getResource("res/clock-ring.png"));

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {

		Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, rowIndex, columnIndex);

		//
		// Object is TaskDateData
		//

		// Affichage vide si pas de donn�es
		if (value == null) {
			setIcon(null);
			return c;
		}

		// recupere la valeur � afficher
		TaskDateData data = (TaskDateData) value;

		// applique le texte
		setText(FormattersString.format(data.getLocalDate(), null));

		// applique la fonte
		setFont(node.getEffectiveDisplay().getFont().getEffectiveFont());

		// calcule l'icone � afficher
		LocalDate refDate = data.getLocalDate();
		if (refDate != null) {
			LocalDate now = LocalDate.now();
			if (!now.isBefore(refDate))
				setIcon(alarmIcon);
			else if (!now.plusDays(7).isBefore(refDate))
				setIcon(warningIcon);
			else
				setIcon(null);
		} else
			setIcon(null);

		return c;
	}

}

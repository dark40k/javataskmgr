package fr.dark40k.taskmgr.gui.nodetable.renderers;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTable;

public class IconCellRenderer extends NodeDefaultCellRenderer {

	@Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {

		Component c = super.getTableCellRendererComponent(table, null, isSelected, hasFocus, rowIndex, columnIndex);
        
        setIcon((Icon) value);
        
        return c;
	}

}

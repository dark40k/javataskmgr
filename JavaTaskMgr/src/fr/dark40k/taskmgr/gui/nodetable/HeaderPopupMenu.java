package fr.dark40k.taskmgr.gui.nodetable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.table.JTableHeader;

import fr.dark40k.taskmgr.gui.nodetable.columns.NodeTableColumn;
import fr.dark40k.taskmgr.gui.nodetable.columns.NodeTableColumnModel;

public class HeaderPopupMenu extends JPopupMenu {

	final JNodeTable table;
	final NodeTableColumnModel columnModel;
	
	final JCheckBoxMenuItem[] listItems;
	
	public HeaderPopupMenu(JNodeTable nodeTable) {
		
		table=nodeTable;
		columnModel = (NodeTableColumnModel)table.getColumnModel();
		
		listItems = new JCheckBoxMenuItem[columnModel.nodeColumns.length];
		
		for (NodeTableColumn column : columnModel.nodeColumns) {

			// recupe le numero de la colonne (dans columnModel)
			final int columnIndex = column.getModelIndex();
			
			// creation de la ligne de menu
			JCheckBoxMenuItem anItem = new JCheckBoxMenuItem();
				
			// Texte de la ligne de menu
			if (column.getHeaderValue()!=null) 
				anItem.setText(column.getHeaderValue());
			else
				anItem.setText(column.getHeaderToolTipText());
			
			// Icone de la ligne de menu
			if (column.getHeaderIcon()!=null) anItem.setIcon(column.getHeaderIcon());
			
			// Ajoute le listener si la ligne est cliqu�e
			anItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
						swapColumn(columnIndex);
						updateState();
				}
			});
			
			// ajoute et sauve la ligne de menu
			this.add(anItem);
			listItems[columnIndex] = anItem; 
		}

		// separateur
		this.add(new Separator());
		
		// commande de r�initialisation
		{
			JMenuItem item = new JMenuItem("Reinitialiser");
			
			item.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					columnModel.reset();
					updateState();
				}
			});
			
			this.add(item);
		}
		
	}
	
	private void swapColumn(int columnModelIndex) {
		
		// cherche et enleve la colonne si trouvee
		for (int viewIndex = 0; viewIndex < columnModel.getColumnCount(); viewIndex++)
			if (columnModel.getColumn(viewIndex).getModelIndex()==columnModelIndex) {
				columnModel.removeColumn(columnModel.getColumn(viewIndex));
				return;
			}
		
		// si pas trouvee alors il fallait en fait l'ajouter a la fin apr�s reinitialisation
		columnModel.nodeColumns[columnModelIndex].reset();
		columnModel.addColumn(columnModel.nodeColumns[columnModelIndex]);
		
	}
	
	
	private void updateState() {
		
		for ( JCheckBoxMenuItem item : listItems)
			item.setState(false);
		
		for (int viewIndex = 0; viewIndex < columnModel.getColumnCount(); viewIndex++)
			listItems[columnModel.getColumn(viewIndex).getModelIndex()].setState(true);
		
	}

	
	public static void registerListener(JNodeTable nodeTable) {

		final HeaderPopupMenu popupMenu = new HeaderPopupMenu(nodeTable);

		final JTableHeader header = nodeTable.getTableHeader();
		
		header.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				if (SwingUtilities.isRightMouseButton(me)) {
					popupMenu.updateState();
					popupMenu.show(header, me.getX(), me.getY());
				}
			}
		});
	}

}

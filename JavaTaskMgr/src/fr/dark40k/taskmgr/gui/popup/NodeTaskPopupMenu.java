package fr.dark40k.taskmgr.gui.popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.NodeTaskLink;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.datalinks.DataLink;
import fr.dark40k.taskmgr.database.interfaces.NodeHiddenInterface;
import fr.dark40k.taskmgr.gui.nodetable.JNodeTable;
import fr.dark40k.taskmgr.gui.nodetable.RowAddress;

public class NodeTaskPopupMenu extends JPopupMenu {

	private JMenuItem mntmSelectTopItem;
	private JMenuItem mntmJumpNextLink;

	// Supprim� car trop compliqu� si liens multiples
	// private JMenuItem mntmJumpLink;

	
	private JSeparator separator_1;
	
	private JMenuItem mntmAjouterNoeudFrere;
	private JMenuItem mntmCrerNoeudFils;
	
	private JMenu mnStatus;
	private JMenu mnModele;
	
	private JMenuItem mntmAjouterh;
	
//	private JMenuItem mntmCouperNoeud;
//	private JMenuItem mntmCopierNoeud;
//	private JMenuItem mntmCollerNoeud;
//	private JSeparator separator_1;
	
	private final TaskMgr taskMgr;
	
	private int initComponentCount;
	
	private Node popupNode;
	private int popupRow;
	private RowAddress popupRowAddress;

	public NodeTaskPopupMenu(TaskMgr taskMgr) {
		
		this.taskMgr=taskMgr; 

		initGUI();
		
		initComponentCount = getComponentCount();
		
	}

	private void updateMenu() {

		// set text for first item
		mntmSelectTopItem.setText(popupNode.getName());
		mntmSelectTopItem.setFont(popupNode.getEffectiveDisplay().getFont().getEffectiveFont());

//		// Enable disable/jump link
//		mntmJumpLink.setVisible(popupNode instanceof NodeTaskLink);
		
		// remove previously added menu lines
		while (getComponentCount() > initComponentCount)
			remove(initComponentCount);

		// add status sub-menu
		if (popupNode instanceof NodeTask) {

			mnStatus.setEnabled(true);

			TaskMgrDB database = popupNode.getDataBase();
			final NodeTask nodeTask = (NodeTask) popupNode;

			mnStatus.removeAll();

			ArrayList<Integer> statusKeys = database.getRootStatus().getKeyAndChildrenKeys(false);
			for (int i = statusKeys.size() - 1; i >= 0; i--)
				if ((((NodeHiddenInterface) database.getNode(statusKeys.get(i))).isHidden()) && (statusKeys.get(i) != nodeTask.getStatusKey()))
					statusKeys.remove(i);

			for (final int statusKey : statusKeys) {
				if (statusKey == database.getRootStatus().getKey())
					continue;
				JCheckBoxMenuItem cbMenuItem = new JCheckBoxMenuItem(database.getNode(statusKey).getStackedName(", "));
				cbMenuItem.setSelected(statusKey == nodeTask.getStatusKey());
				mnStatus.add(cbMenuItem);

				cbMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						nodeTask.setStatusKey(statusKey);
						;
					}
				});

			}

		} else {
			mnStatus.setEnabled(false);
		}

		// add modele sub-menu
		if (popupNode instanceof NodeTask) {

			mnModele.setEnabled(true);

			TaskMgrDB database = popupNode.getDataBase();
			final NodeTask nodeTask = (NodeTask) popupNode;

			mnModele.removeAll();

			ArrayList<Integer> templateKeys = database.getRootTemplate().getKeyAndChildrenKeys(false);
			for (int i = templateKeys.size() - 1; i >= 0; i--)
				if ((((NodeHiddenInterface) database.getNode(templateKeys.get(i))).isHidden()) && (templateKeys.get(i) != nodeTask.getTemplateKey()))
					templateKeys.remove(i);

			for (final int templateKey : templateKeys) {
				if (templateKey == database.getRootTemplate().getKey())
					continue;
				JCheckBoxMenuItem cbMenuItem = new JCheckBoxMenuItem(database.getNode(templateKey).getStackedName(", "));
				cbMenuItem.setSelected(templateKey == nodeTask.getTemplateKey());
				mnModele.add(cbMenuItem);

				cbMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						nodeTask.setTemplateKey(templateKey);
						;
					}
				});
			}

		} else {
			mnModele.setEnabled(false);
		}

		// create new lines
		if (popupNode instanceof NodeTask) {

			NodeTask nodeTask = (NodeTask) popupNode;
			Boolean needSeparator = false;
			
			do {
				
				if (needSeparator&&(nodeTask.getLinkList().size()>0)) {
					JSeparator separator = new JSeparator();
					add(separator);
					needSeparator=false;
				}
								
				for (final DataLink link : nodeTask.getLinkList()) {
					JMenuItem anItem = new JMenuItem();

					anItem.setIcon(nodeTask.getDataBase().getNodeIconsDB().getNodeIcon(link.getIcon16Key(true)).getIcon());
					anItem.setText(link.getDescription());
					anItem.setToolTipText(link.getData());
					anItem.setToolTipText(link.getData());

					anItem.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							link.run();
						}
					});

					add(anItem);
					needSeparator=true;
				}
				
				Node father = (nodeTask instanceof NodeTaskLink) ? ((NodeTaskLink)nodeTask).getLinkNode().getFather() : nodeTask.getFather();
				nodeTask = (father instanceof NodeTask)? (NodeTask) father : null;

			} while (nodeTask != null);

		}
	}

	private void initGUI() {
		
		mntmSelectTopItem = new JMenuItem("Select Top Item");
		mntmSelectTopItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				taskMgr.cmd_SelectRowAddress(popupRowAddress);
			}
		});
		add(mntmSelectTopItem);
		
		mntmJumpNextLink = new JMenuItem("Selectionner lien suivant");
		mntmJumpNextLink.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				taskMgr.cmd_SelectNextLinkNode();
			}
		});
		add(mntmJumpNextLink);
		
//		mntmJumpLink = new JMenuItem("Selectionner tache d'origine");
//		mntmJumpLink.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				taskMgr.cmd_SelectNode(((NodeTaskLink)popupNode).getLinkNode());
//			}
//		});
//		add(mntmJumpLink);
		
		separator_1 = new JSeparator();
		add(separator_1);
		
		mntmCrerNoeudFils = new JMenuItem("Cr\u00E9er noeud fils");
		mntmCrerNoeudFils.setIcon(new ImageIcon(NodeTaskPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/AddChild16.png")));
		mntmCrerNoeudFils.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				taskMgr.cmd_NodeCreateNewChild(popupRowAddress);
			}
		});
		add(mntmCrerNoeudFils);
		
		mntmAjouterNoeudFrere = new JMenuItem("Cr\u00E9er noeud frere");
		mntmAjouterNoeudFrere.setIcon(new ImageIcon(NodeTaskPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/AddBrother16.png")));
		mntmAjouterNoeudFrere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				taskMgr.cmd_NodeCreateNewBrother(popupRowAddress);
			}
		});
		add(mntmAjouterNoeudFrere);
		
		JSeparator separator = new JSeparator();
		add(separator);
		
		mntmAjouterh = new JMenuItem("Ajouter 1h");
		mntmAjouterh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				taskMgr.cmd_NodeExecutionSlotAdd1h(popupNode);
			}
		});
		add(mntmAjouterh);
		
		JSeparator separator2 = new JSeparator();
		add(separator2);
		
		mnStatus = new JMenu("Status");
		add(mnStatus);
		
		mnModele = new JMenu("Mod\u00E8le");
		add(mnModele);
		
		JSeparator separator3 = new JSeparator();
		add(separator3);
		
// 		Cut/Copy/paste, a priori trop peu utilis�s.
//		===========================================
//
//		mntmCouperNoeud = new JMenuItem("Couper noeud");
//		mntmCouperNoeud.setIcon(new ImageIcon(NodeTaskPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-cut-4.png")));
//		mntmCouperNoeud.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				taskMgr.cmd_Cut();
//			}
//		});
//		add(mntmCouperNoeud);
//		
//		mntmCopierNoeud = new JMenuItem("Copier noeud");
//		mntmCopierNoeud.setIcon(new ImageIcon(NodeTaskPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/page_white_copy.png")));
//		mntmCopierNoeud.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				taskMgr.cmd_NodeCopy();
//			}
//		});
//		add(mntmCopierNoeud);
//		
//		mntmCollerNoeud = new JMenuItem("Coller noeud");
//		mntmCollerNoeud.setIcon(new ImageIcon(NodeTaskPopupMenu.class.getResource("/fr/dark40k/taskmgr/res/icon16/page_white_paste.png")));
//		mntmCollerNoeud.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				taskMgr.cmd_NodePaste();
//			}
//		});
//		add(mntmCollerNoeud);
//		
//		separator_1 = new JSeparator();
//		add(separator_1);

	}
	
	public MouseAdapter getNodeTableMouseListener() {
		return mouseListener;
	}

	private final MouseAdapter mouseListener = new MouseAdapter() {

		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger())
				doPop(e);
		}

		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger())
				doPop(e);
		}

		private void doPop(MouseEvent e) {

			JNodeTable table = (JNodeTable) e.getSource();

			popupRow = table.rowAtPoint(e.getPoint());
			popupRowAddress = table.getRowAddressAtDisplayedRow(popupRow);
			popupNode = popupRowAddress.getNode();

			//table.setSelectedNode(menuNode);
			
			updateMenu();

			show(e.getComponent(), e.getX(), e.getY());
		}
	};
}

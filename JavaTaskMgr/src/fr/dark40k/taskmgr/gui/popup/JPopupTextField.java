package fr.dark40k.taskmgr.gui.popup;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

public class JPopupTextField extends JTextField {

	private UndoManager undoManager;

	private JPopupMenu popupMenu;
	private JMenuItem menuItemUndo;
	private JMenuItem menuItemRedo;
	private JSeparator separator;
	private JMenuItem menuItemCut;
	private JMenuItem menuItemCopy;
	private JMenuItem menuItemPaste;
	private JMenuItem menuItemDelete;
	private JSeparator separator_1;
	private JMenuItem menuItemSelectAll;
	
	public JPopupTextField() {
		
		super();
		
		// Add undo/redo capacity and bind keys (CTRL Z, CTRLY)
		undoManager=new UndoManager();
		getDocument().addUndoableEditListener(new UndoableEditListener() {
			public void undoableEditHappened(UndoableEditEvent evt) {
				undoManager.addEdit(evt.getEdit());
			}
		});
		getActionMap().put(undoAction.getValue("Name"), undoAction);
		getInputMap().put(KeyStroke.getKeyStroke("control Z"), undoAction.getValue("Name"));
		getActionMap().put(redoAction.getValue("Name"), redoAction);
		getInputMap().put(KeyStroke.getKeyStroke("control Y"), redoAction.getValue("Name"));

		
		initGUI();
	}
	
	//
	// Methodes
	//
	
	@Override
	public void setText(String t) {
		super.setText(t);
		undoManager.discardAllEdits();
	}
	
	//
	// Actions
	//
	
	private AbstractAction undoAction = new AbstractAction("Undo") {
		public void actionPerformed(ActionEvent evt) {
			try {
				if (undoManager.canUndo()) {
					undoManager.undo();
				}
			} catch (CannotUndoException e) {
			}
		}
	};
	
	private AbstractAction redoAction = new AbstractAction("Redo") {
		public void actionPerformed(ActionEvent evt) {
			try {
				if (undoManager.canRedo()) {
					undoManager.redo();
				}
			} catch (CannotRedoException e) {
			}
		}
	};
	
	private AbstractAction cutAction = new AbstractAction("Couper",new ImageIcon(JPopupTextField.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-cut-4.png"))) {
		public void actionPerformed(ActionEvent evt) {
			cut();
		}
	};
	
	private AbstractAction copyAction = new AbstractAction("Copier",new ImageIcon(JPopupTextField.class.getResource("/fr/dark40k/taskmgr/res/icon16/page_white_copy.png"))) {
		public void actionPerformed(ActionEvent evt) {
			copy();
		}
	};
	
	private AbstractAction pasteAction = new AbstractAction("Coller",new ImageIcon(JPopupTextField.class.getResource("/fr/dark40k/taskmgr/res/icon16/page_white_paste.png"))) {
		public void actionPerformed(ActionEvent evt) {
			paste();
		}
	};
	
	private AbstractAction deleteAction = new AbstractAction("Effacer texte",new ImageIcon(JPopupTextField.class.getResource("/fr/dark40k/taskmgr/res/icon16/edit-delete-6.png"))) {
		public void actionPerformed(ActionEvent evt) {
			if (getSelectedText()!=null)
				JPopupTextField.super.replaceSelection("");
			else
				JPopupTextField.super.setText("");
		}
	};
	
	private AbstractAction selectAllAction = new AbstractAction("Selectionner tout",null) {
		public void actionPerformed(ActionEvent evt) {
			selectAll();
		}
	};
	
	//
	// Utilitaire
	//
	private void initGUI() {
		
		popupMenu = new JPopupMenu();
		addPopup(this, popupMenu);
		
		menuItemUndo = new JMenuItem();
		menuItemUndo.setAction(undoAction);
		menuItemUndo.setText("Undo");
		popupMenu.add(menuItemUndo);
		
		menuItemRedo = new JMenuItem();
		menuItemRedo.setAction(redoAction);
		menuItemRedo.setText("Redo");
		popupMenu.add(menuItemRedo);
		
		separator = new JSeparator();
		popupMenu.add(separator);
		
		menuItemCut = new JMenuItem();
		menuItemCut.setAction(cutAction);
		menuItemCut.setText("Cut");
		popupMenu.add(menuItemCut);
		
		menuItemCopy = new JMenuItem("Copy");
		menuItemCopy.setAction(copyAction);
		popupMenu.add(menuItemCopy);
		
		menuItemPaste = new JMenuItem("Paste");
		menuItemPaste.setAction(pasteAction);
		popupMenu.add(menuItemPaste);
		
		menuItemDelete = new JMenuItem("Delete");
		menuItemDelete.setAction(deleteAction);
		popupMenu.add(menuItemDelete);
		
		separator_1 = new JSeparator();
		popupMenu.add(separator_1);
		
		menuItemSelectAll = new JMenuItem("SelectAll");
		menuItemSelectAll.setAction(selectAllAction);
		popupMenu.add(menuItemSelectAll);
	}

	
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

}

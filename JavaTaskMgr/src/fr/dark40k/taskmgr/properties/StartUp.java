package fr.dark40k.taskmgr.properties;

import fr.dark40k.util.properties.Property;
import fr.dark40k.util.properties.PropertyFieldDescription;
import fr.dark40k.util.properties.PropertyFieldHelp;

public class StartUp extends Property {

	public final static int DEFAULT_START_FILTER = 0; // starting number for next backup

	@PropertyFieldDescription("Filtre initial")
	@PropertyFieldHelp("Code : \n-1 => filtre persistant\n 0 => aucun filtre \n key => clef du filtre � utiliser")
	private int startFilter = DEFAULT_START_FILTER; // starting number for next backup

	public int getStartFilter() {
		return startFilter;
	}
	
	public void setStartFilter(int startKey) {
		if (this.startFilter == startKey) return;
		this.startFilter = startKey;
		fireUpdateEvent();
	}
	
}

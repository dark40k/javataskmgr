package fr.dark40k.taskmgr.properties;

import fr.dark40k.util.properties.Property;
import fr.dark40k.util.properties.PropertyFieldDescription;
import fr.dark40k.util.xmlimportexport.XMLClassDefaultTag;
import fr.dark40k.util.xmlimportexport.XMLFieldAliasTag;

@XMLClassDefaultTag("properties")
public class TaskMgrProperties extends Property {
	
	//
	// Donn�es accessibles et stock�es par d�faut
	//
	@XMLFieldAliasTag("saveauto")
	@PropertyFieldDescription("Sauvegarde automatique")
	public final SaveAuto saveAuto = new SaveAuto();
	
	@XMLFieldAliasTag("backupauto")
	@PropertyFieldDescription("Backups")
	public final BackupAuto backupAuto = new BackupAuto();
	
	@XMLFieldAliasTag("startup")
	@PropertyFieldDescription("Demarrage")
	public final StartUp startup = new StartUp();
	
	
	//
	// Constructeur
	//
	
	public TaskMgrProperties() {
		saveAuto.addUpdateListener(this);
		backupAuto.addUpdateListener(this);
	}

}

package fr.dark40k.taskmgr.properties;

import java.time.Duration;

import fr.dark40k.util.properties.Property;

public class SaveAuto extends Property {


	//
	// Valeurs par defaut des donn�es
	//
	
	public final static Boolean DEFAULT_SAVE_AUTO = true;
	public final static Duration DEFAULT_SAVE_MAX_DELAY = Duration.ofMinutes(5); // maximum delay for saving delay in seconds after new data has been entered

	
	//
	// Donn�es
	//
	
	private Boolean saveAuto = DEFAULT_SAVE_AUTO; // set if autosave is on or off
	
	private Duration saveMaxDelay = DEFAULT_SAVE_MAX_DELAY; // maximum delay for saving delay in seconds after new data has been entered
	
	//
	// Getters
	//

	public Duration getSaveMaxDelay() {
		return saveMaxDelay;
	}


	public Boolean getSaveAuto() {
		return saveAuto;
	}

	//
	// Setters
	//
	
	public void setSaveMaxDelay(Duration saveMaxDelay) {
		if (this.saveMaxDelay == saveMaxDelay) return;
		this.saveMaxDelay = saveMaxDelay;
		fireUpdateEvent();
	}


	public void setSaveAuto(Boolean saveAuto) {
		if (this.saveAuto == saveAuto) return;
		this.saveAuto = saveAuto;
		fireUpdateEvent();
	}

}

package fr.dark40k.taskmgr.properties;

import java.time.Duration;

import fr.dark40k.util.properties.Property;
import fr.dark40k.util.properties.PropertyFieldDescription;

public class BackupAuto extends Property {
	
	//
	// Valeurs par defaut des donn�es
	//
	
	public final static Boolean DEFAULT_BACKUP_AUTO = true;
	public final static String DEFAULT_BACKUP_FOLDER_NAME = "Old"; // Subfolder name used to store backups
	public final static Duration DEFAULT_BACKUP_DELAY = Duration.ofHours(4); // Maximum delay for creating a new backup file
	public final static int DEFAULT_BACKUP_NEXT_NBR = 0; // starting number for next backup
	
	//
	// Donn�es
	//
	
	@PropertyFieldDescription("Activation")
	private Boolean backupAuto = DEFAULT_BACKUP_AUTO; // set if autosave is on or off

	@PropertyFieldDescription("Nom du sous repertoire")
	private String backupFolderName = DEFAULT_BACKUP_FOLDER_NAME; // Subfolder name used to store backups
	
	@PropertyFieldDescription("Dur�e max. avant nouveau backup (s)")
	private Duration backupMaxDelay = DEFAULT_BACKUP_DELAY; // Maximum delay for creating a new backup file

	@PropertyFieldDescription("Numero chrono prochain backup")
	private int backupNextNbr = DEFAULT_BACKUP_NEXT_NBR; // starting number for next backup
		
	//
	// Getters
	//

	public String getBackupFolderName() {
		return backupFolderName;
	}


	public Duration getBackupMaxDelay() {
		return backupMaxDelay;
	}


	public int getBackupNextNbr() {
		return backupNextNbr;
	}


	public Boolean getBackupAuto() {
		return backupAuto;
	}


	//
	// Setters
	//
	
	public void setBackupFolderName(String backupFolderName) {
		if (this.backupFolderName.equals(backupFolderName)) return;
		this.backupFolderName = backupFolderName;
		fireUpdateEvent();
	}


	public void setBackupMaxDelay(Duration backupMaxDelay) {
		if (this.backupMaxDelay == backupMaxDelay) return;
		this.backupMaxDelay = backupMaxDelay;
		fireUpdateEvent();
	}


	public void setBackupNextNbr(int backupNextNbr) {
		if (this.backupNextNbr == backupNextNbr) return;
		this.backupNextNbr = backupNextNbr;
		fireUpdateEvent();
	}

	public void incBackupNextNbr() {
		this.backupNextNbr++;
	}

	public void setBackupAuto(Boolean backupAuto) {
		if (this.backupAuto == backupAuto) return;
		this.backupAuto = backupAuto;
		fireUpdateEvent();
	}

		
}

package fr.dark40k.taskmgr.database.nodeicons;

public enum DefaultIconEnum {

//	NOCICON(-1,null), => pas d'icones null
	
	FILE16(-2,"/fr/dark40k/taskmgr/res/icon16/document_empty.png"),
	FILE32(-3,"/fr/dark40k/taskmgr/res/icon32/document_empty.png"),
	
	DIRECTORY16(-4,"/fr/dark40k/taskmgr/res/icon16/folder_vertical_document.png"),
	DIRECTORY32(-5,"/fr/dark40k/taskmgr/res/icon32/folder_vertical_document.png"),
	
	OUTLOOKDIRECTORY16(-6,"/fr/dark40k/taskmgr/res/icon16/mail-folder-sent.png"),
	OUTLOOKDIRECTORY32(-7,"/fr/dark40k/taskmgr/res/icon32/mail-folder-sent.png"),
	
	OUTLOOKEMAIL16(-8,"/fr/dark40k/taskmgr/res/icon16/email.png"),
	OUTLOOKEMAIL32(-9,"/fr/dark40k/taskmgr/res/icon32/email.png"),
	
	LOCALDIRECTORY16(-20,"/fr/dark40k/taskmgr/res/icon16/user-home.png"),
	LOCALDIRECTORY32(-21,"/fr/dark40k/taskmgr/res/icon32/user-home.png"),
	
	NETDIRECTORY16(-22,"/fr/dark40k/taskmgr/res/icon16/network-server.png"),
	NETDIRECTORY32(-23,"/fr/dark40k/taskmgr/res/icon32/network-server.png"),
	
	PDFFILE16(-24,"/fr/dark40k/taskmgr/res/icon16/application-pdf.png"),
	PDFFILE32(-25,"/fr/dark40k/taskmgr/res/icon32/application-pdf.png");
	
	public final int key;
	public final String name;
	public final NodeIcon nodeIcon;
	
	private DefaultIconEnum(int key, String path) {
		this.key = key;
		this.name = path;
		this.nodeIcon = new NodeIcon(key, name, DefaultIconEnum.class.getResource(path));
	}

}

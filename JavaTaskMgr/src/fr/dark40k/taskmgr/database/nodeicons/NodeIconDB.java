package fr.dark40k.taskmgr.database.nodeicons;

import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.jdom2.Element;

public class NodeIconDB {

	public static final int NO_ICON_KEY=-1;
	
	public static final String XMLTAG = "iconsdb";
	
	public int nextKey=0;
	
	private final HashMap<Integer, NodeIcon> list = new HashMap<Integer, NodeIcon>();

	public NodeIconDB() {
		resetDB();
	}
	
	private void resetDB() {
		nextKey=0;
		list.clear();
		for (DefaultIconEnum defaultIcon : DefaultIconEnum.values())
			list.put(defaultIcon.key, defaultIcon.nodeIcon);
	}
	
	public void loadFromXML(Element rootXMLDatabaseNode) {
		
		resetDB();
		
		for (Element iconElement : rootXMLDatabaseNode.getChildren("icon")) {
			int newKey=Integer.valueOf(iconElement.getAttributeValue("key"));
			nextKey = (nextKey > newKey ) ? nextKey : newKey +1 ;
			list.put(newKey, new NodeIcon(newKey,iconElement.getAttributeValue("name"),iconElement.getAttributeValue("data")));
		}
	}

	public Element saveToXML() {
		
		Element eltIcons = new Element(XMLTAG); 
		for (Entry<Integer, NodeIcon> entry : list.entrySet())
			if (entry.getKey() >= 0) {
				Element elt = new Element("icon");
				elt.setAttribute("key", entry.getKey().toString());
				elt.setAttribute("name", entry.getValue().getName());
				elt.setAttribute("data", entry.getValue().exportBase64());
				eltIcons.addContent(elt);
			}

		return eltIcons;
		
	}
	
	public NodeIcon getNodeIcon(int key) {
		if (key == NO_ICON_KEY) return null;
		return list.get(key);
	}
	
	public Set<Integer> keySet() {
		return list.keySet();
	}
	
	public NodeIcon addNewIcon(File file) {
		NodeIcon newNodeIcon = new NodeIcon(this, file);
		list.put(newNodeIcon.getKey(), newNodeIcon);
		return newNodeIcon;
	}

	public boolean removeIcon(int key) {
		if (key<0) return false;
		if (getNodeIcon(key).isUsed()) return false;
		list.remove(key);
		return true;
	}
	
}

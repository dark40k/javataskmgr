package fr.dark40k.taskmgr.database.nodeicons;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.HashSet;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NodeIcon  {
	
	private final static Logger LOGGER = LogManager.getLogger();
	
	private int key;
	private ImageIcon icon;
	private String name;
	
	private HashSet<Object> users = new HashSet<Object>();
	
	protected NodeIcon(int key, String name, URL ressourceURL) {
		this.key=key;
		this.name=name;
		this.icon = new ImageIcon(ressourceURL);
	}
	
	protected NodeIcon(NodeIconDB nodeIconDB, File file) {
		this.key = nodeIconDB.nextKey++;
		this.name = file.getName();
		this.icon = new ImageIcon(file.getAbsolutePath());
	}
	
	protected NodeIcon(int newKey, String name, String data) {
		this.key=newKey;
		this.name=name;
		this.icon = new ImageIcon(Base64.getDecoder().decode(data));
	}

	protected String exportBase64() {
		
		BufferedImage bi = new BufferedImage( icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		icon.paintIcon(null, g, 0,0);
		g.dispose();
		
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    try {
			ImageIO.write(bi, "png", baos);
		} catch (IOException e) {
			LOGGER.error("Error can't export image",e);
			throw new RuntimeException("Error can't export image");
		}
	    String encodedImage = Base64.getEncoder().encodeToString(baos.toByteArray());

	    return encodedImage;
	    
	}
	
	public int getKey() {
		return key;
	}
	 
	public String getName() {
		return name;
	}
	
	public Icon getIcon() {
		return icon;
	}
	
	public void lock(Object o) {
		users.add(o);
	}

	public int usersCount() {
		return users.size();
	}
	
	public boolean isUsed() {
		return users.size()>0;
	}
	
	public void release(Object o) {
		users.remove(o);
	}
	
}
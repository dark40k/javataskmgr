package fr.dark40k.taskmgr.database.executionslots;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.EventListener;
import java.util.EventObject;

import javax.swing.event.EventListenerList;

import org.jdom2.Element;

import fr.dark40k.util.FormattersXML;

public class ExecutionSlot implements Cloneable {

	public final static String XMLTAG="execslot";

	private LocalDateTime start;
	private Duration duree;
	private String comment;
	
	public ExecutionSlot(Duration duree) {
		this.start = LocalDateTime.now();
		this.duree = duree;
		this.comment="";
	}

	public ExecutionSlot(LocalDateTime start, Duration duree) {
		this.start = start;
		this.duree=duree;
		this.comment="";
	}
	
	public ExecutionSlot(Element elt) {
		start = FormattersXML.parseLocalDateTime(elt, "start");
		duree = FormattersXML.parseDuration(elt, "duree", Duration.ZERO);
		comment = elt.getAttributeValue("comment","");
	}
		
	public Element exportXML() {
		Element elt=new Element(XMLTAG);
		FormattersXML.formatAttribute(elt, "start", start);
		FormattersXML.formatAttribute(elt, "duree", duree);
		elt.setAttribute("comment", comment);
		return elt;
	}

	public ExecutionSlot clone() {
		
		ExecutionSlot catClone;

		try {
			catClone = (ExecutionSlot) super.clone();
		} catch(CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
		
		return catClone;
	}

	public String toString() {
		return start.toString()+" => "+duree.toString()+" sec";
	}
	
	//
	// Getters / Setters
	//
		
	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		if (this.start.equals(start)) return;
		this.start = start;
		fireUpdateEvent();
	}

	public Duration getDuration() {
		return duree;
	}

	public void setDuration(Duration duree) {
		if (this.duree.equals(duree)) return;
		this.duree = duree;
		fireUpdateEvent();
	}

	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		if (this.comment.equals(comment)) return;
		this.comment=comment;
		fireUpdateEvent();
	}
	

	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class UpdateEvent extends EventObject {
		public UpdateEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateListener extends EventListener {
		public void executionSlotUpdated(UpdateEvent evt);
	}

	public void addUpdateListener(UpdateListener listener) {
		listenerList.add(UpdateListener.class, listener);
	}
	
	public void removeUpdateListener(UpdateListener listener) {
		listenerList.remove(UpdateListener.class, listener);
	}
	
	public void fireUpdateEvent() {
		UpdateEvent evt=new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i+1]).executionSlotUpdated(evt);
			}
		}
	}

}

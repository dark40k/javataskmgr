package fr.dark40k.taskmgr.database.executionslots;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Iterator;

import javax.swing.event.EventListenerList;

import org.jdom2.Element;

public class ExecutionSlotList implements Cloneable, Iterable<ExecutionSlot>, ExecutionSlot.UpdateListener {
	
	private final ArrayList<ExecutionSlot> slotList=new ArrayList<ExecutionSlot>();

	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------
	
	public void exportXML(Element elt) {
		for (ExecutionSlot timer : slotList) elt.addContent(timer.exportXML());
	}
	
	public void importXMLData(Element elt) {
		slotList.clear();
		for (Element linkElt : elt.getChildren(ExecutionSlot.XMLTAG)) addExecutionSlot(new ExecutionSlot(linkElt));
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Clone implementation
	//
	// -------------------------------------------------------------------------------------------
	
	public ExecutionSlotList clone() {
		ExecutionSlotList listClone = null;
		
		try {
			listClone = (ExecutionSlotList) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		
		listClone.slotList.clear();
		for (ExecutionSlot timer : slotList) listClone.addExecutionSlot(timer.clone());

		return listClone;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Getters & Setters
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Iterator<ExecutionSlot> iterator() {
		return slotList.iterator();
	}

	public void addExecutionSlot(ExecutionSlot newExecutionSlot) {
		slotList.add(newExecutionSlot);
		newExecutionSlot.addUpdateListener(this);
		fireUpdateListEvent();
	}

	public int size() {
		return slotList.size();
	}

	public ExecutionSlot get(int index) {
		if (index>=slotList.size()) return null;
		return slotList.get(index);
	}

	public void removeExecutionSlot(int rowIndex) {
		ExecutionSlot oldSlot = slotList.remove(rowIndex);
		oldSlot.removeUpdateListener(this);
		fireUpdateListEvent();
	}
	
	public void moveUp(int rowIndex) {
		ExecutionSlot link=slotList.get(rowIndex);
		slotList.set(rowIndex, slotList.get(rowIndex-1));
		slotList.set(rowIndex-1,link);
		fireUpdateListEvent();
	}
	
	public void moveDown(int rowIndex) {
		ExecutionSlot link=slotList.get(rowIndex);
		slotList.set(rowIndex, slotList.get(rowIndex+1));
		slotList.set(rowIndex+1,link);
		fireUpdateListEvent();
	}
	
	//
	// ExecutionSlot update event
	//
	
	@Override
	public void executionSlotUpdated(fr.dark40k.taskmgr.database.executionslots.ExecutionSlot.UpdateEvent evt) {
		fireUpdateSlotEvent();
	}

	//
	// ExecutionSlotList Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class UpdateEvent extends EventObject {
		public UpdateEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateListener extends EventListener {
		public void executionSlotListUpdated(UpdateEvent evt);
		public void executionSlotUpdated(UpdateEvent evt);
	}

	public void addUpdateListener(UpdateListener listener) {
		listenerList.add(UpdateListener.class, listener);
	}
	
	public void removeUpdateListener(UpdateListener listener) {
		listenerList.remove(UpdateListener.class, listener);
	}
	
	public void fireUpdateListEvent() {
		UpdateEvent evt=new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i+1]).executionSlotListUpdated(evt);
			}
		}
	}

	public void fireUpdateSlotEvent() {
		UpdateEvent evt=new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i+1]).executionSlotUpdated(evt);
			}
		}
	}

}

package fr.dark40k.taskmgr.database.scriptmgr;

import javax.script.Compilable;
import javax.script.ScriptEngine;

import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

// TODO Remplacer Nashorn qui est bientot obsolete
public class TaskMgrScriptMgr {

	ScriptEngine engine;
	Compilable compilingEngine;

	@SuppressWarnings({ "removal"})
	public TaskMgrScriptMgr() {

		NashornScriptEngineFactory factory = new NashornScriptEngineFactory();

		engine = factory.getScriptEngine(new String[] {"--no-deprecation-warning"}, null, className -> false);

//		@SuppressWarnings("removal");
//        engine =  new ScriptEngineManager().getEngineByName("nashorn");
        compilingEngine = (Compilable) engine;

	}

}

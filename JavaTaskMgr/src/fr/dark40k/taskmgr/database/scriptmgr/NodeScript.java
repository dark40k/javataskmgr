package fr.dark40k.taskmgr.database.scriptmgr;

import java.util.Map.Entry;

import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;

public class NodeScript {

	private final static Logger LOGGER = LogManager.getLogger();

	public final static String XMLTAG="script";

	private static TaskMgrScriptMgr scriptMgr;
	private final Node node;

	private String source;

	private String errorMsg;

	private Bindings bindings;
	private Invocable invocable;

	public NodeScript(Node node) {
		if (scriptMgr==null) scriptMgr=new TaskMgrScriptMgr();
		this.node = node;
		source = null;
		invocable = null;
		errorMsg="";

	}

	public Element exportXML() {
		Element elt  = new Element(XMLTAG);
		if (source!=null) elt.setText(source);
		return elt;
	}

	public void importXMLData(Element elt) {
		Element scriptElt = elt.getChild(XMLTAG);
		if (scriptElt!=null) source = scriptElt.getText();
		update();
	}


	public void showBindings() {
		for (Entry<String, Object> me : bindings.entrySet()) {
			LOGGER.info(String.format("%s: %s\n", me.getKey(), String.valueOf(me.getValue())));
		}
	}

	//
	//
	//

	public boolean sourceExists() {
		if (source==null) return false;
		if (source.isEmpty()) return false;
		return true;
	}

	public boolean isValid() {
		return (invocable!=null);
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String newSource) {

		// verifie que le script a bien chang�
		if (source == newSource) return;
		if ((source != null) && (source.equals(newSource))) return;

		// gere le cas ou la source est vide
		if ((newSource!=null) && newSource.replaceAll("\n", "").trim().isEmpty()) newSource=null;

		// applique la modification
		source = newSource;

		// essaie de recompiler le script
		update();

		// enregistre la modification du noeud
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);

	}

	private void update() {

		// desactive pour le cas d'un echec de la compilation
		invocable = null;
		errorMsg="Pas d'erreur.";

		// cas ou il n'y a pas de code source
		if ((source==null) || (source.isEmpty())) {
			return;
		}

		// essaie de recompiler le script
		try {

			// initialise les param�tres
			bindings = scriptMgr.engine.createBindings();

			// ajout des variables globales
			bindings.put("node", node);
			bindings.put("LOGGER", LOGGER);

			// applique les param�tre au moteur
			scriptMgr.engine.setBindings (bindings, ScriptContext.ENGINE_SCOPE);

			// compilation
			//CompiledScript cscript = scriptMgr.compilingEngine.compile(source);

			// execution
//			LOGGER.trace("Chargement script, noeud : "+node);
			scriptMgr.engine.eval(source);
//			LOGGER.trace("Fin chargement script");
			//cscript.eval(bindings);

			// creation de l'appel
			invocable = (Invocable) scriptMgr.engine;

		} catch (ScriptException e) {
			LOGGER.info("Echec du chargement. Script desactiv�. Error Msg =\n"+e.getMessage());
			errorMsg=e.getMessage();
		}

	}

	public void run(String call, Object... objects) {

		if (invocable==null) {
			//LOGGER.debug("Tentative d'execution d'un script desactiv�. Action annul�e.");
			return;
		}

		if (!bindings.containsKey(call)) return;

		scriptMgr.engine.setBindings (bindings, ScriptContext.ENGINE_SCOPE);

//		LOGGER.trace("start, call=" + call);
//		for (Object o : objects) LOGGER.debug("param : "+o.toString());

		try {
			invocable.invokeFunction(call, objects);
		} catch (NoSuchMethodException | ScriptException e1) {
			LOGGER.warn("Echec de la l'execution.\n Noeud="+node);
			LOGGER.warn("Erreur : \n"+ e1.getMessage());
		}
//		LOGGER.trace("end.");
	}

	public void recursiveRun(String call, Object... objects) {
		if (node.hasFather()) node.getFather().getScript().recursiveRun(call,objects);
		run(call,objects);
	}

}

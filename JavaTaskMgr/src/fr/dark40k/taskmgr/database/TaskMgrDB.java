package fr.dark40k.taskmgr.database;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.event.EventListenerList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import fr.dark40k.taskmgr.database.NodeTaskLink.BrokenLinkException;
import fr.dark40k.taskmgr.database.display.DisplayNode;
import fr.dark40k.taskmgr.database.nodeicons.NodeIconDB;

public class TaskMgrDB implements Iterable<Node> {

	private final static Logger LOGGER = LogManager.getLogger();

	public final static String XMLTAG="database";
	public final static String XMLTAG_NODES="nodes";

	public final static int NO_NODE_KEY=-1;

	public final static int ROOT_BASE_KEY=0;
	public final static int ROOT_STATUS_KEY=1;
	public final static int ROOT_TEMPLATE_KEY=2;
	public final static int ROOT_FILTER_KEY=3;
	public final static int ROOT_TASK_KEY=4;

	private int nextKey=100;

	private final NodeIconDB nodeIconDB = new NodeIconDB();
	private final LinkedHashMap<Integer, Node> dataBaseMap = new LinkedHashMap<Integer, Node>();

	// -------------------------------------------------------------------------------------------
	//
	// Constructor
	//
	// -------------------------------------------------------------------------------------------

	public TaskMgrDB() {

		LOGGER.info("Creating new dataBase");

		NodeRaw rootBaseNode = new NodeRaw(this, ROOT_BASE_KEY, "Root", true, new DisplayNode());
		dataBaseMap.put(ROOT_BASE_KEY, rootBaseNode);

		NodeStatus rootStatus = new NodeStatus(this, ROOT_STATUS_KEY, "Status", true, new DisplayNode(),true);
		rootBaseNode.addChild(rootStatus,null);

		NodeStatus rootStatus1 = new NodeStatus(this, getNewKey(), "Defaut", false, new DisplayNode(), false);
		rootStatus.addChild(rootStatus1,null);

		NodeTemplate rootTemplate = new NodeTemplate(this, ROOT_TEMPLATE_KEY, "Modeles", true, new DisplayNode(),true, null, null);
		rootBaseNode.addChild(rootTemplate,null);

		NodeTemplate rootTemplate1 = new NodeTemplate(this, getNewKey(), "Defaut", false, new DisplayNode(),false, null, null);
		rootTemplate.addChild(rootTemplate1,null);

		NodeFilter rootFilter = new NodeFilter(this, ROOT_FILTER_KEY, "Filtres",true, new DisplayNode(),true);
		rootBaseNode.addChild(rootFilter,null);

		NodeFilter rootFilter1 = new NodeFilter(this, getNewKey(), "Defaut", false, new DisplayNode(),false);
		rootFilter.addChild(rootFilter1,null);

		NodeTask rootTask = new NodeTask(this, ROOT_TASK_KEY, "Base", true, new DisplayNode(),rootTemplate1.getKey(),rootStatus1.getKey());
		rootBaseNode.addChild(rootTask,null);

	}

	public TaskMgrDB(Element rootXMLDatabaseNode) {
		reloadFromXML(rootXMLDatabaseNode);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Database handling
	//
	// -------------------------------------------------------------------------------------------

	public boolean checkNodeClass(int key, Class<?> checkClass) {
		Node node = dataBaseMap.get(key);
		if (node==null) return false;
		return (node.getClass() == checkClass);
	}

	public void cleanDatabase() {

		HashSet<Integer> orphanNodes = new HashSet<Integer>();
		orphanNodes.addAll(dataBaseMap.keySet());
		orphanNodes.removeAll(getRootBaseNode().getKeyAndChildrenKeys(true));

		if (orphanNodes.size()>0) {

			LOGGER.info("Orphan nodes found in database, count= " + orphanNodes.size());

			for (Integer nodeKey : orphanNodes) {
				Node node = getNode(nodeKey);
				if (node.getFather()==null) {
					LOGGER.info("Orphan = " + node.toString());
					outputChildrenList(node,"----> ","--");
				}
			}

			Object[] options = { UIManager.getString("OptionPane.yesButtonText"), UIManager.getString("OptionPane.noButtonText") };
			int n = JOptionPane.showOptionDialog(
					null,
				    orphanNodes.size()+" noeuds orphelins sont pr�sents dans la base (voir liste dans la console)\n Voulez vous les supprimer?",
				    "Verification de la base",
	                JOptionPane.YES_NO_OPTION,
	                JOptionPane.WARNING_MESSAGE,
	                null, options, options[1]);

			if (n==0) {
				LOGGER.info("Removing orphan nodes, count= " + orphanNodes.size());
				for (Integer nodeKey : orphanNodes)
					dataBaseMap.remove(nodeKey);
				LOGGER.info("Done.");
			}

		}

	}

	private void outputChildrenList(Node father, String prefix, String nextPrefix) {
		for (Node child : father.getChildren()) {
			LOGGER.info(prefix + child.toString());
			outputChildrenList(child,nextPrefix+prefix,nextPrefix);
		}
	}

	public void moveNodeAsFirstChild(Node nodeToMove, Node father) {
		nodeToMove.getFather().getChildren().removeChild(nodeToMove);
		father.getChildren().addChildRaw(nodeToMove, null);
		fireDataBaseChanged(this);
	}

	public void moveNodeAsYoungerBrother(Node nodeToMove, Node nodeElderBrother) {
		nodeToMove.getFather().getChildren().removeChild(nodeToMove);
		nodeElderBrother.getFather().getChildren().addChildRaw(nodeToMove, nodeElderBrother);
		fireDataBaseChanged(this);
	}

	// check if can move node as child to fther. Returns null if ok.
	public String canMoveAsChild(Node nodeToMove, Node father) {

		//check if node has correct type
		if (!father.canAddChild(nodeToMove.getClass())) return "Type incompatible";

		//check if node is read-only
		if (nodeToMove.isReadOnly()) return "Noeud en lecture seule";

		//check if new now create circular reference with father
		if (father.getChildren().checkCircularReference(nodeToMove)) return "Reference circulaire";

		return null;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Utilities
	//
	// -------------------------------------------------------------------------------------------

	public NodeIconDB getNodeIconsDB() {
		return nodeIconDB;
	}

	protected int getNewKey() {
		return nextKey++;
	}

	protected void setNode(int key,Node node) {
		dataBaseMap.put(key, node);
		if (key>=nextKey) nextKey=key+1;
	}

	public Node getNode(int key) {
		if (key==NO_NODE_KEY) return null;
		return dataBaseMap.get(key);
	}

	protected void delNode(int key) {
		dataBaseMap.remove(key);
	}

	public int findFirstFatherKey(int key) {
		for (Entry<Integer, Node> entry : dataBaseMap.entrySet())
			if (entry.getValue().getChildren().containsNodeKey(key))
				return entry.getKey();
		return -1;
	}

	@Override
	public Iterator<Node> iterator() {
		return dataBaseMap.values().iterator();
	}

	public BigInteger getDatabaseSHA1Checksum() {

		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(saveToXML(), baos);
			baos.close();

			MessageDigest m = MessageDigest.getInstance("SHA1");
			m.update(baos.toByteArray());

			return new BigInteger(1, m.digest());

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return BigInteger.ZERO;

	}

	public String checkXMLData(Element elt) {

		TaskMgrDB temporaryDB;

		// check export
		try {
			temporaryDB = new TaskMgrDB(elt);
		} catch (Exception e) {
			e.printStackTrace();
			return "Erreur XML : La base export�e n'est pas reconstructible.";
		}

		if (temporaryDB.getDatabaseSHA1Checksum().compareTo(getDatabaseSHA1Checksum())!=0) {

			LOGGER.info("Active base checksum   ="+getDatabaseSHA1Checksum().toString(16));
			LOGGER.info("Exported base checksum ="+temporaryDB.getDatabaseSHA1Checksum().toString(16));

			return "Erreur XML : La base export�e ne recharge pas de mani�re identique.";
		}

		return null;

	}

	// -------------------------------------------------------------------------------------------
	//
	// Acces scripts
	//
	// -------------------------------------------------------------------------------------------

	public void runScripts() {

		LOGGER.info("Execution scripts Status");
		getRootStatus().runScripts();

		LOGGER.info("Execution scripts Templates");
		getRootTemplate().runScripts();

		LOGGER.info("Execution scripts Filtres");
		getRootFilter().runScripts();

		LOGGER.info("Execution scripts TasksNodes");
		getRootTask().runScripts();

	}

	// -------------------------------------------------------------------------------------------
	//
	// Acces statiques
	//
	// -------------------------------------------------------------------------------------------

	public Node getRootBaseNode() { return getNode(ROOT_BASE_KEY); }
	public Node getRootStatus() { return getNode(ROOT_STATUS_KEY); }
	public Node getRootTemplate() { return getNode(ROOT_TEMPLATE_KEY); }
	public Node getRootFilter() {return getNode(ROOT_FILTER_KEY);}
	public Node getRootTask() { return getNode(ROOT_TASK_KEY); }

	// -------------------------------------------------------------------------------------------
	//
	// XML Import & Export
	//
	// -------------------------------------------------------------------------------------------

	public void reloadFromXML(Element rootXMLDatabaseNode) {

		// vide la table de noeuds
		dataBaseMap.clear();

		// importe la base de donn�e des icones
		LOGGER.info("Loading DB, Rebuilding Icons database");
		nodeIconDB.loadFromXML(rootXMLDatabaseNode.getChild(NodeIconDB.XMLTAG));

		// importe les noeuds de la base de donn�es
		LOGGER.info("Loading DB, Rebuilding Nodes");
		List<Element> listChildren = rootXMLDatabaseNode.getChild(XMLTAG_NODES).getChildren();
		Iterator<Element> i = listChildren.iterator();
		while(i.hasNext())
			createFromXML(i.next(), true, true);

		// reconstruit les liaisons de la base
		LOGGER.info("Loading DB, Rebuilding Links");
		for (Node node : this)
			node.rebuildLinks();

		// nettoie la structure de la base
		cleanDatabase();

		// signale la mise � jours de la base
		fireDataBaseChanged(this);

		// affiche le checksum
		LOGGER.info("Loading DB, Checksum="+getDatabaseSHA1Checksum().toString(16));

	}

	private Node createFromXML(Element elt, boolean rawImportKeys, boolean importChildren) {
		Node newNode;
		switch (elt.getName()) {
			case NodeRaw.XMLTAG:
				newNode = new NodeRaw(this, elt, rawImportKeys, importChildren);
				break;
			case NodeStatus.XMLTAG:
				newNode = new NodeStatus(this, elt, rawImportKeys, importChildren);
				break;
			case NodeTemplate.XMLTAG:
				newNode = new NodeTemplate(this, elt, rawImportKeys, importChildren);
				break;
			case NodeFilter.XMLTAG:
				newNode = new NodeFilter(this, elt, rawImportKeys, importChildren);
				break;
			case NodeTask.XMLTAG:
				newNode = new NodeTask(this, elt, rawImportKeys, importChildren);
				break;
			case NodeTaskLink.XMLTAG:
				try {
					newNode = new NodeTaskLink(this, elt, rawImportKeys);
				} catch (BrokenLinkException e) {
					LOGGER.error(e.getMessage()+" - Creating standard Task Node with link values.");
					newNode = new NodeTask(this, elt, rawImportKeys, false);
				}
				break;
			default: {
				LOGGER.error("Skipping unknown Node type :" + elt.getName());
				newNode = new NodeRaw(this, elt, rawImportKeys, importChildren);
				break;
			}
		}
		return newNode;
	}

	public Node createFromXML(Element elt) {
		return createFromXML(elt, false, false);
	}

	public Element saveToXML() {

		Element elt=new Element(XMLTAG);

		elt.addContent(nodeIconDB.saveToXML());

		Element eltNodes = new Element(XMLTAG_NODES);
		for (Entry<Integer, Node> entry : dataBaseMap.entrySet()) {
			eltNodes.addContent(entry.getValue().saveToXML());
		}
		elt.addContent(eltNodes);

		return elt;
	}


	// -------------------------------------------------------------------------------------------
	//
	// Change event
	//
	// -------------------------------------------------------------------------------------------

	public class UpdateNodeEvent extends EventObject {
		private Node node;
		public UpdateNodeEvent(Object source, Node node) {
			super(source);
			this.node=node;
		}
		public Node getNode() {
			return node;
		}
	}

	public interface UpdateNodeListener extends EventListener {
		public void updateDataBase(UpdateNodeEvent evt);
		public void updateNodeContent(UpdateNodeEvent evt);
		public void updateNodeDisplay(UpdateNodeEvent evt);
		public void updateNodeStructure(UpdateNodeEvent evt);
	}

	EventListenerList listenerList = new EventListenerList();

	public void addUpdateListener(UpdateNodeListener l) {
		listenerList.add(UpdateNodeListener.class, l);
	}

	public void removeUpdateListener(UpdateNodeListener l) {
		listenerList.remove(UpdateNodeListener.class, l);
	}

	public void fireDataBaseChanged(Object source) {
		UpdateNodeEvent updateNodeEvent = new UpdateNodeEvent(source, getRootBaseNode());
		Object[] listeners = listenerList.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2)
			if (listeners[i] == UpdateNodeListener.class)
				((UpdateNodeListener) listeners[i + 1]).updateDataBase(updateNodeEvent);
	}

	public void fireNodeStructureChanged(Object source, Node node) {
		UpdateNodeEvent updateNodeEvent = new UpdateNodeEvent(source, node);
		Object[] listeners = listenerList.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2)
			if (listeners[i] == UpdateNodeListener.class)
				((UpdateNodeListener) listeners[i + 1]).updateNodeStructure(updateNodeEvent);
	}

	public void fireNodeContentChanged(Object source, Node node) {
		UpdateNodeEvent updateNodeEvent = new UpdateNodeEvent(source, node);
		Object[] listeners = listenerList.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2)
			if (listeners[i] == UpdateNodeListener.class)
				((UpdateNodeListener) listeners[i + 1]).updateNodeContent(updateNodeEvent);
	}

	public void fireNodeDisplayChanged(Object source, Node node) {
		UpdateNodeEvent updateNodeEvent = new UpdateNodeEvent(source, node);
		Object[] listeners = listenerList.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -= 2)
			if (listeners[i] == UpdateNodeListener.class)
				((UpdateNodeListener) listeners[i + 1]).updateNodeDisplay(updateNodeEvent);
	}

}

package fr.dark40k.taskmgr.database;

import java.util.ArrayList;
import java.util.TreeSet;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.datalinks.DataLinkList;
import fr.dark40k.taskmgr.database.display.Display;
import fr.dark40k.taskmgr.database.display.DisplayNode;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlotList;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlotList.UpdateEvent;
import fr.dark40k.taskmgr.database.interfaces.NodeDefaultTemplateStatusInterface;
import fr.dark40k.taskmgr.database.taskdates.TaskDates;

public class NodeTask extends Node implements ExecutionSlotList.UpdateListener, NodeDefaultTemplateStatusInterface {

	public final static String XMLTAG="nodeTask";

	private int templateKey=TaskMgrDB.NO_NODE_KEY;

	private int statusKey=TaskMgrDB.NO_NODE_KEY;

	private final DataLinkList fileLinkList = new DataLinkList(this);

	private final TaskDates taskDates = new TaskDates(this);

	private final ExecutionSlotList executionSlotList = new ExecutionSlotList();

	private int	childDefaultTemplateKey=TaskMgrDB.NO_NODE_KEY;

	private int	childDefaultStatusKey=TaskMgrDB.NO_NODE_KEY;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------

	public NodeTask(TaskMgrDB dataBase, int key, String name, Boolean readOnly, DisplayNode display, int templateKey, int nodeStatusKey) {
		super(dataBase, key, name, readOnly, display);
		this.templateKey=templateKey;
		this.statusKey=nodeStatusKey;
	}

	public NodeTask(TaskMgrDB dataBase, String name, Boolean readOnly, DisplayNode display, int templateKey, int nodeStatusKey) {
		this(dataBase, dataBase.getNewKey(), name, readOnly, display, templateKey, nodeStatusKey);
	}

	protected NodeTask(TaskMgrDB nodeDataBase, Element elt, boolean rawImportKeys, boolean importChildren) {
		super(nodeDataBase, elt, rawImportKeys, importChildren);

		statusKey=Integer.valueOf(elt.getAttributeValue("statusKey"));
		templateKey=Integer.valueOf(elt.getAttributeValue("templateKey"));

		childDefaultTemplateKey=Integer.valueOf(elt.getAttributeValue("childDefTemp", Integer.toString(TaskMgrDB.NO_NODE_KEY)));
		childDefaultStatusKey=Integer.valueOf(elt.getAttributeValue("childDefStat", Integer.toString(TaskMgrDB.NO_NODE_KEY)));

		// check keys if not rawImportKeys
		if (!rawImportKeys) {
			if (!getDataBase().checkNodeClass(statusKey,NodeStatus.class)) statusKey=TaskMgrDB.NO_NODE_KEY;
			if (!getDataBase().checkNodeClass(templateKey,NodeTemplate.class))  templateKey=TaskMgrDB.NO_NODE_KEY;
			if (!getDataBase().checkNodeClass(childDefaultStatusKey,NodeStatus.class)) childDefaultStatusKey=TaskMgrDB.NO_NODE_KEY;
			if (!getDataBase().checkNodeClass(childDefaultTemplateKey,NodeTemplate.class))  childDefaultTemplateKey=TaskMgrDB.NO_NODE_KEY;
		}

		fileLinkList.importXMLData(elt);
		taskDates.importXMLData(elt);
		executionSlotList.importXMLData(elt);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Element saveToXML() {

		Element elt=super.saveToXML().setName(XMLTAG);

		elt.setAttribute("statusKey", Integer.toString(statusKey));
		elt.setAttribute("templateKey", Integer.toString(templateKey));

		elt.setAttribute("childDefTemp", Integer.toString(childDefaultTemplateKey));
		elt.setAttribute("childDefStat", Integer.toString(childDefaultStatusKey));

		taskDates.exportXML(elt);

		fileLinkList.exportXML(elt);

		executionSlotList.exportXML(elt);

		return elt;
	}

	@Override
	public void reloadFromXML(Element elt) {

		statusKey=Integer.valueOf(elt.getAttributeValue("statusKey"));
		templateKey=Integer.valueOf(elt.getAttributeValue("templateKey"));

		childDefaultTemplateKey=Integer.valueOf(elt.getAttributeValue("childDefTemp", Integer.toString(TaskMgrDB.NO_NODE_KEY)));
		childDefaultStatusKey=Integer.valueOf(elt.getAttributeValue("childDefStat", Integer.toString(TaskMgrDB.NO_NODE_KEY)));

		taskDates.importXMLData(elt);

		fileLinkList.importXMLData(elt);

		executionSlotList.importXMLData(elt);

		super.reloadFromXML(elt);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es particulieres
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String getStackedName(String separator){
		return getStackedName(separator,TaskMgrDB.ROOT_TASK_KEY);
	}

	public NodeTemplate getTemplate() {
		return (NodeTemplate) getDataBase().getNode(templateKey);
	}

	public int getTemplateKey() {
		return templateKey;
	}

	public void setTemplateKey(int key) {
		if(key==templateKey) return;
		checkReadOnlyBeforeWriting();
		templateKey=key;
		updateEffectiveDisplay(true);
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	public NodeStatus getStatus() {
		return (NodeStatus) getDataBase().getNode(statusKey);
	}

	public int getStatusKey() {
		return statusKey;
	}

	public void setStatusKey(int newKey) {
		if(newKey==statusKey) return;
		checkReadOnlyBeforeWriting();
		statusKey=newKey;
		updateEffectiveDisplay(true);
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	public DataLinkList getLinkList() {
		return fileLinkList;
	}

	public TaskDates getDates() {
		return taskDates;
	}

	public ExecutionSlotList getExecutionSlotList() {
		return executionSlotList;
	}

	@Override
	public int getChildDefaultTemplateKey() {
		return childDefaultTemplateKey;
	}

	@Override
	public void setChildDefaultTemplateKey(int childDefaultTemplate) {
		if (this.childDefaultTemplateKey==childDefaultTemplate) return;
		this.childDefaultTemplateKey=childDefaultTemplate;
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	@Override
	public int getChildDefaultStatusKey() {
		return childDefaultStatusKey;
	}

	@Override
	public void setChildDefaultStatusKey(int childDefaultStatus) {
		if (this.childDefaultStatusKey==childDefaultStatus) return;
		this.childDefaultStatusKey=childDefaultStatus;
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Scripts
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void runScripts() {

		NodeStatus status=getStatus();
		if (status!=null) status.getScript().recursiveRun("updateStatus",this);

		NodeTemplate template=getTemplate();
		if (template!=null) template.getScript().recursiveRun("updateTemplate",this);

		super.runScripts();

	}

	// -------------------------------------------------------------------------------------------
	//
	// Listeners
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void executionSlotListUpdated(UpdateEvent evt) {
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this, this);
	}

	@Override
	public void executionSlotUpdated(UpdateEvent evt) {
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this, this);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es d'affichage
	//
	// -------------------------------------------------------------------------------------------

	// Ordre de priorit� d'affichage en cas d'egalit� (ordre decroissant) :
	// 1 - definition locale
	// 2 - status de la tache
	// 3 - categories
	// 4 - template
	@Override
	protected ArrayList<Display> buildEffectiveDisplayList() {
		ArrayList<Display> displayList= new ArrayList<Display>();
		displayList.add(getDisplay());

		NodeStatus status=getStatus();
		if (status!=null)
			displayList.add(status.getEffectiveDisplay());

		NodeTemplate template=getTemplate();
		if (template!=null)
			displayList.add(template.getEffectiveDisplay());

		return displayList;
	}
	// -------------------------------------------------------------------------------------------
	//
	// Gestion des categories
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public TreeSet<String> getRecursiveCategoriesNames() {

		TreeSet<String> listNames = super.getRecursiveCategoriesNames();

		for (String name : getDataBase().getNode(statusKey).getRecursiveCategoriesNames())
			if (!listNames.contains(name))
				listNames.add(name);

		for (String name : getDataBase().getNode(templateKey).getRecursiveCategoriesNames())
			if (!listNames.contains(name))
				listNames.add(name);

		return listNames;

	}

	// -------------------------------------------------------------------------------------------
	//
	// Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	private int getNewChildTemplateKey() {
		if (childDefaultTemplateKey!=TaskMgrDB.NO_NODE_KEY) return childDefaultTemplateKey;
		if (getTemplate().getChildDefaultTemplateKey()!=TaskMgrDB.NO_NODE_KEY) return getTemplate().getChildDefaultTemplateKey();
		return getTemplate().getKey();
	}

	private int getNewChildStatusKey() {
		if (childDefaultStatusKey!=TaskMgrDB.NO_NODE_KEY) return childDefaultStatusKey;
		if (getTemplate().getChildDefaultStatusKey()!=TaskMgrDB.NO_NODE_KEY) return getTemplate().getChildDefaultStatusKey();
		return getStatusKey();
	}

	@Override
	public int addNewChild(Node elderBrother) {
		NodeTask newNode = new NodeTask(
				getDataBase(),
				DEFAULT_NAME,
				DEFAULT_READONLY,
				new DisplayNode(),
				(elderBrother!=null) ? ((NodeTask)elderBrother).templateKey : getNewChildTemplateKey(),
				(elderBrother!=null) ? ((NodeTask)elderBrother).statusKey : getNewChildStatusKey());

		this.addChild(newNode,elderBrother);
		return newNode.getKey();
	}

	@Override
	public boolean canAddChild(Class<? extends Node> nodeClass) {
		return (NodeTask.class.isAssignableFrom(nodeClass));
	}




}

package fr.dark40k.taskmgr.database.display;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.display.attr.Attr;
import fr.dark40k.taskmgr.database.display.attr.AttrColor;
import fr.dark40k.taskmgr.database.display.attr.AttrFont;
import fr.dark40k.taskmgr.database.display.attr.AttrHeight;
import fr.dark40k.taskmgr.database.display.attr.AttrIcon;

public class DisplayNode extends Display implements Attr.UpdateListener {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private static final String HEIGHT = "height";
	private static final String BACK_COLOR_BOTTOM = "backColorBottom";
	private static final String BACK_COLOR_MIDDLE = "backColorMiddle";
	private static final String BACK_COLOR_TOP = "backColorTop";
	private static final String FRONT_COLOR = "frontColor";
	private static final String FONT = "font";
	private static final String ICON2 = "icon2";
	private static final String ICON1 = "icon1";

	public DisplayNode() {
		icon1 = new AttrIcon(AttrIcon.DEFAULT_PRIORITY);
		icon2 = new AttrIcon(AttrIcon.DEFAULT_PRIORITY);
		
		font = new AttrFont(AttrFont.DEFAULT_PRIORITY, 
				AttrFont.DEFAULT_NAME, AttrFont.DEFAULT_STYLE, AttrFont.DEFAULT_SIZE,
				AttrFont.DEFAULT_STRIKETHROUGH,AttrFont.DEFAULT_UNDERLINED);
		
		frontColor=new AttrColor( AttrColor.DEFAULT_PRIORITY, AttrColor.DEFAULT_FOREGROUND);
		backColorTop=new AttrColor( AttrColor.DEFAULT_PRIORITY, AttrColor.DEFAULT_BACKGROUND);
		backColorMiddle=new AttrColor( AttrColor.DEFAULT_PRIORITY, AttrColor.DEFAULT_BACKGROUND);
		backColorBottom=new AttrColor( AttrColor.DEFAULT_PRIORITY, AttrColor.DEFAULT_BACKGROUND);
		
		height=new AttrHeight(AttrIcon.DEFAULT_PRIORITY,AttrHeight.DEFAULT_HEIGHT,AttrHeight.DEFAULT_END_HEIGHT);
		
		registerListeners();		
	}
	
	//
	// XML import/Export
	//
	
	public DisplayNode(TaskMgrDB nodeDataBase, Element elt) {
		
		icon1 = new AttrIcon(AttrIcon.DEFAULT_PRIORITY);
		icon2 = new AttrIcon(AttrIcon.DEFAULT_PRIORITY);
		
		font = new AttrFont(AttrFont.DEFAULT_PRIORITY, 
				AttrFont.DEFAULT_NAME, AttrFont.DEFAULT_STYLE, AttrFont.DEFAULT_SIZE,
				AttrFont.DEFAULT_STRIKETHROUGH,AttrFont.DEFAULT_UNDERLINED);
		
		frontColor=new AttrColor( AttrColor.DEFAULT_PRIORITY, AttrColor.DEFAULT_FOREGROUND);
		backColorTop=new AttrColor( AttrColor.DEFAULT_PRIORITY, AttrColor.DEFAULT_BACKGROUND);
		backColorMiddle=new AttrColor( AttrColor.DEFAULT_PRIORITY, AttrColor.DEFAULT_BACKGROUND);
		backColorBottom=new AttrColor( AttrColor.DEFAULT_PRIORITY, AttrColor.DEFAULT_BACKGROUND);
		
		height=new AttrHeight(AttrIcon.DEFAULT_PRIORITY,AttrHeight.DEFAULT_HEIGHT,AttrHeight.DEFAULT_END_HEIGHT);
		
		try {
			icon1=new AttrIcon(nodeDataBase, elt.getChild(ICON1));
		} catch (RuntimeException e) {
			LOGGER.info("Node " + elt.getParentElement().getAttributeValue("key") + ", Error loading <icon1> Using default, Error = " + e.toString() + " " + e.getLocalizedMessage());
		}
		
		try {
			icon2=new AttrIcon(nodeDataBase, elt.getChild(ICON2));
		} catch (RuntimeException e) {
			LOGGER.info("Node " + elt.getParentElement().getAttributeValue("key") + ", Error loading <icon2> Using default, Error = " + e.toString() + " " + e.getLocalizedMessage());
		}
		
		try {
			font=new AttrFont(elt.getChild(FONT));
		} catch (RuntimeException e) {
			LOGGER.info("Node " + elt.getParentElement().getAttributeValue("key") + ", Error loading <font> Using default, Error = " + e.toString() + " " + e.getLocalizedMessage());
		}
		
		try {
			frontColor=new AttrColor(elt.getChild(FRONT_COLOR));
		} catch (RuntimeException e) {
			LOGGER.info("Node " + elt.getParentElement().getAttributeValue("key") + ", Error loading <frontColor> Using default, Error = " + e.toString() + " " + e.getLocalizedMessage());
		}
		
		try {
			backColorTop=new AttrColor(elt.getChild(BACK_COLOR_TOP));
		} catch (RuntimeException e) {
			LOGGER.info("Node " + elt.getParentElement().getAttributeValue("key") + ", Error loading <backColorTop> Using default, Error = " + e.toString() + " " + e.getLocalizedMessage());
		}
		
		try {
			backColorMiddle=new AttrColor(elt.getChild(BACK_COLOR_MIDDLE));
		} catch (RuntimeException e) {
			LOGGER.info("Node " + elt.getParentElement().getAttributeValue("key") + ", Error loading <backColorMiddle> Using default, Error = " + e.toString() + " " + e.getLocalizedMessage());
		}
		
		try {
			backColorBottom=new AttrColor(elt.getChild(BACK_COLOR_BOTTOM));
		} catch (RuntimeException e) {
			LOGGER.info("Node " + elt.getParentElement().getAttributeValue("key") + ", Error loading <backColorBottom> Using default, Error = " + e.toString() + " " + e.getLocalizedMessage());
		}
		
		try {
			height=new AttrHeight(elt.getChild(HEIGHT));
		} catch (RuntimeException e) {
			LOGGER.info("Node " + elt.getParentElement().getAttributeValue("key") + ", Error loading <height> Using default, Error = " + e.toString() + " " + e.getLocalizedMessage());
		}

		icon1.addUpdateListener(this);
		icon2.addUpdateListener(this);
		
		font.addUpdateListener(this);
		
		frontColor.addUpdateListener(this);
		backColorTop.addUpdateListener(this);
		backColorMiddle.addUpdateListener(this);
		backColorBottom.addUpdateListener(this);
		
		height.addUpdateListener(this);
		
	}
	
	public Element exportXML(String eltName) {

		Element elt=new Element(eltName);
		
		elt.addContent(this.icon1.exportXML(ICON1));
		elt.addContent(this.icon2.exportXML(ICON2));
		elt.addContent(this.font.exportXML(FONT));
		elt.addContent(this.frontColor.exportXML(FRONT_COLOR));
		elt.addContent(this.backColorTop.exportXML(BACK_COLOR_TOP));
		elt.addContent(this.backColorMiddle.exportXML(BACK_COLOR_MIDDLE));
		elt.addContent(this.backColorBottom.exportXML(BACK_COLOR_BOTTOM));
		elt.addContent(this.height.exportXML(HEIGHT));
		
		return elt;
	}

	public void importXMLData(Element elt) {
		releaseListeners();
		icon1.importXMLData(elt.getChild(ICON1));
		icon2.importXMLData(elt.getChild(ICON2));
		font.importXMLData(elt.getChild(FONT));
		frontColor.importXMLData(elt.getChild(FRONT_COLOR));
		backColorTop.importXMLData(elt.getChild(BACK_COLOR_TOP));
		backColorMiddle.importXMLData(elt.getChild(BACK_COLOR_MIDDLE));
		backColorBottom.importXMLData(elt.getChild(BACK_COLOR_BOTTOM));
		height.importXMLData(elt.getChild(HEIGHT));
		registerListeners();
		fireUpdateDisplayEvent();
	}
	
	//
	// Clone implementation
	//
	
	public DisplayNode clone() {
		DisplayNode displayClone = null;
		
		displayClone = (DisplayNode) super.clone();

		return displayClone;
	}

	//
	// Changement d'une des donn�es de display
	//
	
	protected void registerListeners() {
		icon1.addUpdateListener(this);
		icon2.addUpdateListener(this);
		font.addUpdateListener(this);
		frontColor.addUpdateListener(this);
		backColorTop.addUpdateListener(this);
		backColorMiddle.addUpdateListener(this);
		backColorBottom.addUpdateListener(this);
		height.addUpdateListener(this);
	}

	protected void releaseListeners() {
		icon1.removeUpdateListener(this);
		icon2.removeUpdateListener(this);
		font.removeUpdateListener(this);
		frontColor.removeUpdateListener(this);
		backColorTop.removeUpdateListener(this);
		backColorMiddle.removeUpdateListener(this);
		backColorBottom.removeUpdateListener(this);
		height.removeUpdateListener(this);
	}
	
	@Override
	public void displayAttrUpdate(Attr.UpdateEvent evt) {
		fireUpdateDisplayEvent();
	}

}

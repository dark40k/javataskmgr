package fr.dark40k.taskmgr.database.display.attr;

import javax.swing.Icon;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.nodeicons.NodeIcon;
import fr.dark40k.taskmgr.database.nodeicons.NodeIconDB;


public class AttrIcon extends Attr {

	public static final NodeIcon DEFAULT_ICON = null;
	
	private NodeIcon nodeIcon;
	
	public AttrIcon(int priority) {
		super(priority);
		this.nodeIcon=null;
	}

	//
	// Import / Export XML
	//
	
	public AttrIcon(TaskMgrDB nodeDataBase, Element elt) {
		super(elt);
		int key=Integer.valueOf(elt.getAttributeValue("key",Integer.toString(NodeIconDB.NO_ICON_KEY)));
		nodeIcon= nodeDataBase.getNodeIconsDB().getNodeIcon(key);
		if (nodeIcon!=null) nodeIcon.lock(this);
	}
	
	public Element exportXML(String eltName) {
		Element elt = super.exportXML(eltName);
		elt.setAttribute("key", (nodeIcon != null) ? Integer.toString(nodeIcon.getKey()) : Integer.toString(NodeIconDB.NO_ICON_KEY) );
		return elt;
	}
	
	public void importXMLData(TaskMgrDB nodeDataBase, Element elt) {
		int key=Integer.valueOf(elt.getAttributeValue("key",Integer.toString(NodeIconDB.NO_ICON_KEY)));
		nodeIcon=nodeDataBase.getNodeIconsDB().getNodeIcon(key);
		super.importXMLData(elt);
	}

	//
	// Getters et Setters
	//
	
	public String getPath() {
		return nodeIcon.getName();
	}
	
	public Icon getIcon() {
		if (nodeIcon==null) return null;
		return nodeIcon.getIcon();
	}

	public void setNodeIcon(NodeIcon newNodeIcon) {
		if (nodeIcon==newNodeIcon) return;
		if ((nodeIcon!=null)&&(newNodeIcon!=null)&&(nodeIcon.getKey()==newNodeIcon.getKey())) return;
		if (nodeIcon!=null) nodeIcon.release(this);
		nodeIcon=newNodeIcon;
		if (nodeIcon!=null) nodeIcon.lock(this);
		fireUpdateEvent();
	}

	public NodeIcon getNodeIcon() {
		return nodeIcon;
	}

}
 
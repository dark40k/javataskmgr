package fr.dark40k.taskmgr.database.display.attr;

import java.util.EventListener;
import java.util.EventObject;

import javax.swing.event.EventListenerList;

import org.jdom2.Element;

public abstract class Attr implements Cloneable {

	public final static int DEFAULT_PRIORITY = 0;

	private int priority;

	public Attr(int priority) {
		this.priority=priority;
	}

	public static Attr getTopAttrClone(Attr attr1, Attr attr2) {
		if (attr1==null) return attr2.clone();
		if (attr2==null) return attr1.clone();
		return attr1.priority>=attr2.priority ? (Attr) attr1.clone() : (Attr) attr2.clone();
	}

	//
	// XML import/Export
	//

	public Attr(Element elt) {
		priority=Integer.valueOf(elt.getAttributeValue("priority"));
	}

	public Element exportXML(String eltName) {
		Element elt = new Element(eltName);
		elt.setAttribute("priority", Integer.toString(priority));
		return elt;
	}

	public void importXMLData(Element elt) {
		priority = Integer.valueOf(elt.getAttributeValue("priority"));
		fireUpdateEvent();
	}

	//
	// Clone implementation
	//

	@Override
	public Attr clone() {
		Attr attrBaseClone = null;
		try {
			attrBaseClone = (Attr) super.clone();
		} catch(CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}

		attrBaseClone.listenerList = new EventListenerList();

		return attrBaseClone;
	}

	//
	// Getter/Setter
	//
	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		if (this.priority==priority) return;
		this.priority=priority;
		fireUpdateEvent();
	}

	//
	// Change event
	//

	private EventListenerList listenerList = new EventListenerList();

	public class UpdateEvent extends EventObject {
		public UpdateEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateListener extends EventListener {
		public void displayAttrUpdate(UpdateEvent evt);
	}

	public void addUpdateListener(UpdateListener listener) {
		listenerList.add(UpdateListener.class, listener);
	}

	public void removeUpdateListener(UpdateListener listener) {
		listenerList.remove(UpdateListener.class, listener);
	}

	public void fireUpdateEvent() {
		UpdateEvent evt=new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i+1]).displayAttrUpdate(evt);
			}
		}
	}

}

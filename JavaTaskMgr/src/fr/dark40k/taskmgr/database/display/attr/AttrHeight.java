package fr.dark40k.taskmgr.database.display.attr;

import org.jdom2.Element;

public class AttrHeight extends Attr{
	
	public static final int MIN_HEIGHT=5;
	public static final int DEFAULT_HEIGHT=22;
	public static final int DEFAULT_END_HEIGHT=8;
	
	private int height;
	private int endHeight;

	public AttrHeight(int priority, int height, int endHeight) {
		super(priority);
		this.height = height;
		this.endHeight = endHeight;
	}
	
	//
	// XML import/Export
	//
	
	public AttrHeight(Element elt) {
		super(elt);
		height=Integer.valueOf(elt.getAttributeValue("value",Integer.toString(DEFAULT_HEIGHT)));
		endHeight=Integer.valueOf(elt.getAttributeValue("endValue",Integer.toString(DEFAULT_END_HEIGHT)));
	}
	
	public Element exportXML(String eltName) {
		Element elt = super.exportXML(eltName);
		elt.setAttribute("value",Integer.toString(height));
		elt.setAttribute("endValue",Integer.toString(endHeight));
		return elt;
	}
	
	public void importXMLData(Element elt) {
		height=Integer.valueOf(elt.getAttributeValue("value",Integer.toString(DEFAULT_HEIGHT)));
		endHeight=Integer.valueOf(elt.getAttributeValue("endValue",Integer.toString(DEFAULT_END_HEIGHT)));
		super.importXMLData(elt);
	}

	//
	// Getters
	//
	
	public int getHeight() {
		return height;
	}
	
	public int getEndHeight() {
		return endHeight;
	}
	
	//
	// setters
	// 

	public void setHeight(int height) {
		if (this.height==height) return;
		this.height=(height>=MIN_HEIGHT) ? height : MIN_HEIGHT;
		fireUpdateEvent();
	}
	
	public void setEndHeight(int endHeight) {
		if (this.endHeight==endHeight) return;
		this.endHeight=(endHeight>=MIN_HEIGHT) ? endHeight : MIN_HEIGHT;
		fireUpdateEvent();
	}
	
}

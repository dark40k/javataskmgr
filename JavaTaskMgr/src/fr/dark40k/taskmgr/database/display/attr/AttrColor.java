package fr.dark40k.taskmgr.database.display.attr;

import java.awt.Color;
import org.jdom2.Element;

public class AttrColor extends Attr {

	public final static Color DEFAULT_FOREGROUND=Color.black;
	public final static Color DEFAULT_BACKGROUND=Color.white;
	
	private Color color;

	public AttrColor(int priority, Color color) {
		super(priority);
		this.color=color;
	}
	
	//
	// XML import/Export
	//
	
	public AttrColor(Element elt) {
		super(elt);
		color=new Color(Integer.valueOf(elt.getAttributeValue("red")),Integer.valueOf(elt.getAttributeValue("green")),Integer.valueOf(elt.getAttributeValue("blue")));
	}
	
	public Element exportXML(String eltName) {
		Element elt = super.exportXML(eltName);
		elt.setAttribute("red", Integer.toString(color.getRed()));
		elt.setAttribute("green", Integer.toString(color.getGreen()));
		elt.setAttribute("blue", Integer.toString(color.getBlue()));
		return elt;
	}
	
	public void importXMLData(Element elt) {
		color=new Color(Integer.valueOf(elt.getAttributeValue("red")),Integer.valueOf(elt.getAttributeValue("green")),Integer.valueOf(elt.getAttributeValue("blue")));
		super.importXMLData(elt);
	}

	//
	// Getters et Setters
	//
	
	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		if (color.equals(this.color)) return;
		this.color=color;
		fireUpdateEvent();
	}

}

package fr.dark40k.taskmgr.database.display.attr;

import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import org.jdom2.Element;

public class AttrFont extends Attr{
	
	public static final String DEFAULT_NAME="Arial";
	public static final int DEFAULT_STYLE=Font.PLAIN;
	public static final int DEFAULT_SIZE=12;
	public static final Boolean DEFAULT_STRIKETHROUGH=false;
	public static final Boolean DEFAULT_UNDERLINED=false;
	
	private String fontName;
	private int fontStyle;
	private int fontSize;
	private Boolean strikeThrough;
	private Boolean underlined;

	private Font effectiveFont;

	public AttrFont(int priority, String fontName, int fontStyle, int fontSize, boolean strikeThrough, boolean underlined) {
		super(priority);
		this.fontName = fontName;
		this.fontStyle = fontStyle;
		this.fontSize = fontSize;
		this.strikeThrough=strikeThrough;
		this.underlined=underlined;
		updateEffectiveFont();
	}
	
	//
	// XML import/Export
	//
	
	public AttrFont(Element elt) {
		super(elt);
		fontName=elt.getAttributeValue("fontName");
		fontStyle=Integer.valueOf(elt.getAttributeValue("fontStyle"));
		fontSize=Integer.valueOf(elt.getAttributeValue("fontSize"));
		strikeThrough=(elt.getAttribute("strikeThrough")!=null);
		underlined=(elt.getAttribute("underlined")!=null);
		updateEffectiveFont();
	}
	
	public Element exportXML(String eltName) {
		Element elt = super.exportXML(eltName);
		elt.setAttribute("fontName",fontName);
		elt.setAttribute("fontStyle",Integer.toString(fontStyle));
		elt.setAttribute("fontSize",Integer.toString(fontSize));
		if (strikeThrough) elt.setAttribute("strikeThrough","1");
		if (underlined) elt.setAttribute("underlined","1");
		return elt;
	}
	
	public void importXMLData(Element elt) {
		fontName=elt.getAttributeValue("fontName");
		fontStyle=Integer.valueOf(elt.getAttributeValue("fontStyle"));
		fontSize=Integer.valueOf(elt.getAttributeValue("fontSize"));
		strikeThrough=(elt.getAttribute("strikeThrough")!=null);
		underlined=(elt.getAttribute("underlined")!=null);
		updateEffectiveFont();
		super.importXMLData(elt);
	}
	
	//
	// Getters
	//
	
	public String getFontName() {
		return fontName;
	}
	public int getFontStyle() {
		return fontStyle;
	}
	public int getFontSize() {
		return fontSize;
	}
	
	public Boolean isUnderlined() {
		return underlined;
	}
	
	public Boolean isStrikeThroughOut() {
		return strikeThrough;
	}
	
	//
	// setters
	// 

	public void setFontName(String fontName) {
		if (this.fontName.equals(fontName)) return;
		this.fontName=fontName;
		updateEffectiveFont();
		fireUpdateEvent();
	}
	
	public void setFontSize(int fontSize) {
		if (this.fontSize==fontSize) return;
		this.fontSize=fontSize;
		updateEffectiveFont();
		fireUpdateEvent();
	}

	public void setFontStyle(int fontStyle) {
		if (this.fontStyle==fontStyle) return;
		this.fontStyle=fontStyle;
		updateEffectiveFont();
		fireUpdateEvent();
	}
	
	public void setStrikeThrough(Boolean strikeThrough) {
		if (this.strikeThrough==strikeThrough) return;
		this.strikeThrough=strikeThrough;
		updateEffectiveFont();
		fireUpdateEvent();
	}

	public void setUnderlined(Boolean underlined) {
		if (this.underlined==underlined) return;
		this.underlined=underlined;
		updateEffectiveFont();
		fireUpdateEvent();
	}
	
	//
	// recuperation / calcul de la fonte
	//
	public Font getEffectiveFont() {
		return effectiveFont;
	}

	public void updateEffectiveFont() {
		effectiveFont=calcEffectiveFont();
	}

	private Font calcEffectiveFont() {
		if (fontName==null) return null;
		if (fontStyle<0) return null;
		if (fontSize<0) return null;
		Font font= new Font(fontName,fontStyle,fontSize);

		Map<TextAttribute, Object>  attributes = new HashMap<TextAttribute, Object>();
		
		if (strikeThrough) attributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON); 

		if (underlined) attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON); 

		font = font.deriveFont(attributes);

		return font;
	}

	//
	public static Font getStrikethroughFont2(String name, int properties, int size)
	{
	   Font font = new Font(name, properties, size); 
	   Map<TextAttribute, Object>  attributes = new HashMap<TextAttribute, Object>();
	   attributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON); 
	   Font newFont = font.deriveFont(attributes);
	   return newFont;             
	}
	
}

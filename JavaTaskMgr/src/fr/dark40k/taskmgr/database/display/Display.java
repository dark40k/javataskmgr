package fr.dark40k.taskmgr.database.display;

import java.util.EventListener;
import java.util.EventObject;

import javax.swing.event.EventListenerList;

import fr.dark40k.taskmgr.database.display.attr.AttrColor;
import fr.dark40k.taskmgr.database.display.attr.AttrFont;
import fr.dark40k.taskmgr.database.display.attr.AttrHeight;
import fr.dark40k.taskmgr.database.display.attr.AttrIcon;

public abstract class Display implements Cloneable {

	protected AttrIcon icon1;
	protected AttrIcon icon2;

	protected AttrFont font;
	
	protected AttrColor frontColor;
	protected AttrColor backColorTop;
	protected AttrColor backColorMiddle;
	protected AttrColor backColorBottom;
	
	protected AttrHeight height;
	
	//
	// Enregistrement / liberations des listeners pour objets dependants 
	//
	
	protected abstract void registerListeners();
	
	protected abstract void releaseListeners();
		
	//
	// Clone implementation
	//
	
	public Display clone() {
		Display displayClone = null;
		try {
			displayClone = (Display) super.clone();
		} catch(CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
		
		displayClone.icon1=(AttrIcon)displayClone.icon1.clone();
		displayClone.icon2=(AttrIcon)displayClone.icon2.clone();
		displayClone.font=(AttrFont)displayClone.font.clone();
		displayClone.frontColor=(AttrColor)displayClone.frontColor.clone();
		displayClone.backColorTop=(AttrColor)displayClone.backColorTop.clone();
		displayClone.backColorMiddle=(AttrColor)displayClone.backColorMiddle.clone();
		displayClone.backColorBottom=(AttrColor)displayClone.backColorBottom.clone();
		displayClone.height=(AttrHeight)displayClone.height.clone();
		
		displayClone.listenerList = new EventListenerList();
		registerListeners();
		
		return displayClone;
	}

	//
	// Getters / Seters
	//
	
	public AttrIcon getIcon1() {
		return icon1;
	}

	public AttrIcon getIcon2() {
		return icon2;
	}

	public AttrFont getFont() {
		return font;
	}

	public AttrColor getFrontColor() {
		return frontColor;
	}

	public AttrColor getBackColorTop() {
		return backColorTop;
	}

	public AttrColor getBackColorMiddle() {
		return backColorMiddle;
	}

	public AttrColor getBackColorBottom() {
		return backColorBottom;
	}

	public AttrHeight getHeight() {
		return height;
	}

	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public int listenersCount() { return listenerList.getListenerCount(); };
	
	public interface UpdateDisplayListener extends EventListener {
		public void displayUpdated(UpdateDisplayEvent evt);
	}

	public class UpdateDisplayEvent extends EventObject {
		public UpdateDisplayEvent(Object source) {
			super(source);
		}
	}

	public void addUpdateListener(UpdateDisplayListener listener) {
		listenerList.add(UpdateDisplayListener.class, listener);
	}
	
	public void removeUpdateListener(UpdateDisplayListener listener) {
		listenerList.remove(UpdateDisplayListener.class, listener);
	}
	
	public void fireUpdateDisplayEvent() {
		UpdateDisplayEvent evt=new UpdateDisplayEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateDisplayListener.class) {
				((UpdateDisplayListener) listeners[i+1]).displayUpdated(evt);
			}
		}
	}

}

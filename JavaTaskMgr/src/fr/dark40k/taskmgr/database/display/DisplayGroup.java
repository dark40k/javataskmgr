package fr.dark40k.taskmgr.database.display;

import java.util.ArrayList;

import fr.dark40k.taskmgr.database.display.attr.Attr;
import fr.dark40k.taskmgr.database.display.attr.AttrColor;
import fr.dark40k.taskmgr.database.display.attr.AttrFont;
import fr.dark40k.taskmgr.database.display.attr.AttrHeight;
import fr.dark40k.taskmgr.database.display.attr.AttrIcon;

public class DisplayGroup extends Display implements Display.UpdateDisplayListener {

	private ArrayList<Display> displayList = new ArrayList<Display>();

	//
	// Controle
	//
	
	public boolean isEmpty() {
		return displayList.isEmpty();
	}

	public void setOrderedLinkedDisplays(ArrayList<Display> newList) {
		clearLinkedDisplays();
		displayList.addAll(newList);
		update();
		registerListeners();
	}

	public void clearLinkedDisplays() {
		releaseListeners();
		displayList.clear();
	}

	@Override
	protected void registerListeners() {
		for (Display nodeDisplay : displayList ) nodeDisplay.addUpdateListener(this);
	}

	@Override
	protected void releaseListeners() {
		for (Display nodeDisplay : displayList ) nodeDisplay.removeUpdateListener(this);
	}


	//
	// Calcul du nouvel affichage
	//
	
	public void update() {

		Display firstDisplay=displayList.get(0);
		
		icon1 = (AttrIcon) firstDisplay.icon1.clone();
		icon2 = (AttrIcon) firstDisplay.icon2.clone();
		font = (AttrFont) firstDisplay.font.clone();
		frontColor = (AttrColor) firstDisplay.frontColor.clone();
		backColorTop = (AttrColor) firstDisplay.backColorTop.clone();
		backColorMiddle = (AttrColor) firstDisplay.backColorMiddle.clone();
		backColorBottom = (AttrColor) firstDisplay.backColorBottom.clone();
		height = (AttrHeight) firstDisplay.height.clone();
		
		for (int i=1; i<displayList.size(); i++ ) {
			Display nodeDisplay = displayList.get(i);
			icon1= (AttrIcon) Attr.getTopAttrClone(icon1, nodeDisplay.getIcon1()) ;
			icon2= (AttrIcon) Attr.getTopAttrClone(icon2, nodeDisplay.getIcon2()) ;
			font= (AttrFont) Attr.getTopAttrClone(font, nodeDisplay.getFont()) ;
			frontColor= (AttrColor) Attr.getTopAttrClone(frontColor, nodeDisplay.getFrontColor()) ;
			backColorTop= (AttrColor) Attr.getTopAttrClone(backColorTop, nodeDisplay.getBackColorTop()) ;
			backColorMiddle= (AttrColor) Attr.getTopAttrClone(backColorMiddle, nodeDisplay.getBackColorMiddle()) ;
			backColorBottom= (AttrColor) Attr.getTopAttrClone(backColorBottom, nodeDisplay.getBackColorBottom()) ;
			height= (AttrHeight) Attr.getTopAttrClone(height, nodeDisplay.getHeight()) ;
		}

		fireUpdateDisplayEvent();
	}

	//
	// Evolution d'un des display
	//
	
	@Override
	public void displayUpdated(Display.UpdateDisplayEvent evt) {
		update();
	}


	
}

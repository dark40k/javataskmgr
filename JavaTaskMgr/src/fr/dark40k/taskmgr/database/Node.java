package fr.dark40k.taskmgr.database;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.TreeSet;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.categories.Categories;
import fr.dark40k.taskmgr.database.categories.Categories.UpdateCategoryEvent;
import fr.dark40k.taskmgr.database.display.Display;
import fr.dark40k.taskmgr.database.display.Display.UpdateDisplayEvent;
import fr.dark40k.taskmgr.database.notes.Notes;
import fr.dark40k.taskmgr.database.display.DisplayGroup;
import fr.dark40k.taskmgr.database.display.DisplayNode;
import fr.dark40k.taskmgr.database.scriptmgr.NodeScript;
import fr.dark40k.util.FormattersXML;


public abstract class Node implements  Display.UpdateDisplayListener, Categories.UpdateCategoryListener {

	public final static String DEFAULT_NAME="Nouveau";
	public final static Boolean DEFAULT_READONLY=false;

	public final static String XMLTAG="";

	// -------------------------------------------------------------------------------------------
	//
	// Donnn�es du noeud
	//
	// -------------------------------------------------------------------------------------------

	private final TaskMgrDB dataBase;
	private int key = TaskMgrDB.NO_NODE_KEY;

	private int fatherKey = TaskMgrDB.NO_NODE_KEY;

	private String name;

	private LocalDateTime creationDateTime;
	private LocalDateTime modificationDateTime;

	private Boolean readOnly;

	private final Notes notes;

	private final Children children;

	private final DisplayNode display;

	protected final DisplayGroup effectiveDisplay;

	private final Categories categories;

	private final NodeScript nodeScript;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------

	protected Node(TaskMgrDB dataBase, int key, String name, Boolean readOnly, DisplayNode display){

		this.dataBase=dataBase;
		this.key=key;
		dataBase.setNode(key,this);

		children = new Children(this);
		effectiveDisplay = new DisplayGroup();
		categories=new Categories();

		this.name=name;

		this.readOnly=readOnly;
		this.display=display;

		this.creationDateTime=LocalDateTime.now();
		this.modificationDateTime=LocalDateTime.now();

		notes=new Notes(this);

		nodeScript = new NodeScript(this);

		display.addUpdateListener(this);
		categories.addUpdateListener(this);

	}

	protected Node(TaskMgrDB dataBase, String name, Boolean readOnly, DisplayNode display) {
		this(dataBase, dataBase.getNewKey(), name, readOnly, display);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Destructeur
	//
	// -------------------------------------------------------------------------------------------

	public void doBeforeDeleteNode() {}

	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	// force key + loads children only if rawImport
	protected Node(TaskMgrDB nodeDataBase, Element elt, boolean rawImportKeys, boolean importChildren) {

		this(nodeDataBase,
				(rawImportKeys) ? Integer.valueOf(elt.getAttributeValue("key")) : nodeDataBase.getNewKey() ,
				elt.getAttributeValue("name",DEFAULT_NAME),
				elt.getAttribute("readOnly")!=null,
				new DisplayNode(nodeDataBase, elt.getChild("display")));

		notes.importNotesFromXML(elt);

		creationDateTime = FormattersXML.parseLocalDateTime(elt, "creation", null);
		modificationDateTime = FormattersXML.parseLocalDateTime(elt, "modification", null);

		categories.removeUpdateListener(this);
		categories.importXMLData(elt);
		categories.addUpdateListener(this);

		nodeScript.importXMLData(elt);

		if (importChildren) {
			getChildren().importXML(elt);
		}

	}

	public Element saveToXML() {

		Element elt=new Element("node");

		elt.setAttribute("key",Integer.toString(key));
		elt.setAttribute("name",name);
		if (creationDateTime!=null) FormattersXML.formatAttribute(elt, "creation", creationDateTime);
		if (modificationDateTime!=null) FormattersXML.formatAttribute(elt, "modification", modificationDateTime);

		notes.addNotesToXML(elt);

		if (readOnly) elt.setAttribute("readOnly","1");

		elt.addContent(getDisplay().exportXML("display"));

		elt.addContent(categories.exportXML());

		elt.addContent(nodeScript.exportXML());

		getChildren().exportXML(elt);

		return elt;
	}

	public void reloadFromXML(Element elt) {

		name=elt.getAttributeValue("name",DEFAULT_NAME);
		notes.importNotesFromXML(elt);

		readOnly=elt.getAttribute("readOnly")!=null;
		creationDateTime = FormattersXML.parseLocalDateTime(elt, "creation", null);
		modificationDateTime = FormattersXML.parseLocalDateTime(elt, "modification", null);

		display.removeUpdateListener(this);
		display.importXMLData(elt.getChild("display"));
		display.addUpdateListener(this);

		categories.removeUpdateListener(this);
		categories.importXMLData(elt);
		categories.addUpdateListener(this);

		nodeScript.importXMLData(elt);

		clearEffectiveDisplay();

		dataBase.fireNodeContentChanged(this,this);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es de base
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return name + " [" + key + "]";
	}

	public static String toString(Node node) {
		if (node==null) return "null";
		return node.toString();
	}

	public TaskMgrDB getDataBase() {
		return dataBase;
	}

	public int getKey() {
		return key;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Getters
	//
	// -------------------------------------------------------------------------------------------

	public String getName() {
		return name;
	}

	public abstract String getStackedName(String separator);

	// Extraction recursive des noms jusqu'� la base qui n'est pas incluse dans le nom
	// leve une erreur si la racine n'est pas trouv�e.
	protected String getStackedName(String separator, int stackRootKey) {

		if (fatherKey==TaskMgrDB.NO_NODE_KEY) throw new RuntimeException("Root node not found key="+stackRootKey);

		if (fatherKey==stackRootKey)
			return getName();
		else
			return getFather().getStackedName(separator, stackRootKey)+separator+getName();

	}

	public Notes getNotes() {
		return notes;
	}

	public Boolean isReadOnly() {
		return readOnly;
	}

	public LocalDateTime getCreationDateTime() {
		return creationDateTime;
	}

	public LocalDateTime getModificationDateTime() {
		return modificationDateTime;
	}

	public NodeScript getScript() {
		return nodeScript;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Setters
	//
	// -------------------------------------------------------------------------------------------

	public void setName(String text) {
		checkReadOnlyBeforeWriting();
		if (name.equals(text)) return;
		name=text;
		updateModificationTime();
		dataBase.fireNodeContentChanged(this,this);
	}

	public void setReadOnly(Boolean newReadOnly) {
		if (this.readOnly==newReadOnly) return;
		this.readOnly=newReadOnly;
		updateModificationTime();
		dataBase.fireNodeContentChanged(this,this);
	}

	public void updateModificationTime() {
		modificationDateTime=LocalDateTime.now();
	}

	// -------------------------------------------------------------------------------------------
	//
	// Scripts
	//
	// -------------------------------------------------------------------------------------------

	public void runScripts() {

		nodeScript.run("updateNode");

		for (Node node : children) node.runScripts();

	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es d'affichage
	//
	// -------------------------------------------------------------------------------------------

	public DisplayNode getDisplay() {
		return display;
	}

	public Display getEffectiveDisplay() {
		if (effectiveDisplay.isEmpty()) updateEffectiveDisplay(true);
		return effectiveDisplay;
	}

	public void updateEffectiveDisplay(boolean forceComplete){

		if ( (effectiveDisplay.isEmpty()) || forceComplete) {

			clearEffectiveDisplay();

			effectiveDisplay.setOrderedLinkedDisplays(buildEffectiveDisplayList());

			effectiveDisplay.addUpdateListener(this);

		} else

			effectiveDisplay.update();

	}

	private void clearEffectiveDisplay() {

		effectiveDisplay.removeUpdateListener(this);
		effectiveDisplay.clearLinkedDisplays();

	}

	protected abstract ArrayList<Display> buildEffectiveDisplayList();

	// -------------------------------------------------------------------------------------------
	//
	// Gestion des categories
	//
	// -------------------------------------------------------------------------------------------

	public Categories getCategories() {
		return categories;
	}

	public TreeSet<String> getRecursiveCategoriesNames() {
		return getRecursiveCategoriesNames(false);
	}

	private TreeSet<String> getRecursiveCategoriesNames(boolean inheritableOnly) {
		TreeSet<String> listNames = new TreeSet<String>();

		for (String name : categories.listCategoriesNames(inheritableOnly))
			if (!listNames.contains(name))
				listNames.add(name);

		if (hasFather())
			for (String name : getFather().getRecursiveCategoriesNames(true))
				if (!listNames.contains(name))
					listNames.add(name);

		return listNames;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	public void rebuildLinks() {
		children.reassignFather();
	}

	public Children getChildren() {
		return children;
	}

	public boolean hasFather() {
		return fatherKey!=TaskMgrDB.NO_NODE_KEY;
	}

	public Node getFather() {
		if (!hasFather()) return null;
		return dataBase.getNode(fatherKey);
	}

	// reserved for NodeDataBase
	protected void setFatherKey(int fatherKey) {
		if (this.fatherKey!=TaskMgrDB.NO_NODE_KEY) throw new RuntimeException("Father is already set for node "+toString());
		this.fatherKey = fatherKey;
	}

	// reserved for NodeDataBase
	protected void clearFatherKey() {
		this.fatherKey = TaskMgrDB.NO_NODE_KEY;
	}

	public boolean isAncestorOf(Node node) {
		if (node == null) return false;
		if (node == this) return true;
		return isAncestorOf(node.getFather());
	}

	public abstract int addNewChild(Node elderBrother);

	public Node addChild(Node newChild, Node elderBrother) {
		if (!canAddChild(newChild.getClass())) throw new IllegalArgumentException("Wrong type of node sent " + newChild.getClass());
		return getChildren().addChild(newChild,elderBrother);
	}

	public abstract boolean canAddChild(Class<? extends Node> nodeClass);

	public ArrayList<Integer> getKeyAndChildrenKeys(boolean includingLinks) {
		return addKeyAndChildrenKeys(new ArrayList<Integer>(),includingLinks);
	}

	// only for internal use
	protected ArrayList<Integer> addKeyAndChildrenKeys(ArrayList<Integer> list, boolean includingLinks) {
		list.add(key);
		for (Node child : getChildren()) {
			child.addKeyAndChildrenKeys(list,includingLinks);
		}
		return list;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Listeners
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void displayUpdated(UpdateDisplayEvent evt) {

		if (evt.getSource() == display) {
			Node.this.updateModificationTime();
			dataBase.fireNodeContentChanged(Node.this,Node.this);
			return;
		}

		if (evt.getSource() == effectiveDisplay) {
			dataBase.fireNodeDisplayChanged(Node.this,Node.this);
			return;
		}

		throw new RuntimeException("Unknown display event origin");

	}

	@Override
	public void categoriesUpdated(UpdateCategoryEvent evt) {
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this, this);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Utilitaires
	//
	// -------------------------------------------------------------------------------------------

	protected void checkReadOnlyBeforeWriting() {
		if (isReadOnly()) throw new UnsupportedOperationException("Node is readOnly, Node="+key);
	}

	public Boolean isOrHasReadOnlyDescendant() {

		for (Node child : getChildren())
			if (child.isOrHasReadOnlyDescendant())
				return true;

		return readOnly;
	}

	//
	// Check depending nodes (used check before deleting a node)
	//
	// Search criterias :
	// => effective display is tracked by more than current node
	// => display is tracked by more than current node and associated effective display (if exists)
	//
	public Boolean hasDependingNodes() {

		for (Node child : getChildren())
			if (child.hasDependingNodes())
				return true;

		if (effectiveDisplay!=null)
			if (effectiveDisplay.listenersCount()>1)
				return true;

		if (display.listenersCount()>((effectiveDisplay!=null)?2:1))
			return true;

		return false;
	}

}

package fr.dark40k.taskmgr.database.interfaces;

public interface NodeDefaultTemplateStatusInterface {

	public int getChildDefaultStatusKey();
	
	public void setChildDefaultStatusKey(int childDefaultStatus);

	public int getChildDefaultTemplateKey();

	public void setChildDefaultTemplateKey(int childDefaultTemplate);
	
}

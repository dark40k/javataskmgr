package fr.dark40k.taskmgr.database.interfaces;

import fr.dark40k.taskmgr.database.Node;

public interface NodeLinkInterface {

	public int getLinkKey();
	public Node getLinkNode();
}

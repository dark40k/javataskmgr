package fr.dark40k.taskmgr.database.interfaces;

public interface NodeHiddenInterface {
	
	public Boolean isHidden();
	
	public void setHidden(Boolean val);

}

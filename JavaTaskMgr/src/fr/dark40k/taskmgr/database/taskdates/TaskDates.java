package fr.dark40k.taskmgr.database.taskdates;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.NodeTask;

public class TaskDates {

	private final TaskDateData expectedStartDate = new TaskDateData(this, "expectedStartDate");
	private final TaskDateData expectedEndDate = new TaskDateData(this, "expectedEndDate");
	private final TaskDateData effectiveStartDate = new TaskDateData(this, "effectiveStartDate");
	private final TaskDateData effectiveEndDate = new TaskDateData(this, "effectiveEndDate");

	final NodeTask nodeTask;
	
	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------
	
	public TaskDates(NodeTask nodeTask) {
		this.nodeTask=nodeTask;
	}
		
	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------
	
	public void exportXML(Element elt) {
		
		expectedStartDate.exportXML(elt);
		expectedEndDate.exportXML(elt);
		effectiveStartDate.exportXML(elt);
		effectiveEndDate.exportXML(elt);
		
	}

	public void importXMLData(Element elt) {
		
		expectedStartDate.importXML(elt);
		expectedEndDate.importXML(elt);
		effectiveStartDate.importXML(elt);
		effectiveEndDate.importXML(elt);
		
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Getters
	//
	// -------------------------------------------------------------------------------------------
	
	public TaskDateData getExpectedStart() {
		return expectedStartDate;
	}

	public TaskDateData getExpectedEnd() {
		return expectedEndDate;
	}

	public TaskDateData getEffectiveStart() {
		return effectiveStartDate;
	}

	public TaskDateData getEffectiveEnd() {
		return effectiveEndDate;
	}
		
}


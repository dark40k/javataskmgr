package fr.dark40k.taskmgr.database.taskdates;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.jdom2.Element;

import fr.dark40k.util.FormattersXML;

public class TaskDateData {

	private final TaskDates taskDates;

	private final String xmlAttribute;

	private LocalDate localDate;

	//
	// Constructeur
	//
	
	public TaskDateData(TaskDates taskDates, String xmlAttribute) {
		this.taskDates = taskDates;
		this.xmlAttribute = xmlAttribute;
	}

	//
	// Getters
	//
	
	public boolean exists() {
		return localDate!=null;
	}
	
	public LocalDate getLocalDate() {
		return localDate;
	}

	public Date getDate() {
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Teste s'il reste moins de days jours la localDate enregistr�e
	 * 
	 * @param days
	 *            nombre de jours restant (si negatif, nombre de jours de d�passement)
	 * @return true si le d�lai est inf�rieur ou �gal � days
	 */
	public boolean isPassed(long days) {
		if (localDate == null) return false;
		return (ChronoUnit.DAYS.between(LocalDate.now(), localDate) <= days);
	}

	//
	// Setters
	// 
	
	public void clearDate() {
		if (localDate == null) return;
		localDate = null;
		taskDates.nodeTask.updateModificationTime();
		taskDates.nodeTask.getDataBase().fireNodeContentChanged(this, taskDates.nodeTask);
	}

	public void setLocalDate(LocalDate newDate) {
		if ((localDate == null) ? (newDate == null) : (localDate.equals(newDate))) return;
		localDate = newDate;
		taskDates.nodeTask.updateModificationTime();
		taskDates.nodeTask.getDataBase().fireNodeContentChanged(this, taskDates.nodeTask);
	}

	public void setDate(Date date) {
		setLocalDate(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
	}

	//
	// Import / Export XML
	//
	
	public void exportXML(Element elt) {
		if (localDate == null) return;
		FormattersXML.formatAttribute(elt, xmlAttribute, localDate);
	}

	public void importXML(Element elt) {
		localDate = FormattersXML.parseLocalDate(elt, xmlAttribute,null);
	}

}
package fr.dark40k.taskmgr.database.datalinks;

import java.io.File;
import java.io.IOException;
import java.util.function.Predicate;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.taskmgr.database.nodeicons.DefaultIconEnum;
import fr.dark40k.taskmgr.util.OutlookConnector;

public enum DataLinkType {
	
	COMMAND("0", "Fichier executable / Commande",
			(data) -> execCommand("cmd /C \"\""+data+"\"\""),
			DefaultIconEnum.FILE16.key,
			DefaultIconEnum.FILE32.key
			),

	DIRECTORY("1", "Rep�rtoire / Adresse web",
			(data) -> execCommand("cmd /C explorer \""+data+"\""),
			DefaultIconEnum.DIRECTORY16.key,
			DefaultIconEnum.DIRECTORY32.key
			),

	OUTLOOKEMAIL("2", "Mail Outlook",
			(data) -> OutlookConnector.openMail(data),
			DefaultIconEnum.OUTLOOKEMAIL16.key,
			DefaultIconEnum.OUTLOOKEMAIL32.key
			),

	OUTLOOKFOLDER("3", "Repertoire Outlook",
			(data) -> OutlookConnector.openFolder(data),
			DefaultIconEnum.OUTLOOKDIRECTORY16.key,
			DefaultIconEnum.OUTLOOKDIRECTORY32.key
			),

	LOCALDIRECTORY("4", "Rep�rtoire local",
			(data) -> openFile(data),
			DefaultIconEnum.LOCALDIRECTORY16.key,
			DefaultIconEnum.LOCALDIRECTORY32.key
			),
	
	NETDIRECTORY("5", "Rep�rtoire reseau",
			(data) -> openFile(data),
			DefaultIconEnum.NETDIRECTORY16.key,
			DefaultIconEnum.NETDIRECTORY32.key
			),
	
	PDFFILE("6", "Fichier PDF",
			(data) -> execCommand("cmd /C \"\""+data+"\"\""),
			DefaultIconEnum.PDFFILE16.key,
			DefaultIconEnum.PDFFILE32.key
			);
	
	//
	// Code
	// 
	
	private final static Logger LOGGER = LogManager.getLogger();
	
	public final String codeXML;
	public final String name;
	private final Predicate<String> runPredicate;
	public final int icon16Key;
	public final int icon32Key;
	
	DataLinkType(String code, String name, Predicate<String> runConsumer, int icon16Key, int icon32Key ) {
		this.codeXML=code;
		this.name=name;
		this.runPredicate=runConsumer;
		this.icon16Key=icon16Key;
		this.icon32Key=icon32Key;
	}
	
	public static DataLinkType valueOfByName(String name) {
		for (DataLinkType type : values())
			if (type.name.equals(name)) return type;
		return COMMAND;
	}
	
	public static DataLinkType valueOfByCodeXML(String codeXML) {
		for (DataLinkType type : values())
			if (type.codeXML.equals(codeXML)) return type;
		return COMMAND;
	}
	
	public String toString() {
		return name;
	}

	void run(String data) {
		boolean success= runPredicate.test(data);

		if (!success)
			JOptionPane.showMessageDialog(null, "Echec de l'execution du lien.\ndata=\n"+data, "Warning", JOptionPane.ERROR_MESSAGE);
		
	}
	
	//
	// Outils publiques
	//
	
	static public DataLinkType getDefaultFileLinkType(File file) {
		
		if (file.isDirectory()) {
			if ((file.getAbsolutePath().matches("^([a-zA-Z]\\:\\\\.+)"))) 
				return DataLinkType.LOCALDIRECTORY;
			else 
				return DataLinkType.NETDIRECTORY;
		} else if (file.isFile()) {
			if (file.getName().toLowerCase().endsWith("pdf"))
				return DataLinkType.PDFFILE;
			else
				return DataLinkType.COMMAND;
		}
		
		return DataLinkType.COMMAND;
		
	}

	//
	// Outils priv�s
	//
	
	private static boolean openFile(String data) {
	
		if (!(new File(data)).exists()) return false;
		
		return execCommand("cmd /C explorer \""+data+"\"");
	}
	
	private static boolean execCommand(String cmd) {
		boolean success=false;
		LOGGER.info("Running ="+cmd);
		Runtime rt = Runtime.getRuntime();
		try {
			rt.exec(cmd);
			success=true;
		} catch (IOException e) {
			LOGGER.warn("Execution Failed.",e);
		}
		return success;
	}
	

}
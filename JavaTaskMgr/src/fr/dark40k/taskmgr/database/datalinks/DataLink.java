package fr.dark40k.taskmgr.database.datalinks;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.NodeTask;

public class DataLink {
	
	@SuppressWarnings("unused")
	private final static Logger LOGGER = LogManager.getLogger();

	public final static String XMLTAG="link";

	private String description="";
	private String data="";
	private DataLinkType type=DataLinkType.COMMAND;

	private Integer icon32Key = null;
	private Integer icon16Key = null;
	
	private final NodeTask node;
	
	// -------------------------------------------------------------------------------------------
	//
	// Constructor
	//
	// -------------------------------------------------------------------------------------------
	
	public DataLink(NodeTask node) {
		this.node=node;
	}
	
	public DataLink(NodeTask node, Element elt) {
		this.node=node;
		description = elt.getAttributeValue("description");
		data = elt.getAttributeValue("data");
		type = DataLinkType.valueOfByCodeXML(elt.getAttributeValue("type"));
		
		// Gestion ancien format.
		if (data == null) {
			data = elt.getAttributeValue("path");
			type=(elt.getAttributeValue("directory")!=null)?DataLinkType.DIRECTORY:DataLinkType.COMMAND;
		}
		
		icon16Key = (elt.getAttribute("icon16") != null) ? Integer.valueOf(elt.getAttributeValue("icon16")) : null;
		icon32Key = (elt.getAttribute("icon32") != null) ? Integer.valueOf(elt.getAttributeValue("icon32")) : null;
		
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------
	
	public DataLink(NodeTask node, String description, String data,DataLinkType type) {
		this.node=node;
		this.description=description;
		this.data=data;
		this.type=type;
	}

	public DataLink(NodeTask node, String description, File file) {
		this.node=node;
		this.description=description;
		this.data=file.getAbsolutePath();
		this.type=DataLinkType.getDefaultFileLinkType(file);
	}
	
	public Element exportXML() {
		Element elt=new Element(XMLTAG);
		elt.setAttribute("description", description);
		elt.setAttribute("data",data);
		elt.setAttribute("type",type.codeXML);
		if (icon16Key!=null) elt.setAttribute("icon16",Integer.toString(icon16Key));
		if (icon32Key!=null) elt.setAttribute("icon32",Integer.toString(icon32Key));
		return elt;
	}
		
	// -------------------------------------------------------------------------------------------
	//
	// Getters & Setters
	//
	// -------------------------------------------------------------------------------------------
	
	public NodeTask getNodeTask() {
		return node;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		if (this.description.equals(description)) return;
		this.description = description;
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		if (this.data.equals(data)) return;
		this.data = data;
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	
	public void setType(DataLinkType type) {
		if (this.type==type) return;
		this.type = type;
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	public DataLinkType getType() {
		return type;
	}

	public void run() {
		type.run(data);
	}
	
	public Integer getIcon16Key(boolean useDefault) {
		if ((icon16Key==null)&&(useDefault)) return type.icon16Key;
		return icon16Key;
	}

	public Integer getIcon32Key(boolean useDefault) {
		if ((icon32Key==null)&&(useDefault)) return type.icon32Key;
		return icon32Key;
	}

	public void setIcon32Key(Integer bigIconKey) {
		this.icon32Key = bigIconKey;
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	public void setIcon16Key(Integer icon16Key) {
		this.icon16Key = icon16Key;
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
		
}

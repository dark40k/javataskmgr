package fr.dark40k.taskmgr.database.datalinks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.NodeTask;

public class DataLinkList implements Iterable<DataLink> {
	
	private final static Logger LOGGER = LogManager.getLogger();

	public final static int MIN_FASTLINK_NBR = 4;
	
	public final static String FASTLINK_XMLTAG="fastlink";
	
	private final NodeTask node;
	
	private File pdfFile;
	private File localDirectory;
	private File netDirectory;

	private ArrayList<DataLink> fastLinks=new ArrayList<DataLink>();
	private ArrayList<DataLink> links=new ArrayList<DataLink>();

	// -------------------------------------------------------------------------------------------
	//
	// Constructeur
	//
	// -------------------------------------------------------------------------------------------
	
	public DataLinkList(NodeTask node) {
		this.node=node;
		while(fastLinks.size()<MIN_FASTLINK_NBR) fastLinks.add(null);
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------
	
	public void exportXML(Element elt) {
		
		for (DataLink link : links) {
			
			Element newElt = link.exportXML();

			int index = fastLinks.indexOf(link);
			if (index >= 0 )
				newElt.setAttribute("fastLink",Integer.toString(fastLinks.indexOf(link)));
			
			elt.addContent(newElt);
		}

		for (DataLink link : fastLinks) {
			
			Element newElt = new Element(FASTLINK_XMLTAG);
			
			if (link!=null)
				newElt.setAttribute("index",Integer.toString(links.indexOf(link)));
			else
				newElt.setAttribute("index","-1");
			
			elt.addContent(newElt);
			
		}
		
	}
	
	public void importXMLData(Element elt) {
		
		// Import des elements
		
		links.clear();
		for (Element linkElt : elt.getChildren(DataLink.XMLTAG)) {
			DataLink link = new DataLink(node,linkElt);
			links.add(link);
		}
		
		// Import des fastLinks
		
		fastLinks.clear();
		for (Element fastLinkElt : elt.getChildren(FASTLINK_XMLTAG)) {
			
			int index = Integer.valueOf(fastLinkElt.getAttributeValue("index"));
			
			if (index>=0)
				fastLinks.add(links.get(index));
			else
				fastLinks.add(null);
		}
		while(fastLinks.size()<MIN_FASTLINK_NBR) fastLinks.add(null);
		
		//
		// TODO A supprimer quand bases migr�es
		//
		
		String pdfFilePath = elt.getAttributeValue("pdfFile");
		if (pdfFilePath!=null) {
			
			LOGGER.info("node = "+node.getKey()+" Importing pdfFile link = "+pdfFilePath);
			
			DataLink foundLink1 = null;
			for (DataLink link : links)
				if (pdfFilePath.equals(link.getData()))
					foundLink1 = link;
					
			if (foundLink1 == null) {
				foundLink1 = new DataLink(node,(new File(pdfFilePath)).getName(), pdfFilePath, DataLinkType.PDFFILE);
				links.add(foundLink1);
			}
						
			setFastLinkRaw(0, foundLink1);
		}

		String localDirectoryPath = elt.getAttributeValue("localDirectory");
		if (localDirectoryPath!=null) {
			
			LOGGER.info("node = "+node.getKey()+" Importing localDirectory link = "+localDirectoryPath);
			
			DataLink foundLink2 = null;
			for (DataLink link : links)
				if (localDirectoryPath.equals(link.getData()))
					foundLink2 = link;
			
			if (foundLink2 == null) {
				foundLink2 = new DataLink(node,(new File(localDirectoryPath)).getName(), localDirectoryPath, DataLinkType.LOCALDIRECTORY);
				links.add(foundLink2);
			}
			
			setFastLinkRaw(1, foundLink2);
		}
		
		String netDirectoryPath = elt.getAttributeValue("netDirectory");
		if (netDirectoryPath!=null) {
			
			LOGGER.info("node = "+node.getKey()+" Importing netDirectory link = "+netDirectoryPath);

			DataLink foundLink3 = null;
			for (DataLink link : links)
				if (netDirectoryPath.equals(link.getData()))
					foundLink3 = link;
					
			if (foundLink3 == null) {
				foundLink3 = new DataLink(node,(new File(netDirectoryPath)).getName(), netDirectoryPath, DataLinkType.NETDIRECTORY);
				links.add(foundLink3);
			}
						
			setFastLinkRaw(2, foundLink3);
		}
		
		
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Getters & Setters
	//
	// -------------------------------------------------------------------------------------------

	public File getPdfFile() {
		return pdfFile;
	}

	public File getHeritedPdfFile() {
		if (pdfFile!=null) return pdfFile;
		if (!(node.getFather() instanceof NodeTask)) return null;
		return ((NodeTask)node.getFather()).getLinkList().getHeritedPdfFile();
	}
	
	public void setPdfFile(File pdfFile) {
		
		if (pdfFile == this.pdfFile) return;
		if (this.pdfFile != null) if (this.pdfFile.equals(pdfFile)) return;
		
		if (node.isReadOnly()) throw new UnsupportedOperationException("Node is readOnly, Node="+node.getKey()); 
		
		this.pdfFile = pdfFile;
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}

	public void runPdfFile(boolean includeHerited) {
		File file = (includeHerited)?getPdfFile():getHeritedPdfFile();
		if ((file==null)||(!file.exists())) return;
		execCommand("cmd /C \"\""+file.getPath()+"\"\"");
	}
	
	public File getLocalDirectory() {
		return localDirectory;
	}

	public File getHeritedLocalDirectory() {
		if (localDirectory!=null) return localDirectory;
		if (!(node.getFather() instanceof NodeTask)) return null;
		return ((NodeTask)node.getFather()).getLinkList().getHeritedLocalDirectory();
	}
	
	public void setLocalDirectory(File localDirectory) {
		
		if (localDirectory == this.localDirectory) return;
		if (this.localDirectory != null) if (this.localDirectory.equals(localDirectory)) return;
		
		if (node.isReadOnly()) throw new UnsupportedOperationException("Node is readOnly, Node="+node.getKey()); 
		
		this.localDirectory = localDirectory;
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}

	public void runLocalDirectory(boolean includeHerited) {
		File file = (includeHerited)?getLocalDirectory():getHeritedLocalDirectory();
		if ((file==null)||(!file.exists())) return;
		execCommand("cmd /C explorer \""+file.getPath()+"\"");
	}

	public File getNetDirectory() {
		return netDirectory;
	}

	public File getHeritedNetDirectory() {
		if (netDirectory!=null) return netDirectory;
		if (!(node.getFather() instanceof NodeTask)) return null;
		return ((NodeTask)node.getFather()).getLinkList().getHeritedNetDirectory();
	}
	
	public void setNetDirectory(File netDirectory) {
		
		if (netDirectory == this.netDirectory) return;
		if (this.netDirectory != null) if (this.netDirectory.equals(netDirectory)) return;
		
		if (node.isReadOnly()) throw new UnsupportedOperationException("Node is readOnly, Node="+node.getKey()); 
		
		this.netDirectory = netDirectory;
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}

	public void runNetDirectory(boolean includeHerited) {
		File file = (includeHerited)?getNetDirectory():getHeritedNetDirectory();
		if ((file==null)||(!file.exists())) return;
		execCommand("cmd /C explorer \""+file.getPath()+"\"");
	}


	// -------------------------------------------------------------------------------------------
	//
	// FastLinks access
	//
	// -------------------------------------------------------------------------------------------

	public int getHeritedFastLinkCount() {
		return 
				(node.getFather() instanceof NodeTask)
					? 
				Math.max(fastLinks.size(), ((NodeTask)node.getFather()).getLinkList().getHeritedFastLinkCount())
					: 
				fastLinks.size();
	}
	
	public DataLink getHeritedFastLink(int index) {
		
		if (index < fastLinks.size()) {
			DataLink link = fastLinks.get(index);
			if (link!=null) return link;
		}
		
		if (!(node.getFather() instanceof NodeTask)) return null;
				
		return ((NodeTask)node.getFather()).getLinkList().getHeritedFastLink(index);
		
	}
	
	public boolean isDirectFastLink(int index) {
		return (index < fastLinks.size()) && (fastLinks.get(index)!=null);
	}
	
	public int getFastLinkCount() {
		return fastLinks.size();
	}
	
	public DataLink getFastLink(int index) {
		if (index>=fastLinks.size()) return null;
		return fastLinks.get(index);
	}
	
	private void setFastLinkRaw(int index, DataLink link) {
		
		if ((link!=null)&&(!links.contains(link))) links.add(link);

		while (fastLinks.size()<=index) fastLinks.add(null);
		
		if (fastLinks.get(index)==link) return;

		int oldIndex = fastLinks.indexOf(link);
		if (oldIndex>=0) fastLinks.set(oldIndex, null);
		
		fastLinks.set(index, link);
		
	}
	
	public DataLink setFastLink(int index, DataLink link) {
		
		setFastLinkRaw(index, link);
		
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
		
		return link;
		
	}
	
	public int getFastLinkIndexOf(DataLink link) {
		return fastLinks.indexOf(link);
	}

	protected void clearAllFastLinkRaw(DataLink link) {
		for (int index = 0; index < fastLinks.size(); index++)
			if (fastLinks.get(index)==link)
				fastLinks.set(index,null);
	}
	
	public void clearAllFastLink(DataLink link) {
		clearAllFastLinkRaw(link);
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	public void addFastLink() {
		fastLinks.add(null);
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	public boolean removeFastLink() {
		if (fastLinks.size()<=MIN_FASTLINK_NBR) return false;
		fastLinks.remove(fastLinks.size()-1);
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
		return true;
	}

	public void moveUpFastLink(int index) {
		
		DataLink link = fastLinks.get(index);
		fastLinks.set(index, fastLinks.get(index-1));
		fastLinks.set(index-1, link);
		
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	public void moveDownFastLink(int index) {

		DataLink link = fastLinks.get(index);
		fastLinks.set(index, fastLinks.get(index+1));
		fastLinks.set(index+1, link);
		
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Links access
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Iterator<DataLink> iterator() {
		return links.iterator();
	}

	public DataLink addLink(DataLink newLink) {
		links.add(newLink);
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
		return newLink;
	}

	public int size() {
		return links.size();
	}

	public DataLink get(int index) {
		return links.get(index);
	}

	public void removeLink(int rowIndex) {

		clearAllFastLinkRaw(links.get(rowIndex));

		links.remove(rowIndex);
		
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	public void moveUp(int rowIndex) {
		DataLink link=links.get(rowIndex);
		links.set(rowIndex, links.get(rowIndex-1));
		links.set(rowIndex-1,link);
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}
	
	public void moveDown(int rowIndex) {
		DataLink link=links.get(rowIndex);
		links.set(rowIndex, links.get(rowIndex+1));
		links.set(rowIndex+1,link);
		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this, node);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Utilities
	//
	// -------------------------------------------------------------------------------------------

	private void execCommand(String cmd) {
		LOGGER.info("Running ="+cmd);
		Runtime rt = Runtime.getRuntime();
		try {
			rt.exec(cmd);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Could execute\n"+cmd, "Warning", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	
	
}

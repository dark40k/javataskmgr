package fr.dark40k.taskmgr.database;

import java.util.ArrayList;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.display.Display;
import fr.dark40k.taskmgr.database.display.DisplayNode;
import fr.dark40k.taskmgr.database.interfaces.NodeDefaultTemplateStatusInterface;
import fr.dark40k.taskmgr.database.interfaces.NodeHiddenInterface;

public class NodeTemplate extends Node implements NodeHiddenInterface, NodeDefaultTemplateStatusInterface {

	public final static String XMLTAG="nodeTemplate";

	public final static Boolean DEFAULT_HIDDEN = false;

	private Boolean	hidden;

	private int	childDefaultTemplateKey;

	private int	childDefaultStatusKey;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------

	public NodeTemplate(TaskMgrDB dataBase, int key, String name, Boolean readOnly, DisplayNode display, Boolean hidden, NodeTemplate childDefaultTemplate, NodeStatus childDefaultStatus) {
		super(dataBase, key, name, readOnly, display);
		this.hidden=hidden;
		this.childDefaultTemplateKey= (childDefaultTemplate!=null) ? childDefaultTemplate.getKey() : TaskMgrDB.NO_NODE_KEY;
		this.childDefaultStatusKey= (childDefaultStatus!=null) ? childDefaultStatus.getKey() : TaskMgrDB.NO_NODE_KEY;
	}

	public NodeTemplate(TaskMgrDB dataBase, String name, Boolean readOnly, DisplayNode display, Boolean hidden, NodeTemplate childDefaultTemplate, NodeStatus childDefaultStatus) {
		this(dataBase, dataBase.getNewKey(), name, readOnly, display, hidden, childDefaultTemplate, childDefaultStatus);
	}

	// loads children only if rawImport
	protected NodeTemplate(TaskMgrDB nodeDataBase, Element elt, boolean rawImportKeys, boolean importChildren) {
		super(nodeDataBase,elt,rawImportKeys,importChildren);
		hidden=elt.getAttribute("hidden")!=null;

		childDefaultTemplateKey=Integer.valueOf(elt.getAttributeValue("childDefTemp", Integer.toString(TaskMgrDB.NO_NODE_KEY)));
		if ((!getDataBase().checkNodeClass(childDefaultTemplateKey,NodeTemplate.class)) && !rawImportKeys) childDefaultTemplateKey=TaskMgrDB.NO_NODE_KEY;

		childDefaultStatusKey=Integer.valueOf(elt.getAttributeValue("childDefStat", Integer.toString(TaskMgrDB.NO_NODE_KEY)));
		if ((!getDataBase().checkNodeClass(childDefaultStatusKey,NodeStatus.class)) && !rawImportKeys) childDefaultTemplateKey=TaskMgrDB.NO_NODE_KEY;

	}

	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Element saveToXML() {
		Element elt = super.saveToXML().setName(XMLTAG);
		if (hidden) elt.setAttribute("hidden","1");
		elt.setAttribute("childDefTemp", Integer.toString(childDefaultTemplateKey));
		elt.setAttribute("childDefStat", Integer.toString(childDefaultStatusKey));
		return elt;
	}

	@Override
	public void reloadFromXML(Element elt) {
		hidden=elt.getAttribute("hidden")!=null;
		childDefaultTemplateKey=Integer.valueOf(elt.getAttributeValue("childDefTemp", Integer.toString(TaskMgrDB.NO_NODE_KEY)));
		childDefaultStatusKey=Integer.valueOf(elt.getAttributeValue("childDefStat", Integer.toString(TaskMgrDB.NO_NODE_KEY)));
		super.reloadFromXML(elt);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es particulieres
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String getStackedName(String separator){
		return getStackedName(separator,TaskMgrDB.ROOT_TEMPLATE_KEY);
	}

	@Override
	public Boolean isHidden() {
		return hidden;
	}

	@Override
	public void setHidden(Boolean val) {
		if (hidden==val) return;
		hidden=val;
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	@Override
	public int getChildDefaultTemplateKey() {
		return childDefaultTemplateKey;
	}

	@Override
	public void setChildDefaultTemplateKey(int childDefaultTemplate) {
		if (this.childDefaultTemplateKey==childDefaultTemplate) return;
		this.childDefaultTemplateKey=childDefaultTemplate;
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	@Override
	public int getChildDefaultStatusKey() {
		return childDefaultStatusKey;
	}

	@Override
	public void setChildDefaultStatusKey(int childDefaultStatus) {
		if (this.childDefaultStatusKey==childDefaultStatus) return;
		this.childDefaultStatusKey=childDefaultStatus;
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es d'affichage
	//
	// -------------------------------------------------------------------------------------------

	// Ordre de priorit� d'affichage en cas d'egalit� (ordre decroissant) :
	// 1 - definition locale
	// 2 - parents jusqu'au pere final
	@Override
	protected ArrayList<Display> buildEffectiveDisplayList() {
		ArrayList<Display> displayList= new ArrayList<Display>();
		displayList.add(getDisplay());
		int fkey=getDataBase().findFirstFatherKey(getKey());
		if (fkey>0)
			if (fkey!=TaskMgrDB.ROOT_TEMPLATE_KEY)
				displayList.add(getDataBase().getNode(fkey).getEffectiveDisplay());
		return displayList;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public int addNewChild(Node elderBrother) {
		NodeTemplate newNode = new NodeTemplate(getDataBase(), DEFAULT_NAME, DEFAULT_READONLY, new DisplayNode(), DEFAULT_HIDDEN, null,null);
		this.addChild(newNode,elderBrother);
		return newNode.getKey();
	}

	@Override
	public boolean canAddChild(Class<? extends Node> nodeClass) {
		return (NodeTemplate.class.isAssignableFrom(nodeClass));
	}



}

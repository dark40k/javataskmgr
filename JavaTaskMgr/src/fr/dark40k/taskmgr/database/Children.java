package fr.dark40k.taskmgr.database;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.interfaces.NodeLinkInterface;

public class Children implements Iterable<Node> {

	private final static Logger LOGGER = LogManager.getLogger();

	private final TaskMgrDB dataBase;
	private final Node node;

	private final ArrayList<Integer> childrenKeys = new ArrayList<Integer>();

	public Children(Node node) {
		this.node=node;
		this.dataBase=node.getDataBase();
	}

	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	public void importXML(Element elt) {
		List<Element> listChildren = elt.getChildren("child");
		Iterator<Element> i = listChildren.iterator();
		while(i.hasNext())
		{
			Element courant = i.next();
			int key = Integer.valueOf(courant.getAttributeValue("key"));
			childrenKeys.add(key);
		}
	}

	public void exportXML(Element elt) {
		for (Integer childKey : this.childrenKeys) {
			Element newElt = new Element("child");
			newElt.setAttribute("key",childKey.toString());
			elt.addContent(newElt);
		}
	}

	public void reassignFather() {
		for (Node child : this) {
			child.setFatherKey(node.getKey());
		}
	}

	// -------------------------------------------------------------------------------------------
	//
	// Iterateur sur les enfants
	//
	// -------------------------------------------------------------------------------------------
	@Override
	public Iterator<Node> iterator() {
		return new Iterator<Node>() {
			Iterator<Integer> keyIterator = childrenKeys.iterator();
			@Override
			public boolean hasNext() {
				return keyIterator.hasNext();
			}
			@Override
			public Node next() {
				return dataBase.getNode(keyIterator.next());
			}
			@Override
			public void remove() {
				keyIterator.remove();
			}
		};
	}

	// -------------------------------------------------------------------------------------------
	//
	// Informations sur les enfants
	//
	// -------------------------------------------------------------------------------------------

	public Node getChild(int index) {
		if (index<childrenKeys.size()) return dataBase.getNode(childrenKeys.get(index));
		return null;
	}

	public int getChildCount() {
		return childrenKeys.size();
	}

	public int getIndexOfChild(Node child) {
		if (child == null) return -1;
		return childrenKeys.indexOf(child.getKey());
	}

	public boolean containsNodeKey(int key) {
		return childrenKeys.contains(key);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	protected void addChildRaw(Node newChild, Node elderBrother) {

		// check if new child is valid
		if (newChild==null) throw new RuntimeException("Empty child");
		if (newChild.getKey()==TaskMgrDB.NO_NODE_KEY) throw new RuntimeException("Empty child key for "+newChild);

		// calculate upper brother position
		int index = -1;
		if (elderBrother!=null) {
			index = childrenKeys.indexOf(elderBrother.getKey());
		}

		// insert child (last position if null)
		if (index<0)
			childrenKeys.add(0,newChild.getKey());
		else
			childrenKeys.add(index+1, newChild.getKey());

		// set father
		newChild.setFatherKey(node.getKey());

	}


	public Node addChild(Node newChild, Node elderBrother) {

		addChildRaw(newChild, elderBrother);

		// fire structure update
		dataBase.fireNodeStructureChanged(this,node);

		return newChild;
	}

	// supprime un enfant sans l'enlever de la base
	protected void removeChild(Node childNode) {
		if (childNode.isReadOnly()) throw new UnsupportedOperationException("deleteChild Error - Node is readOnly, Node="+node.getKey());
		childrenKeys.remove(Integer.valueOf(childNode.getKey()));
		childNode.clearFatherKey();
	}

	// return false if a problem (dependant node) was found.
	public boolean deleteChild(Node childNode) {

		LOGGER.info("Deleting Node = " + childNode.toString());

		// teste s'il s'agit d'un lien ou d'un veritable enfant
		if (!(childNode instanceof NodeLinkInterface)) {

			// verifie si le noeud n'est pas bloqu� en ecriture
			if (childNode.isReadOnly()) throw new UnsupportedOperationException("deleteChild Error - Node is readOnly, Node="+node.getKey());

			// supprime tous les liens dans la base vers le noeud
			ArrayDeque<Node> linksToDelete = new ArrayDeque<Node>();
			for (Node linkNode : dataBase)
				if (linkNode instanceof NodeLinkInterface)
					if (((NodeLinkInterface)linkNode).getLinkKey()==childNode.getKey())
						linksToDelete.push(linkNode);
			while (!linksToDelete.isEmpty()) {
				Node nextLinkToDelete = linksToDelete.pop();
				if (!nextLinkToDelete.getFather().getChildren().deleteChild(nextLinkToDelete)) return false;
			}

			// recursively delete sub-children first
			while (childNode.getChildren().getChildCount()>0) {
				Node subChildNode = childNode.getChildren().getChild(0);
//				LOGGER.info(" => Deleting Child = " + subChildNode.toString());
				if (!childNode.getChildren().deleteChild(subChildNode)) return false;
			}

			//release listeners of childNode
			if (childNode.effectiveDisplay!=null) {
				childNode.effectiveDisplay.removeUpdateListener(childNode);
				childNode.effectiveDisplay.clearLinkedDisplays();
			}
			childNode.getDisplay().removeUpdateListener(childNode);

		}

		// remove node from dataBase
		childNode.doBeforeDeleteNode();
		childrenKeys.remove(Integer.valueOf(childNode.getKey()));
		dataBase.delNode(childNode.getKey());

		// fire info to listenners
		dataBase.fireNodeStructureChanged(this,node);

		//LOGGER.info(" => Father= " + this.name + " (" + this.key + ")");
		//LOGGER.info(" => Done");

		return true;
	}

	public void moveUpChild(Node child) {

		if (childrenKeys.size()<=1) return;

		int pos=childrenKeys.indexOf(child.getKey());

		if (pos<=0) return;

		childrenKeys.set(pos, childrenKeys.get(pos-1));
		childrenKeys.set(pos-1, child.getKey());

		dataBase.fireNodeStructureChanged(this, node);
	}

	public void moveDownChild(Node child) {

		if (childrenKeys.size()<=1) return;
		int pos=childrenKeys.indexOf(child.getKey());

		if (pos<0) return;
		if (pos==childrenKeys.size()-1) return;

		childrenKeys.set(pos, childrenKeys.get(pos+1));
		childrenKeys.set(pos+1, child.getKey());

		dataBase.fireNodeStructureChanged(this, node);
	}

	public static void swapChildren(Node child1, Node child2) {

		if (child1==null) return;
		Node father1 = child1.getFather();
		if (father1==null) return;
		int index1 = father1.getChildren().getIndexOfChild(child1);

		if (child2==null) return;
		Node father2 = child2.getFather();
		if (father2==null) return;
		int index2 = father2.getChildren().getIndexOfChild(child2);

		if (father1.getChildren().dataBase != father2.getChildren().dataBase) throw new RuntimeException("Swapping between different databases");

		child1.clearFatherKey();
		child1.setFatherKey(father2.getKey());

		child2.clearFatherKey();
		child2.setFatherKey(father1.getKey());

		father1.getChildren().childrenKeys.set(index1,child2.getKey());
		father2.getChildren().childrenKeys.set(index2,child1.getKey());

		father1.getChildren().dataBase.fireDataBaseChanged(father1);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Utilitaires
	//
	// -------------------------------------------------------------------------------------------

	public boolean checkCircularReference(Node nodeToAdd) {

		HashSet<Integer> listHeirs=buildAllLinkedHeirs(new HashSet<Integer>(), nodeToAdd);

		Node father=node;

		while (father!=null) {
			if (father.getKey()==nodeToAdd.getKey()) return true;
			if (listHeirs.contains(father.getKey())) return true;
			if (father instanceof NodeLinkInterface)
				if (listHeirs.contains(((NodeLinkInterface)father).getLinkKey()))
					return true;
			father=father.getFather();
		}
		return false;
	}

	private HashSet<Integer> buildAllLinkedHeirs(HashSet<Integer> list, Node nodeToAdd) {
		if (nodeToAdd instanceof NodeLinkInterface) list.add(((NodeLinkInterface)nodeToAdd).getLinkKey());
		for (Node nodeToAddChild : nodeToAdd.getChildren()) buildAllLinkedHeirs(list,nodeToAddChild);
		return list;
	}

}

package fr.dark40k.taskmgr.database;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.TreeSet;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeEvent;
import fr.dark40k.taskmgr.database.TaskMgrDB.UpdateNodeListener;
import fr.dark40k.taskmgr.database.categories.Categories;
import fr.dark40k.taskmgr.database.datalinks.DataLinkList;
import fr.dark40k.taskmgr.database.display.Display;
import fr.dark40k.taskmgr.database.display.Display.UpdateDisplayEvent;
import fr.dark40k.taskmgr.database.display.DisplayNode;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlotList;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlotList.UpdateEvent;
import fr.dark40k.taskmgr.database.interfaces.NodeLinkInterface;
import fr.dark40k.taskmgr.database.notes.Notes;
import fr.dark40k.taskmgr.database.taskdates.TaskDates;

public class NodeTaskLink extends NodeTask implements NodeLinkInterface, UpdateNodeListener {

	public class BrokenLinkException extends Exception {
		public BrokenLinkException(String message) {
	        super(message);
	    }
	};

	public final static String XMLTAG="nodeLink";

	private NodeTask nodeTaskLink;
	private int nodeTaskLinkKey=TaskMgrDB.NO_NODE_KEY;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------

	public NodeTaskLink(TaskMgrDB dataBase, int key, NodeTask linkedNodeTask) {
		super(dataBase, key, "Link", false, new DisplayNode(),TaskMgrDB.NO_NODE_KEY,TaskMgrDB.NO_NODE_KEY);
		this.nodeTaskLink=linkedNodeTask;
		this.nodeTaskLinkKey=linkedNodeTask.getKey();
		dataBase.addUpdateListener(this);
	}

	public NodeTaskLink(NodeTask linkedNodeTask) {
		this(linkedNodeTask.getDataBase(), linkedNodeTask.getDataBase().getNewKey(), linkedNodeTask);
	}

	protected NodeTaskLink(TaskMgrDB nodeDataBase, Element elt, boolean rawImportKeys) throws BrokenLinkException {
		super(nodeDataBase,elt,rawImportKeys,false); //never import children
		nodeTaskLinkKey=Integer.valueOf(elt.getAttributeValue("linkKey"));
		if (!rawImportKeys)
			if ( (nodeDataBase.getNode(nodeTaskLinkKey)==null) || !(nodeDataBase.getNode(nodeTaskLinkKey) instanceof NodeTask))
				throw new BrokenLinkException("Node key not found ="+nodeTaskLinkKey);
		nodeTaskLink=(NodeTask) nodeDataBase.getNode(nodeTaskLinkKey);
		nodeDataBase.addUpdateListener(this);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Link specific methods
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public int getLinkKey() {
		return nodeTaskLinkKey;
	}

	@Override
	public NodeTask getLinkNode() {
		return nodeTaskLink;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Link specific methods
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void updateDataBase(UpdateNodeEvent evt) {}

	@Override
	public void updateNodeContent(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==nodeTaskLinkKey)
			getDataBase().fireNodeContentChanged(evt.getSource(), this);
	}

	@Override
	public void updateNodeDisplay(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==nodeTaskLinkKey)
			getDataBase().fireNodeDisplayChanged(evt.getSource(), this);
	}

	@Override
	public void updateNodeStructure(UpdateNodeEvent evt) {
		if (evt.getNode().getKey()==nodeTaskLinkKey)
			getDataBase().fireNodeStructureChanged(evt.getSource(), this);
	}



	// -------------------------------------------------------------------------------------------
	//
	// NodeTask Delegates - Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Element saveToXML() {
		Element elt=nodeTaskLink.saveToXML().setName(XMLTAG);
		elt.setAttribute("key",Integer.toString(getKey()));
		elt.setAttribute("linkKey", Integer.toString(nodeTaskLinkKey));
		return elt;
	}

	@Override
	public void reloadFromXML(Element elt) {
		nodeTaskLinkKey=Integer.valueOf(elt.getAttributeValue("linkKey"));
		super.reloadFromXML(elt);
	}

	// -------------------------------------------------------------------------------------------
	//
	// NodeTask Delegates - Donn�es particulieres
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String getStackedName(String separator) {
		return nodeTaskLink.getStackedName(separator);
	}

	@Override
	public NodeTemplate getTemplate() {
		return nodeTaskLink.getTemplate();
	}

	@Override
	public int getTemplateKey() {
		return nodeTaskLink.getTemplateKey();
	}

	@Override
	public void setTemplateKey(int key) {
		nodeTaskLink.setTemplateKey(key);
	}

	@Override
	public NodeStatus getStatus() {
		return nodeTaskLink.getStatus();
	}

	@Override
	public int getStatusKey() {
		return nodeTaskLink.getStatusKey();
	}

	@Override
	public void setStatusKey(int newKey) {
		nodeTaskLink.setStatusKey(newKey);
	}

	@Override
	public DataLinkList getLinkList() {
		return nodeTaskLink.getLinkList();
	}

	@Override
	public TaskDates getDates() {
		return nodeTaskLink.getDates();
	}

	@Override
	public ExecutionSlotList getExecutionSlotList() {
		return nodeTaskLink.getExecutionSlotList();
	}

	@Override
	public int getChildDefaultTemplateKey() {
		return nodeTaskLink.getChildDefaultTemplateKey();
	}

	@Override
	public void setChildDefaultTemplateKey(int childDefaultTemplate) {
		nodeTaskLink.setChildDefaultTemplateKey(childDefaultTemplate);
	}

	@Override
	public int getChildDefaultStatusKey() {
		return nodeTaskLink.getChildDefaultStatusKey();
	}

	@Override
	public void setChildDefaultStatusKey(int childDefaultStatus) {
		nodeTaskLink.setChildDefaultStatusKey(childDefaultStatus);
	}


	// -------------------------------------------------------------------------------------------
	//
	// NodeTask Delegates - Listeners
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void executionSlotListUpdated(UpdateEvent evt) {
		nodeTaskLink.executionSlotListUpdated(evt);
	}

	@Override
	public void executionSlotUpdated(UpdateEvent evt) {
		nodeTaskLink.executionSlotUpdated(evt);
	}

	// -------------------------------------------------------------------------------------------
	//
	// NodeTask Delegates - Donn�es d'affichage
	//
	// -------------------------------------------------------------------------------------------

	// protected ArrayList<Display> buildEffectiveDisplayList()

	// -------------------------------------------------------------------------------------------
	//
	// NodeTask Delegates - Gestion des categories
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public TreeSet<String> getRecursiveCategoriesNames() {
		return nodeTaskLink.getRecursiveCategoriesNames();
	}

	// -------------------------------------------------------------------------------------------
	//
	// NodeTask Delegates - Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public int addNewChild(Node elderBrother) {
		return nodeTaskLink.addNewChild(elderBrother);
	}

	@Override
	public boolean canAddChild(Class<? extends Node> nodeClass) {
		return nodeTaskLink.canAddChild(nodeClass);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Destructeur
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void doBeforeDeleteNode() {
		getDataBase().removeUpdateListener(this);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	// public Element saveToXML()

	// public void reloadFromXML(Element elt)

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Donn�es de base
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return " [" + super.getKey() + "] =>" + Integer.toString(nodeTaskLinkKey) + " = " + Node.toString(nodeTaskLink);
	}

	// public TaskMgrDB getDataBase()

	// public int getKey()

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Getters
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String getName() {
		return nodeTaskLink.getName();
	}

	// public abstract String getStackedName(String separator);

	// protected String getStackedName()

	@Override
	public Notes getNotes() {
		return nodeTaskLink.getNotes();
	}

	@Override
	public Boolean isReadOnly() {
		return nodeTaskLink.isReadOnly();
	}

	@Override
	public LocalDateTime getCreationDateTime() {
		return nodeTaskLink.getCreationDateTime();
	}

	@Override
	public LocalDateTime getModificationDateTime() {
		return nodeTaskLink.getModificationDateTime();
	}

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Setters
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void setName(String text) {
		nodeTaskLink.setName(text);
	}

	@Override
	public void setReadOnly(Boolean newReadOnly) {
		nodeTaskLink.setReadOnly(newReadOnly);
	}

	@Override
	public void updateModificationTime() {
		throw new RuntimeException("Not supported, should not be called.");
	}

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Donn�es d'affichage
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public DisplayNode getDisplay() {
		return nodeTaskLink.getDisplay();
	}

	@Override
	public Display getEffectiveDisplay() {
		return nodeTaskLink.getEffectiveDisplay();
	}

	@Override
	public void updateEffectiveDisplay(boolean forceComplete) {
		throw new RuntimeException("Not supported, should not be called.");
	}

	@Override
	protected ArrayList<Display> buildEffectiveDisplayList() {
		throw new RuntimeException("Not supported, should not be called.");
	}

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Gestion des categories
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Categories getCategories() {
		return nodeTaskLink.getCategories();
	}

	// public TreeSet<String> getRecursiveCategoriesNames()

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void rebuildLinks() {
		nodeTaskLink=(NodeTask) getDataBase().getNode(nodeTaskLinkKey);
		if (nodeTaskLink==null) throw new RuntimeException("Lien key=" + nodeTaskLinkKey + " bris� pour noeud=" + super.getKey());
	}

	@Override
	public Children getChildren() {
		return nodeTaskLink.getChildren();
	}

	//public boolean hasFather()

	//public Node getFather()

	//protected void setFatherKey(int fatherKey)

	//protected void clearFatherKey()

	@Override
	public boolean isAncestorOf(Node node) {
		return nodeTaskLink.isAncestorOf(node);
	}

	//public abstract int addNewChild(Node elderBrother);

	@Override
	public Node addChild(Node node,Node elderBrother) {
		return nodeTaskLink.addChild(node,elderBrother);
	}

	//public abstract boolean canAddChild(Class<? extends Node> nodeClass);

	//public ArrayList<Integer> getKeyAndChildrenKeys(boolean includingLinks)

	@Override
	protected ArrayList<Integer> addKeyAndChildrenKeys(ArrayList<Integer> list, boolean includingLinks) {
		if (includingLinks) list.add(nodeTaskLinkKey);
		super.addKeyAndChildrenKeys(list,includingLinks);
		return list;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Listeners
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public void displayUpdated(UpdateDisplayEvent evt) {
		throw new RuntimeException("Not supported, should not be called.");
	}

	@Override
	public void categoriesUpdated(fr.dark40k.taskmgr.database.categories.Categories.UpdateCategoryEvent evt) {
		throw new RuntimeException("Not supported, should not be called.");
	}

	// -------------------------------------------------------------------------------------------
	//
	// Node Delegates - Utilitaires
	//
	// -------------------------------------------------------------------------------------------

	@Override
	protected void checkReadOnlyBeforeWriting() {
		nodeTaskLink.checkReadOnlyBeforeWriting();
	}

	@Override
	public Boolean isOrHasReadOnlyDescendant() {
		return nodeTaskLink.isOrHasReadOnlyDescendant();
	}

	@Override
	public Boolean hasDependingNodes() {
		return nodeTaskLink.hasDependingNodes();
	}

	// -------------------------------------------------------------------------------------------
	//
	// Object Delegates
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public int hashCode() {
		return nodeTaskLink.hashCode();
	}


}

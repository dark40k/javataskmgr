package fr.dark40k.taskmgr.database.categories;

import java.util.EventListener;
import java.util.EventObject;

import javax.swing.event.EventListenerList;

import org.jdom2.Element;

public class Category implements Cloneable {

	public final static String XMLTAG="category";
	
	private static final String DEFAULT_NAME = "New category";
	private static final Boolean DEFAULT_INHERIT = false;
	
	public String name;
	public Boolean inheritable;
	
	public Category() {
		name = DEFAULT_NAME;
		inheritable=DEFAULT_INHERIT;
	}
	
	public Category(String name, Boolean status) {
		if (name==null) throw new NullPointerException("Name ne peux pas etre null.");
		this.name=name;
		this.inheritable=status;
	}

	public Category(Element elt) {
		name=elt.getAttributeValue("name",DEFAULT_NAME);
		inheritable=elt.getAttribute("inheritable")!=null;
	}
	
	public static int compare(Category cat1, Category cat2) {
		return cat1.name.compareTo(cat2.name);
	}
	
	public Element exportXML() {
		Element elt=new Element(XMLTAG);
		elt.setAttribute("name", name);
		if (inheritable) elt.setAttribute("inheritable","1");
		return elt;
	}

	public Category clone() {
		
		Category catClone;

		try {
			catClone = (Category) super.clone();
		} catch(CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
		
		return catClone;
	}

	public String toString() {
		return name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name.equals(this.name)) return;
		this.name = name;
		fireUpdateEvent();
	}

	/**
	 * @return the isActive
	 */
	public Boolean getInheritable() {
		return inheritable;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setInheritable(Boolean isActive) {
		if (this.inheritable==isActive) return;
		this.inheritable = isActive;
		fireUpdateEvent();
	}

	
	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class UpdateEvent extends EventObject {
		public UpdateEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateListener extends EventListener {
		public void categoryUpdated(UpdateEvent evt);
	}

	public void addUpdateListener(UpdateListener listener) {
		listenerList.add(UpdateListener.class, listener);
	}
	
	public void removeUpdateListener(UpdateListener listener) {
		listenerList.remove(UpdateListener.class, listener);
	}
	
	public void fireUpdateEvent() {
		UpdateEvent evt=new UpdateEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateListener.class) {
				((UpdateListener) listeners[i+1]).categoryUpdated(evt);
			}
		}
	}
	
}

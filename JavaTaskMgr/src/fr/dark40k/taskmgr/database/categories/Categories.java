package fr.dark40k.taskmgr.database.categories;

import java.util.EventListener;
import java.util.EventObject;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.event.EventListenerList;

import org.jdom2.Element;

public class Categories implements Cloneable, Category.UpdateListener, Iterable<Category>  {

	public final static String XMLTAG="categories";
	
	private Vector<Category> list = new Vector<Category>();

	
	//
	// Getters
	// 
	
	public int getCount() {
		return list.size();
	}

	public Category getCategory(int index) {
		return list.get(index);
	}
	
	public Category getCategory(String name) {
		if (name==null) return null;
		
		for (Category cat : list)
			if (cat.name.equals(name))
				return cat;
		
		return null;
	}
	
	@Override
	public Iterator<Category> iterator() {
		return list.iterator();
	}

	public TreeSet<String> listCategoriesNames(boolean inheritableOnly) {
		TreeSet<String> listNames = new TreeSet<String>();
		for (Category cat : list)
			if (!inheritableOnly || cat.inheritable)
				listNames.add(cat.name);
		return listNames;
	}

	public String toString() {
		String ret="";
		for (Category cat : list)
			ret=ret+" , "+cat+((cat.inheritable)?"*":"");
		return "[ "+((ret.length()>0)?ret.substring(3):"")+" ]";
	}
	 

	//
	// Setters et modifications
	//
	
	public Category setCategory(String name, boolean inheritable) {

		Category cat =getCategory(name);
		if (cat!=null) {
			cat.setInheritable(inheritable);
			return cat;
		}
		
		cat = new Category(name,inheritable);
		
		list.add(cat);
		
		cat.addUpdateListener(this);

		fireUpdateEvent();
		
		return cat;
		
	}
	
	public Category insertNewCategory(int index) {
		
		Category cat = new Category();
		
		if ((index<0) || (index>list.size()))
			list.add(cat);
		else
			list.add(index,cat);
		
		cat.addUpdateListener(this);
		
		fireUpdateEvent();
		
		return cat;
	}

	public boolean removeCategory(int index) {
		if ((index<0) || (index>list.size())) return false;
		list.remove(index);
		fireUpdateEvent();
		return true;
	}

	public boolean removeCategory(String name) {
		for (int index=0; index<list.size(); index++) 
			if (list.get(index).name.equals(name)) {
				list.remove(index);
				fireUpdateEvent();
				return true;
			}
		return false;
	}

	public boolean moveUp(int index) {
		if ((index<1) || (index>=list.size())) return false;
		Category cat=list.get(index);
		list.set(index, list.get(index-1));
		list.set(index-1,cat);
		fireUpdateEvent();
		return true;
	}
	
	public boolean moveDown(int index) {
		if ((index<0) || (index>=list.size()-1)) return false;
		Category cat=list.get(index);
		list.set(index, list.get(index+1));
		list.set(index+1,cat);
		fireUpdateEvent();
		return true;
	}
	
	
	//
	// Import / Export XML
	// 
	
	public Element exportXML() {
		Element elt=new Element(XMLTAG);
		for (Category cat : list) {
			elt.addContent(cat.exportXML());
		}
		return elt;
	}

	public void importXMLData(Element elt) {

		list.clear();
		
		Element categories = elt.getChild(XMLTAG);
		
		if (categories==null) return;
		
		for (Element subElt : categories.getChildren()) {
			Category cat = new Category(subElt); 
			list.add(cat);
			cat.addUpdateListener(this);
		}
		
		fireUpdateEvent();
		
	}

	//
	// Autres
	//
	
	public Categories clone() {
		
		Categories catClone;

		try {
			catClone = (Categories) super.clone();
		} catch(CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
		
		list = new Vector<Category>();
		
		for (Category cat : list ) {
			Category newCat = cat.clone();
			catClone.list.add(newCat);
			newCat.addUpdateListener(catClone);
		}
		
		return null;
		
	}
	
	
	//
	// Listeners
	//
	
	@Override
	public void categoryUpdated(Category.UpdateEvent evt) {
		fireUpdateEvent();
	}

	//
	// Change event
	//
	
	private final EventListenerList listenerList = new EventListenerList();

	public class UpdateCategoryEvent extends EventObject {
		public UpdateCategoryEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateCategoryListener extends EventListener {
		public void categoriesUpdated(UpdateCategoryEvent evt);
	}

	public void addUpdateListener(UpdateCategoryListener listener) {
		listenerList.add(UpdateCategoryListener.class, listener);
	}
	
	public void removeUpdateListener(UpdateCategoryListener listener) {
		listenerList.remove(UpdateCategoryListener.class, listener);
	}
	
	public void fireUpdateEvent() {
		UpdateCategoryEvent evt=new UpdateCategoryEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateCategoryListener.class) {
				((UpdateCategoryListener) listeners[i+1]).categoriesUpdated(evt);
			}
		}
	}



}

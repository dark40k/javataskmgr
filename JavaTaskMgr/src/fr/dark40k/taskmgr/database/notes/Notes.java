package fr.dark40k.taskmgr.database.notes;

import java.util.ArrayList;
import java.util.Iterator;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;

public class Notes implements Iterable<Notes.Note> {

	public static final String DEFAULT_NOTE_NAME = "Note";
	public static final String EMPTY_NOTE_CONTENT = "";

	final Node node;

	Note mainNote;
	final ArrayList<Note> notesList;

	//
	// Constructeur
	//
	public Notes(Node node) {
		this.node=node;
		notesList = new ArrayList<Note>();
		notesList.add(new Note(DEFAULT_NOTE_NAME,EMPTY_NOTE_CONTENT));
		mainNote = notesList.get(0);
	}

	//
	// Import / Export XML
	//

	public void importNotesFromXML(Element elt) {

		// reinitialise les donn�es
		notesList.clear();
		mainNote=null;

		// recuperation de l'ancien format XML ou les notes etaient sous le champ "description"
		Element  descElt=elt.getChild("description");
		if (descElt!=null)
			notesList.add(new Note(DEFAULT_NOTE_NAME, descElt.getText()));

		// recuperation des notes
		for (Element noteElt : elt.getChildren("note")) {
			Note newNote = new Note(noteElt.getAttributeValue("name",DEFAULT_NOTE_NAME), noteElt.getText());
			notesList.add(newNote);
			if (noteElt.getAttributeValue("main","0").equals("1")) mainNote = newNote;
		}

		// s'il n'y a rien cree une note vide
		if  (notesList.size()==0)
			notesList.add(new Note(DEFAULT_NOTE_NAME,EMPTY_NOTE_CONTENT));

		// rempli mainNote si pas d�fini
		if (mainNote==null) mainNote = notesList.get(0);

	}

	public void addNotesToXML(Element elt) {
		for (Note note : notesList) {
			Element subElt = new Element("note");
			subElt.setAttribute("name", note.name);
			if (note==mainNote) subElt.setAttribute("main","1");
			subElt.setText(note.content);
			elt.addContent(subElt);
		}
	}

	//
	// Getters/Setters
	//

	public boolean isEmpty() {
		if (notesList.size()>1) return false;
		if (notesList.get(0).content.length()>0) return false;
		return true;
	}

	@Override
	public Iterator<Note> iterator() {
		return notesList.iterator();
	}

	public Note getNote(int index) {
		return notesList.get(index);
	}

	public Note getFirstNote(String newName) {
		for (Note note : notesList) if (note.name.equals(newName)) return note;
		return null;
	}

	public void remove(Note note) {

		notesList.remove(note);

		if (notesList.size()==0) notesList.add(new Note(DEFAULT_NOTE_NAME,EMPTY_NOTE_CONTENT));

		if (mainNote==note) mainNote=notesList.get(0);

		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(note,node);
	}

	public Note getMainNote() {
		return mainNote;
	}

    /**
     * Inserts the specified element at the specified position in this
     * list. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (adds one to their indices).
     *
     * Inserts at the end if index<0
     *
     * @param index index at which the specified element is to be inserted
     */
	public Note addNewNote(int index) {
		return addNewNote(index, DEFAULT_NOTE_NAME,EMPTY_NOTE_CONTENT);
	}

    /**
     * Inserts the specified element at the specified position in this
     * list. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (adds one to their indices).
     *
     * Inserts at the end if index<0
     *
     * @param index index at which the specified element is to be inserted
     * @param name name of new note
     */
	public Note addNewNote(int index, String name) {
		return addNewNote(index, name,EMPTY_NOTE_CONTENT);
	}

    /**
     * Inserts the specified element at the specified position in this
     * list. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (adds one to their indices).
     *
     * Inserts at the end if index<0
     *
     * @param index index at which the specified element is to be inserted
     * @param name name of new note
     * @param content content of new note
     */
	public Note addNewNote(int index, String name, String content) {

		if (name==null) throw new RuntimeException("Name is null.");

		Note note = new Note(name, content);
		if (index<0)
			notesList.add(note);
		else
			notesList.add(index,note);

		node.updateModificationTime();
		node.getDataBase().fireNodeContentChanged(this,node);

		return note;
	}

	//
	// Definition d'une note
	//

	public class Note {

		String name;
		String content;

		public Note(String name, String content) {
			this.name=name;
			this.content=content;
		}

		public String getName() {
			return name;
		}

		public String getContent() {
			return content;
		}

		public void setName(String name) {

			if (name==null) throw new RuntimeException("Name is null.");

			this.name = name;
			node.updateModificationTime();
			node.getDataBase().fireNodeContentChanged(this,node);
		}

		public void setContent(String content) {

			if (content==null) throw new RuntimeException("Name is null.");

			if (content.equals(this.content)) return;

			this.content = content;
			node.updateModificationTime();
			node.getDataBase().fireNodeContentChanged(this,node);
		}

		public boolean exists() {
			return notesList.contains(this);
		}

		public void remove() {
			Notes.this.remove(this);
		}

		public void move(int newIndex) {
			notesList.remove(this);
			notesList.add(newIndex,this);
			node.updateModificationTime();
			node.getDataBase().fireNodeContentChanged(this,node);
		}

		public void setMain() {
			mainNote=Note.this;
		}

		public boolean isMain() {
			return (mainNote==Note.this);
		}

	}


}

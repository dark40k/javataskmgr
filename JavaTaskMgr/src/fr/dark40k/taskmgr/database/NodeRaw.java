package fr.dark40k.taskmgr.database;

import java.util.ArrayList;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.display.Display;
import fr.dark40k.taskmgr.database.display.DisplayNode;

public class NodeRaw extends Node {

	public final static String XMLTAG="node";

	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------

	public NodeRaw(TaskMgrDB dataBase, int key, String name, Boolean readOnly, DisplayNode display) {
		super(dataBase, key, name, readOnly, display);
	}

	public NodeRaw(TaskMgrDB dataBase, String name, Boolean readOnly, DisplayNode display) {
		this(dataBase, dataBase.getNewKey(), name, readOnly, display);
	}

	// loads children only if rawImport
	protected NodeRaw(TaskMgrDB nodeDataBase, Element elt, boolean rawImportKeys, boolean importChildren) {
		super(nodeDataBase,elt,rawImportKeys,importChildren);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Element saveToXML() {
		return super.saveToXML().setName(XMLTAG);
	}

	@Override
	public void reloadFromXML(Element elt) {
		super.reloadFromXML(elt);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es particulieres
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String getStackedName(String separator){
		return getStackedName(separator,TaskMgrDB.ROOT_BASE_KEY);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es d'affichage
	//
	// -------------------------------------------------------------------------------------------

	@Override
	protected ArrayList<Display> buildEffectiveDisplayList() {
		ArrayList<Display> displayList= new ArrayList<Display>();
		displayList.add(getDisplay());
		return displayList;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public int addNewChild(Node elderBrotherNode) {
		NodeRaw newNode = new NodeRaw(getDataBase(), DEFAULT_NAME, DEFAULT_READONLY, new DisplayNode());
		this.addChild(newNode,elderBrotherNode);
		return newNode.getKey();
	}

	@Override
	public boolean canAddChild(Class<? extends Node> nodeClass) {
		return true;
	}

}

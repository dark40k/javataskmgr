package fr.dark40k.taskmgr.database;

import java.util.ArrayList;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.display.Display;
import fr.dark40k.taskmgr.database.display.DisplayNode;

public class NodeStatus extends NodeFilter {

	public final static String XMLTAG="nodeStatus";

	public final static Boolean DEFAULT_HIDDEN = false;

	private Boolean	hidden;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------

	public NodeStatus(TaskMgrDB dataBase, int key, String name, Boolean readOnly, DisplayNode display, Boolean hidden) {
		super(dataBase, key, name, readOnly, display, hidden);
		this.hidden = hidden;
	}

	public NodeStatus(TaskMgrDB dataBase, String name, Boolean readOnly, DisplayNode display, Boolean hidden) {
		this(dataBase, dataBase.getNewKey(), name, readOnly, display, hidden);
	}

	// loads children only if rawImport
	protected NodeStatus(TaskMgrDB nodeDataBase, Element elt, boolean rawImportKeys, boolean importChildren) {
		super(nodeDataBase,elt,rawImportKeys,importChildren);
		hidden=elt.getAttribute("hidden")!=null;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Element saveToXML() {
		Element elt = super.saveToXML().setName(XMLTAG);
		if (hidden) elt.setAttribute("hidden","1");
		return elt;
	}

	@Override
	public void reloadFromXML(Element elt) {
		hidden=elt.getAttribute("hidden")!=null;
		super.reloadFromXML(elt);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es particulieres
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String getStackedName(String separator){
		return getStackedName(separator,TaskMgrDB.ROOT_STATUS_KEY);
	}

	@Override
	public Boolean isHidden() {
		return hidden;
	}

	@Override
	public void setHidden(Boolean val) {
		if (hidden==val) return;
		hidden=val;
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es d'affichage
	//
	// -------------------------------------------------------------------------------------------

	// Ordre de priorit� d'affichage en cas d'egalit� (ordre decroissant) :
	// 1 - definition locale
	// 2 - parents jusqu'au pere final
	@Override
	protected ArrayList<Display> buildEffectiveDisplayList() {
		ArrayList<Display> displayList= new ArrayList<Display>();
		displayList.add(getDisplay());
		int fkey=getDataBase().findFirstFatherKey(getKey());
		if (fkey>0)
			if (fkey!=TaskMgrDB.ROOT_STATUS_KEY)
				displayList.add(getDataBase().getNode(fkey).getEffectiveDisplay());
		return displayList;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public int addNewChild(Node elderBrother) {
		NodeStatus newNode = new NodeStatus(getDataBase(), DEFAULT_NAME, DEFAULT_READONLY, new DisplayNode(), DEFAULT_HIDDEN);
		this.addChild(newNode,elderBrother);
		return newNode.getKey();
	}

	@Override
	public boolean canAddChild(Class<? extends Node> nodeClass) {
		return (NodeStatus.class.isAssignableFrom(nodeClass));
	}



}

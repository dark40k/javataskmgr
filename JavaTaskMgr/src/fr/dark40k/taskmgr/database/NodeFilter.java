package fr.dark40k.taskmgr.database;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.database.display.Display;
import fr.dark40k.taskmgr.database.display.DisplayNode;
import fr.dark40k.taskmgr.database.interfaces.NodeHiddenInterface;
import fr.dark40k.taskmgr.util.StringFormulaFilter;
import fr.dark40k.taskmgr.util.StringFormulaFilter.UnresolvedFilter;

public class NodeFilter extends Node implements NodeHiddenInterface {

	private final static Logger LOGGER = LogManager.getLogger();

	public final static String XMLTAG="nodeFilter";

	public final static Boolean DEFAULT_HIDDEN = false;

	private Boolean	hidden;

	private String filterText;
	private StringFormulaFilter filter;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeurs
	//
	// -------------------------------------------------------------------------------------------

	public NodeFilter(TaskMgrDB dataBase, int key, String name, Boolean readOnly, DisplayNode display, Boolean hidden) {
		super(dataBase, key, name, readOnly, display);
		this.hidden = hidden;
	}

	public NodeFilter(TaskMgrDB dataBase, String name, Boolean readOnly, DisplayNode display, Boolean hidden) {
		this(dataBase, dataBase.getNewKey(), name, readOnly, display, hidden);
	}

	// loads children only if rawImport
	protected NodeFilter(TaskMgrDB nodeDataBase, Element elt, boolean rawImportKeys, boolean importChildren) {
		super(nodeDataBase,elt,rawImportKeys,importChildren);
		hidden=elt.getAttribute("hidden")!=null;
		filterText=elt.getAttributeValue("filterText");
		calculateFilter();
	}

	// -------------------------------------------------------------------------------------------
	//
	// Import / Export XML
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public Element saveToXML() {
		Element elt = super.saveToXML().setName(XMLTAG);
		if (hidden) elt.setAttribute("hidden","1");
		if (filterText!=null) elt.setAttribute("filterText",filterText);
		return elt;
	}

	@Override
	public void reloadFromXML(Element elt) {
		hidden=elt.getAttribute("hidden")!=null;
		filterText=elt.getAttributeValue("filterText");
		calculateFilter();
		super.reloadFromXML(elt);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es particulieres
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public String getStackedName(String separator){
		return getStackedName(separator,TaskMgrDB.ROOT_FILTER_KEY);
	}

	@Override
	public Boolean isHidden() {
		return hidden;
	}

	@Override
	public void setHidden(Boolean val) {
		if (hidden==val) return;
		hidden=val;
		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	public Boolean isFilterValid() {
		return (filter !=null);
	}

	public String getFilterText() {
		return filterText;
	}

	public void setFilterText(String text) {
		if (text==null) {
			if (filterText==null) return;
			filterText=null;
			filter=null;
			updateModificationTime();
			getDataBase().fireNodeContentChanged(this,this);
			return;
		}

		if (text.equals(filterText)) return;

		filterText=text;

		calculateFilter();

		updateModificationTime();
		getDataBase().fireNodeContentChanged(this,this);
	}

	private void calculateFilter() {
		if (filterText==null) {
			filter=null;
			return;
		}
		try {
			filter=new StringFormulaFilter(filterText);
		} catch (UnresolvedFilter e) {
			filter=null;
			LOGGER.error("Error processing filter:"+filterText+"\n message="+e.getMessage());
		}
	}

	public StringFormulaFilter getFilter() {
		return filter;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Clone implementation
	//
	// -------------------------------------------------------------------------------------------

//	public NodeFilter clone() {
//		NodeFilter nodeClone = null;
//		nodeClone = (NodeFilter) super.clone();
//		return nodeClone;
//	}

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es d'affichage
	//
	// -------------------------------------------------------------------------------------------

	// Ordre de priorit� d'affichage en cas d'egalit� (ordre decroissant) :
	// 1 - definition locale
	// 2 - parents jusqu'au pere final
	@Override
	protected ArrayList<Display> buildEffectiveDisplayList() {
		ArrayList<Display> displayList= new ArrayList<Display>();
		displayList.add(getDisplay());
		int fkey=getDataBase().findFirstFatherKey(getKey());
		if (fkey>0)
			if (fkey!=TaskMgrDB.ROOT_STATUS_KEY)
				displayList.add(getDataBase().getNode(fkey).getEffectiveDisplay());
		return displayList;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Manipulations des enfants
	//
	// -------------------------------------------------------------------------------------------

	@Override
	public int addNewChild(Node elderBrother) {
		NodeFilter newNode = new NodeFilter(getDataBase(), DEFAULT_NAME, DEFAULT_READONLY, new DisplayNode(), DEFAULT_HIDDEN);
		this.addChild(newNode,elderBrother);
		return newNode.getKey();
	}

	@Override
	public boolean canAddChild(Class<? extends Node> nodeClass) {
		return (NodeFilter.class.isAssignableFrom(nodeClass));
	}


	// -------------------------------------------------------------------------------------------
	//
	// Gestion des filtres
	//
	// -------------------------------------------------------------------------------------------



}

package fr.dark40k.taskmgr.util;

import java.util.ArrayList;
import java.util.HashSet;

import org.jdom2.Element;

import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.NodeTaskLink;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.interfaces.NodeLinkInterface;

public class CopyNode {

	Class<? extends Node> nodeClass;

	int nodeKey=TaskMgrDB.NO_NODE_KEY;
	String nodeName;
	int linkKey=TaskMgrDB.NO_NODE_KEY;

	Element nodeXML;
	ArrayList<CopyNode> children;

	// -------------------------------------------------------------------------------------------
	//
	// Constructor
	//
	// -------------------------------------------------------------------------------------------
	public CopyNode(Node node) {

		nodeKey=node.getKey();
		nodeName=node.getName();

		if (node instanceof NodeLinkInterface) linkKey=((NodeLinkInterface)node).getLinkKey();

		nodeXML = node.saveToXML();
		nodeClass = node.getClass();

		children = new ArrayList<CopyNode>();

		for (int i=0; i<node.getChildren().getChildCount();i++)
			children.add(i, new CopyNode(node.getChildren().getChild(i)));

	}

	// -------------------------------------------------------------------------------------------
	//
	// Information
	//
	// -------------------------------------------------------------------------------------------
	public int getNodeKey() {
		return nodeKey;
	}

	public String getNodeName() {
		return nodeName;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Paste node and children
	//
	// -------------------------------------------------------------------------------------------

	public Node pasteNode(Node node) {

		if (linkKey!=TaskMgrDB.NO_NODE_KEY) return pasteLink(node);

		Node newNode = node.getDataBase().createFromXML(nodeXML);
		node.addChild(newNode,null);
		// propage les enfants si ce n'est pas un lien
		if (linkKey==TaskMgrDB.NO_NODE_KEY) for (CopyNode childNode : children) childNode.pasteNode(newNode);
		return newNode;
	}

	// return null if can paste, otherwise provides error message
	public String canPaste(Node node) {

		if (linkKey!=TaskMgrDB.NO_NODE_KEY) return canPasteLink(node);

		if (!node.canAddChild(nodeClass)) return "Noeuds de type diff�rent "+node.getClass() + " / "+ nodeClass;

		if (checkCircularReference(node,buildAllLinkedHeirs())) return "Reference circulaire interdite.";

		return null;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Paste link to node
	//
	// -------------------------------------------------------------------------------------------

	public Node pasteLink(Node node) {
		int pasteKey = (linkKey!=TaskMgrDB.NO_NODE_KEY)?linkKey:nodeKey;
		return node.addChild(new NodeTaskLink((NodeTask) node.getDataBase().getNode(pasteKey)),null);
	}

	// return null if can paste link, otherwise provides error message
	public String canPasteLink(Node node) {

		int pasteKey = (linkKey!=TaskMgrDB.NO_NODE_KEY)?linkKey:nodeKey;

		if (node.getDataBase().getNode(pasteKey)==null) return "Le noeud source n'existe plus.";

		if (!node.canAddChild(NodeTask.class)) return "Collage de lien Tache non autoris�.";

		HashSet<Integer> listHeirs=buildAllLinkedHeirs();
		listHeirs.add(pasteKey);

		if (checkCircularReference(node,listHeirs)) return "Reference circulaire interdite.";

		return null;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Internal tools
	//
	// -------------------------------------------------------------------------------------------

	private boolean checkCircularReference(Node father, HashSet<Integer> listHeirs) {

		while (father!=null) {
			if (listHeirs.contains(father.getKey())) return true;
			if (father instanceof NodeLinkInterface)
				if (listHeirs.contains(((NodeLinkInterface)father).getLinkKey()))
					return true;
			father=father.getFather();
		}
		return false;
	}

	private HashSet<Integer> buildAllLinkedHeirs() {
		return buildAllLinkedHeirs(new HashSet<Integer>());
	}

	private HashSet<Integer> buildAllLinkedHeirs(HashSet<Integer> list) {
		if (linkKey!=TaskMgrDB.NO_NODE_KEY) list.add(linkKey);
		for (CopyNode copyNode : children) copyNode.buildAllLinkedHeirs(list);
		return list;
	}

}

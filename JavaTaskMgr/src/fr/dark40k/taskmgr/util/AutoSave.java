package fr.dark40k.taskmgr.util;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.util.properties.PropertyUpdateEvent;
import fr.dark40k.util.properties.PropertyUpdateListener;

public class AutoSave implements PropertyUpdateListener {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private Timer autoSaveTimer;
	
	private TaskMgr taskMgr;
	
	public AutoSave(TaskMgr taskMgr) {
		this.taskMgr=taskMgr;
		taskMgr.getProperties().addUpdateListener(this);
	}

	@Override
	public void propertiesUpdated(PropertyUpdateEvent evt) {
		reset();
	}
	
	public void reset() {
		
		// reset timer
		if (autoSaveTimer != null) {
			autoSaveTimer.cancel();
			autoSaveTimer = null;
		}
		
		// check if database has an active file defined
		if (taskMgr.getCurFile()==null) return;
		
		// check if autosave is inactive => exit
		if (taskMgr.getProperties().saveAuto.getSaveAuto() == false)
			return;

		// calculate next save time and launch timer
		Date autoSaveReference = new Date();
		Date nextAutoSave = new Date( autoSaveReference.getTime() + taskMgr.getProperties().saveAuto.getSaveMaxDelay().toMillis());
		LOGGER.info("Next save time = " + nextAutoSave.toString());

		// launch timer
		autoSaveTimer = new Timer();
		autoSaveTimer.schedule( new TimerTask() {
		    	  @Override
		    	  public void run() {
		    		  autoSave();
		    	  }
		    	},
		    	nextAutoSave);    
		
	}

	public void beforeExit() {
		if (taskMgr.getProperties().saveAuto.getSaveAuto() == true) {
			LOGGER.info("Start Autosave on Exit");
			  taskMgr.cmd_Save();
		}
	}

	private void autoSave() {
		  LOGGER.info("Start Autosave");
		  taskMgr.cmd_Save();
	}
	
}

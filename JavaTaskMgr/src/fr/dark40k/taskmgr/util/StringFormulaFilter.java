package fr.dark40k.taskmgr.util;

import java.util.ArrayList;
import java.util.Set;

//
// Generate a filter that can handle StringSets
// accepted functions OR, AND, NOR, NAND
// Example of formula:
// OR(cat00,AND(cat01,cat02),NOR(cat03))


public class StringFormulaFilter {

	private boolean opAnd = false;
	private boolean opRev = false;

	private String text;
	
	private ArrayList<String> listStrings = new ArrayList<String>();
	private ArrayList<StringFormulaFilter> listFilters = new ArrayList<StringFormulaFilter>();

	
	public StringFormulaFilter(String filter) throws UnresolvedFilter {
		
		String baseFilter = filter;
		
		int p1 = filter.indexOf(',');
		int p2 = filter.indexOf('(');
		
		// cas d'un seul mot
		if ((p1<0) && (p2<0)) {
			listStrings.add(filter);
			return;
		}

		// le premier terme n'est pas une commande
		if ((p1>=0) && ((p2<0)||(p1<p2)))
			throw new UnresolvedFilter("Missing starting operator :"+baseFilter);
		
		// detection de la commande
		switch (filter.substring(0, p2)) {
			case "AND":
				opAnd = true;
				opRev = false;
				break;
			case "OR":
				opAnd = false;
				opRev = false;
				break;
			case "NAND":
				opAnd = true;
				opRev = true;
				break;
			case "NOR":
				opAnd = false;
				opRev = true;
				break;
			default:
				throw new UnresolvedFilter("Unkonwn operator :" + baseFilter);
		}
		
		//v�rifie que le filtre se termine bien par la fermeture de la parenth�se
		if (!filter.endsWith(")"))
			throw new UnresolvedFilter("Missing terminating ) :"+baseFilter);

		//verifie que la parenth�se n'est pas vide
		if (p2+1>=filter.length()-1)
			throw new UnresolvedFilter("Missing operator parameters :"+baseFilter);
		
		//reduit aux termes de la commande
		filter = filter.substring(p2+1,filter.length()-1);
		
		//balaye les parametres
		while (filter.length()>0) {
		
			p1 = filter.indexOf(',');
			p2 = filter.indexOf('(');
			
			// un seul mot
			if ((p1<0) && (p2<0)) {
				listStrings.add(filter);
				return;
			}
		
			// premier terme est une virgule
			if (p1==0) 
				throw new UnresolvedFilter("Empty parameter :"+baseFilter);
			
			// le premier terme est un mot
			if ((p1>0) && ((p2<0)||(p1<p2))) {
				listStrings.add(filter.substring(0,p1));
				filter=filter.substring(p1+1);
				continue;
			}
			
			//recherche la parenth�se fermante correspondant � l'ouvrante 
			int count = 1;
			int p3 = p2;
			do {
				// trouve la parenth�se fermante suivante.
				p3 = filter.indexOf(')', p3 + 1);
				if (p3 < 0)
					throw new UnresolvedFilter("Closing parenthesis not found: " + baseFilter);
				// decompte la parenthese
				count--;

				// cherche la prochaine parenth�se ouvrante
				if (p2 > 0) p2 = filter.indexOf('(', p2 + 1);
				// ajoute un niveau si la parenth�se ouvrante est avant la virgule suivante
				if ((p2 > 0) && (p2 < p3)) count++;
			
			// fin de la commande si on a autant de fermant que d'ouvrant
			} while (count > 0);

			//ajoute la commande � la liste
			listFilters.add(new StringFormulaFilter(filter.substring(0, p3+1)));

			//recalle filter
			filter = filter.substring(p3 + 1);
			
		}
		
	}
	
	public String toString() {
		return text;
	}
	
	public boolean checkStringSet(Set<String> list) { 
				
		if (list==null) return false;
		if (listStrings==null) return false;
		if (listStrings.size()==0) return false;
		
		for (String filter : listStrings) 
			if (list.contains(filter) ^ opAnd)
				return (!opAnd) ^ opRev;
		
		for (StringFormulaFilter filter : listFilters)
			if (filter.checkStringSet(list) ^ opAnd )
				return (!opAnd) ^ opRev;
		
		return opAnd ^ opRev;
		
	}
	
	public class UnresolvedFilter extends Exception {
		public UnresolvedFilter(String message) {
			super(message);
		}
	}
	
}

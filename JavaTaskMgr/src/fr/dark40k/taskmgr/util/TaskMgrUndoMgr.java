package fr.dark40k.taskmgr.util;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeTaskLink;
import fr.dark40k.taskmgr.gui.nodetable.NodeTableModel.ExpansionState;

public class TaskMgrUndoMgr {
	
	private final static Logger LOGGER = LogManager.getLogger();
	
	private UndoManager undoMgr = new UndoManager();

	private TaskMgr taskMgr;
	
	public TaskMgrUndoMgr(TaskMgr taskMgr) {
		this.taskMgr=taskMgr;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Commandes
	//
	// -------------------------------------------------------------------------------------------
	
	// Undo
	
	public boolean canUndo() {
		return undoMgr.canUndo();
	}

	public void undo() {
		LOGGER.info("Undo = " + undoMgr.getUndoPresentationName());
		undoMgr.undo();
	}

	// Redo
	
	public boolean canRedo() {
		return undoMgr.canRedo();
	}

	public void redo() {
		LOGGER.info("Redo = " + undoMgr.getRedoPresentationName());
		undoMgr.redo();
	}

	// Manage pile
	
	public void discardAllEdits() {
		undoMgr.discardAllEdits();
	}

	public void addEdit(UndoableNodeDatabaseEdit edit) {
		undoMgr.addEdit(edit);
	}

	public String getUndoPresentationName() {
		if (canUndo()) return undoMgr.getUndoPresentationName();
		return "Annuler <vide>";
	}
	
	public String getRedoPresentationName() {
		if (canRedo()) return undoMgr.getRedoPresentationName();
		return "Refaire <vide>";
	}
	
	// Database edit
	
	public UndoableNodeDatabaseEdit createUndoableGlobalEdit(String actionName) {
		return new UndoableGlobalEdit(actionName);
	}
	
	public void addUndoableGlobalEdit(String actionName) {
		undoMgr.addEdit(createUndoableGlobalEdit(actionName));
	}
	
	// Node edit
	
	public UndoableNodeEdit createUndoablenNodeEdit(Node node, String actionName, boolean mergeSameActionName) {
		return new UndoableNodeEdit(node,actionName, mergeSameActionName);
	}
	
	public void addUndoableNodeEdit(Node node, String actionName, boolean mergeSameActionName) {
		undoMgr.addEdit(createUndoablenNodeEdit(node,actionName, mergeSameActionName));
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Edition globale de la base
	//
	// -------------------------------------------------------------------------------------------
	
	public interface UndoableNodeDatabaseEdit extends UndoableEdit {}
	
	public class UndoableGlobalEdit implements UndoableNodeDatabaseEdit {
		
		private Element undoDatabase;
		private ExpansionState undoExpansionState;
		
		private Element redoDatabase;
		private ExpansionState redoExpansionState;

		private String actionName;
		
		public UndoableGlobalEdit(String actionName) {
			undoDatabase=taskMgr.getTaskMgrDB().saveToXML();
			undoExpansionState = taskMgr.getMainFrame().nodeListPanel.getExpansionState();
			this.actionName=actionName;
		}

		@Override
		public void undo() throws CannotUndoException {
			
			redoDatabase=taskMgr.getTaskMgrDB().saveToXML();
			redoExpansionState = taskMgr.getMainFrame().nodeListPanel.getExpansionState();
			
			taskMgr.getTaskMgrDB().reloadFromXML(undoDatabase);
			taskMgr.cmd_Update();
			taskMgr.getMainFrame().nodeListPanel.setExpansionState(undoExpansionState);
		}

		@Override
		public boolean canUndo() {
			return true;
		}

		@Override
		public void redo() throws CannotRedoException {
			if (!canRedo()) throw new CannotRedoException();
			taskMgr.getTaskMgrDB().reloadFromXML(redoDatabase);
			taskMgr.cmd_Update();
			taskMgr.getMainFrame().nodeListPanel.setExpansionState(redoExpansionState);
		}

		@Override
		public boolean canRedo() {
			return (redoDatabase!=null);
		}

		@Override
		public void die() {
		}

		@Override
		public boolean addEdit(UndoableEdit anEdit) {
			return false;
		}

		@Override
		public boolean replaceEdit(UndoableEdit anEdit) {
			return false;
		}

		@Override
		public boolean isSignificant() {
			return true;
		}

		@Override
		public String getPresentationName() {
			return actionName;
		}

		@Override
		public String getUndoPresentationName() {
			return "Annuler "+actionName;
		}

		@Override
		public String getRedoPresentationName() {
			return "Refaire "+actionName;
		}
		
	}

	// -------------------------------------------------------------------------------------------
	//
	// Edition d'un noeud
	//
	// -------------------------------------------------------------------------------------------
	
	public class UndoableNodeEdit implements UndoableNodeDatabaseEdit {
		
		private boolean mergeSameActionName;
		
		private Element undoNode;
		private int undoNodeKey;
		
		private Element redoNode;

		private String actionName;
		
		public UndoableNodeEdit(Node node, String actionName, boolean mergeSameActionName) {
			if (node instanceof NodeTaskLink) node = ((NodeTaskLink)node).getLinkNode();
			undoNode=node.saveToXML();
			undoNodeKey=node.getKey();
			this.actionName=actionName;
			this.mergeSameActionName=mergeSameActionName;
		}

		@Override
		public void undo() throws CannotUndoException {
			redoNode=taskMgr.getTaskMgrDB().getNode(undoNodeKey).saveToXML();
			taskMgr.getTaskMgrDB().getNode(undoNodeKey).reloadFromXML(undoNode);
		}

		@Override
		public boolean canUndo() {
			return true;
		}

		@Override
		public void redo() throws CannotRedoException {
			if (!canRedo()) throw new CannotRedoException();
			taskMgr.getTaskMgrDB().getNode(undoNodeKey).reloadFromXML(redoNode);
		}

		@Override
		public boolean canRedo() {
			return (redoNode!=null);
		}

		@Override
		public void die() {
		}

		@Override
		public boolean addEdit(UndoableEdit anEdit) {
			if (!mergeSameActionName) return false;
			if (!(anEdit instanceof UndoableNodeEdit)) return false;
			UndoableNodeEdit nodeEdit = (UndoableNodeEdit) anEdit;
			if (nodeEdit.undoNodeKey != undoNodeKey ) return false;
			if (!nodeEdit.actionName.equals(actionName)) return false;
			return true;
		}

		@Override
		public boolean replaceEdit(UndoableEdit anEdit) {
			return false;
		}

		@Override
		public boolean isSignificant() {
			return true;
		}

		@Override
		public String getPresentationName() {
			return actionName;
		}

		@Override
		public String getUndoPresentationName() {
			return "Annuler "+actionName;
		}

		@Override
		public String getRedoPresentationName() {
			return "Refaire "+actionName;
		}
		
	}


}

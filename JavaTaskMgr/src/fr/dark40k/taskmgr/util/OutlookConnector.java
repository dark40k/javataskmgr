package fr.dark40k.taskmgr.util;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComFailException;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class OutlookConnector {

	private final static Logger LOGGER = LogManager.getLogger();

	public static class Email {
		public String subject;
		public String entryID;
		public Email(String subject, String entryID) {
			this.subject=subject;
			this.entryID=entryID;
		}
	}

	public static class Folder {
		public String name;
		public String entryID;
		public Folder(String name, String entryID) {
			this.name=name;
			this.entryID=entryID;
		}
	}

	public static ArrayList<Email> ListSelectedEMails() {

		ArrayList<Email> emailList = new ArrayList<Email>();

			try {
				LOGGER.info("Connection � Outlook");
				ComThread.InitSTA();
				ActiveXComponent xl = connectOutlook();

				LOGGER.info("Recuperation des donn�es");
				Dispatch explorer = Dispatch.get(xl,"ActiveExplorer").toDispatch();
				Dispatch selection = Dispatch.get(explorer, "Selection").toDispatch();
				Variant count = Dispatch.get(selection, "Count");

				for (int mailIndex = 1; mailIndex <= count.getInt(); mailIndex++ ) {


					Dispatch mailItem = Dispatch.call(selection, "Item", new Variant(mailIndex)).toDispatch();

					Variant subject = Dispatch.get(mailItem, "Subject");
					Variant entryID = Dispatch.get(mailItem, "entryID");

					LOGGER.info("Mail trouv� : \""+subject.toString()+"\", EntryID="+entryID.toString());
					emailList.add(new Email(subject.toString(),entryID.toString()));

				}
				LOGGER.info("Fin Connection � Outlook");
			} catch (Exception e) {
				LOGGER.warn(e);
				LOGGER.info("Annulation Connection � Outlook");
			} finally {
				ComThread.Release();
			}

		return emailList;

	}

	public static Folder getSelectedFolder() {

		Folder selFolder=null;

		try {
			LOGGER.info("Connection � Outlook");
			ComThread.InitSTA();
			ActiveXComponent xl = connectOutlook();

			LOGGER.info("Recuperation des donn�es");
			Dispatch explorer = Dispatch.get(xl,"ActiveExplorer").toDispatch();
			Dispatch folder = Dispatch.get(explorer, "CurrentFolder").toDispatch();

			Variant description = Dispatch.get(folder, "Name");
			Variant entryID = Dispatch.get(folder, "entryID");

			selFolder = new Folder(description.toString(),entryID.toString());

			LOGGER.info("Fin Connection � Outlook");
		} catch (Exception e) {
			LOGGER.error("Fin Connection � Outlook - Erreure");
			e.printStackTrace();
		} finally {
			ComThread.Release();
		}


		return selFolder;

	}

	public static boolean openMail(String entryID) {
		boolean success = false;
		try {
			LOGGER.info("Connection � Outlook");
			ComThread.InitSTA();
			ActiveXComponent axOutlook = connectOutlook();
			Dispatch oNameSpace = axOutlook.getProperty("Session").toDispatch();
			LOGGER.info("oNameSpace=" + oNameSpace);

			Dispatch mailItem = Dispatch.call(oNameSpace, "GetItemFromID", new Variant(entryID)).toDispatch();
			Dispatch.call(mailItem,"Display");
			LOGGER.info("Fin Connection � Outlook");
			success = true;
		} catch (Exception e) {
			LOGGER.warn("Fin Connection � Outlook, Erreur : " + entryID,e);
		} finally {
			ComThread.Release();
		}
		return success;
	}

	public static boolean openFolder(String entryID) {
		boolean success = false;
		try {
			LOGGER.info("Connection � Outlook");
			ComThread.InitSTA();
			ActiveXComponent axOutlook = connectOutlook();

			Dispatch oNameSpace = axOutlook.getProperty("Session").toDispatch();
			LOGGER.info("oNameSpace=" + oNameSpace);

			Dispatch folder = Dispatch.call(oNameSpace, "GetFolderFromID", new Variant(entryID)).toDispatch();

			Dispatch explorer = Dispatch.get(axOutlook,"ActiveExplorer").toDispatch();
			Dispatch.putRef(explorer,"CurrentFolder",folder);

			//TODO send window to front

			LOGGER.info("Fin Connection � Outlook");
			success = true;
		} catch (Exception e) {
			LOGGER.warn("Fin Connection � Outlook, Erreur : " + entryID,e);
		} finally {
			ComThread.Release();
		}
		return success;
	}

	private final static ActiveXComponent connectOutlook() {

		ActiveXComponent tryOutlookActiveX = null;

		try {
			LOGGER.info("Outlook (Outlook.Application)");
			tryOutlookActiveX = new ActiveXComponent("Outlook.Application");
			LOGGER.info("Success");
			return tryOutlookActiveX;
		} catch (ComFailException e) {
			LOGGER.info("Failed, cause = "+e.getLocalizedMessage());
		}

		try {
			LOGGER.info("Outlook 2007 (Outlook.Application.12)");
			tryOutlookActiveX = new ActiveXComponent("Outlook.Application.12");
			LOGGER.info("Success");
			return tryOutlookActiveX;
		} catch (ComFailException e) {
			LOGGER.info("Failed, cause = "+e.getLocalizedMessage());
		}

		throw new ComFailException("Impossible de se connecter � Outlook.");

	}


}

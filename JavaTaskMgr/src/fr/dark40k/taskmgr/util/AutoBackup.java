package fr.dark40k.taskmgr.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.taskmgr.TaskMgr;
import fr.dark40k.util.properties.PropertyUpdateEvent;
import fr.dark40k.util.properties.PropertyUpdateListener;

public class AutoBackup implements PropertyUpdateListener {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private Timer autoBackupTimer;

	private TaskMgr taskMgr;
	
	private File backupDir;
	
	public AutoBackup(TaskMgr taskMgr) {
		this.taskMgr=taskMgr;
		taskMgr.getProperties().addUpdateListener(this);
	}

	@Override
	public void propertiesUpdated(PropertyUpdateEvent evt) {
		reset();
	}

	
	public void reset() {
		 
		try {
			updateAutoBackup();
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(taskMgr.getMainFrame(), "Attention: Autobackup a �chou�","Avertissement", JOptionPane.WARNING_MESSAGE);
		}
		
	}
		
	public void updateAutoBackup() throws IOException {
		
		LOGGER.info("Start");
		
		// reset timer
		if (autoBackupTimer!=null) {
			LOGGER.info("Stopping autobackup");
			autoBackupTimer.cancel();
			autoBackupTimer=null;
		}
		
		// check if auto backup is on adn delay >0
		if ((taskMgr.getProperties().backupAuto.getBackupAuto()==false) || (taskMgr.getProperties().backupAuto.getBackupMaxDelay().toMillis()<=0)) return;
		
		// check if cuFile is set
		if (taskMgr.getCurFile()==null) return;
		
		// Set autobackup directory, create it if it does not exist
		backupDir = new File(taskMgr.getCurFile().getParentFile(), taskMgr.getProperties().backupAuto.getBackupFolderName());
		if (!backupDir.exists()) 
			if(!backupDir.mkdir()) {
				LOGGER.error("Cancelling backup, Could not create directory :"+backupDir.getAbsolutePath());
				return;
			};
			
		// calculate lastBackupTime (null if not backup found)
		FileTime lastBackupTime = null;
		for (File file : backupDir.listFiles()) {
			FileTime time = null;
			time = Files.getLastModifiedTime(file.toPath());
			if (lastBackupTime==null) lastBackupTime = time;
			if (lastBackupTime.compareTo(time)<0) lastBackupTime = time;
		}
			
		// calcule la date du prochain backup
		// Principe : lance un backup immediat si le backup existant est plus vieux que le d�lai ou qu'il n'y a pas de backup (plus facile pour debug)
		Date nextBackupDate = new Date( 
					(lastBackupTime != null) 
				? 
					lastBackupTime.toMillis() + taskMgr.getProperties().backupAuto.getBackupMaxDelay().toMillis() 
				: 
					System.currentTimeMillis()
			);

//		// Ancienne approche abandonn�e		
//		Date nextBackupDate = 
//		new Date(
//				(
//					(lastBackupTime != null) ? 
//					Math.max(lastBackupTime.toMillis(), System.currentTimeMillis()) : 
//					System.currentTimeMillis()
//				)
//				+ taskMgr.getProperties().backupAuto.getBackupMaxDelay().toMillis()
//		);

		// informe du prochain backup
		LOGGER.info("Next backup time = "+new Timestamp(nextBackupDate.getTime()));
		
		// cree le timer pour execution du backup
		autoBackupTimer = new Timer();
		
		// programme les backups
		autoBackupTimer.schedule( new TimerTask() {
		    	  @Override
		    	  public void run() {
//		    		  LOGGER.info("Lauching backup");
		    		  createNewBackup();
		    	  }
		    	},
		    nextBackupDate,
		    taskMgr.getProperties().backupAuto.getBackupMaxDelay().toMillis());    
		
	}
	
	public File createNewBackup() {
		
		File backupFile;
		do {
			backupFile = new File(backupDir, backupFileName(taskMgr.getCurFile().getName(),taskMgr.getProperties().backupAuto.getBackupNextNbr()));
			taskMgr.getProperties().backupAuto.incBackupNextNbr();
		} while (backupFile.exists());
		
		LOGGER.info("Saving Backup file = " + backupFile.getAbsolutePath());
		taskMgr.cmd_SaveBackup(backupFile);
		
		return backupFile;
		
	}
	
	private String backupFileName(String fileName, int bckpNbr) {
		
		int extPos=fileName.lastIndexOf(".xml");
		if (extPos<0) extPos = fileName.length()-1;
		return fileName.substring(0, extPos)+"-"+bckpNbr+".xml";
		
	}
	
}

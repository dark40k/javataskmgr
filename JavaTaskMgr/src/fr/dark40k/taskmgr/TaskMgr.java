package fr.dark40k.taskmgr;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Iterator;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import fr.dark40k.pdfeditor.PdfEditor;
import fr.dark40k.taskmgr.database.Children;
import fr.dark40k.taskmgr.database.Node;
import fr.dark40k.taskmgr.database.NodeFilter;
import fr.dark40k.taskmgr.database.NodeTask;
import fr.dark40k.taskmgr.database.NodeTaskLink;
import fr.dark40k.taskmgr.database.TaskMgrDB;
import fr.dark40k.taskmgr.database.executionslots.ExecutionSlot;
import fr.dark40k.taskmgr.database.interfaces.NodeLinkInterface;
import fr.dark40k.taskmgr.gui.TaskMgrFrame;
import fr.dark40k.taskmgr.gui.nodetable.NodeTableModel.ExpansionState;
import fr.dark40k.taskmgr.gui.nodetable.RowAddress;
import fr.dark40k.taskmgr.properties.TaskMgrProperties;
import fr.dark40k.taskmgr.util.AutoBackup;
import fr.dark40k.taskmgr.util.AutoSave;
import fr.dark40k.taskmgr.util.CopyNode;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr;
import fr.dark40k.taskmgr.util.TaskMgrUndoMgr.UndoableNodeDatabaseEdit;
import fr.dark40k.util.console.MessageConsoleMgr;
import fr.dark40k.util.lockedfile.LockedDocumentFile;
import fr.dark40k.util.lockedfile.LockedFile.LockedFileException;
import fr.dark40k.util.properties.dialog.PropertyEditDialog;

public class TaskMgr {

	public final static MessageConsoleMgr messageConsoleMgr = new MessageConsoleMgr();
	private final static Logger LOGGER = LogManager.getLogger();

	final static Properties gitProperties = new Properties(); // fichier de propri�t�s du build

	private final String title;

	private final static FileNameExtensionFilter taskMgrFileFilter = new FileNameExtensionFilter("Task Manager File (xml)", "xml");

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es statiques partag�es
	//
	// -------------------------------------------------------------------------------------------

	public static File currentDirectory;

	public static CopyNode copyNode;

	private TaskMgrDB taskMgrDB;

	// -------------------------------------------------------------------------------------------
	//
	// Donn�es priv�es
	//
	// -------------------------------------------------------------------------------------------

	private final TaskMgrUndoMgr undoMgr = new TaskMgrUndoMgr(this);

	private final TaskMgrFrame taskMgrFrame;

	private final TaskMgrProperties taskMgrProperties = new TaskMgrProperties();

	private final AutoSave autoSave = new AutoSave(this);
	private final AutoBackup autoBackup = new AutoBackup(this);

	private LockedDocumentFile lockedFile;

	// -------------------------------------------------------------------------------------------
	//
	// Constructeur
	//
	// -------------------------------------------------------------------------------------------

	public TaskMgr(File startFile) {

		try {
			LOGGER.info("Initialisation graphique");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		try {
			LOGGER.info("Chargement du fichier de proprietes (git.properties)");
			final InputStream stream = TaskMgr.class.getClassLoader().getResourceAsStream("git.properties");
			if (stream != null) {
				gitProperties.load(stream);
				stream.close();
			} else
				LOGGER.error("git.properties non trouvees.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// calcul du titre
		title = "Gestionnaire de tache <" + ((!gitProperties.isEmpty()) ? gitProperties.getProperty("git.commit.id.describe-short") : "git.properties missing") + ">";

		LOGGER.info("");
		LOGGER.info("");
		LOGGER.info("Starting - " + title);
		LOGGER.info("");
		LOGGER.info("");

		//
		// Initialisation graphique
		//
		taskMgrFrame = new TaskMgrFrame(this);
		taskMgrFrame.setTitle(title);
		taskMgrFrame.setVisible(true);

		//
		// Chargement des readers pour ImageIO
		//
		// Pas du tout sur que cela marche veritablement

		LOGGER.info("Loading ImageIO readers");
		ImageIO.scanForPlugins();
		Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("jbig2");
		while (readers.hasNext()) {
			LOGGER.info("reader: " + readers.next());
		}

		// active l'autogarbage collection de JACOB
		System.setProperty("com.jacob.autogc", "true");

//		System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
//		System.setProperty("org.apache.pdfbox.rendering.UsePureJavaCMYKConversion","true");

		//
		// Chargement de la base
		//
		if (startFile != null)
			openTaskMgrDB(startFile);
		else
			startNewTaskMgrDB();

	}


	// -------------------------------------------------------------------------------------------
	//
	// Getters
	//
	// -------------------------------------------------------------------------------------------

	public TaskMgrProperties getProperties() {
		return taskMgrProperties;
	}

	public TaskMgrFrame getMainFrame() {
		return taskMgrFrame;
	}

	public File getCurFile() {
		if (lockedFile==null) return null;
		return lockedFile.getFile();
	}

	public TaskMgrUndoMgr getUndoMgr() {
		return undoMgr;
	}

	/**
	 * @return the taskMgrDB
	 */
	public TaskMgrDB getTaskMgrDB() {
		return taskMgrDB;
	}

	// -------------------------------------------------------------------------------------------
	//
	// Setters
	//
	// -------------------------------------------------------------------------------------------

	/**
	 * @param taskMgrDB the taskMgrDB to set
	 */
	public void setTaskMgrDB(TaskMgrDB taskMgrDB) {

		this.taskMgrDB = taskMgrDB;

		taskMgrFrame.setDatabase(getTaskMgrDB());

	}

	private static void setCurrentDirectory(File newDirectory) {

		if (currentDirectory==null)
			currentDirectory = new File(System.getProperty("user.dir"));

		if (newDirectory == null)
			return;

		if (newDirectory.isFile()) {
			setCurrentDirectory(newDirectory.getParentFile());
			return;
		}

		if (!newDirectory.isDirectory())
			return;

		if (newDirectory.equals(currentDirectory))
			return;

		LOGGER.info("directory="+newDirectory);
		currentDirectory = newDirectory;

	}

	// -------------------------------------------------------------------------------------------
	//
	// Outils et Commandes logiciel
	//
	// -------------------------------------------------------------------------------------------

	private void updateDatabaseAndDisplay(){

		// applique les scripts
		getTaskMgrDB().runScripts();

		// relance l'affichage quand on aura le temps
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				taskMgrFrame.updateDisplay();
			}
		});

	}

	private void startInterfaceAndTimers() {

		// initialise le titre
		taskMgrFrame.setTitle(title+" - "+ ((lockedFile!=null)?lockedFile.getFile().getAbsolutePath():"Nouvelle base"));

		// lance les backups et la sauvegarde automatique
		autoSave.reset();
		autoBackup.reset();

		// intialise le filtre
		taskMgrFrame.filterToolbar.setStartFilter(getProperties().startup.getStartFilter());

	}

	private void startNewTaskMgrDB() {

		undoMgr.discardAllEdits();

		setTaskMgrDB(new TaskMgrDB());

		if (lockedFile!=null) lockedFile.close();
		lockedFile=null;

		startInterfaceAndTimers();

		updateDatabaseAndDisplay();

	}

	private void openTaskMgrDB(File fileToOpen) {

		// intialisation methode
		Element rootElt=null;

		// check if file exists
		if (!fileToOpen.exists()) {
			LOGGER.error("File not found");
			return;
		}

		// met � jours le repertoire courant
		setCurrentDirectory(fileToOpen.getParentFile());

		// vide la pile de undo/redo
		undoMgr.discardAllEdits();

		// charge la base
		try
		{

			LOGGER.info("Opening existing dataBase = "+fileToOpen);
			lockedFile=new LockedDocumentFile(fileToOpen);

			LOGGER.info("Loading XML");
			rootElt = lockedFile.readRootElement();

			LOGGER.info("Rebuild database");
			setTaskMgrDB(new TaskMgrDB(rootElt.getChild(TaskMgrDB.XMLTAG)));

		}
		catch (Exception e) {
			LOGGER.error("File could not be loaded");
			e.printStackTrace();
			JOptionPane.showMessageDialog(taskMgrFrame, "Error: File could not be loaded.","Loading error", JOptionPane.ERROR_MESSAGE);
			startNewTaskMgrDB();
			return;
		}

		// charge les proprietes de la base (parametres autosauvergarde par ex.)
		LOGGER.info("Load taskMgrProperties");
		try
		{
			taskMgrProperties.importXML(rootElt);
		}
		catch (Exception e) {
			LOGGER.warn("Persistant properties could not be loaded",e);
			// TODO Ajouter une reinitialisation des proprietes
		}

		// charge les parametres persistants de l'affichage
		LOGGER.info("Load display");
		try {
			taskMgrFrame.importXML(rootElt);
		} catch (Exception e) {
			LOGGER.warn("Graphic configuration could not be loaded",e);
		}

		// demarre l'interface et les routines internes
		startInterfaceAndTimers();

		// Lance mise � jours de la base (execution des scripts + rafraichissement affichage)
		LOGGER.info("Update database");
		updateDatabaseAndDisplay();

	}

	private void saveTaskMgrDB() {

		//
		// Genere les donn�es XML � exporter
		//
		Element elt = exportXML();

		//
		// Cree le fichier de sortie
		//

		LOGGER.info("Saving to file = "+lockedFile.getFile().getAbsolutePath());

		try {
			lockedFile.writeRootElement(elt);
		}
		catch (IOException e) {
			LOGGER.error("File could not be saved");
			e.printStackTrace();
			JOptionPane.showMessageDialog(taskMgrFrame, "Error: File could not be saved.","Saving error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}

		// met a jour la sauvegarde
		autoSave.reset();

		// applique les scripts
		getTaskMgrDB().runScripts();

	}


	private Element exportXML() {

		// Genere la racine pour l'ensemble des donn�es
		Element elt=new Element("taskmgr");

		// exporte la base et verifie les donn�es g�n�r�es
		LOGGER.info("Exportation XML de la base");
		Element dbElt = getTaskMgrDB().saveToXML();

		// verifie l'exportation
		LOGGER.info("Verification de la base");
		String checkDB = getTaskMgrDB().checkXMLData(dbElt);
		if (checkDB!=null) {
			JOptionPane.showMessageDialog(taskMgrFrame, checkDB,"XML Export warning", JOptionPane.ERROR_MESSAGE);
		}

		// insere la base
		elt.addContent(dbElt);

		// export des proprietes
		LOGGER.info("Export des proprietes");
		elt.addContent(taskMgrProperties.exportXML());

		// export des param�tres persistants de l'affichage
		LOGGER.info("Export de l'affichage");
		elt.addContent(taskMgrFrame.exportXML());

		return elt;

	}

	// -------------------------------------------------------------------------------------------
	//
	// Commandes de menu
	//
	// -------------------------------------------------------------------------------------------

	public void cmd_Update() {

		LOGGER.info("start.");

		updateDatabaseAndDisplay();

	}

	public void cmd_NewBase() {

		LOGGER.info("start.");

		startNewTaskMgrDB();
	}

	public void cmd_OpenDataBase(){

		LOGGER.info("start.");

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(currentDirectory);
		chooser.setFileFilter(taskMgrFileFilter);

		int res = chooser.showOpenDialog(null);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			LOGGER.error("No file selected");
			return;
		}

		openTaskMgrDB(sgFile);

	}

	public void cmd_SaveAs(){

		LOGGER.info("start.");

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(currentDirectory);
		chooser.setFileFilter(taskMgrFileFilter);

		int res = chooser.showOpenDialog(null);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			LOGGER.error("No file selected");
			return;
		}

		if (!sgFile.getName().endsWith(".xml")) sgFile=new File(sgFile+".xml");

		if (lockedFile!=null) lockedFile.close();

		try {
			lockedFile = new LockedDocumentFile(sgFile);
		} catch (FileNotFoundException e) {
			LOGGER.error("File could not be saved: File not found.");
			e.printStackTrace();
			JOptionPane.showMessageDialog(taskMgrFrame, "Error: File could not be saved: File not found.","Saving error", JOptionPane.ERROR_MESSAGE);
			return;
		} catch (LockedFileException e) {
			LOGGER.error("File could not be saved: Could not place lock.");
			e.printStackTrace();
			JOptionPane.showMessageDialog(taskMgrFrame, "Error: File could not be saved: Could not place lock.","Saving error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		saveTaskMgrDB();

		autoSave.reset();
		autoBackup.reset();

		setCurrentDirectory(sgFile.getParentFile());

		taskMgrFrame.setTitle(title+" - "+sgFile.getAbsolutePath());

	}

	public void cmd_Save(){

		LOGGER.info("start.");

		if (getCurFile()!=null)
			saveTaskMgrDB();
		else
			cmd_SaveAs();

	}

	public void cmd_SaveBackup(File backupFile) {

		LOGGER.info("start.");

		//
		// Genere les donn�es XML � exporter
		//
		Element rootElt = exportXML();

		//
		// Cree le fichier de sortie
		//

		LOGGER.info("Saving to file = "+backupFile.getAbsolutePath());

		XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());

		try {
			FileOutputStream fileOut = new FileOutputStream(backupFile);
			try {
				sortie.output(rootElt, fileOut);
			} finally {
				fileOut.close();
			}
		}
		catch (IOException e) {
			LOGGER.error("File could not be saved");
			e.printStackTrace();
			JOptionPane.showMessageDialog(taskMgrFrame, "Error: File could not be saved.","Saving error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		catch (Exception e) {

		}

		LOGGER.info("End.");
	}


	public void cmd_Exit() {

		LOGGER.info("start.");

		// autosave database
		if (lockedFile!=null) autoSave.beforeExit();

		// clean JACOB data, inutile car le nettoyage est r�alis� � chaque connection. => OutlookConnnector.java
		// ComThread.Release();

		// exit
		System.exit(0);
	}

	public void cmd_NodeCreateNewChild() {

		LOGGER.info("start.");

		cmd_NodeCreateNewChild(taskMgrFrame.nodeListPanel.getSelectedRowAddress());

	}

	public void cmd_NodeCreateNewChild(RowAddress rowAddress) {

		LOGGER.info("start.");

		// pas autoris� � cr�er si le noeud p�re n'est pas selectionn�
		if (rowAddress == null) {
			LOGGER.info("Pas de cr�ation si noeud pere non selectionn�. Annulation.");
			return;
		}

		Node node = rowAddress.getNode();

		undoMgr.addUndoableGlobalEdit("Cr�er noeud");

		ExpansionState expState = taskMgrFrame.nodeListPanel.getExpansionState(node);
		int newKey=node.addNewChild(null);

		taskMgrFrame.nodeListPanel.setExpansionState(expState);
		taskMgrFrame.nodeListPanel.setExpanded(node);

		taskMgrFrame.nodeListPanel.setSelectedRowAddress(new RowAddress(rowAddress,getTaskMgrDB().getNode(newKey)));

		taskMgrFrame.nodeEditPanel.setSelectedIndex(0);
		taskMgrFrame.nodeEditPanel.nodeEditTitlePanel.textField_Title.requestFocus();
		taskMgrFrame.nodeEditPanel.nodeEditTitlePanel.textField_Title.selectAll();
	}

	public void cmd_NodeCreateNewBrother() {

		LOGGER.info("start.");

		cmd_NodeCreateNewBrother(taskMgrFrame.nodeListPanel.getSelectedRowAddress());
	}

	public void cmd_NodeCreateNewBrother(RowAddress rowAddress) {

		LOGGER.info("start.");

		if (rowAddress == null) return;

		undoMgr.addUndoableGlobalEdit("Cr�er noeud");

		Node node = rowAddress.getNode();
		Node father = node.getFather();

		ExpansionState expState = taskMgrFrame.nodeListPanel.getExpansionState();
		int newKey=father.addNewChild(node);

		taskMgrFrame.nodeListPanel.setExpansionState(expState);
		taskMgrFrame.nodeListPanel.setExpanded(father);

		taskMgrFrame.nodeListPanel.setSelectedRowAddress(new RowAddress(rowAddress.getFatherRowAddress(),getTaskMgrDB().getNode(newKey)));

		taskMgrFrame.nodeEditPanel.setSelectedIndex(0);
		taskMgrFrame.nodeEditPanel.nodeEditTitlePanel.textField_Title.requestFocus();
		taskMgrFrame.nodeEditPanel.nodeEditTitlePanel.textField_Title.selectAll();
	}


	public void cmd_NodeDelete() {

		LOGGER.info("start.");

		// recupere le noeud � supprimer
		RowAddress rowAddressToDelete = taskMgrFrame.nodeListPanel.getSelectedRowAddress();
		if (rowAddressToDelete==null) {
			LOGGER.error("Nothing to delete. Aborting");
			return;
		}
		Node nodeToDelete=rowAddressToDelete.getNode();

		// recupere le pere du noeud a supprimer
		Node father=taskMgrFrame.nodeListPanel.getSelectedNodeFather();

		// recupere l'adresse de la ligne d'avant
		int previousRow = taskMgrFrame.nodeListPanel.getSelectedRow()-1;
		RowAddress previousRowAddress = (previousRow>=0)?taskMgrFrame.nodeListPanel.getRowAddress(previousRow):null;

		// tests supplementaires s'il s'agit d'un noeud plein (par opposition � un lien)
		if (!(nodeToDelete instanceof NodeLinkInterface)) {

			// Check if delete is possible
			if (nodeToDelete.isOrHasReadOnlyDescendant()) {
				JOptionPane.showMessageDialog(taskMgrFrame, "Attention: Ce noeud ou un de ses descendants est en lecture seule. \n Supression annul�e.","Avertissement", JOptionPane.WARNING_MESSAGE);
				taskMgrFrame.nodeListPanel.setSelectedRowAddress(rowAddressToDelete); // restore selection
				return;
			}
			if (nodeToDelete.hasDependingNodes()) {
				JOptionPane.showMessageDialog(taskMgrFrame, "Attention: D'autres noeuds sont d�penant de ce noeud. \n Supression annul�e.","Avertissement", JOptionPane.WARNING_MESSAGE);
				taskMgrFrame.nodeListPanel.setSelectedRowAddress(rowAddressToDelete); // restore selection
				return;
			}

		}

		// Prepare deletion
		ExpansionState expState = taskMgrFrame.nodeListPanel.getExpansionState(father); // Save expansion state of other childs
		UndoableNodeDatabaseEdit undoData = undoMgr.createUndoableGlobalEdit("Supprimer noeud"); // prepare undo

		// do deletion
		father.getChildren().deleteChild(nodeToDelete);

		// Deletion succeded
		undoMgr.addEdit(undoData);
		taskMgrFrame.nodeListPanel.setExpansionState(expState);

		// restaure la selection sur la ligne d'avant si possible
		taskMgrFrame.nodeListPanel.setSelectedRowAddress(previousRowAddress);
	}

	public void cmd_ExpandAllFromRoot() {

		LOGGER.info("start.");

		taskMgrFrame.nodeListPanel.setAllExpanded(taskMgrFrame.nodeListPanel.getDisplayedRootNode());
	}

	public void cmd_CollapseAllFromRoot() {

		LOGGER.info("start.");

		Node node=taskMgrFrame.nodeListPanel.getDisplayedRootNode();
		for (Node child:node.getChildren())
			taskMgrFrame.nodeListPanel.setAllCollapsed(child);
	}

	public void cmd_ExpandNode() {

		LOGGER.info("start.");

		Node node = taskMgrFrame.nodeListPanel.getSelectedNode();

		if (node!=null)
			taskMgrFrame.nodeListPanel.setAllExpanded(node);
		else
			cmd_ExpandAllFromRoot();

	}

	public void cmd_CollapseNode() {

		LOGGER.info("start.");

		Node node = taskMgrFrame.nodeListPanel.getSelectedNode();

		if (node!=null)
			taskMgrFrame.nodeListPanel.setAllCollapsed(node);
		else
			cmd_CollapseAllFromRoot();
	}

	public void cmd_UpdateFilter(NodeFilter node) {

		LOGGER.info("start.");

		if (node == null)
			taskMgrFrame.nodeListPanel.setFilter(null);
		else
			taskMgrFrame.nodeListPanel.setFilter(node);

		//cmd_ExpandAllFromRoot();

	}

	public void cmd_Cut() {

		LOGGER.info("start.");

		Node node = taskMgrFrame.nodeListPanel.getSelectedNode();

		if (node==null) {
			LOGGER.error("Nothing to cut. Aborting");
			return;
		}

		copyNode = new CopyNode(node);

		cmd_NodeDelete();

	}

	public void cmd_NodeCopy() {

		LOGGER.info("start.");

		Node node = taskMgrFrame.nodeListPanel.getSelectedNode();

		// Check if there is a node to copy
		if (node==null) {
			LOGGER.error("Nothing to copy. Aborting");
			return;
		}

		copyNode = new CopyNode(node);

	}

	public void cmd_NodePaste() {

		LOGGER.info("start.");

		if (copyNode==null) {
			LOGGER.error("Nothing to paste. Aborting");
			return;
		}

		RowAddress rowAddress = taskMgrFrame.nodeListPanel.getSelectedRowAddress();
		Node node;

		if (rowAddress != null)
			node = rowAddress.getNode();
		else {

			// pas de creation � la racine de la base de donn�es
			if (taskMgrFrame.nodeListPanel.getDisplayedRootNode() == getTaskMgrDB().getRootBaseNode()) return;

			node = taskMgrFrame.nodeListPanel.getDisplayedRootNode();
		}

		String cantPasteMsg = copyNode.canPaste(node);
		if (cantPasteMsg!=null) {
			JOptionPane.showMessageDialog(taskMgrFrame, "Error: "+cantPasteMsg,"Paste error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		ExpansionState expState = taskMgrFrame.nodeListPanel.getExpansionState(node);

		undoMgr.addUndoableGlobalEdit("Coller noeud");

		Node newNode = copyNode.pasteNode(node);

		taskMgrFrame.nodeListPanel.setExpansionState(expState);

		taskMgrFrame.nodeListPanel.setAllExpanded(newNode);
		taskMgrFrame.nodeListPanel.setSelectedRowAddress(new RowAddress(rowAddress,newNode));


	}

	public void cmd_NodePasteLink() {

		LOGGER.info("start.");

		// verifie s'il y a quelque chose � coller
		if (copyNode==null) {
			LOGGER.error("Nothing to paste. Aborting");
			return;
		}

		// recupere le noeud de destination, utilise la racine s'il n'y en a pas
		RowAddress rowAddress = taskMgrFrame.nodeListPanel.getSelectedRowAddress();
		Node node;

		if (rowAddress != null)
			node = rowAddress.getNode();
		else {

			// pas de creation � la racine de la base de donn�es
			if (taskMgrFrame.nodeListPanel.getDisplayedRootNode() == getTaskMgrDB().getRootBaseNode()) return;

			node = taskMgrFrame.nodeListPanel.getDisplayedRootNode();
		}

		// verifie si on peut coller le lien
		String cantPasteMsg = copyNode.canPasteLink(node);
		if (cantPasteMsg!=null) {
			JOptionPane.showMessageDialog(taskMgrFrame, "Error: "+cantPasteMsg,"Link error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		// effectue le collage

		ExpansionState expState = taskMgrFrame.nodeListPanel.getExpansionState(node);

		undoMgr.addUndoableGlobalEdit("Coller lien");

		Node newNode = copyNode.pasteLink(node);

		taskMgrFrame.nodeListPanel.setExpansionState(expState);

		taskMgrFrame.nodeListPanel.setAllExpanded(newNode);
		taskMgrFrame.nodeListPanel.setSelectedRowAddress(new RowAddress(rowAddress,newNode));


	}


	public void cmd_MoveUp() {

		LOGGER.info("start.");

		RowAddress rowAddress = taskMgrFrame.nodeListPanel.getSelectedRowAddress();
		if (rowAddress == null) return;

		Node node = rowAddress.getNode();

		Node father = taskMgrFrame.nodeListPanel.getSelectedNodeFather();
		if (father==null) return;

		undoMgr.addUndoableGlobalEdit("D�placer Noeud (haut)");

		ExpansionState expState = taskMgrFrame.nodeListPanel.getExpansionState(node.getFather());
		father.getChildren().moveUpChild(node);
		taskMgrFrame.nodeListPanel.setExpansionState(expState);

		taskMgrFrame.nodeListPanel.setSelectedRowAddress(rowAddress);

	}

	public void cmd_MoveDown() {

		LOGGER.info("start.");

		RowAddress rowAddress = taskMgrFrame.nodeListPanel.getSelectedRowAddress();
		if (rowAddress == null) return;

		Node node = rowAddress.getNode();

		Node father = taskMgrFrame.nodeListPanel.getSelectedNodeFather();
		if (father==null) return;

		undoMgr.addUndoableGlobalEdit("D�placer Noeud (bas)");

		ExpansionState expState = taskMgrFrame.nodeListPanel.getExpansionState(node.getFather());
		father.getChildren().moveDownChild(node);
		taskMgrFrame.nodeListPanel.setExpansionState(expState);

		taskMgrFrame.nodeListPanel.setSelectedRowAddress(rowAddress);

	}

	public void cmd_Undo() {

		LOGGER.info("start.");

		if (undoMgr.canUndo()) undoMgr.undo();
	}

	public void cmd_Redo() {

		LOGGER.info("start.");

		if (undoMgr.canRedo()) undoMgr.redo();
	}

	public void cmd_Properties() {

		LOGGER.info("start.");

		PropertyEditDialog.editProperty(taskMgrProperties,false);

	}

	public void cmd_Console() {
		LOGGER.info("start.");
		messageConsoleMgr.setVisible(true);
	}

	public void cmd_NodeExecutionSlotAdd1h() {

		LOGGER.info("start.");

		cmd_NodeExecutionSlotAdd1h(taskMgrFrame.nodeListPanel.getSelectedNode());
	}

	public void cmd_NodeExecutionSlotAdd1h(Node node) {

		LOGGER.info("start.");

		if (node==null) return;
		if (!(node instanceof NodeTask)) return;

		NodeTask nodeTask = (NodeTask) node;

		undoMgr.addUndoableNodeEdit(nodeTask, "Ajouter 1h",false);
		nodeTask.getExecutionSlotList().addExecutionSlot(new ExecutionSlot(Duration.ofHours(1)));

	}

	public void cmd_SelectRowAddress(RowAddress rowAddress) {

		LOGGER.info("start.");

		taskMgrFrame.nodeListPanel.setSelectedRowAddress(rowAddress);
	}

	public void cmd_SwapLink() {

		LOGGER.info("start.");

		RowAddress rowAddress = taskMgrFrame.nodeListPanel.getSelectedRowAddress();
		if (rowAddress == null) return;

		Node linkNode = rowAddress.getNode();

		if (!(linkNode instanceof NodeTaskLink)) return;

		Node originalNode = ((NodeTaskLink)linkNode).getLinkNode();

		undoMgr.addUndoableGlobalEdit("Permutter Noeud");

		Children.swapChildren(linkNode, originalNode);

		taskMgrFrame.nodeListPanel.setSelectedRowAddress(new RowAddress(rowAddress.getFatherRowAddress(),originalNode));

	}

	public void cmd_SelectPreviousLinkNode() {

		LOGGER.info("start.");

		taskMgrFrame.nodeListPanel.selectPreviousLinkNode(taskMgrFrame.nodeListPanel.getSelectedRow());
	}

	public void cmd_SelectNextLinkNode() {

		LOGGER.info("start.");

		taskMgrFrame.nodeListPanel.selectNextLinkNode(taskMgrFrame.nodeListPanel.getSelectedRow());
	}

	public void cmd_SelectPreviousMatch(String text) {

		LOGGER.info("start.");

		taskMgrFrame.nodeListPanel.selectPreviousMatch(text);
	}

	public void cmd_SelectNextMatch(String text) {

		LOGGER.info("start.");

		taskMgrFrame.nodeListPanel.selectNextMatch(text);
	}

	public void cmd_ShowPDFImportFrame() {

		LOGGER.info("start.");

		PdfEditor.showPdfEditor();
	}

	// -------------------------------------------------------------------------------------------
	//
	// Lancement de l'application
	//
	// -------------------------------------------------------------------------------------------

	public static void main(String[] args) {

		//
		// Affichage des parametres d'entr�e
		//
		LOGGER.info("");
		LOGGER.info("Param�tres de commande :");

		for (int argIndex =0; argIndex<args.length; argIndex++)
			LOGGER.info("arg["+argIndex+"]="+args[argIndex]);

		LOGGER.info("");


		//
		// Recuperation des param�tres d'entr�e
		//

		File paramFile=null;

		if (args.length > 0) {

			paramFile = new File(args[0]);

			LOGGER.info("File="+paramFile.getAbsolutePath());

			if (paramFile.exists() && paramFile.isFile()) {
				setCurrentDirectory(paramFile);
			} else {
				LOGGER.warn("File does not exist");
				paramFile = null;
			}
		}

		LOGGER.info("Launching TaskMgr");

		final File startFile=paramFile;
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new TaskMgr(startFile);
			}
		});

	}


}

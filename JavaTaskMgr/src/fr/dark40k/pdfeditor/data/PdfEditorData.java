package fr.dark40k.pdfeditor.data;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;

import fr.dark40k.pdfeditor.util.PdfUtils;

public class PdfEditorData {

	private final static Logger LOGGER = LogManager.getLogger();

	private PDDocument document;
	private File docFile;
	private boolean edited;

	public PdfEditorData() {
	}

	// ========================================================================================
	//
	// Getters / Setters
	//
	// ========================================================================================

	public PDDocument getDocument() {
		return document;
	}

	public File getDocumentFile() {
		return docFile;
	}

	/**
	 * @return the edited
	 */
	public boolean isEdited() {
		return edited;
	}

	public boolean isOpen() {
		return document!=null;
	}

	// ========================================================================================
	//
	// Commandes
	//
	// ========================================================================================

	public void close() {

		LOGGER.info("Fermeture.");

		if(document==null) return;

		try {
			document.close();
		} catch (IOException e) {
			LOGGER.error("Erreur:",e);
		}

		document=null;
		docFile=null;
		edited=false;

	}

	public boolean load(File newFile) {

		LOGGER.info("Loading, file="+newFile.getAbsolutePath());

		if (document!=null) close();

		try {

			document=PDDocument.load(newFile);
			docFile=newFile;
			edited=false;

		} catch (InvalidPasswordException e1) {

			LOGGER.info("Warning : Echec du chargement du doc PDF. InvalidPasswordException. Motif = "+e1.getMessage());
			close();
			return false;

		} catch (IOException e1) {

			LOGGER.info("Warning : Echec du chargement du doc PDF. IOException. Motif = "+e1.getMessage());
			close();
			return false;
		}

		LOGGER.info("Termine.");
		return true;

	}

	public boolean save() {

		LOGGER.info("Saving, file="+docFile.getAbsolutePath());

		try {
			document.save(docFile);
		}  catch (IOException e1) {
			LOGGER.info("Warning : Echec sauvegarde du doc PDF. IOException. Motif = "+e1.getMessage());
			return false;
		}

		edited=false;

		LOGGER.info("Termine.");
		return true;

	}

	public boolean saveAs(File pdfFileOut) {

		LOGGER.info("Saving, file="+pdfFileOut.getAbsolutePath());

		try {
			document.save(docFile);
		}  catch (IOException e1) {
			LOGGER.info("Warning : Echec sauvegarde du doc PDF. IOException. Motif = "+e1.getMessage());
			return false;
		}

		docFile=pdfFileOut;
		edited=false;

		LOGGER.info("Termine.");
		return true;

	}


	public boolean savePagesAs(File fileOut, Collection<Integer> pageList) {

		LOGGER.info("Saving, file="+fileOut.getAbsolutePath());

		boolean status= false;

		PDDocument pdDocumentOut = null;

		// Ouverture et transfert des pages
		try {

			LOGGER.info("Ouverture destination");
			pdDocumentOut = new PDDocument();

			for (Integer pageNbr : pageList) {
				LOGGER.info("ajout page :"+pageNbr);
				pdDocumentOut.addPage(document.getPage(pageNbr));
			}

			LOGGER.info("Sauvegarde destination :"+ fileOut);
			pdDocumentOut.save(fileOut);

		} catch (IOException e1) {
			LOGGER.info("Warning : Echec de creation du PDF. IOException. Motif = ",e1);
			status = false;
		}

		// Fermeture Document sortie
		try {
			LOGGER.info("Fermeture destination");
			pdDocumentOut.close();
		} catch (IOException e1) {
			LOGGER.warn("Erreur de fermeture. Motif=", e1);
			status = false;
		}

		LOGGER.info("Termine.");
		return status;

	}

	public boolean append(File newFile) {

		LOGGER.info("D�but, file="+newFile.getAbsolutePath());

		if (document==null) document = new PDDocument();

		boolean status= false;
		PDDocument documentIn=null;

		try {

			documentIn = PDDocument.load(newFile);

			for (PDPage page : documentIn.getPages()) {
				PdfUtils.importPage(document, page, true);
				edited=true;
			}

		} catch (InvalidPasswordException e) {
			LOGGER.info("Warning : Echec du chargement du doc PDF. InvalidPasswordException. Motif = " + e.getMessage());
			status = false;
		} catch (IOException e) {
			LOGGER.info("Warning : Echec de import du PDF. IOException. Motif = ", e);
			status = false;
		}

		// Fermeture Document sortie
		if (documentIn != null) {
			try {
				LOGGER.info("Fermeture destination");
				documentIn.close();
			} catch (IOException e1) {
				LOGGER.warn("Erreur de fermeture. Motif=", e1);
				status = false;
			}
		}

		return status;
	}

	public void pastePages() {
		PdfEditorClipboard.pastePages(document);
		edited=true;
	}

	public void rotN90(Collection<Integer> pages) {
		for (Integer pageNbr : pages) {
			LOGGER.info("page :"+pageNbr);
			PDPage page = document.getPage(pageNbr);
			page.setRotation((page.getRotation() + 270) % 360);
		}
		edited = true;
	}

	public void rotP90(Collection<Integer> pages) {
		for (Integer pageNbr : pages) {
			LOGGER.info("page :"+pageNbr);
			PDPage page = document.getPage(pageNbr);
			page.setRotation((page.getRotation() + 90) % 360);
		}
		edited = true;
	}

	public void rot180(Collection<Integer> pages) {
		for (Integer pageNbr : pages) {
			LOGGER.info("page :"+pageNbr);
			PDPage page = document.getPage(pageNbr);
			page.setRotation((page.getRotation() + 180) % 360);
		}
		edited = true;
	}

	/**
	 * D�place les pages de 1 rang vers le d�but du document
	 *
	 * @param pages liste des pages � d�placer
	 */
	public void decPagesPosition(Collection<Integer> pages) {
		for (Integer pageNumber = 1; pageNumber<document.getNumberOfPages(); pageNumber++)
			if (pages.contains(pageNumber)) {
				LOGGER.info("Mouvement pages : "+(pageNumber)+" => "+(pageNumber-1));
				PDPage page = document.getPage(pageNumber);
				document.removePage(pageNumber);
				document.getPages().insertBefore(page, document.getPage(pageNumber-1));
				edited=true;
			}
	}

	/**
	 * D�place les pages de 1 rang vers la fin du document
	 *
	 * @param pages liste des pages � d�placer
	 */
	public void incPagesPosition(Collection<Integer> pages) {
		for (Integer pageNumber = document.getNumberOfPages()-2; pageNumber>=0; pageNumber--)
			if (pages.contains(pageNumber)) {
				LOGGER.info("Mouvement pages : "+(pageNumber)+" => "+(pageNumber+1));
				PDPage page = document.getPage(pageNumber);
				document.removePage(pageNumber);
				document.getPages().insertAfter(page, document.getPage(pageNumber));
				edited=true;
			}
	}

	public void remove(LinkedHashSet<Integer> pages) {
		for (Integer pageNbr = document.getNumberOfPages()-1; pageNbr>=0; pageNbr--)
			if (pages.contains(pageNbr)) {
				LOGGER.info("suppresion page :"+pageNbr);
				document.removePage(pageNbr);
				edited=true;
			}
	}


}

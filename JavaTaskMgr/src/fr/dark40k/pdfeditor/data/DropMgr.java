package fr.dark40k.pdfeditor.data;

import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;
import fr.dark40k.util.dragdrop.DropFilesHandler;

public class DropMgr implements DropFilesHandler.DropFileUser {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private PdfEditor pdfEditor;

	private final static String[] IMPORT_OPTIONS= {
			"Ajouter",
			"Charger",
			"Annuler"
	};
	private final static Integer IMPORT_OPTIONS_DEFAULT=JOptionPane.YES_OPTION;
	
	public DropMgr(PdfEditor pdfEditor) {
		this.pdfEditor=pdfEditor;
	}

	@Override
	public void dropFiles(DropTargetDropEvent e, List<File> list) {
		
		LOGGER.info("D�but.");
		
		// Check number of files
		if (list.size() != 1) {
			JOptionPane.showMessageDialog(pdfEditor.display, "Drop error: Only 1 file can be droped", "PdfImportFrame", JOptionPane.ERROR_MESSAGE);
			LOGGER.info("Action annul�e. Plusieurs fichiers : "+list.size());
			return;
		}

		// Recupere le fichier dropp�
		File newFile = list.get(0);

		// teste le fichier dropp�
		if (!newFile.isFile()) {
			LOGGER.info("Action annul�e. Fichier non trouv� : "+newFile.getAbsolutePath());
			return;
		}

		int action;
		
		// si un fichier est d�ja la, on propose de le charger par d�faut
		if (pdfEditor.data.getDocument()!=null)
			action = JOptionPane.showOptionDialog(pdfEditor.display, "Un fichier est d�j� charg�.", "Definir action", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,IMPORT_OPTIONS,IMPORT_OPTIONS_DEFAULT);
		else
			action = JOptionPane.NO_OPTION;
			
		switch (action) {
			
			case JOptionPane.YES_OPTION :
				appendDocument(newFile);
				break;
			
			case JOptionPane.NO_OPTION :
				loadDocument(newFile);
				break;
			
			default : {
				LOGGER.info("Action annul�e.");
				return;
			}
		}
		
		pdfEditor.display.updateDisplay(true);
		
	}
	
	private void loadDocument(File newFile) {
		
		if (pdfEditor.data.isEdited()) {
			
			int ret = JOptionPane.showConfirmDialog(pdfEditor.display, "Le fichier contient des donn�es non enregistr�es qui seront perdues.", "Confirmer fermeture", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
			
			if (ret!=JOptionPane.YES_OPTION) {
				LOGGER.info("Action annul�e.");
				return;
			}
			
		}
		
		pdfEditor.data.load(newFile);

	}
	
	private void appendDocument(File newFile) {
		
		pdfEditor.data.append(newFile);

	}
	
}

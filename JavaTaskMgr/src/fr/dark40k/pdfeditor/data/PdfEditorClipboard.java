package fr.dark40k.pdfeditor.data;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;

import fr.dark40k.pdfeditor.util.PdfUtils;

public class PdfEditorClipboard {
	
	private final static Logger LOGGER = LogManager.getLogger();
	
	private static PDDocument copyDoc;

	public static void copyPages(PDDocument source, LinkedHashSet<Integer> pages) {

		LOGGER.info("D�marrage.");
		
		if (copyDoc!=null) try {
			copyDoc.close();
		} catch (IOException e) {
			LOGGER.warn("Echec fermeture document. source=",e);
		}
		
		copyDoc = new PDDocument();
		
		for (Integer pageNbr : pages) {
			LOGGER.info("ajout page :"+pageNbr);
			try {
				PdfUtils.importPage(copyDoc, source.getPage(pageNbr), true);
			} catch (IOException e) {
				LOGGER.warn("Echec copie page="+pageNbr+". Motif=",e);
			}
		}
		
		LOGGER.info("Termin�.");
	}

	protected static void pastePages(PDDocument destination) {
		
		if(copyDoc==null) return;

		LOGGER.info("D�marrage.");

		for (Integer pageNbr=0; pageNbr<copyDoc.getNumberOfPages(); pageNbr++) {
			LOGGER.info("ajout page :"+pageNbr);
			try {
				PdfUtils.importPage(destination, copyDoc.getPage(pageNbr), true);
			} catch (IOException e) {
				LOGGER.warn("Echec copie page="+pageNbr+". Motif=",e);
			}
		}
						
		LOGGER.info("Termin�.");
	}
	
	
	public static void close() {
		if(copyDoc==null) return;

		LOGGER.info("D�marrage.");
		
		try {
			copyDoc.close();
		} catch (IOException e) {
			LOGGER.warn("Echec fermeture document. source=",e);
		}
		copyDoc=null;
		
		LOGGER.info("Termin�.");
		
	}

	public static int getClipboardCount() {
		if (copyDoc==null) return 0;
		return copyDoc.getNumberOfPages();
	}

	public static boolean pastePages(File file) {

		LOGGER.info("D�marrage.");
		PdfEditorData data = new PdfEditorData();

		boolean load = data.load(file);
		if (!load) return false;
		
		data.pastePages();
		
		boolean save = data.save();
		if (!save) return false;
		
		LOGGER.info("Termin�.");
		
		return true;
		
	}
	
}

package fr.dark40k.pdfeditor;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.actions.PdfEditorActions;
import fr.dark40k.pdfeditor.data.DropMgr;
import fr.dark40k.pdfeditor.data.PdfEditorData;
import fr.dark40k.pdfeditor.gui.PdfEditorFrame;

public class PdfEditor {

	private final static Logger LOGGER = LogManager.getLogger();

	// ========================================================================================
	//
	// Sous-objet en access directe
	//
	// ========================================================================================

	public final PdfEditorFrame display ;

	public final PdfEditorActions actions;

	public final PdfEditorData data;

	public final DropMgr dropMgr;

	// ========================================================================================
	//
	// Constructor
	//
	// ========================================================================================

	private PdfEditor() {
		data = new PdfEditorData();
		actions = new PdfEditorActions(this);
		dropMgr = new DropMgr(this);
		display = new PdfEditorFrame(this);

		display.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		display.addWindowListener(new WindowAdapter() {
            @Override
			public void windowClosing(WindowEvent ev) {
                close();
            }
        });

		display.setVisible(true);
	}

	private PdfEditor(File newFile) {
		this();
		data.load(newFile);
		display.updateDisplay(true);
	}

	// ========================================================================================
	//
	// Commandes
	//
	// ========================================================================================

	public void close() {

		if (data.isEdited()) {

			int ret = JOptionPane.showConfirmDialog(display, "Le fichier contient des donn�es non enregistr�es qui seront perdues.", "Confirmer fermeture", JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);

			if (ret!=JOptionPane.OK_OPTION) {
				LOGGER.info("Action annul�e.");
				return;
			}

		}

		data.close();
		display.setVisible(false);

	}

	// ========================================================================================
	//
	// Affichage
	//
	// ========================================================================================


	public static void showPdfEditor(File file) {
		new PdfEditor(file);
	}

	public static void showPdfEditor() {
		new PdfEditor();
	}

}

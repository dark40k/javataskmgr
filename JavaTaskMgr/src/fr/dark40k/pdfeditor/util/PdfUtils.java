package fr.dark40k.pdfeditor.util;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.action.PDAction;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;

public class PdfUtils {

	private final static Logger LOGGER = LogManager.getLogger();

	/**
	 * Import d'une page dans un document
	 *
	 * @param document Document dans lequel la page est import�e
	 * @param page Page � importer
	 * @param removeLinkAnnotations True : supprimer les liens vers d'autres pages
	 * @throws IOException
	 */
	public static void importPage(PDDocument document, PDPage page, boolean removeLinkAnnotations) throws IOException {

		if (document==null) {
			LOGGER.warn("Document is null, skipping import.");
			return;
		}

		PDPage newPage = document.importPage(page);

		newPage.setRotation(page.getRotation());

		newPage.setCropBox(page.getCropBox());
		newPage.setMediaBox(page.getMediaBox());
		// only the resources of the page will be copied
		newPage.setResources(page.getResources());
		newPage.setRotation(page.getRotation());

		if (removeLinkAnnotations) processAnnotations(newPage);

	}

	private static void processAnnotations(PDPage imported) throws IOException
    {
        List<PDAnnotation> annotations = imported.getAnnotations();
        for (PDAnnotation annotation : annotations)
        {
            if (annotation instanceof PDAnnotationLink)
            {
                PDAnnotationLink link = (PDAnnotationLink)annotation;
                PDDestination destination = link.getDestination();
                if (destination == null && link.getAction() != null)
                {
                    PDAction action = link.getAction();
                    if (action instanceof PDActionGoTo)
                    {
                        destination = ((PDActionGoTo)action).getDestination();
                    }
                }
                if (destination instanceof PDPageDestination)
                {
                    // TODO preserve links to pages within the splitted result
                    ((PDPageDestination) destination).setPage(null);
                }
            }
            else
            {
                // TODO preserve links to pages within the splitted result
                annotation.setPage(null);
            }
        }
    }

}

package fr.dark40k.pdfeditor.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;
import fr.dark40k.pdfeditor.actions.view.SizeAbstractAction;
import fr.dark40k.util.dragdrop.DropFilesHandler;

public class PdfEditorFrame extends JFrame {
	
	@SuppressWarnings("unused")
	private final static Logger LOGGER = LogManager.getLogger();
	
	//
	// Constantes
	//
	private final static String FILE_TITLE="FichierSource :";
	
	private final static String EDITED_FILE_TITLE="FichierSource (edit�) :";
	
	//
	// Donnees
	//
	
	// lien amont
	protected final PdfEditor pdfEditor;
	
	// affichage secondaire
	public final PdfPageListPanel pdfPageListPanel;
	
	// Event handlers
	public final DropFilesHandler dropPDFHandler;

	// Swing objects
	public JPanel panel;
	public JLabel lblNewLabel_1;
	public JMenuBar menuBar;
	public JMenu mnFichier;
	public JMenu mnAffichage;
	public JMenuItem mntmSelectEnregistrerSous;
	public JCheckBoxMenuItem chckbxmntmLargX;
	public JMenuItem mntmFermer;
	public JCheckBoxMenuItem chckbxmntmManuel;
	public JMenuItem mntmEffacerMarquage;
	public JSeparator separator_1;
	public JPanel buttonPanel;
	public JButton btnCancel;
	public JLabel lblNewLabel;
	public JScrollPane scrollPane;
	public JMenu mnEditer;
	public JMenuItem mntmSupprimer;
	public JMenuItem mntmDeplDroite;
	public JMenuItem mntmDeplGauche;
	public TitledBorder titleBorder;

	//
	// Constructor
	//
	
	public PdfEditorFrame(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
		
		pdfPageListPanel = new PdfPageListPanel();
		
		initGUI();
		
		dropPDFHandler = new DropFilesHandler(this,pdfEditor.dropMgr);
		
	}
		
	public void updateDisplay(boolean reloadPdf) {
		titleBorder.setTitle(pdfEditor.data.isEdited() ? EDITED_FILE_TITLE : FILE_TITLE);
		lblNewLabel.setText((pdfEditor.data.getDocumentFile() != null) ? pdfEditor.data.getDocumentFile().getPath() : "<vide>");
		if (reloadPdf) pdfPageListPanel.setPDDocument(pdfEditor.data.getDocument());
	}
	
	//
	// Initialisation graphique
	// 
	
	private void initGUI() {
		
		setTitle("PDF Light Editor");
		setLocationByPlatform(true);
		setSize(687, 464);
		
		buttonPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) buttonPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
		btnCancel = new JButton("Close");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		buttonPanel.add(btnCancel);
				
		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		scrollPane.setViewportView(pdfPageListPanel); 
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		titleBorder = new TitledBorder(null, "FichierSource :", TitledBorder.LEADING, TitledBorder.TOP, null, null);
		lblNewLabel = new JLabel("<vide>");
		lblNewLabel.setBorder(titleBorder);
		
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		//
		// Menu
		//
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		// Fichier
		
		mnFichier = new JMenu("Fichier");
		menuBar.add(mnFichier);
		
		mnFichier.add(new JMenuItem(pdfEditor.actions.newPdfEditorAction));
		mnFichier.add(new JSeparator());
		mnFichier.add(new JMenuItem(pdfEditor.actions.openAction));
		mnFichier.add(new JMenuItem(pdfEditor.actions.appendAction));
		mnFichier.add(new JSeparator());
		mnFichier.add(new JMenuItem(pdfEditor.actions.saveAction));
		mnFichier.add(new JMenuItem(pdfEditor.actions.saveAsAction));
		mnFichier.add(new JMenuItem(pdfEditor.actions.saveSelectionAsAction));
		mnFichier.add(new JSeparator());
		mnFichier.add(new JMenuItem(pdfEditor.actions.closeAction));
		mnFichier.add(new JSeparator());
		mnFichier.add(new JMenuItem(pdfEditor.actions.closePdfEditorAction));
		
		// Editer
		mnEditer = new JMenu("Editer");
		menuBar.add(mnEditer);
		
		mnEditer.add(new JMenuItem(pdfEditor.actions.copyAction));
		mnEditer.add(new JMenuItem(pdfEditor.actions.pasteAction));
		mnEditer.add(new JSeparator());
		mnEditer.add(new JMenuItem(pdfEditor.actions.removeAction));
		mnEditer.add(new JSeparator());
		mnEditer.add(new JMenuItem(pdfEditor.actions.decPagesAction));
		mnEditer.add(new JMenuItem(pdfEditor.actions.incPagesAction));
		mnEditer.add(new JSeparator());
		mnEditer.add(new JMenuItem(pdfEditor.actions.rotP90Action));
		mnEditer.add(new JMenuItem(pdfEditor.actions.rotN90Action));
		mnEditer.add(new JMenuItem(pdfEditor.actions.rot180Action));
		
		// Affichage
		
		mnAffichage = new JMenu("Affichage");
		menuBar.add(mnAffichage);
		
		mnAffichage.add(new JMenuItem(pdfEditor.actions.markSetAction));
		mnAffichage.add(new JMenuItem(pdfEditor.actions.markClearAction));
		mnAffichage.add(new JMenuItem(pdfEditor.actions.markClearAllAction));
		
		mnAffichage.add(new JSeparator());
		
		ButtonGroup buttonGroup = new ButtonGroup();
		for (SizeAbstractAction dimAction : pdfEditor.actions.dimActions) {
			JRadioButtonMenuItem buttton = new JRadioButtonMenuItem(dimAction);
			buttonGroup.add(buttton);
			mnAffichage.add(buttton);
		}
		pdfEditor.actions.dimActions[0].putValue(Action.SELECTED_KEY, true);
	}

}

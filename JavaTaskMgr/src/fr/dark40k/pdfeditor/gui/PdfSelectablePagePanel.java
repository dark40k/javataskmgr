package fr.dark40k.pdfeditor.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.rendering.PDFRenderer;

import fr.dark40k.util.swing.GUIUtilities;

public class PdfSelectablePagePanel extends JPanel {

	private final static Logger LOGGER = LogManager.getLogger();

	private final static ImageIcon BROKEN_ICON_BASE = new ImageIcon(PdfSelectablePagePanel.class.getResource("/fr/dark40k/pdfeditor/res/icon64/document-close-4.png"));
	private final static ImageIcon SUSPENDED_ICON_BASE = new ImageIcon(PdfSelectablePagePanel.class.getResource("/fr/dark40k/pdfeditor/res/icon64/document-open-recent-3.png"));
	private final static ImageIcon LOADING_ICON_BASE = new ImageIcon(PdfSelectablePagePanel.class.getResource("/fr/dark40k/pdfeditor/res/icon64/document-print-3.png"));
	
	protected JLabel lblNewLabel;
	
	private ImageIcon thumbnailIcon;

	private int pageIndex;
	
	private static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	public PdfSelectablePagePanel(PDFRenderer pdfRenderer, int pageIndex, Dimension maxSize) {

		// Initialise l'affichage
		setLayout(new BorderLayout(0, 0));
		lblNewLabel = new JLabel();
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(lblNewLabel);
		
		// Marque l'icone en attente de chargement
		lblNewLabel.setIcon(calculateDefaultImage(SUSPENDED_ICON_BASE,maxSize));
				
		lblNewLabel.setSize(maxSize);
		
		// stocke le N� de page
		this.pageIndex=pageIndex;
		
		// lance la tache de chargement de l'icone
		executor.submit(() -> { updateImage(pdfRenderer, maxSize); });
		
	}
	
	public int getPageIndex() {
		return pageIndex;
	}

	//
	// Utilitaires
	//
	
	private void updateImage(PDFRenderer pdfRenderer, Dimension maxSize) {
		
		// affichage
		GUIUtilities.runEDT(new Runnable() {
			@Override
			public void run() {
				lblNewLabel.setIcon( (thumbnailIcon!=null) ? thumbnailIcon : calculateDefaultImage(LOADING_ICON_BASE,maxSize) );
				lblNewLabel.repaint();
			}
		});

		// calcule l'image reduite
		thumbnailIcon=null;
		
		try {
			
			//calcule l'image
			BufferedImage image = pdfRenderer.renderImage(pageIndex,2.0f);

			LOGGER.info("Image "+pageIndex+", size= "+image.getWidth()+" x "+image.getHeight());
			
			//redimensionne et stocke l'image
			BufferedImage thumbnailImage = getScaledImage(image, maxSize.width, maxSize.height);
			
//			LOGGER.info("Image "+pageIndex+", size= "+thumbnailImage.getWidth()+" x "+thumbnailImage.getHeight()+" / max= "+maxSize.width+" x "+maxSize.height);
			
			//cree l'icone a afficher
			thumbnailIcon = new ImageIcon(thumbnailImage);
			
		} catch (IOException e) {
			LOGGER.info("Skipping page "+pageIndex+" from "+pdfRenderer.toString()+", error ="+e.getMessage());
		}
		
		// affichage
		GUIUtilities.runEDT(new Runnable() {
			@Override
			public void run() {
				lblNewLabel.setIcon( (thumbnailIcon!=null) ? thumbnailIcon : calculateDefaultImage(BROKEN_ICON_BASE,maxSize) );
				lblNewLabel.repaint();
			}
		});

		
	}
	
	// extrait une image r�duite de l'image trait�e typiquement pour utilisation thumbnails
	private final static BufferedImage getScaledImage(BufferedImage imgIn, int maxWidth, int maxHeight) {
		
		if (imgIn==null) return null;
		
		float scaleWidth = (float)imgIn.getWidth()/maxWidth;
		float scaleHeight = (float)imgIn.getHeight()/maxHeight;
		
		int newWidth; 
		int newHeight;
		
		if (scaleWidth>scaleHeight) {
			newWidth=maxWidth;
			newHeight=(int) (imgIn.getHeight()/scaleWidth);
		} else {
			newWidth=(int) (imgIn.getWidth()/scaleHeight);
			newHeight=maxHeight;
		}
		
		BufferedImage imgOut = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB); 
		Graphics2D g2d = imgOut.createGraphics(); 

		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		boolean ret=g2d.drawImage(imgIn, 0, 0, newWidth, newHeight, null); 
		
		if(!ret) 
			LOGGER.error("Image incomplete on drawimage return.");

		return imgOut;
		
	}

	private static ImageIcon calculateDefaultImage(ImageIcon iconBase, Dimension maxDim) {
		int maxThumbWidth = maxDim.width;
		int maxThumbHeight = maxDim.height;
		Image img = iconBase.getImage(); 
		BufferedImage bi = new BufferedImage(maxThumbWidth, maxThumbHeight, BufferedImage.TYPE_INT_RGB); 
		Graphics g = bi.createGraphics(); 
		g.setColor(Color.WHITE);
		g.fillRect( 0, 0, maxThumbWidth, maxThumbHeight );
		g.drawImage(img, (maxThumbWidth-img.getWidth(null))/2, (maxThumbHeight-img.getHeight(null))/2, img.getWidth(null), img.getHeight(null), null); 
		return new ImageIcon(bi); 		
	}

}

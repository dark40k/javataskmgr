package fr.dark40k.pdfeditor.gui;

import java.awt.Dimension;
import java.util.Collection;
import java.util.LinkedHashSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import fr.dark40k.util.selectablethumbnaillist.SelectableThumbnailListPanel;

public class PdfPageListPanel extends SelectableThumbnailListPanel<PdfSelectablePagePanel> {
	
	@SuppressWarnings("unused")
	private final static Logger LOGGER = LogManager.getLogger();
	
	private static int maxThumbWidth = 384;
	private static int maxThumbHeight = 576;
	
	private Dimension size = defaultSize();

	private PDDocument pdDocument;
	
	public final static Dimension defaultSize() {
		return new Dimension(maxThumbWidth,maxThumbHeight);
	}

	//
	// Chargement document
	//
	public void setPDDocument(PDDocument document) {
		this.pdDocument=document;
		update();
	}
	
	//
	// Affichage
	//
	public void setThumbnailSize(Dimension rect) {
		if ((size.width==rect.width) && (size.height==rect.height)) return;
		size=(Dimension) rect.clone();
		update();
	}

	public Dimension getThumbnailSize() {
		return (Dimension) size.clone();
	}
	
	//
	// Marques
	//
	
	public void markSet(Integer pageNbr) {
		for (int index = 0; index < getComponentCount(); index++) {
			
			PdfSelectableThumbnailPanel thumbnail = (PdfSelectableThumbnailPanel)getComponent(index);
			
			if (thumbnail.getSubPanel().getPageIndex()==pageNbr) {
				thumbnail.setBoldTitle(true);
				thumbnail.setTitle("["+Integer.toString(thumbnail.getSubPanel().getPageIndex()+1)+"]");
				thumbnail.repaint();
				return;
			}
		}
	}

	public void markSet(Collection<Integer> pages) {
		for (Integer page : pages) markSet(page);
	}

	public void markClear(Integer pageNbr) {
		for (int index = 0; index < getComponentCount(); index++) {

			PdfSelectableThumbnailPanel thumbnail = (PdfSelectableThumbnailPanel)getComponent(index);
			
			if (thumbnail.getSubPanel().getPageIndex()==pageNbr) {
				thumbnail.setBoldTitle(false);
				thumbnail.setTitle(Integer.toString(thumbnail.getSubPanel().getPageIndex()+1));
				thumbnail.repaint();
				return;
			}
		}
	}
	
	public void markClearAll() {
		for (int index = 0; index < getComponentCount(); index++) {
			PdfSelectableThumbnailPanel thumbnail = (PdfSelectableThumbnailPanel)getComponent(index);
			thumbnail.setTitle(Integer.toString(thumbnail.getSubPanel().getPageIndex()+1));
			thumbnail.setBoldTitle(false);
		}
		repaint();
	}
	
	//
	// Selection
	//
	
	public LinkedHashSet<Integer> getSelectedPagesNumbers() {
		
		LinkedHashSet<Integer> listPages = new LinkedHashSet<Integer>();
		
		for (PdfSelectablePagePanel pagePanel:listSelected() )
			listPages.add(pagePanel.getPageIndex());
			
		return listPages;
	}

	//
	// Interne
	//
	
	public void update() {
		
		// vide l'affichage
		removeAll();
		
		// recalcule les vignettes
		if (pdDocument != null) generateThumbnails();
		
		// relance l'affichage
		repaint();
		revalidate();

	}

	private void generateThumbnails() {
		
		// construit le renderer
		PDFRenderer pdfRenderer = new PDFRenderer(pdDocument);

		// construit les pages
		for (int pageIndex = 0; pageIndex < pdDocument.getNumberOfPages(); pageIndex++) {
			PdfSelectablePagePanel pagePanel = new PdfSelectablePagePanel(pdfRenderer, pageIndex, size);
			addSelectablePanel(new PdfSelectableThumbnailPanel(Integer.toString(pageIndex + 1), pagePanel));
		}
		
	}




}

package fr.dark40k.pdfeditor.gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import fr.dark40k.util.selectablethumbnaillist.SelectableThumbnail;

public class PdfSelectableThumbnailPanel extends SelectableThumbnail<PdfSelectablePagePanel> {
	
	private final Font titleFont;
	private final Font boldTitleFont;

	protected final TitledBorder border;	

	public PdfSelectableThumbnailPanel(String title, PdfSelectablePagePanel subPanel) {
		super(subPanel);
		
		border = new TitledBorder(UIManager.getBorder("TitledBorder.border"), title, TitledBorder.CENTER, TitledBorder.BOTTOM, null, new Color(0, 0, 0));
		setBorder(border);
		
		titleFont = border.getTitleFont();
		boldTitleFont = titleFont.deriveFont(Font.BOLD);
		
	}
	
	public void setTitle(String title) {
		border.setTitle(title);
	}
	
	public String getTitle() {
		return border.getTitle();
	}

	public void setBoldTitle(boolean bold) {
		border.setTitleFont((bold)?boldTitleFont:titleFont);
	}

}

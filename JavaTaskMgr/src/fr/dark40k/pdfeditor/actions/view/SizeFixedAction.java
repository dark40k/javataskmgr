package fr.dark40k.pdfeditor.actions.view;

import java.awt.Rectangle;

import fr.dark40k.pdfeditor.PdfEditor;

public class SizeFixedAction extends SizeAbstractAction {

	private int width;
	private int height;
	
	public SizeFixedAction(PdfEditor pdfEditor, int width, int height) {
		
		super(pdfEditor);
		
		this.width=width;
		this.height=height;
		
		putValue(NAME, width + " x " + height);
		putValue(SHORT_DESCRIPTION, "Dimensions des images = " + width + " x " + height);
	}

	@Override
	public Rectangle getDimensions() {
		return new Rectangle(width,height);
	}
	
}

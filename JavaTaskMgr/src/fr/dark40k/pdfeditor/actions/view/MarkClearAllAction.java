package fr.dark40k.pdfeditor.actions.view;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;

public class MarkClearAllAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final PdfEditor pdfEditor;

	public MarkClearAllAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
				
		putValue(NAME, "Effacer ttes marques");
		putValue(SHORT_DESCRIPTION, "Effacer toutes les marques.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");

		pdfEditor.display.pdfPageListPanel.markClearAll();
	}

	
}

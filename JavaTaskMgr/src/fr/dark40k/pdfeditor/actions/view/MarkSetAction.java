package fr.dark40k.pdfeditor.actions.view;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;

public class MarkSetAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final PdfEditor pdfEditor;

	public MarkSetAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
				
		putValue(NAME, "Marquer page");
		putValue(SHORT_DESCRIPTION, "Marque la page.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");
		
		for (Integer pageNbr : pdfEditor.display.pdfPageListPanel.getSelectedPagesNumbers()) {
			LOGGER.info("Ajouter marque, page :"+pageNbr);
			pdfEditor.display.pdfPageListPanel.markSet(pageNbr);
		}
		
	}
	
}

package fr.dark40k.pdfeditor.actions.view;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.dark40k.pdfeditor.PdfEditor;

public class SizeManualAction extends SizeAbstractAction {

	public SizeManualAction(PdfEditor pdfEditor) {
		
		super(pdfEditor);
		
		putValue(NAME, "Manuel");
		putValue(SHORT_DESCRIPTION, "Defini la taille des images manuellement.");
		
	}

	@Override
	public Rectangle getDimensions() {

		Dimension dims = pdfEditor.display.pdfPageListPanel.getThumbnailSize();
		
		JTextField wField = new JTextField(Integer.toString(dims.width), 5);
		JTextField hField = new JTextField(Integer.toString(dims.height), 5);

		JPanel myPanel = new JPanel();
		myPanel.add(new JLabel("Largeur :"));
		myPanel.add(wField);
		myPanel.add(Box.createHorizontalStrut(15)); // a spacer
		myPanel.add(new JLabel("Hauteur :"));
		myPanel.add(hField);

		int result = JOptionPane.showConfirmDialog(null, myPanel, "Entrer Hauteur et Largeur", JOptionPane.OK_CANCEL_OPTION);

		if (result != JOptionPane.OK_OPTION) return null;

		System.out.println("w value: " + wField.getText());
		System.out.println("h value: " + hField.getText());

		int width = Integer.valueOf(wField.getText());
		int height = Integer.valueOf(hField.getText());

		return new Rectangle(width, height);
		
	}

}

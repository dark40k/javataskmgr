package fr.dark40k.pdfeditor.actions.view;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;

public class MarkClearAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final PdfEditor pdfEditor;

	public MarkClearAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
				
		putValue(NAME, "Effacer marques sel.");
		putValue(SHORT_DESCRIPTION, "Effacer les marques des fichiers selectionnes.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");

		for (Integer pageNbr : pdfEditor.display.pdfPageListPanel.getSelectedPagesNumbers()) {
			LOGGER.info("Effacer marque, page :"+pageNbr);
			pdfEditor.display.pdfPageListPanel.markClear(pageNbr);
		}
	}
	
}

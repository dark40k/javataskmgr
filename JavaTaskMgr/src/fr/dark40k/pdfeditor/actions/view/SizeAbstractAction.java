package fr.dark40k.pdfeditor.actions.view;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;

public abstract class SizeAbstractAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	protected final PdfEditor pdfEditor;

	public SizeAbstractAction(PdfEditor pdfEditor) {
		this.pdfEditor=pdfEditor;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");

		Rectangle size = getDimensions();
		if (size==null) return;
		pdfEditor.display.pdfPageListPanel.setThumbnailSize(new Dimension(size.width,size.height));
	}
	
	public abstract Rectangle getDimensions();
	
}

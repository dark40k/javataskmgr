package fr.dark40k.pdfeditor.actions;

import javax.swing.Action;

import fr.dark40k.pdfeditor.PdfEditor;
import fr.dark40k.pdfeditor.actions.edit.CopyAction;
import fr.dark40k.pdfeditor.actions.edit.DecPagesAction;
import fr.dark40k.pdfeditor.actions.edit.IncPagesAction;
import fr.dark40k.pdfeditor.actions.edit.PasteAction;
import fr.dark40k.pdfeditor.actions.edit.RemoveAction;
import fr.dark40k.pdfeditor.actions.edit.Rot180Action;
import fr.dark40k.pdfeditor.actions.edit.RotN90Action;
import fr.dark40k.pdfeditor.actions.edit.RotP90Action;
import fr.dark40k.pdfeditor.actions.fichier.AppendAction;
import fr.dark40k.pdfeditor.actions.fichier.CloseAction;
import fr.dark40k.pdfeditor.actions.fichier.ClosePdfEditorAction;
import fr.dark40k.pdfeditor.actions.fichier.NewPdfEditor;
import fr.dark40k.pdfeditor.actions.fichier.OpenAction;
import fr.dark40k.pdfeditor.actions.fichier.SaveAction;
import fr.dark40k.pdfeditor.actions.fichier.SaveAsAction;
import fr.dark40k.pdfeditor.actions.fichier.SaveSelectionAsAction;
import fr.dark40k.pdfeditor.actions.view.MarkClearAction;
import fr.dark40k.pdfeditor.actions.view.MarkClearAllAction;
import fr.dark40k.pdfeditor.actions.view.MarkSetAction;
import fr.dark40k.pdfeditor.actions.view.SizeAbstractAction;
import fr.dark40k.pdfeditor.actions.view.SizeFixedAction;
import fr.dark40k.pdfeditor.actions.view.SizeManualAction;

public class PdfEditorActions {

	// Actions-Fichiers
	public final Action newPdfEditorAction;
	public final Action openAction;
	public final Action appendAction;
	public final Action saveAction;
	public final Action saveAsAction;
	public final Action saveSelectionAsAction;
	public final Action closeAction;
	public final Action closePdfEditorAction;
	
	// Actions-Edition
	public final Action copyAction;
	public final Action pasteAction;
	public final Action removeAction;
	public final Action decPagesAction;
	public final Action incPagesAction;
	public final Action rotP90Action;
	public final Action rotN90Action;
	public final Action rot180Action;
	
	// Actions-Affichage
	public final Action markSetAction;
	public final Action markClearAction;
	public final Action markClearAllAction;
	public final SizeAbstractAction[] dimActions;

	public PdfEditorActions(PdfEditor pdfEditor) {
		
		newPdfEditorAction = new NewPdfEditor(pdfEditor);
		openAction = new OpenAction(pdfEditor);
		appendAction = new AppendAction(pdfEditor);
		saveAction = new SaveAction(pdfEditor);
		saveAsAction = new SaveAsAction(pdfEditor);
		saveSelectionAsAction = new SaveSelectionAsAction(pdfEditor);
		closeAction = new CloseAction(pdfEditor);
		closePdfEditorAction = new ClosePdfEditorAction(pdfEditor);
		
		// Actions-Edition
		copyAction = new CopyAction(pdfEditor);
		pasteAction = new PasteAction(pdfEditor);
		removeAction = new RemoveAction(pdfEditor);
		decPagesAction = new DecPagesAction(pdfEditor);
		incPagesAction = new IncPagesAction(pdfEditor);
		rotP90Action = new RotP90Action(pdfEditor);
		rotN90Action = new RotN90Action(pdfEditor);
		rot180Action = new Rot180Action(pdfEditor);
		
		// Actions-Affichage
		markSetAction = new MarkSetAction(pdfEditor);
		markClearAction = new MarkClearAction(pdfEditor);
		markClearAllAction = new MarkClearAllAction(pdfEditor);
		
		dimActions = new SizeAbstractAction[] {
				new SizeManualAction(pdfEditor),
				new SizeFixedAction(pdfEditor,128, 192),
				new SizeFixedAction(pdfEditor,256, 384),
				new SizeFixedAction(pdfEditor,384, 576),
				new SizeFixedAction(pdfEditor,512, 768)
		};
		
	}
	
}

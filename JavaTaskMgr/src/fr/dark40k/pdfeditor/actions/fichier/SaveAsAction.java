package fr.dark40k.pdfeditor.actions.fichier;

import fr.dark40k.pdfeditor.PdfEditor;

public class SaveAsAction extends SaveAction {
	
	public SaveAsAction(PdfEditor pdfEditor) {
		super(pdfEditor);
		
		putValue(NAME, "Sauvegarder sous.");
		putValue(SHORT_DESCRIPTION, "Enregistre le fichier en cours sous un autre nom.");
		
		forceSaveAs=true;
	}
		
}

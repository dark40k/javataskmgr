package fr.dark40k.pdfeditor.actions.fichier;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;


public class CloseAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final PdfEditor pdfEditor;

	public CloseAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
		
		putValue(NAME, "Fermer fichier");
		putValue(SHORT_DESCRIPTION, "Fermer le fichier ouvert.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");

		if (!pdfEditor.data.isOpen()) {
			LOGGER.info("Pas de document ouvert. Action annul�e.");
	    	JOptionPane.showMessageDialog(pdfEditor.display, "Pas de document ouvert.", "Erreur", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (pdfEditor.data.isEdited()) {
			
			int ret = JOptionPane.showConfirmDialog(pdfEditor.display, "Le fichier contient des donn�es non enregistr�es qui seront perdues.", "Confirmer fermeture", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
			
			if (ret!=JOptionPane.YES_OPTION) {
				LOGGER.info("Action annul�e.");
				return;
			}
			
		}

		pdfEditor.data.close();
		pdfEditor.display.updateDisplay(true);
		
	}

}

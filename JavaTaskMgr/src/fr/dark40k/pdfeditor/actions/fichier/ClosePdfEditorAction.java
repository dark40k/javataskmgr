package fr.dark40k.pdfeditor.actions.fichier;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.dark40k.pdfeditor.PdfEditor;

public class ClosePdfEditorAction extends AbstractAction {

	private final PdfEditor pdfEditor;

	public ClosePdfEditorAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
		
		putValue(NAME, "Fermer editeur");
		putValue(SHORT_DESCRIPTION, "Ferme la fen�tre.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		pdfEditor.close();
	}
	
}

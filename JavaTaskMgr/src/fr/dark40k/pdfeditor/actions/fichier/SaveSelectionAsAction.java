package fr.dark40k.pdfeditor.actions.fichier;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.LinkedHashSet;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;


public class SaveSelectionAsAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final PdfEditor pdfEditor;

	public SaveSelectionAsAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
				
		putValue(NAME, "Sauvegarder selection sous.");
		putValue(SHORT_DESCRIPTION, "Enregistre les pages selectionn�es dans un fichier.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");

		if (!pdfEditor.data.isOpen()) {
			LOGGER.info("Pas de document ouvert. Action annul�e.");
	    	JOptionPane.showMessageDialog(pdfEditor.display, "Pas de document ouvert.", "Erreur", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		// identification des pages � sauvegarder
		LinkedHashSet<Integer> pages = pdfEditor.display.pdfPageListPanel.getSelectedPagesNumbers();
		
		if (pages.size()==0) {
			LOGGER.info("Pas de pages selectionn�es. Action annul�e.");
	    	JOptionPane.showMessageDialog(pdfEditor.display, "Pas de pages selectionn�es.", "Erreur", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		// recuperation du fichier de sortie
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Fichier PDF", "pdf"));
		
		int result = fileChooser.showOpenDialog(pdfEditor.display);
		
		if (result != JFileChooser.APPROVE_OPTION) {
			LOGGER.info("Action annul�e.");
			return;
		}
		
	    File pdfFileOut = fileChooser.getSelectedFile();
	    
	    // verification de l'extension
		if (!pdfFileOut.getName().endsWith(".pdf")) pdfFileOut = new File(pdfFileOut.getAbsolutePath()+".pdf");

		if (pdfFileOut.exists()) {
			LOGGER.info("Le fichier existe d�j�. Confirmation demand�e. Fichier="+pdfFileOut.getAbsolutePath());
			
			int ret = JOptionPane.showConfirmDialog(pdfEditor.display, "Le fichier existe d�j�:\n"+pdfFileOut.getAbsolutePath()+"\nEcraser?", "Confirmer fermeture", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
			
			if (ret!=JOptionPane.YES_OPTION) {
				LOGGER.info("Action annul�e.");
				return;
			}
			
		}

		
		// sauvegarde des pages selectionnees
		boolean status = pdfEditor.data.savePagesAs(pdfFileOut, pages);
		
		// message en cas d'echec
		if (!status) JOptionPane.showMessageDialog(pdfEditor.display, "Echec sauvegarde fichier\n"+pdfFileOut.getAbsolutePath(), "Warning", JOptionPane.ERROR_MESSAGE);

		// visualisation des pages sauvegard�es
		pdfEditor.display.pdfPageListPanel.markSet(pages);

	}
	
}

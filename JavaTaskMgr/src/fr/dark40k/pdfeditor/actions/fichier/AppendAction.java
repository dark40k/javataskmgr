package fr.dark40k.pdfeditor.actions.fichier;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;


public class AppendAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final PdfEditor pdfEditor;

	public AppendAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
		
		putValue(NAME, "Ajouter PDF");
		putValue(SHORT_DESCRIPTION, "Ajoute un fichier PDF en fin de document.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Fichier PDF", "pdf"));
		
		int result = fileChooser.showOpenDialog(pdfEditor.display);
		
		if (result != JFileChooser.APPROVE_OPTION) return;
		
	    File pdfFileOut = fileChooser.getSelectedFile();
	    if (!pdfFileOut.exists()) {
	    	JOptionPane.showMessageDialog(pdfEditor.display, "Le fichier n'existe pas.\n"+pdfFileOut.getAbsolutePath(), "Warning", JOptionPane.ERROR_MESSAGE);
	    	return;
	    }
	    
	    pdfEditor.data.append(pdfFileOut);
		pdfEditor.display.updateDisplay(true);
		
	}

}

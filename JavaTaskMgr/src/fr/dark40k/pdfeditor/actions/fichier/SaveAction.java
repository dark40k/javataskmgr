package fr.dark40k.pdfeditor.actions.fichier;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;

public class SaveAction extends AbstractAction {

	protected final static Logger LOGGER = LogManager.getLogger();
	
	protected final PdfEditor pdfEditor;
	
	protected boolean forceSaveAs = false;

	public SaveAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
				
		putValue(NAME, "Sauvegarder");
		putValue(SHORT_DESCRIPTION, "Enregistre le fichier en cours.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");
		
		if (!pdfEditor.data.isOpen()) {
			LOGGER.info("Pas de document ouvert. Action annul�e.");
	    	JOptionPane.showMessageDialog(pdfEditor.display, "Pas de document ouvert.", "Erreur", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if ((forceSaveAs)||(pdfEditor.data.getDocumentFile()==null))
			saveAs();
		else
			save();
		
		pdfEditor.display.updateDisplay(false);
		
	}
	
	protected void save() {
		boolean status = pdfEditor.data.save();
		if (!status) JOptionPane.showMessageDialog(pdfEditor.display, "Echec sauvegarde fichier\n"+pdfEditor.data.getDocumentFile().getAbsolutePath(), "Warning", JOptionPane.ERROR_MESSAGE);
	}
	
	protected void saveAs() {
		
		// recuperation du fichier de sortie
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Fichier PDF", "pdf"));
		
		int result = fileChooser.showOpenDialog(pdfEditor.display);
		
		if (result != JFileChooser.APPROVE_OPTION) {
			LOGGER.info("Action annul�e.");
			return;
		}
		
	    File pdfFileOut = fileChooser.getSelectedFile();
	    		
	    // verification de l'extension
		if (!pdfFileOut.getName().endsWith(".pdf")) pdfFileOut = new File(pdfFileOut.getAbsolutePath()+".pdf");

		if (pdfFileOut.exists()) {
			LOGGER.info("Le fichier existe d�j�. Confirmation demand�e. Fichier="+pdfFileOut.getAbsolutePath());
			
			int ret = JOptionPane.showConfirmDialog(pdfEditor.display, "Le fichier existe d�j�:\n"+pdfFileOut.getAbsolutePath()+"\nEcraser?", "Confirmer fermeture", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
			
			if (ret!=JOptionPane.YES_OPTION) {
				LOGGER.info("Action annul�e.");
				return;
			}
			
		}

		boolean status = pdfEditor.data.saveAs(pdfFileOut);
		if (!status) JOptionPane.showMessageDialog(pdfEditor.display, "Echec sauvegarde fichier\n"+pdfEditor.data.getDocumentFile().getAbsolutePath(), "Warning", JOptionPane.ERROR_MESSAGE);

	}
	
	
}

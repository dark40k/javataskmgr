package fr.dark40k.pdfeditor.actions.fichier;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.dark40k.pdfeditor.PdfEditor;

public class NewPdfEditor extends AbstractAction {

	@SuppressWarnings("unused")
	private final PdfEditor pdfEditor;

	public NewPdfEditor(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
		
		putValue(NAME, "Nouvel editeur.");
		putValue(SHORT_DESCRIPTION, "Ouvre un nouvel editeur de PDF.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		PdfEditor.showPdfEditor();
	}
	
}

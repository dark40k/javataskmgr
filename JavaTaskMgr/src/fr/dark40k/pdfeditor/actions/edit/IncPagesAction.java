package fr.dark40k.pdfeditor.actions.edit;

import java.awt.event.ActionEvent;
import java.util.LinkedHashSet;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;


public class IncPagesAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final PdfEditor pdfEditor;

	public IncPagesAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
		
		putValue(NAME, "Depl. pages +1");
		putValue(SHORT_DESCRIPTION, "Déplacer vers la fin les pages sélectionnées.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");

		LinkedHashSet<Integer> pages = pdfEditor.display.pdfPageListPanel.getSelectedPagesNumbers();
		
		if (pages.size()==0) {
			LOGGER.info("Pas de pages selectionnées. Action annulée.");
	    	JOptionPane.showMessageDialog(pdfEditor.display, "Pas de pages selectionnées.", "Erreur", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		pdfEditor.data.incPagesPosition(pages);
		
		pdfEditor.display.updateDisplay(true);
		
		LOGGER.info("End.");
	}

}

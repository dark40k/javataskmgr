package fr.dark40k.pdfeditor.actions.edit;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.dark40k.pdfeditor.PdfEditor;
import fr.dark40k.pdfeditor.data.PdfEditorClipboard;

public class PasteAction extends AbstractAction {
	
	private final static Logger LOGGER = LogManager.getLogger();

	private final PdfEditor pdfEditor;

	public PasteAction(PdfEditor pdfEditor) {
		
		this.pdfEditor=pdfEditor;
		
		putValue(NAME, "Coller");
		putValue(SHORT_DESCRIPTION, "Coller les pages a la fin du document.");
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		LOGGER.info("Starting action.");
		
		if (!pdfEditor.data.isOpen()) {
			LOGGER.info("Pas de document ouvert. Action annul�e.");
	    	JOptionPane.showMessageDialog(pdfEditor.display, "Pas de document ouvert.", "Erreur", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (PdfEditorClipboard.getClipboardCount()==0) {
			LOGGER.info("Pas de pages a coller. Action annul�e.");
	    	JOptionPane.showMessageDialog(pdfEditor.display, "Pas de pages a coller.", "Erreur", JOptionPane.ERROR_MESSAGE);
			return;
		}
				
		pdfEditor.data.pastePages();
		pdfEditor.display.updateDisplay(true);
		
		LOGGER.info("End.");
	}
	
}
